

--set the database path
_G.DbFile = os.getenv('DB_FILEPATH')

--Connect to the DB and retrive the config data
sqCfg = require 'sqConfig'
sqCfg.init() -- initialize the config
sqCfg.getConfigData() -- pull the interface config fromDB
-------------------

--load the validation module
sqVal = require 'sqValidation'

sqDB = require('sqDatabase')

--store HL7 message ID
_G.msgCtrlID = ''

--store the returning ACK message
_G.ackMessage = 'OK' -- OK is the default, to override, set to "CA: Warning Text Here..."

----------------------------------------------------------------------------

function main(Data) 
   
   sqUtils.printLog("Starting main...") 
   
   --setting for sending an audit of db inserts and updates to an iguana db audit table
   _G.dbAudit = sqUtils.getMappedValue(sqCfg.extra_params,"dbAudit")

   iguana.stopOnError(false)

   --Check that we have a connection an accesible
   --If it fails then try to reconnect
   if not conn or not conn:check() then
      if conn then conn:close() end
      conn = sqCfg.dbConnect()
   end

   conn:begin{}
   conn:execute{sql="SET SESSION time_zone = 'Europe/Dublin';"}

   --[[
   In order to trap errors we run main in protected mode
   any errors that occur when writing to the the database
   or other will be caught and dictate the ACK message returned
   to the sender
   ]]
   if iguana.isTest() then
      ProtectedMain(Data)
   else
      local Success, Err = pcall(ProtectedMain, Data)
      if not Success then
         sqUtils.printLog("Setting ACK to error")
         setAckMsg("Error processing message")
         
         if sqUtils.getMappedValue(sqCfg.extra_params,"sendEmailAlerts") == '1' then
            --check if error contains ER0001
            local res = string.find(Err,"ER0001")
            if res ~= nil then
               sendEmailAlert(Err)
            end
         end
         
         error(Err)
      else
         local customACK = sqUtils.getMappedValue(sqCfg.extra_params,'customACK')
         if customACK == '1' then 
            sqUtils.printLog("Setting ACK to OK")
            setAckMsg(_G.ackMessage)
         end
      end
   end

   sqUtils.printLog("Comitting database changes..")
   conn:commit{live=true}


end

----------------------------------------------------------------------------

function ProtectedMain(Data)

   sqUtils.printLog("Starting protected main..")

   local vmdFile = sqUtils.getMappedValue(sqCfg.extra_params,"mainVMDFile")
   local MsgIn, MsgType = hl7.parse{vmd=vmdFile, data=Data}
   
   --determine the child facility
   local facilityID = getChildFacilityID(MsgIn)
   
   --determine the parent facility
   local parentFacilityID = getParentFacilityID(facilityID)
   
   --validate before processing
   local customer = sqUtils.getMappedValue(sqCfg.extra_params,"customer")
   local continue = sqVal.validate(customer, parentFacilityID, facilityID, MsgIn)

   if continue then

      --store the message control id for use when writing to the DB ausit log
      _G.msgCtrlID = MsgIn.MSH[10]:S()
          
      local mrn = MsgIn.PID[3][1][1]:S()
      local firstname = MsgIn.PID[5][2]:S()
      local surname = MsgIn.PID[5][1]:S()
      local pmobile = MsgIn.PID[13][1][1]:S()
      local pdob =  sqUtils.parseDOB(MsgIn.PID[7])
      
      local patientID = sqDB.RAD.getPatientIdByMrn(parentFacilityID, mrn, firstname, surname, pmobile, pdob, SiteId)
      
      -- Process the PID segment ----
      
      sqUtils.printLog("Create/Update on PID segment: Begin DB trans...")
     
      conn:begin{live=true}

      local Success, Error = pcall(function()

            if patientID == '0' then
               patientID = sqDB.RAD.createPatient(MsgIn.PID) 
               sqUtils.printLog("PID: Patient ID not found, Created MRN: "..mrn.." and returned ID: "..patientID)
            else
               sqDB.RAD.updatePatient(patientID, MsgIn.PID, checkZSeg, checkZSegVisitflag, facilityID)
               sqUtils.printLog("PID: Updated MRN: "..mrn..", ID: "..patientID)
            end              

            --Add the patient tot he facility
            sqDB.RAD.addToFacility(patientID, parentFacilityID)

         end)

      if Success then
         sqUtils.printLog("Create/Update on PID segment: Committing transactions...")
         conn:commit{live=true}
      else
         sqUtils.printLog("Create/Update on PID segment:: DB activity failed, Rolling back transactions...")
         conn:rollback{live=true}
         error(Error)
      end
      
      
      local msgType = MsgIn.MSH[9][1]:S()
      local msgEvent = MsgIn.MSH[9][2]:S()
      
      if msgType == 'ORM' and msgEvent == 'O01' then
         process_ORM(patientID, facilityID, MsgIn)
      end
      
      if msgType == 'SIU' and msgEvent == 'S12' then
         process_S12(patientID, facilityID, MsgIn)
      end
      
      if msgType == 'SIU' and msgEvent == 'S13' then
         process_S13(patientID, facilityID, MsgIn)
      end
      
      if msgType == 'SIU' and msgEvent == 'S15' then
         process_S15(patientID, facilityID, MsgIn)
      end
      
      if msgType == 'ADT' and msgEvent == 'A40' then
         process_A40(patientID, facilityID, MsgIn)
      end
      
      if msgType == 'ACK' then
         process_ACK(patientID, facilityID, MsgIn)
      end

   end
   
end

---------------------------------------------------------------------------------------

function sendEmailAlert(errorStr)

   print("Sending email alert..")
   
   local toStr = sqUtils.getMappedValue(sqCfg.extra_params,"sendEmailAlertsTO")
  
   if not iguana.isTest() then              

      
      local user = sqUtils.getMappedValue(sqCfg.extra_params,"user")
      local pass = sqUtils.getMappedValue(sqCfg.extra_params,"password")
      local server = sqUtils.getMappedValue(sqCfg.extra_params,"server")
      local toStr = sqUtils.getMappedValue(sqCfg.extra_params,"sendTo")
      local subject = sqUtils.getMappedValue(sqCfg.extra_params,"subject")
      
      emailMsg = errorStr
      
      sqUtils.sendEmailAlert(user, pass, server, subject ,emailMsg, toStr )
      
   end

end

---------------------------------------------------------------------------------------

function process_ORM(patientID, facilityID, MsgIn)
   
   sqUtils.printLog("Calling Process_ORM...")
   
   local OrderID = MsgIn.OBR[3][1]:S()
   local ExamCode = MsgIn.OBR[4][1]:S()
   local ExamDesc = MsgIn.OBR[4][2]:S() 
   local episodeNumber = MsgIn.PV1[19]:S()
   local accessionNumber = MsgIn.PV1[19][1]:S()
   local VettingStatus = MsgIn.ORC[5]:S()
   local cancelReason = MsgIn.ORC[16][1]:S()
   local SiteId = MsgIn.PV1[3][1]:S()
   local RefSourceName = MsgIn.PV1[3][2]:S()
   local referralID = '0'
   local facilitySpeciality = '0'

   local checkZSeg, checkZSegVisitflag = isSQMessage(MsgIn)
   
   -- not a SQ patoient
   if checkZSeg == 'NSQ' and checkZSegVisitflag == 'Y' then
      
      --is vetting cancelled ?
      if VettingStatus == sqUtils.getMappedValue(sqCfg.extra_params,"vetting_Cancel") then
                  
         local res = sqDB.RAD.getResByOrderId(OrderID)
         
         --reservation found
         if res[1]['res_id']:S() ~= '0' then
            
            --flag the appointment as cancelled
            sqDB.RAD.updateResByOrderId(OrderID)
            
            sqDB.RAD.updateUserAudit(patientID, res[1]['res_id']:S(), 'apointment.cancelled')
            
         end

         --Check whether there is a referral associated with this order cancellation, which was previously 'for Swiftqueue'.
         --If referral found, then move it to discharged
         
         local referralID = sqDB.RAD.getPatientReferralByEpisode(patientID, facilityID, episodeNumber)
         
         if referralID ~= '0' then
            
            --flag the referral as discharge
            sqDB.RAD.dischargeReferral(referralID)
      
         end

      end 
      
   else -- Is SQ Message
      
      referralID = sqDB.RAD.getPatientReferralByEpisode(patientID, facilityID, episodeNumber)
      
      referralID = createReferralAndOrder(referralID, patientID, facilityID, OrderID, ExamCode, ExamDesc, 
            episodeNumber, accessionNumber, orderGrouping, checkZSeg, checkZSegVisitflag, 
            SiteId, RefSourceName)
      
      
      --start checking order/vetting status

      if referralID ~= '0' and VettingStatus == sqUtils.getMappedValue(sqCfg.extra_params,"vetting_Vetted") then

         --flag the referral as active
         sqDB.RAD.activateReferral(referralID)

      elseif referralID ~= '0' and VettingStatus == sqUtils.getMappedValue(sqCfg.extra_params,"vetting_LetterSent") then

         --flag as letter sent
         sqDB.RAD.setReferralLetterSent(referralID)

      elseif VettingStatus == sqUtils.getMappedValue(sqCfg.extra_params,"vetting_Cancel") 
             or VettingStatus == sqUtils.getMappedValue(sqCfg.extra_params,"vetting_DNA") then  
         
         local resID = sqDB.RAD.getOrderResID(referralID, OrderID)
         
         local found, cancellationID = sqDB.RAD.getCancellationID(cancelReason)

         --cancellation ID found / not found
         if found ~= '0'  then
            sqDB.RAD.cancelReferralOrder(referralID, OrderID, resID, cancellationID)                  
         else           
            local cancellationID = sqUtils.getMappedValue(sqCfg.extra_params,"cancel_CatchAll")            
            sqDB.RAD.cancelReferralOrder(referralID, OrderID, resID, cancellationID)           
         end
         
         --if it has a reservation id
         --[[
         if resID ~= '0'  then
            sqDB.RAD.updateReferralOrderResID(referralID, OrderID, resID)
         end 
         ]]
         
         if cancelReason == 'DNA' then
            sqDB.RAD.updateUserAudit(patientID, resID, 'apointment.dna')
         end
         
         if cancelReason == 'Exam changed' then
            
            --cancel the referral order with exam_changed
            local cancellationID = sqUtils.getMappedValue(sqCfg.extra_params,"cancel_ExamChanged")
            sqDB.RAD.cancelReferralOrder(referralID, OrderID, resID, cancellationID)
                  
         end 
            
      else
         --PB 08.08.2024 No need to wearn here
         --sqUtils.printLog("Vetting status of : '"..VettingStatus.."' NOT HANDLED !")
      end

   end
   
   --Check Patient Flow
   
   if referralID ~= '0' and VettingStatus == sqUtils.getMappedValue(sqCfg.extra_params,"vetting_IP") then

      sqDB.RAD.updatePatientFlow(patientID, facilityID, MsgIn.OBR)
      --update the referral to make sure that the patient is no longer able to book an appointment
      sqDB.RAD.assignReferral(referralID)

   end

   if referralID ~= '0' and VettingStatus == sqUtils.getMappedValue(sqCfg.extra_params,"vetting_CM") then

      sqDB.RAD.updatePatientFlowComplete(patientID, facilityID, MsgIn.OBR)
      --update the referral to make sure that the patient is no longer able to book an appointment
      sqDB.RAD.dischargeReferral(referralID)

   end
   
end

---------------------------------------------------------------------------------------

function process_S12(patientID, facilityID, MsgIn)
   
   sqUtils.printLog("Calling Process_S12...")
   
   local episodeNumber = MsgIn.PV1[19]:S()
   local accessionNumber = MsgIn.PV1[19][1]:S()
   
   local orderResID = MsgIn.SCH[2][1]:S()   
   local opdAppointment = dateparse.parse(MsgIn.SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)
   local opdAppointmentTime = dateparse.parse(MsgIn.SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   local opdAppointmentTimeNewString = tostring(dateparse.parse(MsgIn.SCH[11][4]:T()))
   local opdAppointmentEndTime = dateparse.parse(MsgIn.SCH[11][5]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentEndTime),12,16)


   --local dept_code = MsgIn.AIL[1][3][1]:S()
   local room_code = MsgIn.AIL[2][3][1]:S()
   local examCode = MsgIn.AIS[3][1]:S()
   local examDesc = MsgIn.AIS[3][2]:S()
   
   --PB 24.07.2024 : IR-346 : If no appointment date then set the referral to 'assigned' and skip app creation
   
   if sqUtils.isEmpty(MsgIn.SCH[11][4]:S()) then
      sqDB.RAD.setReferralStatus(orderResID,'assigned')
      return
   end
   
   
    
   ----------------
   local checkZSeg, checkZSegVisitflag = isSQMessage(MsgIn)

   -- not a SQ patoient
   if checkZSeg == 'NSQ' and checkZSegVisitflag == 'Y' then

      -- Double check if this S12 is a reschedule of a NSQ appt
      -- If it is a reschedule, then cancel the original appointment and book a new appointment
      --sqDB.RAD.cancelAndRebook(facilityID, patientID, episodeNumber, orderResID, room_code, opdAppointmentString, opdAppointmentTimeString, opdAppointmentEndTimeString)
      
      sqDB.RAD.process_NFSQ_S12(facilityID, patientID, episodeNumber, orderResID, room_code, opdAppointmentString, opdAppointmentTimeString, opdAppointmentEndTimeString)

   else

      -- IR-90 2nd time Schedule Appointment causes Cancellation of original
      -- Find out whether the appointment is still active.  
      -- An appointment is active, if cancellation_processed = 0 (e.g not cancelled),
      -- or has a canellation_code = 49 (signifies that this is a rescheduled appointment).
      -- (Added the clause to include rescheduled appointment).

      local oID, resID, refID = sqDB.RAD.isAppReschedule(episodeNumber, examCode)

      --local reflex = sqDB.RAD.isReflexAppointment(episodeNumber, examCode, opdAppointmentTimeNewString, opdAppointmentEndTimeString)

      if resID ~= '0' then  --reschedule 

         sqDB.RAD.rescheduleAppointmentS12(facilityID, resID, oID, refID, orderResID, opdAppointmentTimeNewString, 
            opdAppointmentEndTimeString, room_code, episodeNumber, accessionNumber, examCode, examDesc)

         sqDB.RAD.updateUserAudit(patientID, resID, 'apointment.rescheduled')

         conn:commit{live=true}

      else --create the appointment

         sqDB.RAD.createAppointment(MsgIn, patientID, facilityID, episodeNumber, orderResID, oID, examCode, 
            examDesc, accessionNumber, room_code, opdAppointmentTimeString, opdAppointmentEndTimeString,
            opdAppointmentString)

         sqDB.RAD.updateUserAudit(patientID, resID, 'apointment.created')

      end  
      
   end

end

---------------------------------------------------------------------------------------

function process_S13(patientID, facilityID, MsgIn)
   
   sqUtils.printLog("Calling Process_S13...")
   
   sqUtils.printLog("Redirecting to Process_S12...")
   
   --Imperial/soliton receives S12's for both new and reschuled appointments
   process_S12(patientID, facilityID, MsgIn)
   
end

---------------------------------------------------------------------------------------

--[[
function process_S13_OLD(patientID, facilityID, MsgIn)
   
   sqUtils.printLog("Calling Process_S13...")
   
   sqDB.RAD.rescheduleAppointmentS13(MsgIn , patientID, facilityID)
      
end
]]

---------------------------------------------------------------------------------------

function process_S15(patientID, facilityID, MsgIn)

   sqUtils.printLog("Calling Process_S15...")
--[[
   --get the mapped reason id
   reasonID = sqUtils.getMappedValue2(sqCfg.appreason_map,clinCode1,visitType)

   if reasonID == '' then
      reasonID = 0
      sqUtils.printLogW("Reason ID not found")
   end
]]
   
   reasonID = sqUtils.getMappedValue(sqCfg.extra_params,'defaultAppCancelReasonID')

   sqDB.RAD.cancelAppointment(patientID, facilityID, MsgIn.SCH, reasonID)


end

---------------------------------------------------------------------------------------

function process_A40(patientID, facilityID, MsgIn)
   
   local minorPatient = getPatientIdByMrn(MsgIn.MRG[1][1]:S())
   
   sqDB.RAD.mergePatient(patientID,minorPatient,facilityID)
   
end

---------------------------------------------------------------------------------------

function process_ACK(patientID, facilityID, MsgIn)
end

---------------------------------------------------------------------------------------

function createReferralAndOrder(referralID, patientID, facilityID, OrderID, ExamCode, ExamDesc, 
      episodeNumber, accessionNumber, orderGrouping, checkZSeg, checkZSegVisitflag, 
      SiteId, RefSourceName)
   
   --only file to DB if both referral and order sucessfully created

   local retry = 0
   local pCall_tryCount = sqUtils.getMappedValue(sqCfg.extra_params,"pCall_tryCount")
   local tryCount = tonumber(pCall_tryCount)

   repeat

      print("createReferralAndOrder: Begin DB trans...")
      conn:begin{live=true}

      local Success, Error = pcall(function()

            --if the referral doesn't exist then create it
            if referralID == '0' then         

               referralID = sqDB.RAD.createPatientReferral(patientID, facilityID, OrderID, ExamCode, ExamDesc, 
                  episodeNumber, accessionNumber, orderGrouping, checkZSeg, checkZSegVisitflag, 
                  SiteId, RefSourceName)         

            end

            local oID = sqDB.RAD.getReferralOrderID(OrderID)

            --if the referral already exists (or just created) but has no order then create the order
            if referralID ~= '0' and oID == '0' then

               sqDB.RAD.createPatientReferralOrder(referralID,OrderID,episodeNumber,ExamCode,ExamDesc,accessionNumber) 

            end
         end )

      if Success then

         print("createReferralAndOrder: Committing transactions...")
         conn:commit{live=true}

      else

         print("createReferralAndOrder: DB activity failed, Rolling back transactions...")
         conn:rollback{live=true}

         if Error ~= nil then
            print(Error.code..": "..Error.message.."\n\nRetrying...........")
         else
            print("\n\nRetrying...........")
         end

         --trigger alert and dump status info on first failure
         --[[
         if retry == 0 then

            --PB 21.11.23 : Lost admin access for some reason
            
            print("Running: select * from performance_schema.data_locks;")
            local res = conn:execute{sql="select * from performance_schema.data_locks;", live=true}         
            print(res:S())

            print("Running: select * from performance_schema.data_lock_waits;")
            res = conn:execute{sql="select * from performance_schema.data_lock_waits;", live=true}
            print(res:S())
            
         end
         ]]

         local randomNumber = math.random(3, 10)
         print("Sleeping for "..tostring(randomNumber).." seconds..")
         if not iguana.isTest() then util.sleep(randomNumber*1000) end

      end

      retry = retry + 1
      if retry >= tryCount and not Success then 

         if Error ~= nil then 
            error("Error Code: "..Error.code..": "..Error.message) 
         else
            error("Error retriggered")
         end
      end

   until Success or retry > tryCount

   return referralID

end

---------------------------------------------------------------------------------------

function isSQMessage(MsgIn)

   local checkZSeg = 'N'
   local checkZSegVisitflag = 'N'
   local newZPXForSQ = 'SQ'
   local newZPXForSQflag = 'Y'

   checkZSeg = MsgIn.ZPX[1][2][1]:S()
   checkZSegVisitflag = MsgIn.ZPX[1][3][1]:S()

   newZPXForSQ = MsgIn.ZPX[1][2][1]:S()
   newZPXForSQflag = MsgIn.ZPX[1][3][1]:S()

   if newZPXForSQ == 'SQ' then
      if newZPXForSQflag == 'N' then
         checkZSeg = 'NSQ'
         checkZSegVisitflag = 'Y'
      end
   end
   
   return checkZSeg, checkZSegVisitflag

end

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------

function getParentFacilityID(facilityID)
   
   local customer = sqUtils.getMappedValue(sqCfg.extra_params,"customer")

   if customer == 'NWL' then

      return facilityID

   else

      return sqUtils.getMappedValue(sqCfg.extra_params,"parentFacilityID")

   end
   
end

---------------------------------------------------------------------------------------

function getChildFacilityID(MsgIn)
   
   local customer = sqUtils.getMappedValue(sqCfg.extra_params,"customer")

   if customer == 'NWL' then

      --[[
      RYJ = Imperial
      R1K = LondonNW
      RQM = ChelWest
      ]]

      local sendingFacility = MsgIn.MSH[4][1]:S()
      local facID = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'inboundNACSMap',sendingFacility)

      if sqUtils.isEmpty(facID) then
         error("Failed to find NACS mapping for: "..sendingFacility)
      end

      return facID

   else

      return sqUtils.getMappedValue(sqCfg.extra_params,"parentFacilityID")

   end
   
end

---------------------------------------------------------------------------------------

function setAckMsg(str)
   
   local customACK = sqUtils.getMappedValue(sqCfg.extra_params,'customACK')

   if customACK == '1' then 

      local rconn = db.connect{api=db.MY_SQL, name=sqConfig.dbCredentials.db_name, 
         user=sqConfig.dbCredentials.db_user, 
         password=sqConfig.dbCredentials.db_pass, live=true}

      conn:execute {sql="update iguana_config set ack_msg = '"..str.."' where channel = "..
         rconn:quote(sqConfig.ie_channel).." and ie_instance = "..
         rconn:quote(sqConfig.ie_instance), live=false}

      rconn:close()

   end

end
---------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

