

_G.clientIP = '10.106.10.1'
_G.clientPort = 5120
_G.clientTimeout = 60 --timeout in seconds


_G.masterMessage = ''
_G.validateBufferSize = '0'


function main()

   --set script timeout to 1 year
   iguana.setTimeout(31536000)

   iguana.stopOnError(false) 

   if not iguana.isTest() then

      while true do

         local socket = connectToClient()

         local continue = true
         local data = nil
         local success = nil
         local ackStr = ''

         while true do

            iguana.logInfo("waiting for data...")

            data = getData(socket) 
            
            --if data is nil then the wait times out, nothing to receive
            --so break out of loop and attempt a reconnect            
            if data == nil then
               break
            end

            --validate the data received

            continue, ackStr = validateData(data)

            --good to go, build up the complete message .....

            if continue then

               --DAT001 indicates the start of a message
               if isMessageStart(data) then
                  _G.masterMessage = data
               else
                  _G.masterMessage = _G.masterMessage..data
               end

               --if its the last buffer the push it out and reset the master message  
               if isMessageEnd(data) then
                  iguana.logInfo("Passing on full message : ".._G.masterMessage)
                  queue.push{data=_G.masterMessage}
                  _G.masterMessage = ''
               end   

            end

            --send the ack
            iguana.logInfo(ackStr.." sent to client")
            socket:send(ackStr)

            --have a nap...
            util.sleep(500)

         end

         success, errorMsg = pcall(function() 
               socket:close()
            end)
         if success then
            iguana.logInfo("Closed the socket!")
         else
            iguana.logInfo("Failed to close socket, Error: "..tostring(errorMsg))
         end
                    
         util.sleep(500)

      end

   end

end

---------------------------------------------------------------------

function connectToClient()
   
   iguana.logInfo("Attempting connection to ".._G.clientIP.." : ".._G.clientPort.."...")
   
   local s = nil
   while true do
      success, errorMsg = pcall(function()
            s = net.tcp.connect{host=_G.clientIP, port=_G.clientPort, timeout=_G.clientTimeout}
      end)
      
      if not success or s == nil then
         iguana.logInfo(errorMsg)
         iguana.logInfo("Retrying to connect to ".._G.clientIP.." : ".._G.clientPort)
      else
         iguana.logInfo("Connected to ".._G.clientIP.." : ".._G.clientPort)
         break
      end
      util.sleep(5000)
	end
   
   return s
   
end

---------------------------------------------------------------------

function getData(socket)

   local data = nil

   success, errorMsg = pcall(function() 
         data = socket:recv()
      end)

   if success and data then
      iguana.logInfo("Data received:\n"..data)
   end

   if not success or data == nil then
      
      success, errorMsg = pcall(function() 
               socket:close()
            end)
         if success then
            iguana.logInfo("Closed the socket!!!!")
         end
      
      iguana.logInfo(tostring(errorMsg))
   end

   return data

end

---------------------------------------------------------------------

function getData_ORIG(socket)

   local data = nil
   
   --use pcall to avoid timeout and continue to wait for a message..
   while true do     

      success, errorMsg = pcall(function() 
            data = socket:recv()
         end)

      if success and data then
         iguana.logInfo("Data received:\n"..data)
         break
      end
      
      if not success or data == nil then
         print("no data!!!!!!!!!!")
         print(tostring(errorMsg))
         print(success)
         
      end

      util.sleep(100) 

   end

   return data

end

---------------------------------------------------------------------

function validateData(data)

   local continue = true
   local ackStr = 'ACK'..data:sub(4,9)..'END0016'

   --if not processing first buffer and if the buffer received contains NLS (mid message buffer) and
   --masterMessage = '' meaning we dont have a header buffer yet then skip
   if not isMessageStart(data) then
      if ( isMessageNLS(data) or isMessageEnd(data) ) and _G.masterMessage == '' then
         iguana.logInfo("Broken buffer received, Sending NACK..")
         ackStr = 'NAK'..data:sub(4,9)..'END0016'
         continue = false
      end
   end

   --validate buffer size
   if _G.validateBufferSize == '1' then
      if #data > 16 then
         local bufferSize = tonumber(string.sub(data,13,16))
         if bufferSize ~= #data then
            iguana.logInfo("Buffer received is an invalid size, Sendig NACK..")
            ackStr = 'NAK'..data:sub(4,9)..'END0016'
            continue = false
         end
      else
         iguana.logInfo("Truncated buffer received, Sending NACK..")
         ackStr = 'NAK'..data:sub(4,9)..'END0016'
         continue = false
      end
   end

   return continue, ackStr
   
end

---------------------------------------------------------------------

function isMessageStart(data)

   if #data > 6 then
      local f = data:find("DAT001")
      return f ~= nil
   end
   
   return nil

end

---------------------------------------------------------------------

function isMessageEnd(data)
   
   if #data > 12 then
      local str = data:sub(10,12)
      return str == 'END'
   end

   return nil

end

---------------------------------------------------------------------

function isMessageNLS(data)
   
   if #data > 12 then
      local str = data:sub(10,12)
      return str == 'NLS'
   end

   return nil

end
