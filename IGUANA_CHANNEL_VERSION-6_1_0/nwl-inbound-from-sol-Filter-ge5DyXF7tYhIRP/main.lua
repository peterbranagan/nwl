--set the database path
_G.DbFile = os.getenv('DB_FILEPATH')

--Connect to the DB and retrive the config data
sqCfg = require("sqConfig")
sqCfg.init() -- initialize the config
sqCfg.getConfigData() -- pull the interface config fromDB
-------------------

function main(Data)

   iguana.stopOnError(false)
   local hl7Str = sqUtils.split(Data,'|')
   local msgType = string.sub(hl7Str[9],1,7)
   
   if sqUtils.msgTypeAllowed(hl7Str, msgType) then
      sqUtils.printLog("Message pushed to queue.....")
      queue.push{data=Data}
   else
      sqUtils.printLog("Message type "..msgType.." ignored.")
   end
   
end