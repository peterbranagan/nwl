-- TODO REMOVE
sqUtils = require('sqUtils')

--[[
Open Link Mappings (Item,StartPos,EndPos,Active)

-- How to do per type? (TODO)
-- What about SEG1-7 etc (TODO)
]]

Mappingsopenlink = {}
--setup = require('setup') - TODO Remove

----------------------------------------------------------------------------

function Mappingsopenlink.getMaps()
   local ok=nil
   local map, maps
   
   -- TODO error trap?
   -- ok=setup.setup()

   -- TODO
   map=getAPMaps()
   
   -- TODO Merge into one object
   return maps
end

----------------------------------------------------------------------------

-- Trace and record (only print in test)
function trace(A) 
   if iguana.isTest() then
      print(A)
   end
   --sqUtils.printLog(A)
end

----------------------------------------------------------------------------

-- AP1 - 7 Maps
function getAPMaps()
   -- SEG1 only (TODO others (how?))
   -- DAT001, DAT002 etc show segment - How do we get from segment without lots of Seg1 etc?? - TODO
   sqUtils.printLog("Calling mappings_openlink get AP Maps")
   local items={}
   items = APSegment1(items)
   trace(items)
   
   return items
end

-- AP Segment 1
function APSegment1(items)
	-- ItemName = StartPosition, EndPosition, Active
   -- TODO - prefixing the segment and sequence I really don't like.
   items['AP1NetworkHeader']={1,16,true}
   return items
   --[[items = {
      AP1NetworkHeader = {1,16,true};
      AP1TransactionType = {17,19,true}
   }

   return items--]]

	-- LUA starts at 1!
   --[[
   local APSeg1 = {
      NetworkHeader             =    "1,16";
      TransactionType           =   "17,19";
      SequenceNumber            =   "20,21";
      CaseNumber                =   "22,23";
      InternalPatientNumber     =   "24,33";
      DistrictNumber            =   "33,43";
      CaseNoteNumber            =   "44,58";
      EpisodeNumber             =   "59,68";
      AccountNumber             =   "69,78";
      ClinicCode                =   "79,87";
      DoctorCode                =   "88,97";
      AppointmentDateTime       =  "98,110";
      AppointmentTypeCode       = "111,114";
      TransportCode             = "115,117";
      Comment                   = "118,143";
      CategoryCode              = "145,147";
      BookedDate                = "148,160";
      PurchaserReferenceNumber  = "161,171";
      WaitingGuaranteeException = "172,173";
      BookedAdmissionsFlag      = "174,177";
      PatientComment            = "178,237";
      CABRef                    = "238,267";
  }
   ]]--
end

----------------------------------------------------------------------------

-- Get item details (startPos, endPos) of an item
-- TODO - old remove
function getItem(openLinkMap, item)
   -- TODO change to array
   local values=string:split(openLinkMap(item), ",")
   local startPos=values(1)
   local endPos=values(2)
   return startPos, endPos
end

----------------------------------------------------------------------------

-- Get a value of a map item from a MsgIn (OpenLink Format)
-- i.e.
-- DAT001015NLS0030AP7     100AA0DAT002015NLS0261AP1020001000000100000   400000        000000035100000000SJEYECASEYECAS2 202107191050FU NRA&E                      10 202107191050                      0   A&E                                                                                       
-- TODO old remove
function getMsgValueForItem(openLinkMap, item, MsgIn)
   local startPos, endPos = getitem(openLinkMap, item)
   local value=string.sub(MsgIn, startPos, endPos)
   return value
end

----------------------------------------------------------------------------


----------------------------------------------------------------------------

return Mappingsopenlink
 
