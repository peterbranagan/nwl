-- TODO Remove
-- Setup of openlink table
segment = {}
segments = {}

-- live=false/true??? TODO

-- TODO this is not correct
function main(Data)
   for k,v in pairs(segment) do
      v()
   end
end

----------------------------------------------------------------------------

-- Trace and record (only print in test)
function trace(A) 
   if iguana.isTest() then
      print(A)
   end
   sqUtils.printLog(A)
end

----------------------------------------------------------------------------

-- Primary call to setup (or test for setup) of table
function segment.setup()
   local result, count, tablename
   
   tablename = sqUtils.getJsonMappedValue(sqCfg.extra_params,'openlink_tablename')

   -- default table name
	if tablename=='' then
      -- TODO Schema name
      tablename="openlink_mappings"
   end
   
   sqUtils.printLog("Openlink Setup, table name "..tablename)
   trace(tablename)
   
   -- Check to see if table already exists
   sql="SELECT COUNT(TABLE_SCHEMA) FROM information_schema.TABLES WHERE TABLE_NAME = "..tablename

   result = conn:execute{sql=sql, live=false}
   trace(result)
   if #result == 0 then 
      createtable(tablename)
   end
   sqUtils.printLog("Openlink Setup, table name schema "..#result)

   return tablename
end

----------------------------------------------------------------------------

-- setup new openlink mapping table
function createtable(tablename)
	trace("Openlink Setup, Creating new table "..tablename)

   local sql, ok
	
   -- Create table
   sql = "CREATE "..tablename.." ("..
      "AType varchar(25),"..
      "Segment varchar(5),"..
      "Sequence int,"..
      "ItemName varchar(255),"..
      "StartPosition int,"..
      "EndPosition int,"..
      "Active boolean,"..
   ");"
   -- TODO error check?
   conn:execute{sql=sql, live=false}
   
   local k1, v1, bol, values, k0, v0
   -- TODO programmatic way to call each segment population?
	local segment, sequence, items
   trace(segments)
   for k0, v1 in pairs(segments) do
      trace(k0)
      segment, sequence, items = v1()
      ok = insertIntoTableValues(tablename, segment, sequence, items)
      -- TODO error check on ok?
      trace(segment)
      trace(sequence)
      trace(items)
   end
end

----------------------------------------------------------------------------

-- insert into table for one tablename, segment, sequence, multiple items
function insertIntoTableValues(tablename, segment, sequence, items)
	local type="StringLocation"
   local columns="AType,Segment,Sequence,ItemName,StartPosition,EndPosition,Active"
   
   -- insert into table (walk items) 
   for k1,v1 in pairs(items) do
      -- Boolean to string (needed for SQL?? - TODO)
      if v1[3] == true then
         bol="true"
      else
         bol="false"
      end
      values=type..","..segment ..",".. sequence ..",".. k1 ..","..  v1[1] ..",".. v1[2] ..",".. bol
      sql = "INSERT INTO "..tablename.." ("..columns..") VALUES ("..values..")"
      trace(sql)
   end
   
   -- how to check for error? TODO
   conn:execute{sql=sql, live=false}
	return true
end

----------------------------------------------------------------------------

-- AP Segment 1
function segments.APSegment1(columns)
   -- TODO rest
   local items = {NetworkHeader = {1,16,true};
      TransactionType = {17,19,true}
   }

   return "AP", 1, items

   --[[
   local APSeg1 = {
      NetworkHeader             =    "1,16";
      TransactionType           =   "17,19";
      SequenceNumber            =   "20,21";
      CaseNumber                =   "22,23";
      InternalPatientNumber     =   "24,33";
      DistrictNumber            =   "33,43";
      CaseNoteNumber            =   "44,58";
      EpisodeNumber             =   "59,68";
      AccountNumber             =   "69,78";
      ClinicCode                =   "79,87";
      DoctorCode                =   "88,97";
      AppointmentDateTime       =  "98,110";
      AppointmentTypeCode       = "111,114";
      TransportCode             = "115,117";
      Comment                   = "118,143";
      CategoryCode              = "145,147";
      BookedDate                = "148,160";
      PurchaserReferenceNumber  = "161,171";
      WaitingGuaranteeException = "172,173";
      BookedAdmissionsFlag      = "174,177";
      PatientComment            = "178,237";
      CABRef                    = "238,267";
   }
   return segmentName, segmentSequence, columns, values, 
   ]]--
end

----------------------------------------------------------------------------


----------------------------------------------------------------------------
return segment

