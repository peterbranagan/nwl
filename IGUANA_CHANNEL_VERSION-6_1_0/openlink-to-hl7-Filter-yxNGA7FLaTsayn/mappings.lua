sqUtils = require('sqUtils')

--[[

OpenLink Mappings

This is a common openlink mapping (Based on Viaduct and openlink specs)
Its suppose to be generic and not site specific
TODO - Add if Outbound/inbound and mandatory fields

]]

Mappings = {
   OpenLinkVersion = nil,  -- Version of openlink (default null)
	OpenLinkMaps = nil,  -- DAT maps
   OpenLinkSingleMaps = nil, -- Single use DAT maps (for speed and better pickup)
   SegmentSeqToDat = nil, -- Segment Sequence to DAT location
   EventType = nil, --- HL7 Event Type
   MessageArray = nil, -- In process HL7 message array
   MessageType = nil, -- HL7 Message Type
   CaseNo = nil, -- This is not CaseNumber!
   TriggerEvent = nil, -- HL7 Trigger Event
   EventReason = nil, -- HL7 Event Reason
   TransactionType = nil, -- Openlink Transaction Type (AP, PB, etc)
   Process = true -- Process this entry
}

----------------------------------------------------------------------------
-- Initialize
--- Inputs: MessageIn - the inbound OpenLink Message, Version - The version of openlink (nil is current)
--- Return: Mappings (array of items needed), errorText  
function Mappings.init(messageIn, version)
   local items, singleItems, errorText = {}, {}, ''
   Mappings.OpenLinkVersion = version
   -- Get Common openlink maps (mainly for transaction type)
   items = sqUtils.openLinkGetCommonMaps(items)
	Mappings.OpenLinkMaps = items

   -- Build Message Array and SegmentDat Array
   Mappings.MessageArray, Mappings.SegmentDatArray, Mappings.TransactionType, errorText = Mappings.messageToArray(Mappings, messageIn)

   if ( #errorText>0 ) then
      Mappings.recordError( errorText )
   else 
      -- Get Openlink maps (i.e. locations of items to place in DATmessage)
      items, singleItems = sqUtils.openLinkGetMaps(items, singleItems, version, Mappings.TransactionType)
      trace(items)
      -- Get Event Type, Message Type, etc
      Mappings.getMessageHeaderDetails(Mappings)
      trace(Mappings)
   
      -- Save maps
      Mappings.OpenLinkMaps = items
      Mappings.OpenLinkSingleMaps = singleItems
   end

   return Mappings, errorText
end

----------------------------------------------------------------------------
-- Build HL7 Message
--- Inputs: Mappings, messageOut
--- Return: messageOut
function Mappings.buildHL7Message(Mappings, messageOut)
   trace(Mappings)
   trace(Mappings.MessageType)
   
   sqUtils.printLog("Calling Mappings.BuildHL7Message...")
   
   messageOut = Mappings.mapMSH(messageOut)
   messageOut = Mappings.mapEVN(messageOut)
   
   -- Build based on MessageType
	if  ( Mappings.MessageType == "SIU" ) then 
      -- SIU = S12, S14, S15
      messageOut = Mappings.mapSCH(messageOut)
      messageOut = Mappings.mapNTE(messageOut)
      messageOut = Mappings.mapPID(messageOut)
      messageOut = Mappings.mapPV1(messageOut)
      messageOut = Mappings.mapPV2(messageOut)
      messageOut = Mappings.mapPR1(messageOut)
      messageOut = Mappings.mapZH1(messageOut)
      
   elseif ( Mappings.MessageType == "ADT" ) then
      -- ADT = A01, A11, A28, A31, A40
      messageOut = Mappings.mapPID(messageOut)
      messageOut = Mappings.mapPD1(messageOut)
      messageOut = Mappings.mapPV1(messageOut)
      messageOut = Mappings.mapPR1(messageOut)
      if ( Mappings.EventType == "A40" ) then 
         -- A40
         messageOut = Mappings.mapMRG(messageOut)
      elseif ( Mappings.EventType == "A31" ) then 
         -- A31
         messageOut = Mappings.mapNK1(messageOut)
         messageOut = Mappings.mapPV2(messageOut)
         messageOut = Mappings.mapPR1(messageOut)
         messageOut = Mappings.mapZH1(messageOut)
         messageOut = Mappings.mapOBX(messageOut)
      elseif ( Mappings.EventType == "A28" ) then 
         -- A28
         messageOut = Mappings.mapPV2(messageOut)
         messageOut = Mappings.mapAL1(messageOut)
         messageOut = Mappings.mapDG1(messageOut)
         messageOut = Mappings.mapOBX(messageOut)
      elseif ( Mappings.EventType=="A11" ) then 
         -- A11
         messageOut = Mappings.mapPV2(messageOut)
         messageOut = Mappings.mapPR1(messageOut)
      elseif ( Mappings.EventType=="A01" ) then 
         -- A01
         messageOut = Mappings.mapNK1(messageOut)
         messageOut = Mappings.mapPV2(messageOut)
         messageOut = Mappings.mapPR1(messageOut)
      end
      messageOut = Mappings.mapZU3(messageOut) --- TODO (all types?, other ZU types?)
   else 
      Mappings.recordError(messageType.." : No mappings for messagetype")
   end
   trace(messageOut)
   return messageOut
end

----------------------------------------------------------------------------
-- MessageToArray:
-- Convert DAT... lines to DAT object And SegmentDatArray
--- Inputs: Mappings - Mappings array (from Init), messageIn - Inbound Openlink Message
--- Return: MessareArray, segmentDatArray, transactionType, errortext
function Mappings.messageToArray(Mappings, messageIn)
   -- First is always DAT001 (else its in error)
   trace(#messageIn)
   local datLine, table, transactionType, seqNo, startPos, endPos
   local messageArray, segmentDatArray, datNo, errorText, atEnd = {}, {}, 1, '', false
   
   repeat
     -- Find each DATnnn block
	  startPos = string.find( messageIn, "DAT"..string.format("%03x", datNo) )
     endPos = string.find( messageIn, "DAT"..string.format("%03x", datNo+1) )
     if endPos == nil then
         endPos=#messageIn
         atEnd = true
     else
         endPos=endPos-1
     end
     if ( startPos == nil ) and (endPos ~= nil ) then
         --- Error (DAT sequences aren't sequential)
         errorText = "DAT sequences aren't sequential after "..datNo
         atEnd = true
     end    

     trace(startPos)
     trace(endPos)
     if startPos ~= nil then
         datLine = string.sub(messageIn, startPos, endPos)
         trace(datLine)
         messageArray[datNo] = datLine
         -- Use transaction type location and Sequence (So can map to datline number)
         -- Can't use GetItemFromMap at this point as MessageArray isn't built (we are building it here)
         table = Mappings.OpenLinkMaps['TransactionType']
         transactionType = datLine:sub(table[1], table[2])

         table = Mappings.OpenLinkMaps['SequenceNumber']
         seqNo = Mappings.formatSequenceNumber(datLine:sub( table[1], table[2] ))

         segmentDatArray[ transactionType..seqNo ] = datNo
     else
         ---??
         trace("no StartPos")
     end
     datNo=datNo+1
   until ( atEnd == true )
   trace(messageArray)
   trace(segmentDatArray)
   return messageArray, segmentDatArray, transactionType, errorText
end

----------------------------------------------------------------------------
-- Format sequence number (as 20 is actually 2 (its right padded with zeros)
--- Inputs: seqNo
--- Return: SeqNo
function Mappings.formatSequenceNumber(seqNo)
   return seqNo:sub(1,1)
end

----------------------------------------------------------------------------
-- Get table of transaction type to hl7 type
--- Inputs: nothing
--- Return: table {TransactionType = caseNo Location, eventReason, messageType, eventType, triggerEvent, dontProcess}
function Mappings.getTransactionTypeToHL7Type()
   local table, topTable = {}, Mappings.getExtraParams("OpenLinkTransactionToHL7Type", '')
	-- Default for 1st case (override in config)
   if #topTable == 0 then
      topTable = "{ 'transactions': ["..
      
                 "{ 'transaction': 'AP', 'caseNo': 'AP1,CaseNo', 'eventReason': 'AP', 'messageType': 'SIU',"..
                 "'cases': ["..
                   "{ 'case': '01', 'eventType': 'S12', 'triggerEvent': 'S12' },"..
                   "{ 'case': '02', 'eventType': 'S14', 'triggerEvent': 'S14' },"..
                   "{ 'case': '04', 'eventType': 'S15', 'triggerEvent': 'S15' }"..
                  "]"..
                 "},"..
      
                 "{ 'transaction': 'AT', 'caseNo': 'AT1,CaseNo', 'eventReason': 'AT', 'messageType': 'ADT',"..
                 "'cases': ["..
                    "{ 'case': '01', 'eventType': 'S12', 'triggerEvent': 'S12' },"..
                    "{ 'case': '02', 'eventType': 'S14', 'triggerEvent': 'S14' },"..
                    "{ 'case': '04', 'eventType': 'S14', 'triggerEvent': 'S15' }"..
                   "]"..
                 "},"..
                 "{ 'transaction': 'MP', 'caseNo': 'MP1,CaseNo', 'eventReason': 'MP', 'messageType': 'ADT', 'eventType': 'A40', 'triggerEvent': 'A40'"..
                 "},"..
      
                 "{ 'transaction': 'PB', 'caseNo': 'PB1,CaseNo', 'eventReason': 'PB', 'messageType': 'ADT',"..
                 "'cases': ["..
                    "{ 'case': '01', 'eventType': 'A28', 'triggerEvent': 'A28' },"..
                    "{ 'case': '02', 'eventType': 'A31', 'triggerEvent': 'A31' },"..
                    "{ 'case': '04', 'eventType': 'A29', 'triggerEvent': 'A29', 'process': false }"..
                   "]"..
                 "},"..
      
                 "{ 'transaction': 'PR', 'caseNo': 'PR1,CaseNo', 'eventReason': 'PR', 'messageType': 'ADT', 'eventType': 'A31', 'triggerEvent': 'A31'"..
                 "},"..
      
                 "{ 'transaction': 'OC', 'caseNo': 'OC1,CaseNo', 'eventReason': 'OC', 'messageType': 'SIU',"..
                 "'cases': ["..
                    "{ 'case': '01', 'eventType': 'S15', 'triggerEvent': 'S15' },"..
                    "{ 'case': '02', 'eventType': 'S15', 'triggerEvent': 'S15' },"..
                    "{ 'case': '04', 'eventType': 'S15', 'triggerEvent': 'S15' }"..
                   "]"..
                 "}"..
      
                "]}"
   end
   trace(topTable)
   table = Mappings.jsonStringToTable(topTable, "transaction")
   trace(table)
   return table
end

----------------------------------------------------------------------------
-- json string to table (cascading)
--- Inputs: injson, usek (key to use), cascader (cascader identifier)
--- Return: table
function Mappings.jsonStringCascade(table, usek, cascader)
   local temptable,temptable2, k, v = {}
   if usek == nil then usek = '' end
	if cascader == nil then cascader = '' end
   for k,v in pairs(table) do
      if type(v) == "table" then
         temptable2 = Mappings.jsonStringCascade(v, cascader)
         if #usek > 0 then
            temptable[ temptable2[usek] ] = temptable2
         else
            temptable[ k ] = temptable2
         end
      else
         temptable[ k ] = v
      end
   end
   return temptable
end

----------------------------------------------------------------------------
-- json string to transaction hl7 type map
--- Inputs: injson
--- Return: table, table2
function Mappings.jsonStringToTable(injson)
   local table, jtable, k, v = {}, {}
   jtable = json.parse{ data = injson }
   for k,v in pairs(jtable["transactions"]) do
      table[v["transaction"]] = Mappings.jsonStringCascade(v, '' ,"case")
   end
      
   return table
end


----------------------------------------------------------------------------
-- Get Message Header Details, i.e. EventType, etc
--- Inputs: Mappings
--- Return: nothing
function Mappings.getMessageHeaderDetails(Mappings)
	local k, v, out, table, gotTab, casesTab, caseTab, seqNo, process
   out = {}
   trace(Mappings.OpenLinkMaps)
   
   out["transactionType"] = Mappings.TransactionType
   seqNo = Mappings.getItemFromMap(Mappings, 'SequenceNumber') -- TODO why do we need this at this point?
   trace(out["transactionType"])
   trace(seqNo)

   table = Mappings.getTransactionTypeToHL7Type()
   trace(table)
   gotTab = table[out["transactionType"]]
   trace(gotTab)
	if (gotTab ~= nil) then
      process = gotTab["process"]
      if process == false then
         -- Ignore this one
      else
         out["caseNo"] = Mappings.getItemFromMap(Mappings, gotTab["caseNo"])
         out["eventReason"] = gotTab["eventReason"]
         out["messageType"] = gotTab["messageType"]
         out["eventType"] = gotTab["eventType"]
         out["triggerEvent"] = gotTab["triggerEvent"]
         casesTab = gotTab["cases"]
         if ( #out["caseNo"]>0 ) and ( casesTab ~= nil ) then
            caseTab = casesTab[out["caseNo"]]
            for k, v in pairs(caseTab) do
               if #v > 0 then out[k] = v end
            end
         end
      end
   else
      --- Unknown table transaction type
      Mappings.recordError(out["transactionType"].." has no mappings")
   end
   Mappings.MessageType = out["messageType"]
   Mappings.CaseNo = out["caseNo"]
   Mappings.TriggerEvent = out["triggerEvent"]
   Mappings.EventType = out["eventType"]
   Mappings.EventReason = out["eventReason"]
   Mappings.TransactionType = out["transactionType"]
   Mappings.Process = out["process"]
   return
end
----------------------------------------------------------------------------
-- Record an error
--- Inputs: text (error text)
--- Return: nothing
function Mappings.recordError(text)
   sqUtils.printLog(text)
end

----------------------------------------------------------------------------
-- Get value of item from openlink table item
--- Inputs: Mappings, item, default
--- Return: result
function Mappings.getItemFromMap(Mappings, item, default)
   local result, table, k, sub, endPos, arrayNo = nil
   
   trace(Mappings.OpenLinkMaps)
   trace(item)
   
   table = Mappings.OpenLinkMaps[item]
   if table == nil then 
      Mappings.recordError("No table mapping for "..item)
	else 
      trace(table)

      -- Only populate if enabled
      if table[3] == true then
         arrayNo = Mappings.getDatLineFromSegmentSequence(item)
         trace( Mappings.MessageArray[arrayNo] )
         if arrayNo == nil then
            --- No DAT line with details, return ''
            result = ''
         else
            result = Mappings.MessageArray[ arrayNo ]:sub(table[1], table[2])
         end
      end
      --- Remove trailing spaces
	   -- result = sqUtils.removeTrailing(result, " ")
      -- remove spaces from either end
      if (result ~= nil ) then
         result = result:trimWS()
      end

      trace(result)
   end
   if ( #result == 0 and default ~= nil ) then
      result = default
   end

   return result
end

----------------------------------------------------------------------------
-- Get a DAT Linenumber from a segment_sequence (ie. AP1)
--- Inputs: item
--- Return: result
function Mappings.getDatLineFromSegmentSequence(item)
   local result, prefixPos
   trace(item)
   -- Do we have SegPos first? (i.e. AP1,CaseNo)
   prefixPos = string.find(item, ",")
   if ( prefixPos ~= nil ) then
      trace(string.sub(item, 1, prefixPos-1))
      trace(Mappings.SegmentDatArray)
      -- an AP1etc definition
      result = Mappings.SegmentDatArray[string.sub(item, 1, prefixPos-1)]
   else
      result = 1
   end
   trace(result)
   return result
end

----------------------------------------------------------------------------
-- Get an Extra Params value (with default value)
--- Inputs: name default
--- Return: value
function Mappings.getExtraParams(name, default)
   local value = sqUtils.getJsonMappedValue(sqCfg.extra_params, name)
   if ( value == nil ) or ( #value == 0 ) then
      value = default
   end
   return value
end

----------------------------------------------------------------------------
-- Translate openlink format dates to ipm format
--- Inputs: date
--- Return: date
function Mappings.lookupDate(date)
   ---TODO
   return date
end

----------------------------------------------------------------------------
-- Lookup relationship code
--- Inputs: code
--- Return: code
function Mappings.lookupRelationshipCode(code)
   return code
end

----------------------------------------------------------------------------
-- Translate openlink sex to ipm format
--- Inputs: sex
--- Return: sex
function Mappings.lookupSex(sex)
   --- 'M'ale, 'F'emale, 'O'ther or 'U'nknown
   local valid, trans, char = {} , nil, string.upper( string.sub(sex, 1) )
   valid["M"], valid["F"], valid["O"] = "M", "F", "O"
   default = "U"
   if ( char ~= nil ) and ( #char>0 ) then
      trans = valid[char]
   else
      trans = default
   end
   return trans
end

----------------------------------------------------------------------------
-- Translate openlink marital code to ipm format
--- Inputs: marital
--- Return: marital
function Mappings.lookupMaritalCode(marital)
   --- TODO
   return marital
end

----------------------------------------------------------------------------
-- Translate openlink religion code to ipm format
--- Inputs: religion
--- Return: religion
function Mappings.lookupReligionCode(religion)
   --- TODO
   return religion
end

----------------------------------------------------------------------------
-- Translate openlink ethnic group to ipm format
--- Inputs: ethnic
--- Return: ethnic
function Mappings.lookupEthnicGroup(ethnic)
   --- TODO
   return ethnic
end

----------------------------------------------------------------------------
-- Translate boolean to Yes No
--- Inputs: boolean
--- Return: yesno
function Mappings.lookupBooleanYesNo(boolean)
   local yesNo
   if ( boolean == 0 ) then yesNo = "N"
   elseif ( boolean == 1 ) then yesNo = "Y"
   else
      --- TODO Error in sent value
      yesNo = ''
   end
   return yesNo
end

----------------------------------------------------------------------------
-- string to table (specify delimiter)
--- Inputs: string, del (default ",")
--- Return: result
function Mappings.stringToTable(string, del)
   local atEnd, result, acount, endPos, startPos, found = false, {}, 0, 0
   if del == nil then del = "," end
   if ( string == nil or #string == 0 )then
      --- nothing sent
      result = nil
   else
      if string.find(string, del) == nil then
         result[string] = 1
      else
         while atEnd == false do
            acount = acount + 1
            startPos = endPos
            endPos = string.find(string, del, endPos)
            if endPos == nil then
               endPos = #string + 1
               atEnd = true
            end
            found = string.sub(string, startPos, endPos-1)
            result[found] = acount
            endPos=endPos+1
         end
      end
   end
   return result
end
   
----------------------------------------------------------------------------
-- Is there a map for this item?
--- Inputs: Mappings, item
--- Return: true/false
function Mappings.doesMappingExist(Mappings, item)
   if ( Mappings.OpenLinkSingleMaps[item] ~= nil ) then
      return true
   else
      return false
   end
end


----------------------------------------------------------------------------
-- If multiple transaction types have the same item name (but different segment)
-- Search for it and return it here (regardless of Transaction type)
--- Inputs: Mappings, item, default
--- Return: result
function Mappings.getItemFromAnyMap(Mappings, item, default)
   local result, oResult, tType, endPos, startPos, aTab = nil, nil, nil, -1, 1
   trace(Mappings.OpenLinkSingleMaps)
   trace(Mappings.OpenLinkSingleMaps[item])
   aTab = Mappings.stringToTable(Mappings.OpenLinkSingleMaps[item], ",")
   if ( aTab == nil ) then
      --- Item doesn't exist
      trace("No mapping for "..item)
      result = nil
   else
      for k,v in pairs(aTab) do
         oResult = result
         result = Mappings.getItemFromMap(Mappings, k..','..item)
         if ( result == nil ) and ( oResult ~= nil ) then
            result = oResult
         elseif ( result ~= nil ) and ( oResult ~= nil ) and ( result ~= oResult ) then
            --- TODO different result?, what to do?
         end
      end
   end
   trace(result)
   -- Default (if defined)
   if ( result == nil ) and ( default ~=nil ) then
      result = default
   end
   -- remove spaces from either end
   if (result ~= nil ) then
      result = result:trimWS()
   end
   return result
end

----------------------------------------------------------------------------
-- MSH: Message Header
--- Inputs: messageOut
--- Return: messageOut
function Mappings.mapMSH(messageOut)
   sqUtils.printLog("Calling Mappings.MapMSH...")

   messageOut.MSH[3][1]  = Mappings.getExtraParams('SendingApplication', 'CLINICOM')
   messageOut.MSH[4][1]  = Mappings.getExtraParams('SendingFacility', 'CLINICOM')
   messageOut.MSH[5][1]  = Mappings.getExtraParams('ReceivingApplication', 'SQ')
   messageOut.MSH[6][1]  = Mappings.getExtraParams('ReceivingFacility', 'SQ')
   messageOut.MSH[7]     = os.date('!%Y%m%d%H%M%S')
   
   messageOut.MSH[9][1]  = Mappings.MessageType
   messageOut.MSH[9][2]  = Mappings.EventType

   messageOut.MSH[10]    = util.guid(128)
	messageOut.MSH[11][1] = Mappings.getExtraParams('ProcessingID', 'P')
   messageOut.MSH[13]    = Mappings.getItemFromMap(Mappings, 'MessageSequenceNumber')
   messageOut.MSH[16]    = Mappings.getExtraParams('ApplicationAcknowledgementType', 'NE')
   
   return messageOut
end

----------------------------------------------------------------------------
-- EVN: Event
--- Inputs: messageOut
--- Return: messageOut
function Mappings.mapEVN(messageOut)
   sqUtils.printLog("Calling Mappings.MapEVN...")

   messageOut.EVN[1] = messageOut.MSH[9][2]:nodeValue()
   messageOut.EVN[2] = os.date('!%Y%m%d%H%M%S')
   messageOut.EVN[4] = Mappings.TransactionType -- TODO you sure?

   return messageOut

end

----------------------------------------------------------------------------
-- SCH: Schedule
--- Inputs: messageOut
--- Return: messageOut
function Mappings.mapSCH(messageOut)
   local aptStatus
   sqUtils.printLog("Calling Mappings.MapSCH...")
   local mappingPrefix = ''
   
   messageOut.SCH[1][1]  = Mappings.getItemFromMap(Mappings, Mappings.TransactionType..'1,CaseNo')
   messageOut.SCH[1][2]  = 'SWIFTQ'
   messageOut.SCH[2][1]  = Mappings.getItemFromMap(Mappings, Mappings.TransactionType..'1,CaseNoteNumber')

   -- MP 4/2/25 - session is held in doctor code not account number
   -- e.g. L4AHP - Dr Gregory Anticoagulant -HP/LEI
   -- We'd expect 
   -- AP record 1 field 9 Clinic Code - L4AHP
   -- AP record 1 field 10 Doctor Code - HP/LEI
   messageOut.SCH[5][1]  = Mappings.getItemFromMap(Mappings, Mappings.TransactionType..'1,DoctorCode')
   messageOut.SCH[5][1]  = Mappings.getItemFromMap(Mappings, Mappings.TransactionType..'1,DoctorCode', 'NO_SESSION')
   messageOut.SCH[3]     = Mappings.getItemFromMap(Mappings, Mappings.TransactionType..'1,EpisodeNumber')
   messageOut.SCH[9] = ''
   messageOut.SCH[10][1] = 'MIN'
   messageOut.SCH[11][4] = Mappings.lookupDate(Mappings.getItemFromMap(Mappings, Mappings.TransactionType..'1,AppointmentDateTime'))
   
   trace(Mappings.CaseNo)
   
   -- AP message specific code
   if ( Mappings.TransactionType == "AP" ) then
      messageOut.SCH[4][1]  = Mappings.getItemFromMap(Mappings, 'AP1,CABRef')
      --messageOut.SCH[5][1]  = Mappings.getItemFromMap(Mappings, 'AP1,AccountNumber')
      --messageOut.SCH[5][1]  = Mappings.getItemFromMap(Mappings, 'AP1,AccountNumber', 'NO_SESSION')

      messageOut.SCH[8][1]  = Mappings.getItemFromMap(Mappings, 'AP1,AppointmentTypeCode')

      messageOut.SCH[18][1] = Mappings.getItemFromMap(Mappings, 'AP1,CategoryCode')

      if (Mappings.CaseNo == "01") then
         --- 01 New
         aptStatus = "New"
      elseif (Mappings.CaseNo == "02") then
         --- 02 Update
         aptStatus = "Rescheduled"
      else
         --- 04 Cancel
         aptStatus = "Cancelled"
      end
   end
   
   if ( Mappings.TransactionType == "OC" ) then
      aptStatus = "Cancelled"
      --[[
      Michael Card 15:23
      so,  an OC 01 is an add of a cancel,  so the first cancel
      if a user revises the cancel,  then you'll get an 02
      but the only "revision" they can make is to basic stuff like comments etc

      When an Appt is REINSTATED,   then there's an 04  - so its a deletion of the Cancel
      ]]--
      if (Mappings.CaseNo == "04") then
         error("Not supported OC04 type.")
      end
   end
   
   messageOut.SCH[25][1] = aptStatus
   messageOut.SCH[25][2] = aptStatus
   messageOut.SCH[25][3] = ''      

   return messageOut
end

----------------------------------------------------------
-- NTE: Notes and Comments
--- Inputs: messageOut
--- Return: messageOut
function Mappings.mapNTE(messageOut)
   sqUtils.printLog("Calling Mappings.mapNTE...")
   
   messageOut.NTE[1][1] = '1'
   messageOut.NTE[1][2] = ''
   
   if ( Mappings.TransactionType == "AP" ) then
      -- use PatientComment if populated otherwise use comment      
      if sqUtils.isEmpty(Mappings.getItemFromMap(Mappings, 'AP1,PatientComment')) then
         messageOut.NTE[1][3]  = Mappings.getItemFromMap(Mappings, 'AP1,Comment')         
      else
         messageOut.NTE[1][3]  = Mappings.getItemFromMap(Mappings, 'AP1,PatientComment')
      end
   end
   if ( Mappings.TransactionType == "OC" ) then
      messageOut.NTE[1][3]  = Mappings.getItemFromMap(Mappings, 'OC1,AppointmentCancellationComment')
      trace(Mappings.getItemFromMap(Mappings, 'OC1,AppointmentCancellationReason'))
   end

   
   return messageOut
end

----------------------------------------------------------------------------
-- MRG: Merge
--- Inputs: messageOut
--- Return: messageOut
function Mappings.mapMRG(messageOut)
   sqUtils.printLog("Calling Mappings.MapMRG...")

   trace(Mappings)
	messageOut.MRG[1][1] = Mappings.getItemFromMap(Mappings, 'MP1,OldInternalPatientNumber')
   messageOut.MRG[1][5] = Mappings.getExtraParams('Patient Number Identifier', 'PAS')
   messageOut.MRG[2][1] = Mappings.getItemFromMap(Mappings, 'MP1,OldDistrictNumber')
   messageOut.MRG[2][5] = Mappings.getExtraParams('District Number Identifier', 'DN')
   trace(messageOut)
   return messageOut
end

----------------------------------------------------------------------------
-- PID: Patient Information
--- Inputs: messageOut
--- Return: messageOut
function Mappings.mapPID(messageOut)
   local patNum, disNum, nhsNum, tel, pos, addType
   sqUtils.printLog("Calling Mappings.MapPID...")

   messageOut.PID[1] = '1' 
   
   --- Patient Numbers
   if ( Mappings.TransactionType == "MP" ) then
      patNum = Mappings.getItemFromAnyMap(Mappings, 'OldInternalPatientNumber', '')
      disNum = Mappings.getItemFromAnyMap(Mappings, 'OldDistrictNumber', '')
      
      local nhsNumState = Mappings.getItemFromAnyMap(Mappings, 'NHSStatusCode', '')
      if nhsNumState == '01' then
         nhsNum = Mappings.getItemFromAnyMap(Mappings, 'NHSNumber', '') -- no old?
      else
         nhsNum=''         
      end
   else
      patNum = Mappings.getItemFromAnyMap(Mappings, 'InternalPatientNumber', '')
      disNum = Mappings.getItemFromAnyMap(Mappings, 'DistrictNumber', '')      
      
      local nhsNumState = Mappings.getItemFromAnyMap(Mappings, 'NHSStatusCode', '')
      if nhsNumState == '01' then
         nhsNum = Mappings.getItemFromAnyMap(Mappings, 'NHSNumber', '')
      else
         nhsNum=''
      end
   end
	-- Patient Number
   if #patNum>0 then 
      messageOut.PID[3][1][1] = patNum
      messageOut.PID[3][1][5] = Mappings.getExtraParams('Patient Number Identifier', 'PAS')
   end
	
	-- District Number
   if ( #disNum > 0 ) then 
      messageOut.PID[2][1] = disNum
      messageOut.PID[2][5] = Mappings.getExtraParams('District Number Identifier', 'DN')
   end

   --- NHS Number
   if ( #nhsNum > 0 ) then 
      messageOut.PID[3][2][1] = nhsNum
      messageOut.PID[3][2][5] = Mappings.getExtraParams('NHS Number Identifier', 'NHS')
   end

   --- Name
   messageOut.PID[5][1][1] = Mappings.getItemFromAnyMap(Mappings, 'Surname', '')
   messageOut.PID[5][1][2] = Mappings.getItemFromAnyMap(Mappings, 'Forenames', '')
   messageOut.PID[5][1][5] = Mappings.getItemFromAnyMap(Mappings, 'Title', '')
   messageOut.PID[5][1][7] = Mappings.getExtraParams('Legal Name Identifier', 'L')
   
   messageOut.PID[7] = Mappings.lookupDate( Mappings.getItemFromAnyMap(Mappings, 'DateOfBirth', '') )
   messageOut.PID[8] = Mappings.lookupSex( Mappings.getItemFromAnyMap(Mappings, 'Sex', '') )

   --- Home Address (use Extended (if we have it))
	if ( Mappings.doesMappingExist(Mappings, 'ExtendedPatientAddress1') == true ) and ( Mappings.getItemFromAnyMap(Mappings, 'ExtendedPatientAddress1', '') ~= '') then
      addType = "Extended"
   else
      addType = ''
   end
   
   for i=1,4 do 
      messageOut.PID[11][1][i] = Mappings.getItemFromAnyMap(Mappings, addType..'PatientAddressLine'..i, '')
   end
   messageOut.PID[11][1][5] = Mappings.getItemFromAnyMap(Mappings, addType..'PatientPostCode', '')
   
   --- Phone Numbers
   tel = Mappings.getItemFromAnyMap(Mappings, 'HomeTelephoneNumber', '')
   pos = 1
   if ( #tel > 0 ) then 
      messageOut.PID[13][1][1] = tel
      messageOut.PID[13][1][3] = Mappings.getExtraParams('Home Telephone Number Type', 'PH')
      pos=2
   end
   --- Mobile number - Only PB transaction types send mobile and text message consent (todo sure? - so says spec!)
	if (Mappings.TransactionType == "PB") then
      tel = Mappings.getItemFromMap(Mappings, 'PB3,MobileTelephoneNumber')
      if ( #tel > 0 ) then
         messageOut.PID[13][pos][1] = tel
         messageOut.PID[13][pos][3] = Mappings.getExtraParams('Mobile Telephone Number Type', 'CP')
         messageOut.PID[13][pos][9] = Mappings.lookupBooleanYesNo( Mappings.getItemFromAnyMap(Mappings, 'ConsentToCommunicateViaMessage', '') )
         pos = pos + 1
      end
   end

	-- Work number
   tel = Mappings.getItemFromAnyMap(Mappings, 'WorkTelephoneNumber', '')
   if ( #tel > 0 ) then
      messageOut.PID[13][pos][1] = tel
      messageOut.PID[13][pos][3] = Mappings.getExtraParams('Work Telephone Number Type', 'WPN')
      pos = pos+1
   end

   --- Email address
   tel = Mappings.getItemFromAnyMap(Mappings, 'PatientsEmailAddress', '')
   if ( #tel > 0 ) then
      messageOut.PID[13][pos][1] = tel
      messageOut.PID[13][pos][3] = Mappings.getExtraParams('Email Telephone Number Type', 'Internet')
      pos=pos+1
   end
   
   messageOut.PID[16]    = Mappings.lookupMaritalCode( Mappings.getItemFromAnyMap(Mappings, 'MaritalStatusCode', '') )
	messageOut.PID[17]    = Mappings.lookupReligionCode( Mappings.getItemFromAnyMap(Mappings, 'ReligionCode', '') )
	messageOut.PID[22]    = Mappings.lookupEthnicGroup( Mappings.getItemFromAnyMap(Mappings, 'EthnicGroupCode', '') )
   messageOut.PID[23]    = Mappings.lookupDate( Mappings.getItemFromAnyMap(Mappings, 'PlaceOfBirth', '') )
   messageOut.PID[26]    = Mappings.getItemFromAnyMap(Mappings, 'School', '')
   messageOut.PID[27][2] = Mappings.getItemFromAnyMap(Mappings, 'Occupation', '')
   messageOut.PID[29]    = Mappings.lookupDate( Mappings.getItemFromAnyMap(Mappings, 'DateOfDeath', '') )
   messageOut.PID[30]    = Mappings.lookupBooleanYesNo( Mappings.getItemFromAnyMap(Mappings, 'DeathIndicator', '') )
   return messageOut
end  

----------------------------------------------------------------------------
-- PV1: Patient Visit
--- Inputs: messageOut
--- Return: messageOut
function Mappings.mapPV1(messageOut)
   local visitID, clinicCode, hospCode, appType, docCode, consCode, specCode, repToLoc, aptDateTime, patNum
   sqUtils.printLog("Calling Mappings.MapPV1...")

   clinicCode = ""
	trace(Mappings.TransactionType)

   if ( Mappings.TransactionType == "AP" ) then
      clinicCode  = Mappings.getItemFromAnyMap(Mappings, 'ClinicCode', Mappings.getExtraParams('Default Clinic Code', 'GENMED'))
   end
   
   if ( Mappings.TransactionType == "OC" ) then
      clinicCode  = Mappings.getItemFromAnyMap(Mappings, 'DoctorGroupCode', Mappings.getExtraParams('Default Clinic Code', 'GENMED'))
   end
   
   hospCode    = Mappings.getItemFromAnyMap(Mappings, 'HospitalCode', Mappings.getExtraParams('Default Clinic Code', 'GENMED'))
   appType     = Mappings.getItemFromAnyMap(Mappings, 'AppointmentTypeCode', '')
   clinicSessionCode     = Mappings.getItemFromAnyMap(Mappings, 'DoctorCode', '')
   consCode    = Mappings.getItemFromAnyMap(Mappings, 'ClinicConsulantCode', '')
   specCode    = Mappings.getItemFromAnyMap(Mappings, 'ClinicSpecialtyCode', '')
   repToLoc    = Mappings.getItemFromAnyMap(Mappings, 'ReportToLocation', '')
   aptDateTime = Mappings.lookupDate(Mappings.getItemFromAnyMap(Mappings, 'AppointmentDateTime', ''))

   if ( Mappings.TransactionType == "MP" ) then
      patNum = Mappings.getItemFromAnyMap(Mappings, 'OldInternalPatientNumber', '')
   else
      patNum = Mappings.getItemFromAnyMap(Mappings, 'InternalPatientNumber', '')
   end

   --- Internal Patient ID, Episode, Appt DT, Clinic Code
   visitID = 
      patNum..
      Mappings.getItemFromAnyMap(Mappings, 'EpisodeNumber', '')..
      aptDateTime..
      clinicCode
	---
   
   messageOut.PV1[1]       = 1
   messageOut.PV1[2]       = 'O'

   messageOut.PV1[3][1]    = clinicCode
   messageOut.PV1[3][2]    = clinicSessionCode
	messageOut.PV1[3][3]    = ''
   messageOut.PV1[3][4][1] = ''
   messageOut.PV1[4]       = appType
   messageOut.PV1[5][1]    = ''
   messageOut.PV1[7][1]    = ''
   messageOut.PV1[9][1]    = consCode
   messageOut.PV1[10]      = specCode
   messageOut.PV1[11][1]   = repToLoc
   messageOut.PV1[14]      = ''
   messageOut.PV1[18]      = ''
   messageOut.PV1[19][1]   = visitID
   messageOut.PV1[31]      = ''
   messageOut.PV1[36]      = ''
   messageOut.PV1[44]      = aptDateTime
   messageOut.PV1[45]      = ''

   return messageOut
end

----------------------------------------------------------------------------
-- PV2: Patient Visit Additionla Info
--- Inputs: messageOut
--- Return: messageOut
function Mappings.mapPV2(messageOut)
   --- TODO
   sqUtils.printLog("Calling Mappings.MapPV2...")

   messageOut.PV2[12] = ''
   messageOut.PV2[25] = ''

   return messageOut
end 

----------------------------------------------------------
-- Remove leading characters
--- Inputs: item, char
--- Return: newitem
function Mappings.removeLeading(item, char)
   local newitem = item
   for i = 1, #item do
      if string.sub(item, i, i) == char then
         newitem = string.sub(item, i+1, #item)
      else
         break
      end
   end
   return newitem
end

----------------------------------------------------------------------------
-- setHL7Node (and step down until [1] - incase of schema issues)
--- Inputs: messageOut
--- Return: messageOut
-- TODO needs thinking NOT COMPLETE!
function Mappings.setHL7Node(seg, item)
   if ( seg:childCount() > 0 ) then
      -- Has children - step down again
      trace("Adding [1]")
      seg = Mappings.setHL7Node(seg[1], item)
	else
      -- Bottom level
      seg = Mappings.getItemFromAnyMap(Mappings, item, '')
   end
   trace("Set at "..seg..":"..item)
   return seg
end
   

----------------------------------------------------------------------------
-- PD1: Patient Demographics
--- Inputs: messageOut
--- Return: messageOut
function Mappings.mapPD1(messageOut)
   local count
   sqUtils.printLog("Calling Mappings.MapPD1...")
   trace(messageOut)
   --- Do it this way incase of schema differences (i.e. [3][1] or [3][1][1]
   --Mappings.setHL7Node(messageOut.PID[3][1], "GPAddressLine1") -- TODO this [1] code is awful!
   --[[if messageOut.PD1[3][1]:childCount()>0 then
      messageOut.PD1[3][1][1]  = Mappings.getItemFromAnyMap(Mappings, 'GPAddressLine1', '')
   else
      messageOut.PD1[3][1]  = Mappings.getItemFromAnyMap(Mappings, 'GPAddressLine1', '')
	end
   if messageOut.PD1[3][3]:childCount()>0 then
      messageOut.PD1[3][3][1]  = Mappings.getItemFromAnyMap(Mappings, 'GPPracticeCode', '')
   else
      messageOut.PD1[3][3]  = Mappings.getItemFromAnyMap(Mappings, 'GPPracticeCode', '')
   end
   if messageOut.PD1[3][6]:childCount()>0 then
      messageOut.PD1[3][6][1]  = Mappings.getItemFromAnyMap(Mappings, 'GPAddressLine2', '')
   else
      messageOut.PD1[3][6]  = Mappings.getItemFromAnyMap(Mappings, 'GPAddressLine2', '')
   end
   if messageOut.PD1[3][7]:childCount()>0 then
      messageOut.PD1[3][7][1]  = Mappings.getItemFromAnyMap(Mappings, 'GPAddressLine3', '')
   else
      messageOut.PD1[3][7]  = Mappings.getItemFromAnyMap(Mappings, 'GPAddressLine3', '')
	end
   if messageOut.PD1[3][8]:childCount()>0 then
      messageOut.PD1[3][8][1]  = Mappings.getItemFromAnyMap(Mappings, 'GPAddressLine4', '')
   else
      messageOut.PD1[3][8]  = Mappings.getItemFromAnyMap(Mappings, 'GPAddressLine4', '')
   end

   count = messageOut.PID[3]:childCount()
   if count > 8 then
      messageOut.PD1[3][9]  = Mappings.getItemFromAnyMap(Mappings, 'GPPostCode', '')
   end
   if count > 9 then
      messageOut.PD1[3][10] = Mappings.getItemFromAnyMap(Mappings, 'GPTelephone', '')
   end
	messageOut.PD1[4][1]  = Mappings.getItemFromAnyMap(Mappings, 'GPCode', '')
	messageOut.PD1[4][2]  = Mappings.getItemFromAnyMap(Mappings, 'GPSurname', '')
	messageOut.PD1[4][3]  = Mappings.getItemFromAnyMap(Mappings, 'GPInitials', '')
	messageOut.PD1[4][6]  = Mappings.getItemFromAnyMap(Mappings, 'GPTitle', '')
   --]]
   if messageOut.PD1[3][1]:childCount()>0 then 
      messageOut.PD1[3][1][1]  = Mappings.getItemFromAnyMap(Mappings, 'GPPracticeCode', '')
   else
      messageOut.PD1[3][1]  = Mappings.getItemFromAnyMap(Mappings, 'GPPracticeCode', '')
   end
	messageOut.PD1[4][1]  = Mappings.getItemFromAnyMap(Mappings, 'GPCode', '')
	messageOut.PD1[4][2]  = Mappings.getItemFromAnyMap(Mappings, 'GPSurname', '')
	messageOut.PD1[4][3]  = Mappings.getItemFromAnyMap(Mappings, 'GPInitials', '')
	messageOut.PD1[4][6]  = Mappings.getItemFromAnyMap(Mappings, 'GPTitle', '')
   return messageOut
end 

----------------------------------------------------------------------------
-- AL1: Allergies
--- Inputs: messageOut
--- Return: messageOut
function Mappings.mapAL1(messageOut)
   --- (Don't exist in Openlink)
   sqUtils.printLog("Calling Mappings.MapAL1...")

   return messageOut
end 

----------------------------------------------------------------------------
-- DG1: Diagnosis
--- Inputs: messageOut
--- Return: messageOut
function Mappings.mapDG1(messageOut)
   --- (Don't exist in Openlink)
   sqUtils.printLog("Calling Mappings.MapDG1...")

   return messageOut
end 

----------------------------------------------------------------------------
-- PR1: Procedures
--- Inputs: messageOut
--- Return: messageOut
function Mappings.mapPR1(messageOut)
   --- (Don't exist in Openlink)
   sqUtils.printLog("Calling Mappings.MapPR1...")
   return messageOut
end 

----------------------------------------------------------
-- NK1 - Next of Kin
--- Inputs: messageOut, nkno (NOk Number - default 1)
--- Return: messageOut
function Mappings.mapNK1(messageOut, nkno)
   local tel
   if ( nkno == nil ) then nkno = 1 end

   sqUtils.printLog("Calling Mappings.MapNK1...")
   trace(messageOut.NK1)
   messageOut.NK1[nkno][1]       = nkno
   messageOut.NK1[nkno][2][1]    = Mappings.removeLeading(Mappings.getItemFromAnyMap(Mappings, 'NextOfKinName', ''), ' ')

   --- NOK Relationship code
   messageOut.NK1[nkno][3][1]    = Mappings.lookupRelationshipCode(Mappings.getItemFromAnyMap(Mappings, 'NOKRelationshipCode', ''))

   --- NOK Address
   for i = 1, 4 do
      messageOut.NK1[nkno][4][i] = Mappings.getItemFromAnyMap(Mappings, 'NOKAddressLine'..i, '')
   end
   
   --- NOK telephone numbers
   tel = Mappings.getItemFromAnyMap(Mappings, 'NOKTelephoneHome', '')
   if ( #tel > 0 ) then 
      messageOut.NK1[nkno][5][1][1] = tel
      messageOut.NK1[nkno][5][1][3] = Mappings.getExtraParams('Home Telephone Number Type', 'PH')
   end
   tel = Mappings.getItemFromAnyMap(Mappings, 'NOKTelephoneWork', '')
   if ( #tel > 0 ) then 
      messageOut.NK1[nkno][6][1]    = tel
      messageOut.NK1[nkno][6][3]    = Mappings.getExtraParams('Work Telephone Number Type', 'WPN')
   end

   --- NOK Identifier
   messageOut.NK1[nkno][7][1]       = Mappings.getExtraParams('NOK Identifier', 'NOK')

   return messageOut
end

----------------------------------------------------------------------------
-- ZH1 : Custom HSE
--- Inputs: messageOut
--- Return: messageOut
function Mappings.mapZH1(messageOut)
   sqUtils.printLog("Calling Mappings.MapZH1...")

   return messageOut
end 

----------------------------------------------------------------------------
-- ZU3 : Custom 
--- Inputs: messageOut
--- Return: messageOut
function Mappings.mapZU3(messageOut)
   sqUtils.printLog("Calling Mappings.MapZU3...")
	--- ZU3 doesn't exist? but is in viaduct map --- TODO
   ---MessageOut.ZU3[1] = Mappings.GetItemFromAnyMap(Mappings, 'AttendanceStatus', '')
   
   return messageOut
end 

----------------------------------------------------------------------------
-- OBX: Observations/Results
--- Inputs: messageOut
--- Return: messageOut
function Mappings.mapOBX(messageOut)
   --- Not needed (no order/results details)
   sqUtils.printLog("Calling Mappings.MapOBX...")

   return messageOut
end 

----------------------------------------------------------------------------
return Mappings