
--set the database path
_G.DbFile = os.getenv('DB_FILEPATH')

--Connect to the DB and retrive the config data
sqCfg = require("sqConfig")
sqCfg.init() -- initialize the config
sqCfg.getConfigData() -- pull the interface config fromDB
-------------------

Map = require('mappings')

-------------------- MAIN ----------------------------------------------

function main(messageIn)
   local openLinkVersion, Mappings, messageOut, errotext, hType
   sqUtils.printLog("Starting main..")
   
   -- Get openlink version (typically null for default version)
   openLinkVersion = sqUtils.getJsonMappedValue(sqCfg.extra_params,'OpenLinkVersion')
   
   -- In test show input Message
   if iguana.isTest() then
      trace( messageIn )
      trace( openLinkVersion )
   end
   
   -- Initialise the mappings (get openlinkmaps, messagetype, segmentsequencedatmaps, etc)
   Mappings, errorText = Map.init(messageIn, openLinkVersion)
   if (#errorText == 0) then
      trace(Mappings)
      if Mappings.Process == false then
         -- ignore
      else
         trace(Mappings.MessageType)
         hType = Mappings.MessageType..Mappings.EventType
         --Create a stub HL7 message for MessageType
         --ipm_uki_30082024.vmd
         messageOut = hl7.message{ vmd='dxc_ipm_tas_17.02.25.vmd', name = hType}

         -- Build the HL7 Message
         messageOut = Mappings.buildHL7Message(Mappings, messageOut)
         if iguana.isTest() then
            trace(messageOut)
         end

         -- Push the new message out to the destination queue
         if messageOut ~= nil then
            sqUtils.printLog(hType.." message pushed out")
            queue.push{ data = messageOut:S() }
         else
            sqUtils.printLog("A problem was encountered generating the outbound message of "..hType)
            continue = false
         end
      end
   else
      trace(errorText)
      --- There was an error
   end
end