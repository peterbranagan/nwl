
--set the database path
--_G.DbFile = '/etc/iNTERFACEWARE/test_au_db_credentials'
_G.DbFile = os.getenv('DB_FILEPATH')

--Connect to the DB and retrive the config data
sqCfg = require("sqConfig")
sqCfg.init() -- initialize the config
sqCfg.getConfigData() -- pull the interface config fromDB
-------------------

--PB 12.03.24: Added for use in IPM lookup
--local llp = require 'llp'
--
--

sqDB = require('sqDatabase')

--Switch this to 1 if we want to log database activity to file.
_G.dbAudit = '0'
--store HL7 message ID
_G.msgCtrlID = ''
--store the returning ACK message
_G.ackMessage = 'OK' -- OK is the default, to override, set to "CA: Warning Text Here..."

--============== MAIN ===================================================


function main(Data) 

   --setting for sending an audit of db inserts and updates to an iguana db audit table
   _G.dbAudit = sqUtils.getMappedValue(sqCfg.extra_params,"dbAudit")
          
   iguana.stopOnError(false)
   trace(sqCfg.dbCredentials)
   --[[ TESTING ACK WARNINGS
   sqUtils.setAckWarning(1)
   ]]
   
   
   --[[ TESTING  ]]
   
   
   
   --[[
   In order to trap errors we run main in protected mode
   any errors that occur when writing to the the database
   or other will be caught and dictate the ACK message returned
   to the sender
   ]]
   if iguana.isTest() then
      ProtectedMain(Data)
   else
      local Success, Err = pcall(ProtectedMain, Data)
      if not Success then
         sqUtils.printLog("Setting ACK to error")
         setAckMsg("Error processing message")
         error(Err)
      else
         sqUtils.printLog("Setting ACK to OK")
         setAckMsg(_G.ackMessage)
      end
   end
   
end

----------------------------
function UpdateNHSNumber(PID,patientID)
   -- get the NHS Number from PID based on nhs assigning facility
   local nhsNumber = getMRNbyAssigningFacility(PID, sqUtils.getJsonMappedValue(sqCfg.extra_params,'nhsAssigningFacility' ))
   trace(nhsNumber)
   -- if nhs number is populated
   if not sqUtils.isEmpty(nhsNumber:trimWS()) then
      sqDB.THS.updatePatientNHSNumber(patientID,nhsNumber)
   end   
end
----------------------------
function ProtectedMain(Data)
   
   sqUtils.printLog("Message Received: \n\n"..Data)
      
   iguana.stopOnError(false)

   --Check that we have a connection an accesible
   --If it fails then try to reconnect
   if not conn or not conn:check() then
      if conn then conn:close() end --close off any stale connection
      conn = sqCfg.dbConnect()
   end
      
   conn:begin{}   
   conn:execute{sql="SET SESSION time_zone = 'Europe/Dublin';"}

   -- Parse the incoming message
   local vmdFile = sqUtils.getMappedValue(sqCfg.extra_params,"mainVMDFile")
   local MsgIn = hl7.parse{vmd=vmdFile, data=Data} 
  
   --Store the message control ID
   _G.msgCtrlID = MsgIn.MSH[10]:S()
   
   --check if processing multi facilities
   local multiFacility = sqUtils.getMappedValue(sqCfg.extra_params,"multiFacility")
   
   local msgType  = MsgIn.MSH[9][1]:S()
     
   --Check if it is an MFN Message i.e. process ADT and SIU Messages 
   if MsgIn.MSH[9][1]:S() ~= 'MFN' then

      --get the MRN from PID based on assigning facility
      local mrn = getMRNbyAssigningFacility(MsgIn.PID,sqUtils.getJsonMappedValue(sqCfg.extra_params,'assigningFacility'))

      --get the facility ID based on the sending application and sending facility
      facilityID = json.parse{data=sqCfg.extra_params}.parentFacility[MsgIn.MSH[3][1]:S()][MsgIn.MSH[4]:S()]
      if sqUtils.isEmpty(facilityID) then
         error("No facility ID set up for "..MsgIn.MSH[4]:S())
      end
         
      local parentFacilityID = facilityID
      local consID = facilityID -- default cons id to parent facility id
      
      --get the patient ID
      local patientID = sqDB.THS.getPatientIdByMrn(mrn, parentFacilityID)
         
      --if I12 and PAS lookup switched on
      local PASLookup = sqUtils.getMappedValue(sqCfg.extra_params,"queryPAS")
     
      if MsgIn.MSH[9]:S() == 'REF^I12' and PASLookup == '1' then
         local Msg = sendPASQuery(mrn)
         ----MAY NEED TO MODIFY HOW WE CREATE / UPDATE BELOW IF THE PATIENT IS
         ----BEING CREATED VIA THE LOOKUP
         
      end
      --

      if parentFacilityID ~= nil then
         
         --check for NK1 segments
         local NK1 = nil
         if MsgIn:childCount('NK1') > 0 then
            NK1 = MsgIn.NK1
         end      
         
         --check for GT1 segments
         local GT1 = nil
         if MsgIn:childCount('GT1') > 0 then
            GT1 = MsgIn.GT1
         end  
         
          --check fo ZPV segments
         local ZPV = nil
         if MsgIn:childCount('ZPV') > 0 then
            ZPV = MsgIn.ZPV
            if sqUtils.getJsonMappedValue2(sqCfg.extra_params,'interpreterMap',ZPV[1]:S()) ~= nil then
               sqDB.patientData.interpreter_required = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'interpreterMap',ZPV[1]:S())
            end
         end
         
         sqUtils.printLog("Create/Update on PID segment: Begin DB trans...")
         conn:begin{live=true}
         
         local Success, Error = pcall(function()

               local PID_MsgsForCreate = sqUtils.getJsonMappedValue(sqCfg.extra_params,"PID_MsgTypesToCreate")
               local PID_MsgsForUpdate = sqUtils.getJsonMappedValue(sqCfg.extra_params,"PID_MsgTypesToUpdate")

               local str = MsgIn.MSH[9][1]:S()..'^'..MsgIn.MSH[9][2]:S()

               --Add or update patients based on msg containing a PID segment
               if MsgIn:childCount('PID') > 0 then
                  trace(patientID)
                  -- TODO : remove later
                  -- patientID = '0'
                  if patientID == '0' then
                     --only process the PID segments for defined message types
                     if string.find(PID_MsgsForCreate, str) ~= nil then
                        patientID = sqDB.THS.createPatient(MsgIn.PID, mrn, NK1, IN1, GT1) 
                        sqUtils.printLog("PID: Patient ID not found, Created patient with MRN: "..mrn.." and returned ID: "..patientID)
                        -- default to off
                        sqDB.THS.changeUserNotificationPreferencesForAllEvents(patientID,'sms',false)    
                        sqDB.THS.changeUserNotificationPreferencesForAllEvents(patientID,'email',false)                        
                        -- enable for PB
                        if MsgIn.EVN[4]:S() == "PB" then
                           sqDB.THS.changeUserNotificationPreferencesForAllEvents(patientID,'sms',true)
                           sqDB.THS.changeUserNotificationPreferencesForAllEvents(patientID,'email',true)                           
                        end
                        
                        UpdateNHSNumber(MsgIn.PID,patientID)                        
                     end
                  else
                     --only process the PID segments for defined message types
                     if string.find(PID_MsgsForUpdate, str) ~= nil then
                        --sqDB.THS.updatePatient(patientID, MsgIn.PID, mrn, NK1, IN1, GT1, MsgIn.MSH[9][2]:S())
                        local mobileIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileIdentifier')  
                        sqDB.patientData.mobile = sqDB.THS.getPhoneNumberByType(MsgIn.PID,mobileIdentifier)
                        
                        --if MsgIn.EVN[4]:S() == "PR" then
                        if sqUtils.isEmpty(sqDB.patientData.mobile) then
                           sqDB.THS.updatePatientNoMobile(patientID, MsgIn.PID, mrn, NK1, IN1, GT1, MsgIn.MSH[9][2]:S())
                        else
                           sqDB.THS.updatePatient(patientID, MsgIn.PID, mrn, NK1, IN1, GT1, MsgIn.MSH[9][2]:S())
                        end
                       
                        -- enable for PB
                        if MsgIn.EVN[4]:S() == "PB" then
                           sqDB.THS.changeUserNotificationPreferencesForAllEvents(patientID,'sms',true)
                           sqDB.THS.changeUserNotificationPreferencesForAllEvents(patientID,'email',true)                           
                        end
                        
                        UpdateNHSNumber(MsgIn.PID,patientID)
                        
                        sqUtils.printLog("PID: Updated patient with MRN: "..mrn..", ID: "..patientID)
                     end
                  end        

                  --If we get a GP id in PV1.8 then update the user record
                  if patientID ~= '0' and MsgIn:childCount('PV1') ~= 0 and not sqUtils.isEmpty(MsgIn.PV1[8][1]:S()) then
                     sqDB.THS.updatePatientGP(patientID, MsgIn.PV1[8][1]:S())
                  end

               end
	-- TODO : remove later
               -- patientID = 1
               --Add the patient to he facility
               if patientID ~= '0' then
                  sqDB.THS.addToFacility(patientID, parentFacilityID)
               end

            end)

         if Success then

            sqUtils.printLog("Create/Update on PID segment: Committing transactions...")
            conn:commit{live=true}

         else

            sqUtils.printLog("Create/Update on PID segment:: DB activity failed, Rolling back transactions: "..tostring(Error.message))
            conn:rollback{live=true}

            error(Error)

         end
         
         --do nothing if patient ID = 0
         if patientID == '0' then
            do return end
         end
         
          --check for IN1 segments
         local IN1 = nil
         
         local processInsuranceDetails = sqUtils.getMappedValue(sqCfg.extra_params,"processInsuranceDetails")
         if processInsuranceDetails == '1' then
            if MsgIn:childCount('IN1') > 0 then

               local insJson = '['
               local insText = ''
               for i=1, #MsgIn.IN1 do
                  insText = insText .. MsgIn.IN1[i][3][1]:S()
                  insJson = insJson .. '"' .. MsgIn.IN1[i][3][1]:S() .. '"'
                  if i <  #MsgIn.IN1 then
                     insJson = insJson .. ','
                     insText = insText .. ','
                  end
               end
               insJson = insJson..']'

               if insText ~= '' then
                  sqDB.THS.addUpdateInsurance_NEW(patientID, insJson, insText)
               end
            end
         end

         --Add GP details for all ADT messages
         if MsgIn.MSH[9][1]:S() == 'ADT' then
            
            sqUtils.printLog("Update GP details...")
            sqDB.THS.createGPDetails(patientID, MsgIn.PD1, parentFacilityID)
         
         end

         --Merge message received
         if MsgIn.MSH[9][2]:S() == 'A40' or MsgIn.MSH[9][2]:S() == 'A34' then
            sqUtils.printLog("process A40/A34...")

            local minorMRN = MsgIn.MRG[1][1]:S()
            trace(minorMRN)

            local minorPatient = sqDB.THS.getPatientIdByMrn(minorMRN,parentFacilityID)
            sqDB.THS.mergePatient(patientID,minorPatient,parentFacilityID)
         end
         
         --check for patient alerts
         local processAlerts = sqUtils.getMappedValue(sqCfg.extra_params,"processPatientAlerts")
         if processAlerts == '1' and (MsgIn.MSH[9][2]:S() == 'A28' or MsgIn.MSH[9][2]:S() == 'A31') then
            if sqUtils.segmentExists(MsgIn,"ZPA") then
               sqDB.THS.addUpdatePatientAlerts(patientID,parentFacilityID,MsgIn)
            end
         end

         --patient referral creation
         if MsgIn.MSH[9][2]:S() == 'I12' then
            
            local func = function()
               --act differently for multiple facilityu hospital
               if multiFacility == '1' then
                  parentFacilityID, consID = sqDB.THS.getCSRparentFacilityId(MsgIn)
                  
                  --if the parent facility retirned does not match the overall parent
                  --then it means that this referral is being assigned and we need to
                  --add a facility record for the user
                   --get the facility ID based on the site
                  local str = 'parentFacilityId_'..MsgIn.MSH[4][1]:S()
                  local overallFacilityID = sqUtils.getJsonMappedValue(sqCfg.extra_params,str)
                 
                  if parentFacilityID ~= overallFacilityID then
                     sqDB.THS.addToFacility(patientID, parentFacilityID)
                  end
                                  
               end
               
               process_I12(patientID, parentFacilityID, MsgIn, consID) 
            
            end
            
            sqDB.executeBlock(func,'Processing i12')

         end

         --patient referral modified
         if MsgIn.MSH[9][2]:S() == 'I13' then
            
            local func = function()
               --act differently for multiple facilityu hospital
               if multiFacility == '1' then
                  parentFacilityID, consID = sqDB.THS.getCSRparentFacilityId(MsgIn)
                  --check if the referral is a modifcation to a previously 'staged' or 'bucketed' facility
                  --and if so set the referral to point to facility picked up from the mapping or CSR mechanism
                  --ie. parentFacilityID above
                  sqDB.THS.checkReferralStaging(MsgIn, patientID, parentFacilityID)
               end
               
               process_I13(patientID, parentFacilityID, MsgIn, consID) 
            
            end
            
            sqDB.executeBlock(func,'Processing i13')

         end
        
         --patient referral cancelled
         if MsgIn.MSH[9][2]:S() == 'I14' then
            
            local func = function()
               --act differently for multiple facilityu hospital
               if multiFacility == '1' then
                  parentFacilityID, consID = sqDB.THS.getCSRparentFacilityId(MsgIn)
               end
               
               process_I14(patientID, parentFacilityID, MsgIn)  
            
            end
            
            sqDB.executeBlock(func,'Processing i14')          
         
         end

         --patient added to waitng list
         if MsgIn.MSH[9][2]:S() == 'A05' then   
            
            local func = function()
               process_A05(patientID, MsgIn, parentFacilityID) 
            end
            
            sqDB.executeBlock(func,'Processing A05')
            
         end
         
         --Waiting list delete/removal - A38 , FEEDERS is the code from IPM indicating its a reflex message
         --PB 07.05.2024 : IR-279 : request made to suppress A38 messages
         if MsgIn.MSH[9][2]:S() == 'A38' and MsgIn.EVN[5][1]:S() ~= 'FEEDERS' then
            
            local func = function()
               process_A38(patientID, MsgIn, parentFacilityID) 
            end
            
            sqDB.executeBlock(func,'Processing A38')
         
         end
         
         --waiting list update
         if MsgIn.MSH[9][2]:S() == 'A08' then               
            sqUtils.printLog("A08s are not processed!")
         end 

         --pull out the variables used across all SIU messages
         --
         local AppStatus = ''
         local clinCode1 = ''
         local reasonID = ''
         local ApptComment = ''

         if msgType == 'SIU' then
            
            --determine if we are using the config mapping or CSR lookup
            local useCSRLookup = sqUtils.getJsonMappedValue(sqCfg.extra_params,'useCSRLookup')

            AppStatus = MsgIn.SCH[25][1]:S()
            clinCode1 = MsgIn.PV1[3][1]:S()
            visitType = MsgIn.PV1[4]:S()
            
            -- get the appointment comment if populated
            if not sqUtils.isEmpty(MsgIn.NTE[1][3]:S()) then
               ApptComment = MsgIn.NTE[1][3]:S()
            end                        

            --[[ from GUH
            local communicationMediaCode = MsgIn.PV1[31]:S()
            local visitDesc = MsgIn.PV2[12]:S()
            --]]

            --get the mapped reason id
            --reasonID = sqUtils.getMappedValue2(sqCfg.appreason_map,clinCode1,visitType)
            reasonID = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'appReasonsMap_inbound',visitType)  
            trace(visitType)
            trace(reasonID)
            
            if reasonID == '' or reasonID == nil then
               reasonID = 0
               sqUtils.printLogW("Reason ID not found")
            end

            --Get the facility data based on 'sending facility|clinic|session'
            
            --if not using CSR lookup then use config mapping
            if useCSRLookup ~= '1' then

               local ipmToSQFac = MsgIn.MSH[4]:S().."|"..MsgIn.PV1[3][1]:S().."|"..MsgIn.SCH[5][1]:S()  
               trace(ipmToSQFac)
               ipmToSQFacilityMap = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'SQinboundClinicMap',ipmToSQFac)
               trace(ipmToSQFacilityMap)
               if sqUtils.isEmpty(ipmToSQFacilityMap) then
                  --error("Incoming Sending facility, session, clinic not mapped for "..ipmToSQFac)
                  local str = 'parentFacilityId_'..MsgIn.MSH[4][1]:S()
                  facilityID = sqUtils.getJsonMappedValue(sqCfg.extra_params,str)
                  sqUtils.printLogW("Incoming Sending facility, session, clinic not mapped for "..ipmToSQFac..", defaulting to parent facility of "..facilityID)
               else
                  facilityID = sqUtils.getJsonMappedValue(sqUtils.jsonTableToString(ipmToSQFacilityMap),'facId')
               end
               
            else
               
               facilityID = sqDB.THS.getFacilityFromCSR(MsgIn.MSH[4]:S(), MsgIn.PV1[3][1]:S(), MsgIn.SCH[5][1]:S())
               
            end
            
         end    
       
         --create appointment
         if MsgIn.MSH[9][2]:S() == 'S12' then            
            local func = function()
               sqDB.THS.createAppointment(patientID, facilityID, MsgIn.SCH, clinCode1, reasonID, 
                  MsgIn.PV2, MsgIn.PV1, MsgIn.AIP,ApptComment)
            end
            
            sqDB.executeBlock(func,'Processing S12')
            
         end

         --reschedule appointment
         if MsgIn.MSH[9][2]:S() == 'S13' then

            local func = function()
               if sqUtils.isIn(AppStatus,{"Attended", "CheckedOut"}) then
                  sqDB.THS.updatePatientFlow(patientID, facilityID, MsgIn.SCH, clinCode1, reasonID)
               else
                  sqDB.THS.updateAppointment(patientID, facilityID, MsgIn.SCH, clinCode1, reasonID, 
                     MsgIn.PV2, MsgIn.PV1,MsgIn.AIP)
               end
            end
            
            sqDB.executeBlock(func,'Processing S13')

         end

         --modify appointment
         if MsgIn.MSH[9][2]:S() == 'S14' then

            local func = function()
               if sqUtils.isIn(AppStatus,{"Attended", "CheckedOut"}) then
                  sqDB.THS.updatePatientFlow(patientID, facilityID, MsgIn.SCH, clinCode1, reasonID)
               else
                  sqDB.THS.updateAppointment(patientID, facilityID, MsgIn.SCH, clinCode1, reasonID, MsgIn.PV2, 
                     MsgIn.PV1,MsgIn.AIP)
               end
            end
            
            sqDB.executeBlock(func,'Processing S14')

         end

         --cancel appointment
         if MsgIn.MSH[9][2]:S() == 'S15' then
            
            local func = function()
               sqDB.THS.cancelAppointment(patientID, facilityID, MsgIn.SCH, reasonID)
            end
            
            sqDB.executeBlock(func,'Processing S15')
            
         end

         --DNA
         if MsgIn.MSH[9][2]:S() == 'S26' then
            
            local func = function()
               sqDB.THS.dnaAppointment(patientID, facilityID, MsgIn.SCH, reasonID)
            end
            
            sqDB.executeBlock(func,'Processing S26')
         end

      else
         error('Could not get parent facility ID')
      end

   else
      
      -- process MFN^M02
      if MsgIn.MSH[9][2]:S() == 'M02' then

         --get the facility ID based on the sending application and sending facility
         local parentFacilityID = json.parse{data=sqCfg.extra_params}.parentFacility[MsgIn.MSH[3][1]:S()][MsgIn.MSH[4]:S()]

         local GP = parseGP(MsgIn)
         GP.id = sqDB.THS.findGp(GP,parentFacilityID)

         if GP.id == nil then
            sqDB.THS.createGP(GP,parentFacilityID)
         else
            sqDB.THS.updateGP(GP,parentFacilityID)
         end

      end   

   end
   
   conn:commit{live=true}
   
   

end

--------------------------------------------------------------

function parseGP(message)
   
   local GP = {}
   
   GP.firstname = message.STF[3][1]
   GP.address_1 = message.STF[11][1]
   GP.address_2 = message.STF[11][2]
   GP.address_3 = message.STF[11][3]
   GP.address_4 = message.STF[11][4]
   GP.address_5 = message.STF[11][5]
   GP.phone = message.STF[10][9]
   GP.hospital_code = message.PRA[6][1]
   GP.gmc_code = message.PRA[6][2]
   GP.dh_code = message.PRA[6][3]
   
   return GP
end

--------------------------------------------------------------

function sendPASQuery(mrn)
   
   ----TEST
   mrn = '500214696'
   -----------
   
   local mobile = ''
   local home_phone = ''
   local mobileint = ''
   
   if mrn == '' or mrn == nil then return '','' end
   
   local Message = hl7.message{vmd='IPM_QRY.vmd', name='QRYA19'}
   
   --[[ TAS format
   MSH|^~\&|Swiftqueue||iPM||202404111758||QRY^A19|d1ad8082-18d6-47ee-998e-fa37e6ac405c|P|2.4|
   QRD|202404111758|R|I|d1ad8082-18d6-47ee-998e-fa37e6ac405c|||50^RD|105891538^^^^^^^^^^^^PAS|DEM|
   QRF|UI||||
   ]]

   local msgID = util.guid(128)
   
   Message.MSH[3][1] = 'Swiftqueue'
   Message.MSH[4][1] = ''
   Message.MSH[5][1] = 'IPM'
   Message.MSH[6][1] = ''
   Message.MSH[7] = os.ts.date('!%Y%m%d%H%M%S')
   Message.MSH[9][1] = 'QRY'
   Message.MSH[9][2] = 'A19'
   Message.MSH[10] = msgID
   Message.MSH[11][1] = 'P'
   Message.MSH[12] = '2.4'
   Message.QRD[1] = os.ts.date('!%Y%m%d%H%M%S')
   Message.QRD[2] = 'R'
   Message.QRD[3] = 'I'
   Message.QRD[4] = msgID
   Message.QRD[7][1] = '50'
   Message.QRD[7][2][1] = 'RD'
   Message.QRD[8][1] = mrn
   Message.QRD[8][13] = 'PAS'
   Message.QRD[9][1] = 'DEM'
   Message.QRF[1] = 'UI' 
   
   trace(Message:S())

   local ipAddress = sqUtils.getMappedValue(sqCfg.extra_params,"queryIP")
   local port = sqUtils.getMappedValue(sqCfg.extra_params,"queryPort")
   local sockTimeout = sqUtils.getMappedValue(sqCfg.extra_params,"queryTimeout")
   
   if not sock then
      
      sock = llp.connect{host=ipAddress,
         port=tonumber(port),
         timeout=tonumber(sockTimeout),
         live=false}
   
   end
   
   print("sending: "..Message:S().." to IPM (LLP)")
   sock:send(Message)
   Ack = sock:recv()   
   print("Received: "..Ack.." from IPM")
   trace(Ack)
   if sock then sock:close() end
   sock = nil
   
   if Ack == '' or Ack == '???' then
      error('Failed to retieve patient demographics for: '..mrn)
   end

   local retMsg = hl7.parse{vmd='IPM_QRY.vmd', data=Ack}
      
   return retMsg

end

--------------------------------------------------------------

function process_I12(patientID, parentFacilityID, MsgIn, consID)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")

   --get the referral source segment
   local refSourceSegment = getRefSourceSegment(MsgIn)
   trace(refSourceSegment)
   if refSourceSegment == '' then
      sqUtils.printLog("No PRD RP/PIMS segment found in message, Missing referral source!")
   end
      
   sqDB.THS.createPatientReferral(patientID, parentFacilityID, clinCode, MsgIn.RF1, refSourceSegment, consID)
   
   --update the referral comments
   
   local refComment = MsgIn.NTE[1][3]:S()
   if refComment ~= '' then
      local controlID = MsgIn.RF1[6][1]:S()
      sqDB.THS.addUpdateReferralComments(patientID, parentFacilityID, controlID, refComment)
   end
   
   sqUtils.printLog("Exiting "..getCurrentFunctionName().."...")

end

--------------------------------------------------------------

function process_I13(patientID, parentFacilityID, MsgIn, consID)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
   --check for referral closure
   if MsgIn.RF1[4]:S() == sqUtils.getMappedValue(sqCfg.extra_params,"closeReferralCode") then
      local controlID = MsgIn.RF1[6][1]:S()
      local disOutcome = ''
      sqDB.THS.closePatientReferral(patientID, parentFacilityID, controlID, disOutcome) 
      return
   end
            
   
   --get the referral source segment
   local refSourceSegment = getRefSourceSegment(MsgIn)
   trace(refSourceSegment)
   if refSourceSegment == '' then
      sqUtils.printLog("No PRD RP/PIMS segment found in message, Missing referral source!")
   end

   local refID = MsgIn.RF1[6][1]:S()

   local referralID = sqDB.THS.getPatientReferral(patientID, parentFacilityID, refID)
   sqUtils.printLog("referralID returned: "..referralID)

   if referralID ~= '0' then
      --update referral
      sqDB.THS.updatePatientReferral(patientID, parentFacilityID, clinCode, MsgIn.RF1, refSourceSegment, consID)
   else  
      --create the referral
      referralID = sqDB.THS.createPatientReferral(patientID, parentFacilityID, clinCode, MsgIn.RF1, refSourceSegment, consID)     
   end

   if not sqUtils.isEmpty(MsgIn.PID[29]:S()) then
      sqUtils.printLog("DOD found in PID ")
      sqDB.THS.setDeceasedReferral(patientID, referralID, parentFacilityID)
   end
   
   --update the referral comments
   
   local refComment = MsgIn.NTE[1][3]:S()
   if refComment ~= '' then
      local controlID = MsgIn.RF1[6][1]:S()
      sqDB.THS.addUpdateReferralComments(patientID, parentFacilityID, controlID, refComment)
   end
   
   sqUtils.printLog("Exiting "..getCurrentFunctionName().."...")
   
end

--------------------------------------------------------------

function process_I14(patientID, parentFacilityID, MsgIn)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
    --get the referral source segment
   local refSourceSegment = getRefSourceSegment(MsgIn)
   trace(refSourceSegment)
   if refSourceSegment == '' then
      sqUtils.printLog("No PRD/RP segment found in message, ignoring...")
      sqUtils.printLogW("No referral source segment ( PRD RP/PIMS ) found in message!")
   else

      sqUtils.printLog("Referral source segment: "..refSourceSegment:S())

      --discharge reason
      local disOutcome = MsgIn.RF1[4]:S()

      local refID = MsgIn.RF1[6][1]:S()

      local referralID = sqDB.THS.getPatientReferral(patientID, parentFacilityID, refID)
      sqUtils.printLog("referralID returned: "..referralID)

      if referralID ~= '0' then
         --discharge referral
         sqDB.THS.dischargePatientReferral(patientID, parentFacilityID, refID, disOutcome )
      end

   end
   
   sqUtils.printLog("Exiting "..getCurrentFunctionName().."...")
   
end

--------------------------------------------------------------

function process_A05(patientID, MsgIn, parentFacilityID)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")

   local waitListID = MsgIn.PV1[5][1]:S()
   local controlID = ''
   
   local accClass = MsgIn.ZWL[4][2]:S()
   local WLComment = MsgIn.ZWL[7][2]:S()
   local cpcDesc = MsgIn.ZWL[8][2]:S()

   --   
   -- Need to find ROL segment where ROL[1][2] = 'REFRL'
   --  then referralID = ROL[1][1] 
   local rolGroup = MsgIn:child("ROL", 2)

   for i = 1, #rolGroup do
      if (rolGroup[i][1][2]:S() == 'REFRL') then
         trace("ROL segment found with namespace \'REFRL\'")
         controlID = rolGroup[i][1][1]:S()
         break
      end
   end 
   if controlID == '' then
      sqUtils.printLog("Unable to find control ID in ROL segment")                  
   end
   
   local referralID, parentFacilityID = sqDB.THS.getReferralByControlID(controlID, patientID, parentFacilityID)
   if sqUtils.isEmpty(referralID) then
      sqUtils.printLog("Referral ID not found for patientID: "..patientID..", orderID: "..controlID..", Skipping message...")
      return
   end
   
   --first check if the waitng list was previously removed.
   --If it was then re-establish the waiting list and make visible on SQ
   sqDB.THS.unRemoveWaitList(patientID,controlID,parentFacilityID)

   --  
   --Update the referral
   sqDB.THS.updateWaitList(patientID,waitListID,controlID,parentFacilityID)
   
   --update the account class
   sqDB.THS.addUpdateReferralAccountClass(parentFacilityID, referralID, accClass)
   
   --update the condition type
   sqDB.THS.addUpdateReferralConditionType(parentFacilityID, referralID, cpcDesc)
   

   --update the waiting list comments
   if WLComment ~= '' then
      sqDB.THS.addUpdateWLComments(patientID, parentFacilityID, controlID, WLComment)
   end
   
   sqUtils.printLog("Exiting "..getCurrentFunctionName().."...")
   
end

--------------------------------------------------------------

function process_A38(patientID, MsgIn, parentFacilityID)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")

   local waitListID = MsgIn.PV1[5][1]:S()
   local referralID = ''
   
   if sqUtils.isEmpty(patientID) or sqUtils.isEmpty(waitListID) then
      iguana.logError("A38 missing patient ID and/or waiting list ID")
      return
   end

   --  
   --remove waiting list
   sqDB.THS.removeWaitList(patientID,waitListID,parentFacilityID)
   
   sqUtils.printLog("Exiting "..getCurrentFunctionName().."...")
   
end

--------------------------------------------------------------

function getMRNbyAssigningFacility(PID, aFacility)
   
   local mrn = ''
   local lastEffectiveDate = ''
   
   for i=1, #PID[3] do
      
      if PID[3][i][5]:S() == aFacility then
         
         local effectiveDate = PID[3][i][7]:S()
         local expiryDate = PID[3][i][8]:S()
         
         if effectiveDate >= lastEffectiveDate and expiryDate == '' then
            lastEffectiveDate = effectiveDate
            mrn = PID[3][i][1]:S()
         end
      
      end
   
   end
   
   return mrn

end

--------------------------------------------------------------

function getRefSourceSegment(MsgIn)
   
   local refSourceSegment = ''
   
   for i = 1, #MsgIn.Group2 do
      if (MsgIn.Group2[i].PRD[1][1]:S() == 'RP' or MsgIn.Group2[i].PRD[1][1]:S() == 'PIMS') then
         trace("RP segment found")
         refSourceSegment = MsgIn.Group2[i].PRD
         break
      end
   end
   
   if refSourceSegment == nil or refSourceSegment == '' then 
      iguana.logError("Unable to process referral as no PRD segment found")
   end 
   
   return refSourceSegment
end

--------------------------------------------------------------



--------------------------------------------------------------

function isEmpty(s)
    return s == nil or s == '' or type(s) == 'userdata'
end

--------------------------------------------------------------



--------------------------------------------------------------

function getCurrentFunctionName()
   
    local level = 2 -- Adjust this level to match the desired function's call stack depth.
    local info = debug.getinfo(level, "n")
     
    return info and info.name or "unknown"
    
end
--------------------------------------------------------------

function setAckMsg(str)
   
   local customACK = sqUtils.getMappedValue(sqCfg.extra_params,'customACK')

   if customACK == '1' then 
      
      sqUtils.printLog("Setting ACK to: "..str)

      local rconn = db.connect{api=db.MY_SQL, name=sqConfig.dbCredentials.db_name, 
         user=sqConfig.dbCredentials.db_user, 
         password=sqConfig.dbCredentials.db_pass, live=true}

      conn:execute {sql="update iguana_config set ack_msg = '"..str.."' where channel = "..
         rconn:quote(sqConfig.ie_channel).." and ie_instance = "..
         rconn:quote(sqConfig.ie_instance), live=false}

      rconn:close()

   end

end


