sqUtils = require('sqUtils')

--[[

Mappings module is customized per site
Code here will change and is not generic
Usually we'll send out an ORM or SIU message
depending on client requirements

]]

Mappings = {

   outboundMsgType = '',
   
   siu = {}

}

----------------------------------------------------------------------------

function Mappings.buildSIU(Message, Value, Value2)
   
   sqUtils.printLog("Calling Mappings.buildSIU...")
     
   Message = Mappings.siu.mapMSH(Message, Value.appointment)
   Message = Mappings.siu.mapEVN(Message, Value.appointment)
   Message = Mappings.siu.mapSCH(Message, Value.appointment, Value2)
   Message = Mappings.siu.mapPID(Message, Value.patient, Value2)
   Message = Mappings.siu.mapPV1(Message, Value.appointment)
   Message = Mappings.siu.mapAIL(Message, Value.appointment, Value2)
   
   if sqUtils.getMappedValue(sqCfg.extra_params,"processAppComments") == '1' then
      if Mappings.outboundMsgType == 'S12' or Mappings.outboundMsgType == 'S13' then
         Message = Mappings.siu.mapNTE(Message, Value.appointment)
      end
   end
	
   return Message

end

----------------------------------------------------------------------------

-- SIU Mappings

----------------------------------------------------------------------------

function Mappings.siu.mapMSH(Message, Appointment)
   
   sqUtils.printLog("Calling Mappings.siu.mapMSH...")

   Message.MSH[3][1] = 'SWIFTQUEUE'
   
   local customer = sqUtils.getMappedValue(sqCfg.extra_params,"customer")
   if customer == 'NWL' then
      Message.MSH[4][1] = getSendingFacility(Appointment.facility_id)
   else
      Message.MSH[4][1] = sqUtils.getMappedValue(sqCfg.extra_params,"receivingIdentifier")
   end
  
   Message.MSH[5][1] = sqUtils.getMappedValue(sqCfg.extra_params,"receivingApp")
   Message.MSH[6][1] = sqUtils.getMappedValue(sqCfg.extra_params,"receivingIdentifier")
   Message.MSH[7] = os.ts.date('!%Y%m%d%H%M%S')

   Message.MSH[9][1] = 'SIU'
   Message.MSH[9][2] = Mappings.outboundMsgType

   --Message.MSH[10] = os.ts.date('!%Y%m%d%H%M%S')..Appointment.id
   -- use this if looking for different control id 
   Message.MSH[10] = _G.msgCtrlID
   Message.MSH[11][1] = sqUtils.getMappedValue(sqCfg.extra_params,"processingID")
   Message.MSH[12] = sqUtils.getMappedValue(sqCfg.extra_params,"hl7Version")


   return Message

end

----------------------------------------------------------------------------

function Mappings.siu.mapEVN(Message, Appointment, PatientFlow)
   
   sqUtils.printLog("Calling Mappings.siu.mapEVN...")
 
   Message.EVN[1] = Mappings.outboundMsgType 
   Message.EVN[2] = os.ts.date('!%Y%m%d%H%M%S')

   return Message

end

----------------------------------------------------------------------------

function Mappings.siu.mapSCH(Message, Appointment, Order)
   
   sqUtils.printLog("Calling Mappings.siu.mapSCH...")

   local Appid = 0

   Appid = Order.order_id
   SQAppid = Appointment.id
   Accessionid = Order.order_id

   Message.SCH[1][1] = Appid
   Message.SCH[2][1] = SQAppid
   Message.SCH[3] = Accessionid
   Message.SCH[9] = Appointment.duration
   Message.SCH[10][1] = 'MIN'
   Message.SCH[11][4] = Appointment.start

   local Appmod = Appointment.modified

   if Appointment.modified_source == 'b' then
      if Appointment.status == 'active' then
         Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appRescheduleCode")
      else
         Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appCancelCode")
      end
   elseif Appointment.modified_source == 'c' then
      if Appointment.status == 'active' then
         Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appRescheduleCode")
      else
         Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appCancelCode")
      end
   elseif Appointment.modified_source == 'PR' then
      if Appointment.status == 'active' then
         Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appRescheduleCode")
      else
         Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appCancelCode")
      end
   elseif Appointment.modified_source == 'o' then
      if Appointment.status == 'active' then
         Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appRescheduleCode")
      else
         Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appCancelCode")
      end
   else
      Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appNewCode")
   end
   
   return Message
end

----------------------------------------------------------------------------

function Mappings.siu.mapPID(Message, Patient, Order)
   
   sqUtils.printLog("Calling Mappings.siu.mapPID...")

   Message.PID[1] = Patient.mrn   -- Patient.id
   Message.PID[2][1] = Patient.mrn   -- Patient.id
   Message.PID[3][1][1] = Patient.mrn
   Message.PID[5][1] = Patient.surname
   Message.PID[5][2] = Patient.firstname
   Message.PID[7] = Patient.birth_year_field .. Patient.birth_month_field .. Patient.birth_date_field
   Message.PID[8] = Patient.Gender
   Message.PID[18][1] = Order.episode_number

   return Message
end  

----------------------------------------------------------------------------

function Mappings.siu.mapPV1(Message, Appointment, PatientFlow, Order)
   
   sqUtils.printLog("Calling Mappings.siu.mapPV1...")
   
   Message.PV1[2] = sqUtils.getMappedValue(sqCfg.extra_params,"patientClass")
   Message.PV1[3][1] = sqUtils.getMappedValue(sqCfg.extra_params,"pointOfCare")
   Message.PV1[3][4][1] = sqUtils.getMappedValue(sqCfg.extra_params,"assignedPatientLocation")
   Message.PV1[44] = os.ts.date('!%Y%m%d%H%M%S')
   Message.PV1[51] = sqUtils.getMappedValue(sqCfg.extra_params,"visitIdicator")
   
   return Message
end

----------------------------------------------------------------------------

function Mappings.siu.mapAIL(Message, Appointment, Order)

   sqUtils.printLog("Calling Mappings.siu.mapAIL...")

   AppReasonId = Appointment.reason_id
   FacilityId = Appointment.facility_id
   AppId = Appointment.id
   
   Message.AIL[1] = '1'
   Message.AIL[3][1] = Appointment.custom_fields.room_id
   
   --room code not in payload so do a DB lookup
   if sqUtils.isEmpty(Appointment.custom_fields.room_id) then
      
      sqUtils.printLog("Room code not received in SQ poayload, performing DB lookup...")
      
      local findRoom = conn:execute{sql=
         "select count(id), value_str from reservation_additional_info where reservation_id = " .. AppId, live=true}

      local JTRoom = findRoom[1]['value_str']:S() 

      if findRoom[1]['count(id)']:S() ~= '0' then
         Message.AIL[3][1] = findRoom[1]['value_str']:S()
      end
      
   end
   
   if Message.AIL[3][1]:S() == '' then
      error("AIL Segment: Room code not found")
   end

	--get the site ID
   local findRoom2 = conn:execute{sql=
      "select count(id),room_id, site_id from reason_exam_codes " ..
      "where reason_id = " .. AppReasonId .. " and facility_id = " .. FacilityId .. " limit 1", live=true}

   Message.AIL[3][2] = findRoom2[1]['site_id']:S()
   Message.AIL[3][4][1] = findRoom2[1]['site_id']:S()

   return Message
end

----------------------------------------------------------------------------

function Mappings.siu.mapNTE(Message, Appointment)
   
   sqUtils.printLog("Calling Mappings.siu.mapNTE...")
   
   -- NTE|1||this is the note from the patient|SQ Note
   
   Message.NTE[1] = '1'
   Message.NTE[3] = Appointment.comments
   Message.NTE[4][1] = 'SQ Note'
   
   return Message
end

----------------------------------------------------------------------------

function getSendingFacility(facilityID)
   
   --[[
   RYJ = Imperial
   RV8 = LNWH
   RQM = ChelWest
   ]]
  
   local sendingFacility = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'outboundNACSMap', facilityID)
   
   if sqUtils.isEmpty(sendingFacility) then
      error("Failed to find NACS mapping for : "..facilityID)
   end
   
	return sendingFacility
   
end

----------------------------------------------------------------------------
----------------------------------------------------------------------------
return Mappings