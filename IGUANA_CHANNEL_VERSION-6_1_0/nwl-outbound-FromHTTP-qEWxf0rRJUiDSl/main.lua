
--set the database path
_G.DbFile = os.getenv('DB_FILEPATH')

--Connect to the DB and retrive the config data
sqCfg = require("sqConfig")
sqCfg.init() -- initialize the config
sqCfg.getConfigData() -- pull the interface config fromDB
-------------------

map = require('mappings')

_G.filterPatientID = ''  --PB : internal SQ patient ID, not MRN

----------------------------------------------------------------------------

function main(Data)
   
   --iguana.status()
   
   sqUtils.printLog("Starting main...")
   
   sqUtils.printLog(Data)
   
   iguana.stopOnError(false)
   
   -- Parse the HTTP request
   local Request = net.http.parseRequest{data=Data}

   -- Only accept POST requests
   if Request.method ~= 'POST' then
      sqUtils.printLog("Sending 'method not allowed' response to sender...")
      net.http.respond{body='{"success": false, "message": "Method not allowed"}', code=405}
      do return end
   end

   --Check that we have a connection an accesible
   --If it fails then try to reconnect
   if not conn or not conn:check() then
      if conn then conn:close() end --close off any stale connection
      conn = sqCfg.dbConnect()
   end
   
   conn:begin{}
   conn:execute{sql="SET SESSION time_zone = 'Europe/Dublin';"}
   
   -- Parse the incoming JSON
   local Payload = json.parse{data=net.http.parseRequest{data=Data}.body}
   
   sqUtils.printLog("Processing payload..")
   
   --respond to say payload received
   net.http.respond{body='{"success": true}', code=200}
   
   for Key,Value in pairs(Payload) do
      
      local vmdFile = sqUtils.getMappedValue(sqCfg.extra_params,"outboundVMDFile")
      local Message = hl7.message{vmd=vmdFile, name='SIUS12'}

      sqUtils.printLog("Processing Value.orders..")
      
      for Key2,Value2 in pairs(Value.order) do

         local proceed = okToProceed(Value)

         if proceed == 'Yes' then
            
             _G.msgCtrlID = util.guid(128)

            Message = processAppointmentPayload(Message, Value, Value2)

            if Message ~= '' then

               queue.push{data=Message:S()}

            end

         end

      end
      
   end

   
   conn:commit{live=true}
   
end

----------------------------------------------------------------------------

function okToProceed(Value)
   
   local proceed = 'No'
   
   if Value.appointment.source == 'h' then
      proceed = 'No'
   else
      proceed = 'Yes'
   end
   
   if Value.appointment.modified_source == 'c' then
      proceed = 'Yes'
   end
      
   if Value.appointment.modified_source == 'b' then
      proceed = 'Yes'
   end
      
   if Value.appointment.modified_source == 'PR' then
      proceed = 'Yes'
   end
      
   if Value.appointment.modified_source == 'o' then
      proceed = 'Yes'
   end
      
   if Value.appointment.modified_source == 'sb' then
      proceed = 'No'
   end
   
   return proceed
   
end

----------------------------------------------------------------------------

function determineAppMessageType(Value)

   local msgType = ''
   
   if Value.appointment.modified_source == 'b' then
      if Value.appointment.status == 'active' then
        msgType = 'S13'
      else
         msgType = 'S15'
      end
      
   elseif Value.appointment.modified_source == 'c' then
      if Value.appointment.status == 'active' then
         msgType = 'S13'
      else
         msgType = 'S15'
      end   
      
   elseif Value.appointment.modified_source == 'PR' then
      if Value.appointment.status == 'active' then
         msgType = 'S13'
      else
         msgType = 'S15'
      end
      
   elseif Value.appointment.modified_source == 'o' then
      if Value.appointment.status == 'active' then
         msgType = 'S13'
      else
         msgType = 'S15'
      end 
   else
      msgType = 'S12'
   end
   
   return msgType
   
end

----------------------------------------------------------------------------

function processAppointmentPayload(Message, Value, Value2)
   
   local res = false
   
   local msgType = determineAppMessageType(Value)
   Mappings.outboundMsgType = msgType
   
   if msgType == 'S12' then
      Message = process_S12(Message, Value, Value2)
      
   elseif msgType == 'S13' then
      Message = process_S12(Message, Value, Value2)
      
   elseif msgType == 'S15' then
      Message = process_S12(Message, Value, Value2)
      
   else
      error("Unhandled message type received!")
   
   end
   
   return Message

end

----------------------------------------------------------------------------

function process_S12(Message, Value, Value2) 
   
   return Mappings.buildSIU(Message, Value, Value2)
   
end

----------------------------------------------------------------------------


function process_S14(Value, Value2)
   
   return Mappings.buildSIU(Message, Value, Value2)
   
end

----------------------------------------------------------------------------

function process_S15(Value, Value2)
   
   return Mappings.buildSIU(Message, Value, Value2)
   
end


