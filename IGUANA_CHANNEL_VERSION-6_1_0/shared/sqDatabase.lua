--[[

Author: Peter Branagan
Date: Aug 2023

All database activity needs be executed from this modules, separated from any other scripts


]]

local sqDB = {

   patientData = {
      mrn = '',
      localmrn = '',
      firstname = '',
      surname = '',
      user_type = '',
      user_status = '',
      dob = '',
      gender = '',   
      home_phone = '',
      mobile = '',
      email = '',
      home_phone = '',
      mobile = '',
      mobileint = '',
      address1 = '',
      address2 = '',
      address3 = '',
      address4 = '',
      address5 = '',
      title = '',
      parent_mobile = '',
      medicareNumber = '',
      imedixNumber = '',
      middleName = '',
      interpreter_required = '',
      spokenLanguage = '',
      alias_firstname = '',
      alias_surname = '',
      alias_middlename = '',
      aboriginality = ''
   },
   
   nokData = {
      
      nk1_userID = '',
      nk1_parent = '',
      nk1_firstname = '',
      nk1_surname = '',
      nk1_rel_code =  '',
      nk1_rel_desc =  '',
      nk1_rel_id = '',        
      nk1_mobile = '',
      nk1_mobile_format = '',
      nk1_phone = '',
      nk1_address_1 = '',
      nk1_address_2 = '',
      nk1_address_4 = '',
      nk1_address_5 = '',
      nk1_eircode = '',
      nk1_email = '',
      nk1_dob = '',
      nk1_title = '',
      nk1_gender = ''
      
   },
   
   gtData = {
      
      gt1_userID = '',
      gt1_parent = '',
      gt1_firstname = '',
      gt1_surname = '',
      gt1_rel_code =  '',
      gt1_rel_desc =  '',
      gt1_rel_id = '',        
      gt1_mobile = '',
      gt1_mobile_format = '',
      gt1_phone = '',
      gt1_address_1 = '',
      gt1_address_2 = '',
      gt1_address_4 = '',
      gt1_address_5 = '',
      gt1_eircode = '',
      gt1_email = '',
      gt1_dob = '',
      gt1_title = '',
      gt1_gender = ''
      
   },
   
   THS = {},
   RAD = {},
   TUH = { RAD = {} }
	--------
}

----------------------------------------------------------------------------

function sqDB.execute(val)
   
   --[[
         NOTE: **** The audit table being written to this function 
                     will grow, will need a script to purge monthly ****
   ]]
   
   local channel = iguana.channelName()
   local sql = val.sql:gsub("\\'","'")
   sql = sql:gsub("\n","")
   local msgid = _G.msgCtrlID
   local insert = val.sql:upper():find('INSERT INTO')
   if insert ~= nil then insert = true end
   local update = val.sql:upper():find('UPDATE ')
   if update ~= nil then update = true end
   local delete = val.sql:upper():find('DELETE FROM ')
   if delete ~= nil then delete = true end
   
   if insert or update or delete then
      --check the sql string for HL7 control characters and replace them
      local decodeSpecialCharacters = sqUtils.getMappedValue(sqCfg.extra_params,"decodeSpecialCharacters")
      if decodeSpecialCharacters == '1' then
         val.sql = sqDB.unEncodeString(val.sql)
      end
   end
   
   trace(val.sql)
   
   --execute the SQL caommand
   
   local res = conn:execute(val)
   
   --log the DB activity if set to do so
   
   if _G.dbAudit == '1' then 

      --log all inserts and updates to the iguana db audit table

      if insert or update or delete then
         
         --[[
         conn:execute{sql="insert into iguana_db_audit (channel_name, msgid, sql_str) values ("..    
            conn:quote(channel)..","..
            conn:quote(msgid)..","..
            conn:quote(sql)..")"
            , live=false}
         ]]
         
         local dt = os.date("%Y-%m-%d %H:%M:%S")
         local line = "\""..dt.."\",\""..channel.."\",\""..msgid.."\",\""..sql.."\""
         
         if not iguana.isTest() then
            --sqUtils.printLog("Writing SQL statement to DB Audit File..")
            --local filePath = "/root/iguana_db_audit.txt"
            local filePath = os.getenv("DB_AUDIT_FILEPATH")
            local file, err = io.open(filePath, "a")
            if not file then
               iguana.logError("Error opening file: " .. err)
            end
           
            file:write(line .. "\n") -- Add a newline character to separate lines
            
            --write the returned ID from the insert
            if insert then
               local result = conn:execute{sql="select last_insert_id() as id", live=true}
               sql = "ID FROM LAST INSERT = "..result[1].id:S()
               line = "\""..dt.."\",\""..channel.."\",\""..msgid.."\",\""..sql.."\""
               file:write(line .. "\n")
            end
         
            file:close()
         end
      end
   end
     
   return res
   
end

----------------------------------------------------------------------------

function sqDB.getAdditionalClinicianDetails(facId, accNum)
   
   -- get the custom ids
   
   local res = sqDB.execute{sql="select id from ewaiting_custom_fields where facility_id = "..
      conn:quote(facId).." and field_name = 'Requesting Clinician';", live=true}

   if #res == 0 then
      error("Requesting Clinician not mapped on ewaiting_custom_fields for facility "..facId)
   end

   local requestingClinID = res[1]:S()
   
   res = sqDB.execute{sql="select id from ewaiting_custom_fields where facility_id = "..
      conn:quote(facId).." and field_name = 'Responsible Clinician';", live=true}

   if #res == 0 then
      error("Responsible Clinician not mapped on ewaiting_custom_fields for facility "..facId)
   end

   local responsibleClinID = res[1]:S()
   
   -- get the referral id
   
   res = sqDB.execute{sql="select ewaiting_referral_id from ewaiting_referral_orders "..
         " where accession_number = "..conn:quote(accNum).." limit 1", live=true}
   
   local refId = res[1].ewaiting_referral_id:S()
   
   -- pull out the requesting clinician
   
   res = sqDB.execute{sql="select value_str from ewaiting_referral_custom_field_values where referral_id = "..
         conn:quote(refId).." and custom_field_id = "..requestingClinID, live=true}
   
   local requestingClinician = res[1].value_str:S()
   
   -- pull out the responsible clinician
   
   res = sqDB.execute{sql="select value_str from ewaiting_referral_custom_field_values where referral_id = "..
         conn:quote(refId).." and custom_field_id = "..responsibleClinID, live=true}
   
   local responsibleClinician = res[1].value_str:S()
   
   return requestingClinician, responsibleClinician
   
end

----------------------------------------------------------------------------

function sqDB.saveAdditionalClinicianDetails(facId, refId, requestingClinician, responsibleClinician)
   
   sqUtils.printLog("Adding additional clinician details...")
   
   if requestingClinician:trimWS() ~= '' then

      local res = sqDB.execute{sql="select id from ewaiting_custom_fields where facility_id = "..
         conn:quote(facId).." and field_name = 'Requesting Clinician';", live=true}

      if #res == 0 then
         error("Requesting Clinician not mapped on ewaiting_custom_fields for facility "..facId)
      end

      local customId = res[1]:S()

      res = sqDB.execute{sql="select id from ewaiting_referral_custom_field_values where referral_id = "..
         conn:quote(refId).." and custom_field_id = "..customId, live=true}

      if #res == 0 then

         sqDB.execute{sql='insert into ewaiting_referral_custom_field_values (referral_id,value,value_str,custom_field_id) '..
            ' values ('..refId..', """'..requestingClinician..
            '""", "'..requestingClinician..'",'..customId..') ', live=false}
      else

         local id = res[1]:S()
         res = sqDB.execute{sql='update ewaiting_referral_custom_field_values '..
            ' SET value = """'..requestingClinician..
            '""" , value_str = "'..requestingClinician..'" where id = '..id, live=false}
      end   

   end
   
   if responsibleClinician:trimWS() ~= '' then

      local res = sqDB.execute{sql="select id from ewaiting_custom_fields where facility_id = "..
         conn:quote(facId).." and field_name = 'Responsible Clinician';", live=true}

      if #res == 0 then
         error("Responsible Clinician not mapped on ewaiting_custom_fields for facility "..facId)
      end

      local customId = res[1]:S()

      res = sqDB.execute{sql="select id from ewaiting_referral_custom_field_values where referral_id = "..
         conn:quote(refId).." and custom_field_id = "..customId, live=true}

      if #res == 0 then

         sqDB.execute{sql='insert into ewaiting_referral_custom_field_values (referral_id,value,value_str,custom_field_id) '..
            ' values ('..refId..', """'..responsibleClinician..
            '""", "'..responsibleClinician..'",'..customId..') ', live=false}
      else

         local id = res[1]:S()
         res = sqDB.execute{sql='update ewaiting_referral_custom_field_values '..
            ' SET value = """'..responsibleClinician..
            '""" , value_str = "'..responsibleClinician..'" where id = '..id, live=false}
      end   

   end

end

----------------------------------------------------------------------------

function sqDB.getFacilityCondition(facilityID)

   sqUtils.printLog("Calling sqDB.getFacilityCondition...")

   --[[
   
   PB: 11.12.2024
   
   NOTE: This query will change, at the moment RAD is only usng X-Ray but DB is set up with multiple
   conditions for the facility. Messaging will dictate the condition in future version and 
   hardcoding will be removed
   
   ]]
   
   local res = sqDB.execute{sql="select facility_id, condition_tag_id, ect.name, ect.status "..
      " from ewaiting_facility_condition_tags efct join ewaiting_condition_tags ect on efct.condition_tag_id = ect.id "..
  		"where facility_id = "..facilityID.." and ect.name = 'X-Ray' order by condition_tag_id desc limit 1", live=true}
   

   if #res == 0 then return nil end

   return res[1].condition_tag_id:S()
end

----------------------------------------------------------------------------

function sqDB.getFacilitySpecialty(facilityID)
   
   sqUtils.printLog("Calling sqDB.getFacilitySpecialty...")

   local res = sqDB.execute{sql=
      "select speciality_id from facility_specialities "..
      " where facility_id = "..conn:quote(facilityID).." order by id desc limit 1", live=true}

   if #res == 0 then return nil end

   return res[1].speciality_id:S()
end

----------------------------------------------------------------------------

function sqDB.getFacilityCategory(facilityID)
   
   sqUtils.printLog("Calling sqDB.getFacilityCategory...")
   
   --[[
   
   PB: 11.12.2024
   
   NOTE: This query will change, at the moment RAD is only usng Routine but DB is set up with multiple
   categories for the facility. Messaging will dictate the condition in future version 
   and hardcoding will be removed
   
   ]]

   local res = sqDB.execute{sql=
      "select id from ewaiting_categories where name ='Routine' "..
      " and facility_id = "..conn:quote(facilityID).." order by id desc limit 1", live=true}

   if #res == 0 then return nil end

   return res[1].id:S()
end

----------------------------------------------------------------------------

function sqDB.getFacilitySource(facilityID)
   
   sqUtils.printLog("Calling sqDB.getFacilitySource...")

   local res = sqDB.execute{sql=
      "select referral_source_id from ewaiting_facility_referral_sources "..
      " where facility_id = "..conn:quote(facilityID).." order by referral_source_id desc limit 1", live=true}

   if #res == 0 then return nil end

   return res[1].referral_source_id:S()
end

----------------------------------------------------------------------------

function sqDB.executeBlock(func, blockName)
   
   sqUtils.printLog(blockName.." : Begin DB trans...")
   conn:begin{live=true}

   local Success, Error = pcall(func)

   if Success then

      sqUtils.printLog(blockName.." : Committing DB transactions...")
      conn:commit{live=true}

   else

      sqUtils.printLog(blockName.." : DB activity failed, Rolling back transactions...")
      conn:rollback{live=true}

      error(Error)

   end  

end

----------------------------------------------------------------------------

function sqDB.getFormTemplateUUID(dynamic_forms_responses_id)
   local uuidResult = '0'
   
   local res = sqDB.execute{sql=
      "select count(id) as cnt,form_id from dynamic_forms_responses where uuid = "..conn:quote(dynamic_forms_responses_id), live=true}
   
   if res[1].cnt:S() ~= '0' then
      formid = res[1].form_id:S()
      
      res = sqDB.execute{sql=
      "select count(id) as cnt,uuid from facility_dynamic_forms where id = "..formid, live=true}
      
      if res[1].cnt:S() ~= '0' then
         uuidResult = res[1].uuid:S()
      end

   end   
      
   return uuidResult      
end

----------------------------------------------------------------------------

function sqDB.getReferralPriorityCode(facilityID, categoryID)
   local refPriority = sqDB.execute{sql=
      "select priority from ewaiting_categories where facility_id = " ..
      facilityID .. " and id = ".. categoryID, live=true}
   return refPriority[1].priority
end

----------------------------------------------------------------------------

function sqDB.unEncodeString(str)

   str = str:gsub("\\\\F\\\\", "/")
   str = str:gsub("\\\\T\\\\", "&")
   str = str:gsub("\\\\R\\\\", "~")
   str = str:gsub("\\\\S\\\\", "^")
   str = str:gsub("\\\\E\\\\","\\")

   return str

end

----------------------------------------------------------------------------

function sqDB.getFacilitiesByParent(conn, parent)
   sqUtils.printLog("Calling sqDB.getFacilitiesByParent...")
   
   local res = sqDB.execute{sql=
      "select id from facilities \
      where parent_id = "..conn:quote(parent).." or id ="
      ..conn:quote(parent), live=true}

   if #res == 0 then return nil end
   
   return res
end

----------------------------------------------------------------------------

function sqDB.getPatientIdByMrn(mrn)

   local facilityID  = sqCfg.parent_facility
   
   if mrn == nil or mrn == '' then return '0' end
   if facilityID == nil or facilityID == '' then return '0' end
   
   sqUtils.printLog("Calling sqDB.getPatientIdByMrn...")
   
   local PatientId = sqDB.execute{sql=
      "select count(id), id from users where mrn = " .. conn:quote(mrn) .. 
      " and mrn <> '' and id in (select user_id from facility_user where facility_id in ("..facilityID.."))", 
      live=true}

   if PatientId[1]['count(id)']:S() == '0' then
      return '0'
   else
      return PatientId[1].id:S()
   end

end

----------------------------------------------------------------------------

function sqDB.fillPatientData(PID)
   sqUtils.printLog("Calling sqDB.fillPatientData...")
   
   sqDB.patientData.mrn = PID[3][1][1]:S()
   sqDB.patientData.localmrn = PID[3][2][1]:S()
   sqDB.patientData.firstname = PID[5][2]:nodeValue()
   sqDB.patientData.surname = PID[5][1]:nodeValue()
   sqDB.patientData.user_type = "N"
   sqDB.patientData.user_status = 'active'
   sqDB.patientData.dob = sqUtils.parseDOB(PID[7])
   sqDB.patientData.gender = PID[8]:nodeValue()   
   sqDB.patientData.home_phone = ''
   sqDB.patientData.mobile = ''
   -- jlui2 30/08/2023
   -- sqDB.patientData.email = sqCfg.canc_email_add  
   sqDB.patientData.email = sqUtils.getPhoneNumberByType(PID,'L08')
  
   sqDB.patientData.home_phone = sqUtils.getPhoneNumberByType(PID,'L14')
   sqDB.patientData.mobile = sqUtils.getPhoneNumberByType(PID,'SMS')
   if sqDB.patientData.mobile == nil or sqDB.patientData.mobile == '' then
      sqDB.patientData.mobile = sqUtils.getPhoneNumberByType(PID,'L15')
   end
   
   --internationalize the phone numbers
   sqDB.patientData.mobileint = sqDB.patientData.mobile
   if sqDB.patientData.mobile:sub(1,2) == '08' then
      sqDB.patientData.mobileint = '353' .. sqDB.patientData.mobile:sub(2)
   end
   
   --if the international mobile is still blanck then try use the parent mobile
   if sqDB.patientData.mobileint == '' then
      if sqDB.patientData.parent_mobile ~= '' then
         if sqDB.patientData.parent_mobile:sub(1,2) == '08' then
            sqDB.patientData.mobileint = '353' .. sqDB.patientData.parent_mobile:sub(2)
         end
      end
   end
   
   sqDB.patientData.address1 = PID[11][1][1]:nodeValue() 
   sqDB.patientData.address2 = PID[11][1][2]:nodeValue()
   sqDB.patientData.address3 = PID[11][1][3]:nodeValue()
   sqDB.patientData.address4 = PID[11][1][4]:nodeValue()
   sqDB.patientData.address5 = PID[11][1][5]:nodeValue()
   
   sqDB.patientData.email = PID[11][2][1]:nodeValue()
  
   local t = PID[5][5]:nodeValue()
   --get the mapped title from config
   sqDB.patientData.title = sqUtils.getMappedValue(sqCfg.title_map,t)
   if sqDB.patientData.title == '' then
      sqDB.patientData.title = t:sub(1,1):upper()..t:sub(2, 2+t:len()-2):lower()
   end
end

----------------------------------------------------------------------------

function sqDB.createPatient(PID)
   sqUtils.printLog("Calling sqDB.createPatient...")
   
   --fill the patient demographic variables, used here and update patient
   sqDB.fillPatientData(PID)
   
   sqUtils.printLog("Creating patient mrn = " .. sqDB.patientData.mrn)
    
   sqDB.execute{sql="insert into users (mobile, home_phone, email, mrn, \
      firstname, surname, title, user_type, status, Date_of_Birth, Gender, \
      Address_1, Address_2, Address_3, Address_4, Home_Address, created) " ..
      "values (trim(replace("..
      conn:quote(sqDB.patientData.mobileint)..", ' ', '')), "..
      conn:quote(sqDB.patientData.home_phone)..","..
      conn:quote(sqDB.patientData.email)..","..
      conn:quote(sqDB.patientData.mrn)..","..
      conn:quote(sqDB.patientData.firstname)..","..
      conn:quote(sqDB.patientData.surname)..","..
      conn:quote(sqDB.patientData.title)..","..
      conn:quote(sqDB.patientData.user_type)..","..
      conn:quote(sqDB.patientData.user_status)..","..
      conn:quote(sqDB.patientData.dob)..","..
      conn:quote(sqDB.patientData.gender)..","..
      conn:quote(sqDB.patientData.address1)..","..
      conn:quote(sqDB.patientData.address2)..","..
      conn:quote(sqDB.patientData.address3)..","..
      conn:quote(sqDB.patientData.address4)..","..
      conn:quote(sqDB.patientData.address5)..","..
      "now()"..
      ")"
      , live=false}

   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}

   return result[1].id:S()

end

----------------------------------------------------------------------------

function sqDB.updatePatient(id, PID)
   
   if id == nil or id == '' or id == '0' then return end
   
   sqUtils.printLog("Calling sqDB.updatePatient...")
   
   --fill the patient demographic variables, used here and update patient
   sqDB.fillPatientData(PID)
   
   sqUtils.printLog("Updating patient mrn = " .. sqDB.patientData.mrn)

   local sql ="update users " ..
   "set "..
   "mrn = " .. conn:quote(sqDB.patientData.mrn) .. ", "..
   "firstname = " .. conn:quote(sqDB.patientData.firstname) .. ", "..
   "surname = " .. conn:quote(sqDB.patientData.surname) .. ", "..
   "title = " .. conn:quote(sqDB.patientData.title) .. ", "..
   "Date_of_Birth = " .. conn:quote(sqDB.patientData.dob) .. ", "..
   "mobile = trim(replace(" .. conn:quote(sqDB.patientData.mobileint) .. ", ' ', '')), "..
   "home_phone = " .. conn:quote(sqDB.patientData.home_phone) .. ", "..
   "email = " .. conn:quote(sqDB.patientData.email) .. ", "..
   "Gender = " .. conn:quote(sqDB.patientData.gender) .. ", "..
   "Address_1 = " .. conn:quote(sqDB.patientData.address1) .. ", "..
   "Address_2 = " .. conn:quote(sqDB.patientData.address2) .. ", "..
   "Address_3 = " .. conn:quote(sqDB.patientData.address3) .. ", "..
   "Address_4 = " .. conn:quote(sqDB.patientData.address4) .. ", "..
   "Home_Address = " .. conn:quote(sqDB.patientData.address5) .. ", "..
   "modified = now() " ..
   "where id = ".. id

   sqDB.execute{sql=sql, live=false}

end

----------------------------------------------------------------------------

function sqDB.mergePatient(id, minorid, facilityID)
   sqUtils.printLog("Calling sqDB.mergePatient...")
   
   -- DD Addedd 150123
   -- Desc: Added in the correct functionality for merging patients
   -- We need to merge referrals, reservations and patient flow
   
   -- Validate that major and minor patients exist and 
   -- patients have facility user records for this facility
   sqUtils.printLog("perform merge variable check")
   local isMajorPatientValid = false
   if (id ~= '0') then
      trace(id)
      local facilityUserCheck = sqDB.execute{sql=
         "select count(id), id from users " ..
         "where id = " .. conn:quote(id) .. 
         " and id in (select user_id from facility_user where facility_id in (" ..facilityID.."))", live=true}
      if facilityUserCheck[1]['count(id)']:S() == '0' then
         error("Unable to process merge request, major number not found")
      else
         isMajorPatientValid = true
         trace(isMajorPatientValid)
      end
   else
      error("Unable to process merge request, major number = 0")
   end
   local isMinorPatientValid = false
   if (minorid ~= '0') then
      
      local facilityUserCheck = sqDB.execute{sql=
         "select count(id), id from users " ..
         "where id = " .. conn:quote(minorid) .. 
         " and id in (select user_id from facility_user where facility_id in (" ..facilityID.."))", live=true}
      if facilityUserCheck[1]['count(id)']:S() == '0' then
         error("Unable to process merge request, minor number not found")
      else
         isMinorPatientValid = true
         trace(isMinorPatientValid)
      end
   else
      trace(minorid)
      error("Unable to process merge request, minor number = 0")
   end

   --Referrals First
   local MergeRefs = sqDB.execute{sql=
      "select count(id), id from ewaiting_referrals where user_id = " .. minorid,live=true}
trace(minorid)
   if MergeRefs[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergeRefIDs = sqDB.execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as referralIds from ewaiting_referrals where user_id = " .. minorid,live=true}

      if MergeRefIDs[1]['referralIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, "..
         id ..", "..
         "'{"..sqCfg.merge_site_code.." HL7 Merge - Minor ID: " .. minorid .. " Major ID: " .. id ..
         "ReferralIDs Merged: " .. MergeRefIDs[1]['referralIds'] ..
         "}')"

         iguana.logDebug("mergePatient().Insert merged user details: " .. sql)

         sqDB.execute{sql=sql, live=false}
      end

      local sql ="update ewaiting_referrals set user_id = " ..id..
      " where facility_id = " .. conn:quote(facilityID) ..
      " and user_id = " .. minorid

      iguana.logDebug(" mergePatient(). Update ewaiting_referrals: " .. sql)

      sqDB.execute{sql=sql, live=false} 
   end

   --Reservations Second
   local MergeRes = sqDB.execute{sql=
      "select count(id), id from reservation where user_id = " .. minorid,live=true}

   if MergeRes[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergeResIDs = sqDB.execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as reservationIds from reservation " ..
         "where user_id = " .. minorid,live=true}

      if MergeResIDs[1]['reservationIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, ".. id ..", "..
         "'{"..sqCfg.merge_site_code.." HL7 Merge - Minor ID: [" .. minorid .. "] Major ID: [" .. id ..
         "] ReservationIDs Merged: [" .. MergeResIDs[1]['reservationIds'] ..
         "]}')"
         
         iguana.logDebug("sql merged_users: " .. sql)

         sqDB.execute{sql=sql, live=false}
      end

      local sql ="update reservation set user_id = " ..id..
      " where facility_id in " ..
      "(select id from facilities " ..
      "where id = " .. conn:quote(facilityID) ..
      " or parent_id = " .. conn:quote(facilityID) .. ")" ..
      " and user_id = " .. minorid
      
      iguana.logDebug("sql update reservation: " .. sql)

      sqDB.execute{sql=sql, live=false}
   end

   --Patient Flow Third
   local MergePatFlow = sqDB.execute{sql=
      "select count(id), id from patient_flow where user_id = " .. minorid,live=true}
   
   --iguana.logDebug("MergePatFlow: " .. MergePatFlow)

   if MergePatFlow[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergePatFlowIDs = sqDB.execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as patflowIds from patient_flow where user_id = " .. minorid,live=true}     

      if MergePatFlowIDs[1]['patflowIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, "..
         id ..", "..
         "'{"..sqCfg.merge_site_code.." HL7 Merge - Minor ID: [" .. minorid .. "] Major ID: [" .. id ..
         "] PatientFlowIDs Merged: [" .. MergePatFlowIDs[1]['patflowIds'] ..
         "]}')"
         
         iguana.logDebug("sql insert merged_users: " .. sql)

         sqDB.execute{sql=sql, live=false}
      end

      local sql ="update patient_flow set user_id = " ..id..
      " where facility_id in " ..
      "(select id from facilities " ..
      "where id = " .. conn:quote(facilityID) ..
      " or parent_id = " .. conn:quote(facilityID) .. ")"..
      " and user_id = " .. minorid
      
      iguana.logDebug("sql update: " .. sql)

      sqDB.execute{sql=sql, live=false}
   end

   -- Remove the minor one from thw Facility User view
   local sql ="delete from facility_user " ..
   "where "..
   "facility_id = " .. conn:quote(facilityID) ..
   " and user_id = " .. minorid
   
   iguana.logDebug("sql: " .. sql)

   sqDB.execute{sql=sql, live=false}

end

----------------------------------------------------------------------------

function sqDB.userInFacility(patientId, facilityId)
   local result = sqDB.execute{sql="select id from facility_user where "..
   " user_id = " .. patientId .. " "..
   " and facility_id = " .. facilityId .. ";"
   , live=true}
   if #result == 0 then return '' end
   return result[1].id:nodeValue()
end

----------------------------------------------------------------------------

function sqDB.addToFacility(patientId, facilityId)
   sqUtils.printLog("Calling sqDB.addToFacility...")
   
	if sqDB.userInFacility(patientId, facilityId) == ''
   then
	   sqDB.execute{sql="insert into facility_user (user_id, facility_id) values ("..patientId..", "..facilityId..")", live=false}
   end
end

----------------------------------------------------------------------------

function sqDB.getPatientReferral(id,facilityID)
   sqUtils.printLog("Calling sqDB.getPatientReferral...")

   local referralCount = sqDB.execute{sql=
      "select count(id), id from ewaiting_referrals where facility_id = " .. facilityID .. 
      " and status in ('active', 'pre_triage', 'triage') and user_id = " .. id, live=true}

   if referralCount[1]['count(id)']:S() == '0' then
      return nil
   else
      return referralCount[1].id:S()
   end

end

----------------------------------------------------------------------------

function sqDB.updateReferralStatus(referralID, opdAppointment, status)
	
   if referralID == nil or referralID == '' or referralID == '0' then return end
   
   sqUtils.printLog("Calling sqDB.updateReferralStatus...")
   
   local sql ="update ewaiting_referrals " ..
   "set "..
   "status = '"..status.."' " ..
   "where id = ".. referralID

   sqDB.execute{sql=sql, live=false}
end

----------------------------------------------------------------------------

--[[ 
   Function:  sqDB.referralExists        

   Purpose:   This function is responsible for returning the referral id
              from ewaiting_referrals that match the input parameters criteria.

              (See also:  sqDB.referralExistsMatchEpisode 
                          sqDB.ordersMatchEpisode(episodeNumber) )

   Parameters: 
           facilityId, patientId, episodeNumber
          
   Returns: Two parameters:
            false, 0  - When no referral exists
            true, referral ID - When referral exists that match criteria
   
--]]
function sqDB.referralExists(facilityId, patientId, episodeNumber)
   sqUtils.printLog("Calling sqDB.referralExists...")
   
   local referralCount = sqDB.execute{sql=
      "select count(id), id from ewaiting_referrals " ..
      "where facility_id = " .. facilityId .. 
      " and user_id = " .. patientId .. 
      " and id in " ..
      "(select ewaiting_referral_id from ewaiting_referral_orders " ..
      "where episode_number = " .. conn:quote(episodeNumber) .. ")",
      live=true}

   if referralCount[1]['count(id)']:S() == '0' then
      return false, '0'
   end
   
   return true, referralCount[1]['id']:S()
   
end

--[[ 
   Function:  sqDB.referralExistsMatchEpisode         

   Purpose:   This function is responsible for returning a list of referrals
              that match the given episodeNumber.
              Note: There may be multiple different referrals that match,
              so a list has to be returned.

              (See also:  sqDB.referralExists
                          sqDB.ordersMatchEpisode(episodeNumber) )

   Parameters: 
           facilityId, patientId, episodeNumber
          
   Returns: Two parameters:
            false, 0  - When no referral exists
            true, referralList - When referral exists that match episode number
   
--]]
function sqDB.referralExistsMatchEpisode(facilityId, patientId, episodeNumber)
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
      
   local referralList = sqDB.execute{sql=
      "select * from ewaiting_referrals " ..
      "where facility_id = " .. facilityId .. 
      " and user_id = " .. patientId .. 
      " and id in " ..
      "(select ewaiting_referral_id from ewaiting_referral_orders " ..   
      "where episode_number like " .. "'"..episodeNumber.."%".."'" .. ")",
      live=true}
   
   local referralCount = #referralList
   
   if referralCount == 0 then
      -- Referral does not exist
      return false, 0
   else
      return true, referralList
   end
end


--[[ 
   Function:  appointmentsExistMatchEpisode        

   Purpose:   This function is responsible for returning a list of 
              ewaiting_referral_orders that match the given episodeNumber.

              (See also: sqDB.referralExistsMatchEpisode)

   Parameters: episodeNumber

          
   Returns: appointmentExists (boolean),
            ordersList containing the res_id
   
--]]
function sqDB.appointmentsExistMatchEpisode(episodeNumber)
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
      
   local appointmentExists = false
   
   -- Returns true, if matches first part of episode_number, and res_id must be populated.
   -- (SQ app sets the res_id to the reservation identifier when SQ user
   -- allocates the appointment).  

   local appointmentList = sqDB.execute{sql=
      "select * from ewaiting_referral_orders " ..   
      "where episode_number like " .. "'"..episodeNumber.."%".."'",
      live=true}
   
   trace(#appointmentList)
   
   if #appointmentList ~= 0 then
      for i=1, #appointmentList do
          if appointmentList[i]['res_id']:S() ~= 'NULL' then
              -- An appointment exists if the res_id is populated for the given episodeNumber match 
              appointmentExists = true
          end
      end
   else
      appointmentList = nil
   end
      
   return appointmentExists, appointmentList    
end


--[[ 
   Function:  sqDB.ordersMatchEpisode         

   Purpose:   This function is responsible for returning a list of 
              ewaiting_referral_orders that match the given episodeNumber.

              (See also: sqDB.sqDB.referralExistsMatchEpisode)

   Parameters: 
            episodeNumber
          
   Returns: ewaiting_referral_orders list
   
--]]
function sqDB.ordersMatchEpisode(episodeNumber)
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
      
   local ordersList = sqDB.execute{sql=
      "select * from ewaiting_referral_orders " ..   
      "where episode_number like " .. "'"..episodeNumber.."%".."'",
      live=true}
   
   trace(#ordersList)
      
   return ordersList    
end

----------------------------------------------------------------------------
----------------------------------------------------------------------------
function sqDB.createPatientReferral(patientId, facilityID, orderId, examCode, examDesc, episodeNumber, 
               accessionNumber, refSourceName, PatientType, status, conditionType, specialty, refSourceId, requestedDate, 
               locationType, requestDetails, contactNum, notes, notesHTML, categoryID)
   
   sqUtils.printLog("Calling sqDB.createPatientReferral...")
   
   local ret = '0'

   -- jlui2 20/11/2023.  Use categoryId parameter instead of facilityCategory
   --local ref_params = sqCfg.referral_params  
   --local facilityCategory = sqUtils.getMappedValue(ref_params, "facilityCategory")
   
   --PB 19.02.24 : never file the default value
   if conditionType == sqUtils.getMappedValue(sqCfg.conditiontype_map, "default") then
      conditionType = ''
   end
      
   local referralCount = sqDB.execute{sql=
      "select count(id), id from ewaiting_referrals " ..
      "where facility_id = " .. facilityID .. 
      " and user_id = " .. patientId .. 
      " and id in " ..
      "(select ewaiting_referral_id from ewaiting_referral_orders " ..
      "where episode_number = " .. conn:quote(episodeNumber) .. ")",
      live=true}

   if referralCount[1]['count(id)']:S() == '0' then
      
      --PB 12.02.24
       local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")

      --PB 24.04.2024 : removed the filing of condition : IR-260
      sqDB.execute{sql=
         "insert into ewaiting_referrals (facility_id, user_id, date_referred, category_id, speciality_id, " ..
         "referral_source_id, referral_source_name, status, comments, created, created_by, " ..
         "pas_verified, patient_type) " ..
         "values ("..
         facilityID..","..
         patientId..","..  
         requestedDate..","..  
         categoryID..","..
         specialty..","..
         refSourceId..","..
         conn:quote(refSourceName)..","..
         conn:quote(status)..","..
         --conn:quote(conditionType)..","..
         --"'HL7 Referral'"..","..
         conn:quote(notes)..","..
         "now()"..","..
         conn:quote(sqUser).. "," ..
         "0" .."," ..
         conn:quote(PatientType) ..
         ")"
         , live=false}

      local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
      ret = result[1].id:S()

      --PB 16.02.24 : only write the condition if the condition type is not a default value
      trace(conditionType)
      if conditionType ~= '' and conditionType ~= nil then

         sqUtils.printLog("Inserting "..tostring(conditionType).." into ewaiting_referral_condition_tags for refid "..tostring(result[1].id))
         sqDB.execute{sql=
            "insert into ewaiting_referral_condition_tags " ..
            "values (" .. result[1].id .."," .. conditionType .. ")", 
            live=false}
      end
      
      --Insert referral condition notes
      if notesHTML ~= '' then
         
         sqDB.execute{sql=
            "insert into ewaiting_referrals_condition_notes (ewaiting_referral_id, sq_user_id, condition_note ) " ..
            "values (" .. result[1].id ..", "..conn:quote(sqUser)..","..conn:quote(notesHTML)..")", 
            live=false}
      end

      sqDB.execute{sql=
         "insert into ewaiting_referral_orders " ..
         "(id,ewaiting_referral_id,order_id,episode_number,exam_code,exam_name,accession_number, created_at, cancellation_code) " ..
         "values (null," .. 
         result[1].id .."," .. 
         conn:quote(orderId) .. "," .. 
         conn:quote(episodeNumber) .. "," .. 
         conn:quote(examCode) .. "," .. 
         conn:quote(examDesc) .. "," ..
         conn:quote(accessionNumber) .. "," .. 
         "now(), '')", 
         live=false}

   else
      
      --check if the order already exists
      local orderCount = sqDB.execute{
         sql="select count(id), order_id from ewaiting_referral_orders where order_id = " .. conn:quote(orderId), live=true}

      if orderCount[1]['count(id)']:S() == '0' then
         sqDB.execute{sql=
            "insert into ewaiting_referral_orders " ..
            "(id,ewaiting_referral_id,order_id,episode_number,exam_code,exam_name,accession_number, created_at, cancellation_code) " ..
            "values (null," .. 
            referralCount[1].id:S() .."," .. 
            conn:quote(orderId) .. "," .. 
            conn:quote(episodeNumber) .. "," .. 
            conn:quote(examCode) .. "," .. 
            conn:quote(examDesc) .. "," ..
            conn:quote(accessionNumber) .. "," .. 
            "now(),'')", live=false}      
      end
      
   end
   
   return ret
   
end

----------------------------------------------------------------------------
function sqDB.updatePatientReferral(referralId, patientId, facilityID, orderId, examCode, examDesc, episodeNumber, 
               accessionNumber, refSourceName, PatientType, status, conditionType, specialty, refSourceId, requestedDate, 
               locationType, requestDetails, contactNum, notes, notesHTML, categoryID)
   
   sqUtils.printLog("Calling sqDB.updatePatientReferral...")
   
   --PB 19.02.24 : never file the default value
   if conditionType == sqUtils.getMappedValue(sqCfg.conditiontype_map, "default") then
      conditionType = ''
   end
  
   --PB 12.02.24
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")

   --PB 24.04.2024 : removed the filing of condition : IR-260
   sqDB.execute{sql=
      "update ewaiting_referrals "..
      " set "..
      " date_referred = "..requestedDate..","..  
      " category_id = "..categoryID..","..
      " speciality_id = "..specialty..","..
      " referral_source_id = "..refSourceId..","..
      " referral_source_name = "..conn:quote(refSourceName)..","..
      " status = "..conn:quote(status)..","..
      --" `condition` = "..conn:quote(conditionType)..","..
      " comments = "..conn:quote(notes)..","..
      " modified = now()"..","..
      " modified_by = "..conn:quote(sqUser).. "," ..
      " patient_type = "..conn:quote(PatientType) ..
      " where id = " .. referralId
      , live=false}
   
   --update condition tags
   
   trace(conditionType)
   if conditionType ~= '' and conditionType ~= nil then
      sqDB.execute{sql=
         "update ewaiting_referral_condition_tags " ..
         " set "..
         " condition_tag_id = "..conditionType..
         " where referral_id = "..referralId , 
         live=false}
   end

   --update referral condition notes
  
   if notesHTML ~= '' then

      sqDB.execute{sql=
         "update ewaiting_referrals_condition_notes set "..
         " condition_note = "..conn:quote(notesHTML)..","..
         " updated_at = now() "..
         " where ewaiting_referral_id = "..referralId, 
         live=false}
   end
   
   -- update the order
   --[[
   sqDB.execute{sql=
         "update ewaiting_referral_orders " ..
         " set " ..
         " exam_code = "..conn:quote(examCode) .. "," .. 
         " exam_name = "..conn:quote(examDesc) .. "," ..
         " updated_at = now(), "..
         " where ewaiting_referral_id = "..referralId , 
         live=false}
   ]]

end

----------------------------------------------------------------------------

function sqDB.validateAppointmentDates(facilityID, hl7StartDateTime, hl7EndDateTime, hl7Duration, reasonID)
   
   --check for empty start date
   if sqUtils.isEmpty(hl7StartDateTime) then
      sqUtils.printLogW("HL7 message did not contain an appointment start date")
      return false
   end
   
   local tmpDateTime = string.sub(hl7StartDateTime,1,4).."-"..string.sub(hl7StartDateTime,5,6).."-"..string.sub(hl7StartDateTime,7,8)..
      " "..string.sub(hl7StartDateTime,9,10)..":"..string.sub(hl7StartDateTime,11,12)
   local appStartDate = string.sub(hl7StartDateTime,1,4).."-"..string.sub(hl7StartDateTime,5,6).."-"..string.sub(hl7StartDateTime,7,8).." 00:00:00"
   local appStartTime = string.sub(hl7StartDateTime,9,10)..":"..string.sub(hl7StartDateTime,11,12)
   
   --check for invalid date
   if not sqUtils.isValidDate(string.sub(hl7StartDateTime,1,4).."-"..string.sub(hl7StartDateTime,5,6).."-"..string.sub(hl7StartDateTime,7,8)) then
      sqUtils.printLogW("HL7 message contained an invalid appointment start date")
      return false
   end
   
   --check for invalid start time
   if not sqUtils.isValidTime(string.sub(hl7StartDateTime,9,10)..":"..string.sub(hl7StartDateTime,11,12)) then
      sqUtils.printLogW("HL7 message contained an invalid appointment start time")
      return false
   end
   
   local appEndTime = ''
   
   if not sqUtils.isEmpty(hl7EndDateTime) then

      --check for invalid end time
      if not sqUtils.isValidTime(string.sub(hl7EndDateTime,9,10)..":"..string.sub(hl7EndDateTime,11,12)) then
         sqUtils.printLogW("HL7 message contained an invalid appointment end time")
         return false
      end
      
      appEndTime = string.sub(hl7EndDateTime,9,10)..":"..string.sub(hl7EndDateTime,11,12)
      
      --return the details
      return true, appStartDate, appStartTime, appEndTime
   
   end
   
   --If there is no appointment end date/time then see if we got a duration
   if not sqUtils.isEmpty(hl7Duration) then
      
      local newDateTime = sqUtils.addMinutes(tmpDateTime, tonumber(hl7Duration))
      appEndTime = string.sub(newDateTime,12,16)
      
      --return the details
      return true, appStartDate, appStartTime, appEndTime
   end
   
   --No end time or duration, so get the duration for the facility based on reason id
   if reasonID ~= '' then
      local duration = sqDB.getApptDuration(facilityID, reasonID)

      if not sqUtils.isEmpty(duration) then

         local newDateTime = sqUtils.addMinutes(tmpDateTime, tonumber(duration))
         appEndTime = string.sub(newDateTime,12,16)

         --return the details
         return true, appStartDate, appStartTime, appEndTime
      end
   end
   
   --if all else fails, use the default in the config settings
   duration = sqUtils.getMappedValue(sqCfg.extra_params,"defaultAppDuration")
   local newDateTime = sqUtils.addMinutes(tmpDateTime, tonumber(duration))
   appEndTime = string.sub(newDateTime,12,16)

   trace(appStartDate, appStartTime, appEndTime)

   return true, appStartDate, appStartTime, appEndTime
end

----------------------------------------------------------------------------

function sqDB.createAppointment(id, facilityID, SCH, clinCode1, reasonID, PV2, PV1, NTE)
   sqUtils.printLog("Calling sqDB.createAppointment...")
   
   -- Related to SQBUGS-718: Custom Appointment Duration, based on Appt Reason and Facility ID 
   local continue, appStartDate, appStartTime, appEndTime = sqDB.validateAppointmentDates(facilityID, SCH[11][4]:S(), SCH[11][5]:S(), SCH[9]:S(), reasonID )
   
	trace(continue, appStartDate, appStartTime, appEndTime)
   if not continue then
      sqUtils.printLogW("Cannot determine appointment start and end time, skipping appointment creation...")
      return
   end   
   
   local retResId = '0'
   
   local opdAppointmentString = string.sub(tostring(appStartDate),1,10)
       
   --get the DXC identifier
   local DXCId = SCH[1][1]:S()
   local apptType = PV2[12]:S()
   local apptPriority = PV2[25]:S()
   local apptCategory = PV1[18]:S()
   local appReason = 1
   local apptNotes = NTE[3]:S()  -- Jira IR-256 Appt Comments
   
   --PB 18.10.23: fix for the replacement of nulls with 0000-00-00
   --[[ jlui2 16/07/2024 comment out
   if opdAppointmentString == nil or opdAppointmentString == '' then 
      iguana.logWarning("Invalid request date in SCH[11][4]")
      opdAppointmentString = '0000-00-00' 
   end
   ]]
   
   --Check if an appointment already exists for this patient
   --PB 03.10.23: Modified quesry to include the facility ID when checking if the
   --             appointment exists. Sometimes a user can have an appointment for the
   --             same date and time but under a different facility
   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. 
      " and reservation_date = " .. conn:quote(opdAppointmentString) .. 
      " and status = 'active' and reservation_time = " ..  conn:quote(appStartTime) ..
      " and facility_id = "..facilityID, live=true}

   if ResId[1]['count(id)']:S() == '0' then
      
      --Debug:    "substring(convert("..conn:quote(opdAppointmentEndTimeString)..", time) + interval "..appointmentDuration.." minute, 1, 5)"..","..

      --insert the new appointment
      sqDB.execute{sql="insert into reservation (facility_id, user_id, facility_type, consultant_id, location_pref, " ..
         "reservation_date, reservation_time, reservation_end_time, google_cal_id, reason_id,recurrence,date,status," ..
         "attended_by, is_attended,booking_user_id, source, print,stats, quality,capacity_sync, is_urgent, comments) " .. 
         "values ("..
         facilityID..","..
         id..","..
         "'f',0"..","..
         conn:quote(DXCId).."," ..
         conn:quote(opdAppointmentString)..","..
         conn:quote(appStartTime)..","..
         conn:quote(appEndTime)..","..
         conn:quote(clinCode1)..","..
         reasonID..","..
         "'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0"..","..
         conn:quote(apptNotes)..
         ")"
         , live=false}

      local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
      
      retResId = result[1].id:S() -- return the res id later

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'TYPE'"..","..
         conn:quote(apptType)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'PRIORITY'"..","..
         conn:quote(apptPriority)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'CATEGORY'"..","..
         conn:quote(apptCategory)..
         ")"
         , live=false}

   else
      
      -- Jira IR-340.  sqDB.updateAccessionNumber(appId) has a dependency on the sqDB.getSQIdsByIPMAppId.
      -- The latter function requires the location_pref (here) set to the iPM Appointment ID.
      -- So set live = true to execute SQL interactively in the Iguana script editor 
      -- Jira IR-345 The 'source' is still being set as H which means HL7 (=via the ipm interface).  
      -- Change this to 'modified_source' 
      sqDB.execute{sql="update reservation set location_pref = " ..conn:quote(DXCId)..
         ", google_cal_id = " ..conn:quote(clinCode1).. 
         ", modified_source = 'h' where id = "..ResId[1].id, live=false}   
      
      -- check if the reservation_Ipm records exist - if not create them
      local IPMResId = sqDB.execute{sql=
         "select count(res_id), res_id from reservation_ipm where res_id = " .. ResId[1].id, live=true}
      --
      if IPMResId[1]['count(res_id)']:S() == '0' then

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id..","..
            "'TYPE'"..","..
            conn:quote(apptType)..
            ")"
            , live=false}

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id..","..
            "'PRIORITY'"..","..
            conn:quote(apptPriority)..
            ")"
            , live=false}

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id..","..
            "'CATEGORY'"..","..
            conn:quote(apptCategory)..
            ")"
            , live=false}
         --
      end
      --
   end
   
   return retResId
   
end

----------------------------------------------------------------------------

function sqDB.updatePatientFlow(id, facilityID, SCH, clinCode, reasonID, PV1)
   sqUtils.printLog("Calling sqDB.updatePatientFlow...")

   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)

   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   
   local DXCId = SCH[1][1]:S()
   local appReason = 1

   local patient_flow_stage = ''

   local timestamp = os.date( "%Y-%m-%d %X" )

   if not sqUtils.isEmpty(SCH[11][4]:S()) then
      timestamp = tostring(dateparse.parse(SCH[11][4]:S()))
   end

   if SCH[25][1]:S() == 'Attended' then
      patient_flow_stage =  'CHECK-IN'  
   elseif SCH[25][1]:S() == 'Did Not Attend' then
      patient_flow_stage = 'DNA'
   elseif SCH[25][1]:S() == 'CheckedOut' then
      patient_flow_stage = 'COMPLETE'

      -- Checkout time should come from the PV1-45.1.1 Discharge Date/Time field
      if not sqUtils.isEmpty(PV1[45]:S()) then
         timestamp = tostring(dateparse.parse(PV1[45]:S()))
      end      
   end

   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. 
      " and location_pref = " .. conn:quote(DXCId), live=true}

   if ResId[1]['count(id)']:S() ~= '0' then
      --
      local ResId2 = sqDB.execute{sql=
         "select count(id), id from patient_flow where res_id = " .. conn:quote(ResId[1]['id']:S()), live=true}

      if ResId2[1]['count(id)']:S() ~= '0' then

         -- Set current_stage to 'N' for all stages, before adding new stage
         sqDB.execute{sql=
            "update patient_flow " ..
            "set current_stage = 'N' " ..
            "where res_id = " .. conn:quote(ResId[1]['id']:S()) .. 
            " and user_id = " ..id ..
            " and facility_id = " .. facilityID,
            live = false}

         -- It shouldn't matter if previously, the current stage was 'DNA'.  
         -- Above code forces the current stage for that 'DNA' stage to value 'N' 

      end

      --insert the new patient flow
      
       --PB 12.02.24
       local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")

      sqDB.execute{sql="insert into patient_flow (id, timestamp, facility_id, user_id, \
         res_id, sq_user_id, stage, nurse, current_stage, bay, source) " .. 
         "values (" ..
         "null, " ..
         conn:quote(timestamp) .. ", " ..
         facilityID ..", " ..
         id..", " ..
         conn:quote(ResId[1]['id']:S()) ..", " ..
         conn:quote(sqUser)..", " ..
         conn:quote(patient_flow_stage) .."," ..
         "NULL, 'Y', NULL, 'hl7'" .. 
         ")"
         , live=false}         

   end
   --
end

----------------------------------------------------------------------------

function sqDB.deleteUpdatePatientFlow(id, facilityID, SCH, clinCode, reasonID)
   sqUtils.printLog("Calling sqDB.deleteUpdatePatientFlow...")

   local DXCId = SCH[1][1]:S()

   local patient_flow_stage = ''

   if SCH[25][1]:S() == 'CancelAttended' then
      patient_flow_stage =  'CHECK-IN' -- get the previous stage to operate on
   elseif SCH[25][1]:S() == 'CancelCheckedOut' then
      patient_flow_stage = 'COMPLETE'  -- get the previous stage to operate on
   elseif SCH[25][1]:S() == 'Cancel Did Not Attend' then
      patient_flow_stage = 'DNA'  -- get the previous stage to operate on
   end

   local timestamp = os.date( "%Y-%m-%d %X" )

   if not sqUtils.isEmpty(SCH[11][4]:S()) then
      timestamp = tostring(dateparse.parse(SCH[11][4]:S()))
   end

   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. 
      " and location_pref = " .. conn:quote(DXCId), live=true}

   if ResId[1]['count(id)']:S() ~= '0' then

      local ResId2 = sqDB.execute{sql="select count(id), id, res_id, current_stage from patient_flow where stage = " 
         .. conn:quote(patient_flow_stage) .. " and res_id = " .. conn:quote(ResId[1]['id']:S()), live=true}

      if ResId2[1]['count(id)']:S() == '0' and patient_flow_stage == 'COMPLETE' then

         -- Could be a DNA         
         patient_flow_stage = 'DNA'

         ResId2 = sqDB.execute{sql="select count(id), id, res_id, current_stage from patient_flow where stage = " 
            .. conn:quote(patient_flow_stage) .. " and res_id = " .. conn:quote(ResId[1]['id']:S()), live=true}                
      end

      if ResId2[1]['current_stage']:S() == 'Y' then            
         local sql = "delete from patient_flow " ..
         "where "..
         "id = " .. ResId2[1]['id']:S()

         sqDB.execute{sql=sql, live=false}          
      else
         local infoTxt = "Unable to deal with " .. SCH[25][1]:S() .. " for res_id " 
         .. ResId2[1]['id']:S() .. " and facilityID " .. facilityID
         
         sqUtils.printLog(infoTxt)
      end
   else
      local infoTxt = "Unable to deal with " .. SCH[25][1]:S() .. " for user_id " .. id .. 
      " and facilityID " .. facilityID
      sqUtils.printLog(infoTxt)
   end

   if SCH[25][1]:S() == 'CancelCheckedOut' then
      -- Set the current_stage of the last/latest entry for that reservation (res_id) to 'Y'
      local ResId3 = sqDB.execute{sql="select count(id), id from patient_flow where res_id = " 
         .. conn:quote(ResId[1]['id']:S() .. " order by timestamp desc"), live=true}

      if ResId3[1]['count(id)']:S() ~= '0' then
         sqDB.execute{sql=
            "update patient_flow " ..
            "set current_stage = 'Y' " ..
            "where id = " .. conn:quote(ResId3[1]['id']:S()),
            live = false}
      end      
   end
   --
end

function sqDB.getPatientFlowStage(res_id)
    
    local stage = ''
   
    if res_id ~= 'NULL' then
       stage = sqDB.execute{sql="select stage from patient_flow " ..
                                 "where current_stage = 'Y' " ..
                                 "and res_id = " .. conn:quote(res_id),
                                 live = true}
       return stage:S()
    else
       return stage
    end
end

----------------------------------------------------------------------------
function sqDB.updateAppointment(id, facilityID, SCH, clinCode1, reasonID, PV2, PV1, NTE)
   sqUtils.printLog("Calling sqDB.updateAppointment...")
   
   -- Related to SQBUGS-718: Custom Appointment Duration, based on Appt Reason and Facility ID 
   local continue, appStartDate, appStartTime, appEndTime = sqDB.validateAppointmentDates(facilityID, SCH[11][4]:S(), SCH[11][5]:S(), SCH[9]:S(), reasonID )
   
	trace(continue, appStartDate, appStartTime, appEndTime)
   if not continue then
      return
   end   
   
   local retResId = '0'
   
   local opdAppointmentString = string.sub(tostring(appStartDate),1,10)
       
   --get the DXC identifier
   local DXCId = SCH[1][1]:S()
   local apptType = PV2[12]:S()
   local apptPriority = PV2[25]:S()
   local apptCategory = PV1[18]:S()
   local appReason = 1
   local apptNotes = NTE[3]:S()  -- Jira IR-256 Appt Comments
   
   local reservation_status = ''
   
   -- patient_flow_stage not used ...
   if SCH[25][1]:S() == 'Attended' then
      patient_flow_stage =  'CHECK-IN'
   elseif SCH[25][1]:S() == 'Did Not Attend' then
      patient_flow_stage = 'DNA'
   elseif SCH[25][1]:S() == 'CheckedOut' then
      patient_flow_stage = 'COMPLETE'
   end

   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. 
      " and reservation_date = " .. conn:quote(opdAppointmentString) .. 
      " and status = 'active' and reservation_time = " ..  conn:quote(appStartTime), live=true}
   
   if ResId[1]['count(id)']:S() == '0'
      then

      --check if an existing appointment exists
      local ResIdUpd = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. 
         " and location_pref = " .. conn:quote(DXCId) .. " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0'
         then
         sqDB.execute{sql="update reservation set " .. 
            "status = 'cancelled'" ..
            " where location_pref = " .. conn:quote(DXCId).." and " ..
            "user_id = " .. id
            , live=false}
      end

      --insert the new appointment
      sqDB.execute{sql="insert into reservation (facility_id, user_id, facility_type, consultant_id, location_pref, " ..
         "reservation_date, reservation_time, reservation_end_time, google_cal_id, reason_id,recurrence,date,status," ..
         "attended_by, is_attended,booking_user_id, source, print,stats, quality,capacity_sync, is_urgent, comments) " .. 
         "values ("..
         facilityID..","..
         id..","..
         "'f',0"..","..
         conn:quote(DXCId).."," ..
         conn:quote(opdAppointmentString)..","..
         conn:quote(appStartTime)..","..
         conn:quote(appEndTime)..","..
         conn:quote(clinCode1)..","..
         reasonID..","..
         "'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0"..","..
         conn:quote(apptNotes)..
         ")"
         , live=false}  

      local result = sqDB.execute{sql="select last_insert_id() as id", live=true}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'TYPE'"..","..
         conn:quote(apptType)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'PRIORITY'"..","..
         conn:quote(apptPriority)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'CATEGORY'"..","..
         conn:quote(apptCategory)..
         ")"
         , live=false}
      --
   else 
      sqDB.execute{sql="update reservation set " .. 
         "reservation_date = " .. conn:quote(opdAppointmentString)..","..
         "reservation_end_time = " .. conn:quote(appEndTime)..","..
         "google_cal_id = " .. conn:quote(clinCode1)..","..
         "modified = now(), modified_source = 'h'"..","..
         "reservation_time = " ..  conn:quote(appStartTime)..","..
         "comments = " ..conn:quote(apptNotes)..
         " where location_pref = " .. conn:quote(DXCId).." and status = 'active' and " ..
         "user_id = " .. id
         , live=false}
   end
   --
end

----------------------------------------------------------------------------
function sqDB.cancelAppointment(id, facilityID, SCH, reasonID)
   
   if id == nil or id == '' or id == '0' then return end
   
   sqUtils.printLog("Calling sqDB.cancelAppointment...")
   
   -- Related to SQBUGS-718: Custom Appointment Duration, based on Appt Reason and Facility ID 
   local continue, appStartDate, appStartTime, appEndTime = sqDB.validateAppointmentDates(facilityID, SCH[11][4]:S(), SCH[11][5]:S(), SCH[9]:S(), reasonID )
   
	trace(continue, appStartDate, appStartTime, appEndTime)
   if not continue then
      return
   end   
   
   local opdAppointmentString = string.sub(tostring(appStartDate),1,10)
   
   local DXCId = SCH[1][1]:S()

   sqDB.execute{sql="update reservation set " .. 
      "reservation_date = " .. conn:quote(opdAppointmentString)..","..
      "reservation_time = " .. conn:quote(appStartTime)..","..
      "reservation_end_time = " .. conn:quote(appEndTime)..","..
      "status = 'cancelled'" ..","..
      "modified = now(), modified_source = 'h'"..
      " where location_pref = " .. conn:quote(DXCId).." and " ..
      "user_id = " .. id
      , live=false}
end

function sqDB.updateComments(id, facilityID, SCH, NTE)
   sqUtils.printLog("Calling sqDB.updateComments...")

   --extract the res date and time
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   local DXCId = SCH[1][1]:S()
   
   local apptNotes = NTE[3]:S()  -- Jira IR-256 Appt Comments
   
   if apptNotes == '""' then
       apptNotes = '' --Fix issue where empty, or cleared comment, is shown as double quotes "" in the SQ App Calendar
   end
   
   --PB 18.10.23: fix for the replacement of nulls with 0000-00-00
   --[[
   if opdAppointmentString == nil or opdAppointmentString == '' then 
      iguana.logWarning("Invalid request date in SCH[11][4]")
      opdAppointmentString = '0000-00-00' 
   end
   ]]
   --

   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. 
      " and reservation_date = " .. conn:quote(opdAppointmentString) .. 
      " and status = 'active' and reservation_time = " ..  conn:quote(opdAppointmentTimeString), live=true}
   
   if ResId[1]['count(id)']:S() == '0'
      then

      --check if an existing appointment exists
      local ResIdUpd = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. 
         " and location_pref = " .. conn:quote(DXCId) .. " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0' then         
          sqDB.execute{sql="update reservation set " .. 
             "modified = now(), modified_source = 'h'"..","..
             "comments = " .. conn:quote(apptNotes)..
             " where location_pref = " .. conn:quote(DXCId).." and status = 'active' and " ..
             "user_id = " .. id
             , live=false}
      end
   else 
      sqDB.execute{sql="update reservation set " .. 
         "modified = now(), modified_source = 'h'"..","..
         "comments = " .. conn:quote(apptNotes)..
         " where location_pref = " .. conn:quote(DXCId).." and status = 'active' and " ..
         "user_id = " .. id
         , live=false}
   end
end

----------------------------------------------------------------------------
function sqDB.getFacilityIDGivenPatientID_CHO8(patientId)
  local result = sqDB.execute{sql="select facility_id from facility_user where "..
  " user_id = " .. patientId .. ";"
  , live=true}
   
   return result[1].facility_id:nodeValue()
end

function sqDB.getFacilityIDGivenPatientID(patientId)
  local result = sqDB.execute{sql="select facility_id from facility_user where "..
  " user_id = " .. patientId .. ";"
  , live=true}
   
   local str = ''
   for i=1, #result do
      str = str..result[i].facility_id
      if i+1 <= #result then
         str = str..","
      end
   end
   
   return str
end

----------------------------------------------------------------------------
function sqDB.getReservationParentID(AppId)
   local ResId = sqDB.execute{sql=
      "select IFNULL(parent_id,0) as parent_id from reservation where id = " .. AppId, live=true}
   return ResId
end

----------------------------------------------------------------------------
function sqDB.getAppCategory(resId)
   local AppCategory = sqDB.execute{sql=
         "select ipm_value from reservation_ipm " ..
         "where ipm_name = 'CATEGORY' and res_id = " .. resId, live=true}
   return AppCategory
end


----------------------------------------------------------------------------
function sqDB.getAppCategoryNoParentId(AppId)
   local AppCategory = sqDB.execute{sql=
         "select ipm_value from reservation_ipm " ..
         "where ipm_name = 'CATEGORY' and res_id = " .. AppId, live=true}
   return AppCategory
end

----------------------------------------------------------------------------
function sqDB.getAttendId(fID)
   local AttendId = sqDB.execute{sql=
         "select id from reservation_custom_fields rcf " ..
         "where facility_id = " .. fID ..
         " and field_section = 'follow_up_flow' " ..
         "and field_name = 'ATTND (Attended Status)'", live=true}
   return AttendId
end

----------------------------------------------------------------------------
function sqDB.getOutcomeId(fID)
   local OutcomeId = sqDB.execute{sql=
         "select id from reservation_custom_fields rcf " ..
         "where facility_id = " .. fID ..
         " and field_section = 'follow_up_flow' " ..
         "and field_name = 'SCOCM (Outcome)'", live=true} 
   return OutcomeId
end

----------------------------------------------------------------------------
function sqDB.getAttendCategory(AttendID, AppId)
   local AttendCategory = sqDB.execute{sql=
            "select value_str from reservation_additional_info " ..
            "where reservation_custom_field_id = " .. AttendID ..
            " and reservation_id = " .. AppId, live=true}
   return AttendCategory
end

----------------------------------------------------------------------------
function sqDB.getOutcomeCategory(OutcomeId, AppId)
   local OutcomeCategory = sqDB.execute{sql=
            "select value_str from reservation_additional_info " ..
            "where reservation_custom_field_id = " .. OutcomeId ..
            " and reservation_id = " .. AppId, live=true}
   return OutcomeCategory
end

----------------------------------------------------------------------------
function sqDB.getAppType(parentId)
   local AppType = sqDB.execute{sql=
         "select ipm_value from reservation_ipm " ..
         "where ipm_name = 'TYPE' and res_id = " .. parentId, live=true}
   return AppType
end

----------------------------------------------------------------------------
function sqDB.getAppTypeByAppId(AppId)
   local AppType = sqDB.execute{sql=
         "select IFNULL(ipm_value,'') as ipm_value from reservation_ipm " ..
         "where ipm_name = 'TYPE' and res_id = " .. AppId, live=true}
   return AppType
end

----------------------------------------------------------------------------
function sqDB.getAppPriority(parentId)
   local AppPriority = sqDB.execute{sql=
         "select IFNULL(ipm_value,'') as ipm_value from reservation_ipm " ..
         "where ipm_name = 'PRIORITY' and res_id = " .. parentId, live=true}  
   return AppPriority
end

----------------------------------------------------------------------------
function sqDB.getAppPriorityByAppId(AppId)
   local AppPriority = sqDB.execute{sql=
         "select ipm_value from reservation_ipm " ..
         "where ipm_name = 'PRIORITY' and res_id = " .. AppId, live=true}
   return AppPriority
end




----------------------------------------------------------------------------
function sqDB.getReferralIdByAppId(appID)
   local findReferral = sqDB.execute{sql=
            "select count(id), ewaiting_referral_id from ewaiting_referral_orders where res_id = " .. appID, live=true}
   return findReferral
end

----------------------------------------------------------------------------
function sqDB.getLocationPref(appId)
   local lpref = sqDB.execute{sql="select location_pref from reservation where id = " .. conn:quote(appId), live=true}

   return lpref[1]['location_pref']:S()
end

----------------------------------------------------------------------------
function sqDB.getActiveAppts(eRefId)
   local activeAppts = sqDB.execute{sql= 
            "select count(id) " ..
            "from reservation " .. 
            "where id in (select res_id from ewaiting_referral_orders where ewaiting_referral_id = " ..eRefId .. ") " ..
            "and status = 'active'"
            , live=true}
   return activeAppts
end

--[[ 
   Function:  sqDB.isApptActive       

   Purpose:   This function determines if an appointment is active, or not.
              An active appointment should be determined by checking whether an appointment 
              has an active status (reservation.status = 'active') and whether the 
              appointment start date (reservation.reservation_date) is NOT in the past 
              (= can be either today's date or in the future)

   Parameters: 
             resID
          
   Returns:  true - Appt is active
             false - Appt is not active

--]]
function sqDB.isApptActive(appId)
   
   local apptActive = false
   
   --local currentDate = os.date("%Y-%m-%d".. " 00:00:00")
   
   local activeApptCount = sqDB.execute{sql=
         "select count(id) from reservation " ..
         "where status = 'active' and reservation_date >= current_date() and id = "..appId, live=true}
   
   trace(activeApptCount[1]['count(id)']:S())
   
    -- if activeApptCount is 1, then it satisfies criteria of being active
   if activeApptCount[1]['count(id)']:S() == '1' then
      apptActive = true
   end
     
   return apptActive
end

----------------------------------------------------------------------------
function sqDB.setReferralToDischarged(eRefId)
   
   if eRefId == nil or eRefId == '' or eRefId == '0' then return end
   
   sqUtils.printLog("Calling sqDB.setReferralToDischarged...")
   
   sqDB.execute{sql=
               "update ewaiting_referrals " ..
               "set status = 'discharged' " ..
               "where id = " .. eRefId, live=false} 
end

----------------------------------------------------------------------------
function sqDB.getReferralByOrderId(orderId)
   local findReferral2 = sqDB.execute{sql=
      "select count(id), ewaiting_referral_id, IFNULL(cancellation_date,0) as cancellation_date " ..
      "from ewaiting_referral_orders " ..
      "where order_id = " .. conn:quote(orderId), live=true}
end

----------------------------------------------------------------------------
function sqDB.getEwaitingDischargeTimestamp(eRefId)
   local findReferral2a = sqDB.execute{sql=
         "select count(id), timestamp from ewaiting_discharges " ..
         "where referral_id = " .. eRefId, live=true} 
   return findReferral2a
end

----------------------------------------------------------------------------
function sqDB.getEwaitingReservationsCount(eRefId)
   local findReferral2b = sqDB.execute{sql=
            "select count(res_id) from ewaiting_reservations " ..
            "where ewaiting_referral_id = " .. eRefId, live=true}
   return findReferral2b
end

----------------------------------------------------------------------------
function sqDB.updateGoogleCalId(appId, googleCalId)
   
   if appId == nil or appId == '' or appId == '0' then return end
   
   sqUtils.printLog("Calling sqDB.updateGoogleCalId...")
   
   local ResId = sqDB.execute{sql=
      "select IFNULL(parent_id,0) as parent_id from reservation where id = " .. appId, live=true}

   if ResId[1]['parent_id']:S() ~= '0' then
      sqDB.execute{sql="update reservation set " .. 
         "google_cal_id = " .. conn:quote(googleCalId)..
         " where id = " .. appId
         , live=false}
   end
end

----------------------------------------------------------------------------
function sqDB.getReferralComments(refId)
   local findReferral = sqDB.execute{sql=
      "select count(id), comments from ewaiting_referrals where id = " .. conn:quote(refId), live=true}
   return findReferral
end

----------------------------------------------------------------------------
function sqDB.getReferralProvider(id)
   local findReferralProv = sqDB.execute{sql=
      "select count(id), name from ewaiting_facility_providers " ..
      "where meditech_id = " .. conn:quote(id), live=true}
   return findReferralProv
end

----------------------------------------------------------------------------
function sqDB.getCancelledOrdersNoAppts(orderId)
   local findReferral3 = sqDB.execute{sql=
      "select count(id), ewaiting_referral_id, IFNULL(cancellation_date,0) as cancellation_date " ..
      "from ewaiting_referral_orders " ..
      "where order_id = " .. conn:quote(orderId), live=true}
   return findReferral3
end

----------------------------------------------------------------------------
function sqDB.getSQIdsByIPMAppId(appId)
   
   --pulls the Ids for reservation, ewaiting_referrals and ewaiting_referral_orders
   --from SQ based on the IPM app Id stored in location_pref
   --return reservation ID, Order ID, Referral ID
   --If order and referral IDs are null then they dont exist for the appointment
   
   if appId == nil or Id == '' then
      return nil,nil,nil
   end
   
   local res = sqDB.execute{sql=
      "select IFNULL(res.id,'') as 'ResID', IFNULL(ero.id,'') as 'OrderID', IFNULL(ero.ewaiting_referral_id,'') as 'RefID' \
      from reservation res left outer join ewaiting_referral_orders ero on ero.res_id = res.id \
      where location_pref = ".. conn:quote(appId)
      , live=true}

   return res[1].ResID:S(), res[1].OrderID:S(), res[1].RefID:S()
end

----------------------------------------------------------------------------
function sqDB.getReferringDoctor(appId)
   
   local refDoc = nil
   
   if appId == nil or Id == '' then
      return '','',''
   end
   
   local resId, orderId, refId = sqDB.getSQIdsByIPMAppId(appId)
   if refId ~= '' then
      local res = sqDB.execute{sql=
         "select referral_source_name from ewaiting_referrals \
         where id = ".. conn:quote(refId)
         , live=true}
      refDoc = res[1].referral_source_name:S()
   end
   
   return refDoc
end

----------------------------------------------------------------------------

function sqDB.updateAccessionNumber(appId)
   
   sqUtils.printLog("Calling sqDB.updateAccessionNumber...")
   
   if appId == nil or appId == '' or appId == '0' then return end
   
   local resId, orderId, refId = sqDB.getSQIdsByIPMAppId(appId)
   
   if orderId ~= '' then
      local res = sqDB.execute{sql=
         "update ewaiting_referral_orders set accession_number = 'C"..appId.."'"..
         " where id = ".. orderId , live=false}
   end
   
   return
end

----------------------------------------------------------------------------

function sqDB.updateResID_Accession_onOrders(appId, orderId)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
   if appId == nil or appId == '' or appId == '0' then return end
   if orderId == nil or orderId == '' or orderId == '0' then return end
   
   -- Should be fine about calling this function, because (previous)
   -- call to sqDB.updateAccessionNumber(appId) yields the resId
   local resId, localorderId, refId = sqDB.getSQIdsByIPMAppId(appId)
   
   local appIdStr = "C"..appId
   
   if orderId ~= '' then
      sqDB.execute{sql=
         "update ewaiting_referral_orders set res_id = "..resId..
         ", accession_number = "..conn:quote(appIdStr).." where id = "..orderId, live=false}
   end
   
   return
end

----------------------------------------------------------------------

function sqDB.createEWaiting_Reservation_Link(orderID) 
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
   if orderID == nil or orderID == '' or orderID == '0' then return end
   
   local orders = sqDB.execute{sql=
         "select * from ewaiting_referral_orders \
          where id = "..orderID, live=true}
   
   local res_id = orders[1]['res_id']:S()
   local referral_id = orders[1]['ewaiting_referral_id']:S()
   
   if referral_id == 'NULL' or referral_id == nil or referral_id == '' then return end
   if res_id == 'NULL' or res_id == nil or res_id == '' then return end
      
   sqDB.execute{sql=
         "insert into ewaiting_reservations (ewaiting_referral_id, res_id) \
          select "..referral_id..", "..res_id.." where not exists \
          (select * from ewaiting_reservations where ewaiting_referral_id = "..referral_id.." and res_id = "..res_id..")", live=false}
      
   return referral_id
end

----------------------------------------------------

function sqDB.updateStatusOnEWaitingReferral(referralID, status)
   
     sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
     if referralID == nil or referralID == '' or referralID == '0' then return end
   
     sqDB.execute{sql=
         "update ewaiting_referrals set status = "..conn:quote(status)..
         " where id = "..referralID, live=false}
end

----------------------------------------------------------------------------
function sqDB.updateReservationModifiedSource(id, SCH)
   --get the DXC identifier
   local DXCId = SCH[1][1]:S()
  
   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. 
      " and location_pref = " .. conn:quote(DXCId), live=true}

   if ResId[1]['count(id)']:S() ~= '0' then
      
      local sql ="update reservation set modified_source = " .. conn:quote("h") .. " where id = " ..
      conn:quote(ResId[1]['id']:S())

      sqDB.execute{sql=sql, live=false}

   else
      local infoTxt = "Unable to deal with " .. SCH[25][1]:S() .. " for user_id " .. id .. 
      " and facilityID " .. facilityID
      sqUtils.printLog(infoTxt)
   end   
end

----------------------------------------------------------------------------

function sqDB.getExamDetailsOnIPMID(AppId)
   
   -- Specific to Galway where there will only ever be one order per referral
   local examCode = ''
   local examDesc = ''
   
   -- SELECT id from demo.reservation where location_pref = <<AppId>>
   -- SELECT ewaiting_referral_id from demo.ewaiting_reservations where res_id = <<id>>  ( "Reservation to Referral" lookup table)
   -- SELECT exam_code, exam_name from demo.ewaiting_referral_orders where ewaiting_referral_id = <<ewaiting_referral_id
   
   --[[
   local res = sqDB.execute{sql=
      "select exam_code, exam_name from reservation res left outer join ewaiting_reservations ewr on (res.id = ewr.res_id ) "..
      "left outer join ewaiting_referral_orders ero on (ero.ewaiting_referral_id = ewr.ewaiting_referral_id) "..
      "where location_pref = "..conn:quote(AppId)
      , live=true}
   ]]--
   
   --PB 24.02.2025 : IR-778 : query taking too long and causes queueing on inbound
   --replaced above query
   
   --[[
   local res = sqDB.execute{sql=
      "select exam_code, exam_name from ewaiting_referral_orders ero join reservation r on r.id = ero.res_id  "..
      "where r.location_pref = "..conn:quote(AppId)
      , live=true}
   ]]--
   
   -- IR-778 jlui2 26/02/2025 - above SQL query for retrieving exam_code was taking too long and causes queueing on inbound
    local res = sqDB.execute{sql=      
      "select exam_code, exam_name from ewaiting_referral_orders where ewaiting_referral_id in "..
      "(select ewaiting_referral_id from ewaiting_reservations where res_id in "..
      "(select id from reservation where location_pref = "..conn:quote(AppId).."))"
      , live = true}
         
   if #res > 0 then
      examCode = res[1].exam_code:S()
      examDesc = res[1].exam_name:S()
   end
   
   return examCode, examDesc

end

----------------------------------------------------------------------------

function sqDB.getExamDetailsOnIPMID_CVIS(AppId)
   
   -- Specific to Galway where there will only ever be one order per referral
   local examCode = ''
   local examDesc = ''
   
   -- SELECT id from demo.reservation where location_pref = <<AppId>>
   -- SELECT ewaiting_referral_id from demo.ewaiting_reservations where res_id = <<id>>  ( "Reservation to Referral" lookup table)
   -- SELECT exam_code, exam_name from demo.ewaiting_referral_orders where ewaiting_referral_id = <<ewaiting_referral_id
   
   local res = sqDB.execute{sql=
      "select exam_code, exam_name from reservation res left outer join ewaiting_reservations ewr on (res.id = ewr.res_id ) "..
      "left outer join ewaiting_referral_orders ero on (ero.ewaiting_referral_id = ewr.ewaiting_referral_id) "..
      "where res.id = "..conn:quote(AppId)
      , live=true}
   
   if #res > 0 then
      examCode = res[1].exam_code:S()
      examDesc = res[1].exam_name:S()
   end
   
   return examCode, examDesc

end

----------------------------------------------------------------------------
function sqDB.getAppointmentReason(appId)
   
   local res = sqDB.execute{sql=
         "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'REASON' and res_id = " .. appId
         , live=true}
   
   return res
end

----------------------------------------------------------------------------
function sqDB.getClinicConfigMapping(key, ...)
      
   --needs 2 parameters, facility id and reason id
   if select('#', ...) <= 2 then
      local facilityId, reasonId = select(1,...)
      
      local res = sqDB.execute{sql=
            "select "..key.." from facility_clinic_codes where facility_id = "..facilityId.." and reason_id = "..reasonId,
            live=true}
      return res[1][key]:S()
    
   else
      return ''
   end  
   
end
----------------------------------------------------------------------------
function sqDB.getSpecialtyCode(appId)
   
   local res = sqDB.execute{sql=
         "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'DXCSPEC' and res_id = " .. appId
         , live=true}
   
   return res
end

----------------------------------------------------------------------------
function sqDB.getAttendingDoctor(appId, facilityId)
   
   local docCode = ''
   local docName = ''
   
   local res = sqDB.execute{sql=
         "select count(*) as cnt, IFNULL(ipm_value,'') as ipm_value " ..
         "from reservation_ipm " ..
         "where ipm_name = 'AIPID' and res_id = " .. appId
         , live=true}
   
   if tonumber(res[1].cnt:S()) >= 1 then
      if not sqUtils.isEmpty(res[1]['ipm_value']:S()) then
         docCode = res[1]['ipm_value']:S()
      end
   else
      --the appointment was not created on IPM therefore entry wont exist
      local parentFacilityId = sqUtils.getJsonMappedValue(sqCfg.extra_params,'parentFacilityId')
      
      res = sqDB.execute{sql=
         "select firstname from facilities where parent_id = "..
         parentFacilityId.." and facility_type = 'sp'"
         , live=true}
      
      docName = res[1].firstname:S()
   
   end
   
   return docCode,docName
end

----------------------------------------------------------------------------
function sqDB.getParentFacilityId(facilityID)

   local parentFacilityID = ''
   local parentFID = sqDB.execute{sql="select parent_id from facilities where id = "..facilityID, live=true}

   if parentFID[1]['parent_id']:S() ~= '0' then
      parentFacilityID = parentFID[1]['parent_id']:S()
   else
      parentFacilityID = facilityID
   end

   return parentFacilityID

end

----------------------------------------------------------------------------

function sqDB.getApptDuration(facilityID, apptReasonID)
   
   local res = sqDB.execute{sql=
         "select duration from facility_reasons where facility_id = "..
         facilityID.." and reason_id = "..apptReasonID
         , live=true}
   
   local apptDuration =  res[1].duration:S()
           
   if apptDuration == 'NULL' or apptDuration == nil or apptDuration == '' then
      
      local parentFacilityID = sqDB.getParentFacilityId(facilityID)
            
      -- Now try the Parent Facility
      res = sqDB.execute{sql=
         "select duration from facility_reasons where facility_id = "..
         parentFacilityID.." and reason_id = "..apptReasonID
         , live=true}
      
      apptDuration =  res[1].duration:S()
      
      if apptDuration == 'NULL' or apptDuration == nil or apptDuration == '' then
         res = sqDB.execute{sql=
         "select duration from facilities where id = "..facilityID
         , live=true}
         
         apptDuration =  res[1].duration:S()
      end
   end
   
   return apptDuration
   
end

----------------------------------------------------------------------------

function sqDB.addUpdatePatientAlerts(patientID,parentFacilityID,MsgIn)
   
   sqUtils.printLog("add/updating patient alert(s) for patient ID: "..patientID)
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
   
   --remove existing alerts that where hl7 generated for the patient as 
   --full alert history will come across in message
   local res = sqDB.execute{sql="delete from user_alerts where "..
            "user_id = "..conn:quote(patientID).." and sq_user_id = "..conn:quote(sqUser) , live=false}
  
   for i = 1, #MsgIn.OBX do  

      local alertStart = MsgIn.OBX[i][12]:S()
      local alertEnd = MsgIn.OBX[i][14]:S()
      local alertCode = MsgIn.OBX[i][5][1][1]:S()
      local alertDesc = MsgIn.OBX[i][5][1][3]:S()
      local alertDomain = MsgIn.OBX[i][2]:S()
      local alertComment = MsgIn.OBX[i][6]:S()

      --insert the alert
      local sqlStr = "insert into user_alerts (user_id, alert, code, domain, start_date, end_date,sq_user_id, comments, created_At) " ..
      "values ("..conn:quote(patientID)..", ".. 
      conn:quote(alertDesc)..", "..
      conn:quote(alertCode)..", "..
      conn:quote(alertDomain)..", "
      
      if alertStart == '' then
         sqlStr = sqlStr.."NULL"..", "
      else
         sqlStr = sqlStr..conn:quote(alertStart)..", "
      end

      if alertEnd == '' then
         sqlStr = sqlStr.."NULL"..", "
      else
         sqlStr = sqlStr..conn:quote(alertEnd)..", "
      end

      sqlStr = sqlStr..
      conn:quote(sqUser)..", "..
      conn:quote(alertComment)..", "..
      "now()"..
      ")"
      trace(sqlStr)
      sqDB.execute{sql=sqlStr, live=false}

   end
   
end


----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------

--     THS IMPLEMENTATION  -----


-----------------------------------------------------------------
----------------------------------------------------------------------------

function sqDB.THS.fillPatientData(PID, mrn)
   
   sqUtils.printLog("Loading patient data...")
   
   local patFirstname = ''
   local patSurname = ''
   local patMiddlename = ''
   local patTitle = ''
   
   --if we are using an identifier for patient names then pull the name based on that
   --example identifer L = Legal Name, if no identifer then pull first name
   local nameIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'patientNameIdentifier')   
   patFirstname, patSurname, patMiddlename, patTitle = sqDB.THS.getPatientName(PID,nameIdentifier)
   
   local alias_patFirstname = ''
   local alias_patSurname = ''
   local alias_patMiddlename = ''
   local slias_patTitle = ''
   
   --pull oput the alias name
   local aliasNameIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'aliasNameIdentifier')   
   alias_patFirstname, alias_patSurname, alias_patMiddlename, alias_patTitle = sqDB.THS.getPatientName(PID,aliasNameIdentifier)
      
   sqDB.patientData.mrn = mrn
   sqDB.patientData.firstname = patFirstname
   sqDB.patientData.surname = patSurname
   sqDB.patientData.middleName = patMiddlename
   sqDB.patientData.user_type = "N"
   sqDB.patientData.dob = sqUtils.parseDOB(PID[7])
   sqDB.patientData.gender = PID[8]:nodeValue()   
   sqDB.patientData.home_phone = ''
   sqDB.patientData.mobile = ''
   sqDB.patientData.email = ''
   sqDB.patientData.title = patTitle
   sqDB.patientData.spokenLanguage = PID[15][2]:S()
   sqDB.patientData.aboriginality = PID[10][1]:S()
   
   sqDB.patientData.alias_firstname = alias_patFirstname
   sqDB.patientData.alias_middlename = alias_patMiddlename
   sqDB.patientData.alias_surname = alias_patSurname
   
   --get the mapped title from config
   sqDB.patientData.title = sqUtils.getMappedValue(sqCfg.title_map, patTitle)
   if sqDB.patientData.title == '' then
      sqDB.patientData.title = patTitle:sub(1,1):upper()..patTitle:sub(2, 2+patTitle:len()-2):lower()
   end
   
   sqDB.patientData.Home_Address = PID[11][1][5]:S()  
   
   --get the phone identifier variables
   local homePhoneIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'homePhoneIdentifier')
   local mobileIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileIdentifier')
   local emailIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'emailIdentifier')
   local mobileCountryCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCountryCode')
   local mobileCheckDigits = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCheckDigits')
   
   sqDB.patientData.home_phone = sqDB.THS.getPhoneNumberByType(PID,homePhoneIdentifier)
   sqDB.patientData.mobile = sqDB.THS.getPhoneNumberByType(PID,mobileIdentifier)
   sqDB.patientData.email = sqDB.THS.getPhoneNumberByType(PID,emailIdentifier)
   
   sqDB.patientData.mobileint = ''  
   if sqDB.patientData.mobile:sub(1,2) == mobileCheckDigits then
      sqDB.patientData.mobileint = mobileCheckDigits .. sqDB.patientData.mobile:sub(2)
   else
      sqDB.patientData.mobileint = sqDB.patientData.mobile
   end
   
   --PB: 17.01.2025 : IR-687 : check for ie SMS conversion
   -- Example: replacing mobile numbers that start with '08', stripping off the '0' and replacing with '353'
   if sqDB.patientData.mobile:sub(1,2) == json.parse{data=sqCfg.extra_params}.ieMobileCheckDigits then
      sqDB.patientData.mobileint = json.parse{data=sqCfg.extra_params}.ieInternationalCode .. sqDB.patientData.mobile:sub(2)
   end 
   trace(sqDB.patientData.mobile, sqDB.patientData.mobileint)
   -------------------
   
   sqDB.patientData.address1 = PID[11][1][1]:nodeValue() -- PID[11][1][2]
   sqDB.patientData.address2 = PID[11][1][2]:nodeValue()
   sqDB.patientData.address3 = '' --PID[11][1][3]:nodeValue()
   sqDB.patientData.address4 = PID[11][1][3]:nodeValue()
   sqDB.patientData.address5 = PID[11][1][4]:nodeValue() 
   
   local mcCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'medicareCode')
   sqDB.patientData.medicareNumber = sqDB.THS.getMedicareNumbers(PID,mcCode)
   
   local imCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'iMedixCode')
   sqDB.patientData.imedixNumber = getMRNbyAssigningFacility(PID,imCode)

end

-----------------------------------------------------------------
function sqDB.THS.fillNOKData(patientID, NK1)

   sqUtils.printLog("Loading NOK data...")
   
   sqDB.nokData.nk1_userID = ''
   sqDB.nokData.nk1_parent = patientID
   sqDB.nokData.nk1_firstname = NK1[1][2][2]:S()
   sqDB.nokData.nk1_surname = NK1[1][2][1]:S()
   
   sqDB.nokData.nk1_rel_code =  NK1[1][3][1]:S()
   sqDB.nokData.nk1_rel_desc =  NK1[1][3][2]:S()
   if sqDB.nokData.nk1_rel_code == '' then
      iguana.logWarning("Warning: Attempt to process NK1 segment for patient [" ..patientID.. "] with no relationship defined")
   end
   
   --get the relationship ID
   local res = sqDB.execute{sql="select id from family_relationship where code = "  .. 
      conn:quote(sqDB.nokData.nk1_rel_code).." or description = "..conn:quote(sqDB.nokData.nk1_rel_desc), live=true} 
   sqDB.nokData.nk1_rel_id = res[1].id:S()
   
   sqDB.nokData.nk1_phone = sqDB.THS.getNKPhoneNumberByType(NK1,'PH')
   sqDB.nokData.nk1_mobile = sqDB.THS.getNKPhoneNumberByType(NK1,'CP')   
   sqDB.nokData.nk1_email = sqDB.THS.getNKPhoneNumberByType(NK1,'Internet')
  
   if sqDB.nokData.nk1_mobile:sub(1,2) == '04' then
      local mobileCountryCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCountryCode')
      sqDB.nokData.nk1_mobile_format = mobileCountryCode .. sqDB.nokData.nk1_mobile:sub(2)
   else
      sqDB.nokData.nk1_mobile_format = sqDB.nokData.nk1_mobile
   end 
   
   sqDB.nokData.nk1_address_1 = NK1[1][4][1]:S()
   sqDB.nokData.nk1_address_2 = NK1[1][4][2]:S()
   sqDB.nokData.nk1_address_3 = NK1[1][4][3]:S()
   sqDB.nokData.nk1_address_4 = NK1[1][4][4]:S()
   sqDB.nokData.nk1_eircode = NK1[1][4][5]:S()
   
   sqDB.nokData.nk1_dob = sqUtils.parseDOB(NK1[1][16])
   local t = NK1[1][2][5]:nodeValue()
   --get the mapped title from config
   sqDB.nokData.nk1_title = sqUtils.getMappedValue(sqCfg.title_map,t)
   if sqDB.nokData.nk1_title == '' then
      sqDB.nokData.nk1_title = t:sub(1,1):upper()..t:sub(2, 2+t:len()-2):lower()
   end

   sqDB.nokData.nk1_gender = NK1[1][15]:S()

end

----------------------------------------------------------------------------
function sqDB.THS.fillNKContactData(patientID, nokContacts)
   
   --get the phone identifier variables
   local homePhoneIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'homePhoneIdentifier')
   local mobileIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileIdentifier')
   local emailIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'emailIdentifier')
   local mobileCountryCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCountryCode')
   local mobileCheckDigits = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCheckDigits')

   function getPhoneNumberByType(NK1, pType)   
      if NK1:childCount() >=1 then
         for i=1, NK1:childCount() do
            if string.upper(NK1[i][3]:S()) == string.upper(pType) then               
               return NK1[i][1]:S()
            end
         end
      end  
      return ''   
   end
   
   sqDB.nokData.nk1_userID = ''
   sqDB.nokData.nk1_parent = patientID
   
   sqDB.nokData.nk1_firstname = nokContacts[2][2]:S()
   sqDB.nokData.nk1_surname = nokContacts[2][1]:S()
   
   sqDB.nokData.nk1_rel_code = nokContacts[3][1]:S()
   sqDB.nokData.nk1_rel_desc = nokContacts[3][2]:S()
   
   if sqDB.nokData.nk1_rel_code == '' then
      iguana.logWarning("Warning: Attempt to process NK1 segment for patient [" ..patientID.. "] with no relationship defined")
   end
   
   --get the relationship ID
   local res = sqDB.execute{sql="select id from family_relationship where code = "  .. 
      conn:quote(sqDB.nokData.nk1_rel_code).." or description = "..conn:quote(sqDB.nokData.nk1_rel_desc), live=true} 
   sqDB.nokData.nk1_rel_id = res[1].id:S()
   
   sqDB.nokData.nk1_phone = getPhoneNumberByType(nokContacts[5],homePhoneIdentifier)   
   sqDB.nokData.nk1_mobile = getPhoneNumberByType(nokContacts[5],mobileIdentifier)
   sqDB.nokData.nk1_email = getPhoneNumberByType(nokContacts[5],emailIdentifier)

   if sqDB.nokData.nk1_mobile:sub(1,2) == mobileCheckDigits then
      local mobileCountryCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCountryCode')
      sqDB.nokData.nk1_mobile_format = mobileCountryCode .. sqDB.nokData.nk1_mobile:sub(2)
   else
      sqDB.nokData.nk1_mobile_format = sqDB.nokData.nk1_mobile
   end 
    
   sqDB.nokData.nk1_address_1 = nokContacts[4][1]:S()
   sqDB.nokData.nk1_address_2 = nokContacts[4][2]:S()
   sqDB.nokData.nk1_address_3 = nokContacts[4][3]:S()
   sqDB.nokData.nk1_address_4 = nokContacts[4][4]:S()
   sqDB.nokData.nk1_eircode = nokContacts[4][5]:S()

   sqDB.nokData.nk1_dob = sqUtils.parseDOB(nokContacts[16])
   local t = nokContacts[2][5]:nodeValue()
   --get the mapped title from config
   sqDB.nokData.nk1_title = sqUtils.getMappedValue(sqCfg.title_map,t)
   if sqDB.nokData.nk1_title == '' then
      sqDB.nokData.nk1_title = t:sub(1,1):upper()..t:sub(2, 2+t:len()-2):lower()
   end

   sqDB.nokData.nk1_gender = nokContacts[15]:S()
   
end

----------------------------------------------------------------------------
function sqDB.THS.fillGTContactData(patientID, gtContacts)
   
   --get the phone identifier variables
   local homePhoneIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'homePhoneIdentifier')
   local mobileIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileIdentifier')
   local emailIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'emailIdentifier')
   local mobileCountryCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCountryCode')
   local mobileCheckDigits = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCheckDigits')
   
   function getPhoneNumberByType(GT1, pType)
      if GT1:childCount() >=1 then
         for i=1, GT1:childCount() do
            if GT1[i][3]:S() == pType then
               return GT1[i][1]:S()
            end
         end
      end  
      return ''   
   end
   
   sqDB.gtData.gt1_userID = ''
   sqDB.gtData.gt1_parent = patientID
   
   sqDB.gtData.gt1_firstname = gtContacts[3][1][2]:S()
   sqDB.gtData.gt1_surname = gtContacts[3][1][1]:S()
   
   sqDB.gtData.gt1_rel_code = gtContacts[11][1]:S()
   sqDB.gtData.gt1_rel_desc = gtContacts[11][2]:S()
   
   if sqDB.gtData.gt1_rel_code == '' then
      iguana.logWarning("Warning: Attempt to process GT1 segment for patient [" ..patientID.. "] with no relationship defined")
   end
   
   --get the relationship ID
   local res = sqDB.execute{sql="select id from family_relationship where code = "  .. 
      conn:quote(sqDB.gtData.gt1_rel_code).." or description = "..conn:quote(sqDB.gtData.gt1_rel_desc), live=true} 
   sqDB.gtData.gt1_rel_id = res[1].id:S()
     
   sqDB.gtData.gt1_phone = getPhoneNumberByType(gtContacts[6],homePhoneIdentifier)   
   sqDB.gtData.gt1_mobile = getPhoneNumberByType(gtContacts[6],mobileIdentifier)
   sqDB.gtData.gt1_email = getPhoneNumberByType(gtContacts[6],emailIdentifier)
  
   if sqDB.gtData.gt1_mobile:sub(1,2) == mobileCheckDigits then
      local mobileCountryCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'mobileCountryCode')
      sqDB.gtData.gt1_mobile_format = mobileCountryCode .. sqDB.gtData.gt1_mobile:sub(2)
   else
      sqDB.gtData.gt1_mobile_format = sqDB.gtData.gt1_mobile
   end 
   
   sqDB.gtData.gt1_address_1 = gtContacts[5][1][1]:S()
   sqDB.gtData.gt1_address_2 = gtContacts[5][1][2]:S()
   sqDB.gtData.gt1_address_3 = gtContacts[5][1][3]:S()
   sqDB.gtData.gt1_address_4 = gtContacts[5][1][4]:S()
   sqDB.gtData.gt1_eircode = gtContacts[5][1][5]:S()
   
   sqDB.gtData.gt1_dob = sqUtils.parseDOB(gtContacts[8])
   
   local t = gtContacts[3][1][5]:nodeValue()
   --get the mapped title from config
   sqDB.gtData.gt1_title = sqUtils.getMappedValue(sqCfg.title_map,t)
   if sqDB.gtData.gt1_title == '' then
      sqDB.gtData.gt1_title = t:sub(1,1):upper()..t:sub(2, 2+t:len()-2):lower()
   end

   sqDB.gtData.gt1_gender = gtContacts[9]:S()
   
end

----------------------------------------------------------------------------
function sqDB.THS.getReferralPriorityCode(facilityID, categoryID)
   local refPriority = sqDB.execute{sql=
      "select priority from ewaiting_categories where facility_id = " ..
      facilityID .. " and id = ".. categoryID, live=true}
   return refPriority[1].priority
end

----------------------------------------------------------------------------
function sqDB.THS.getReferralByControlID(controlID , userID, facilityID)
   
   local suppressFacilityCheck = sqUtils.getMappedValue(sqCfg.extra_params,"suppressReferralFacilityCheck")
   
   local sqlSTR = "select id, facility_id from ewaiting_referrals where control_id = " ..
      conn:quote(controlID) .. " and user_id = "..userID.." and facility_id = "..facilityID

   if suppressFacilityCheck == '1' then
      sqlSTR = "select id, facility_id from ewaiting_referrals where control_id = " ..
      conn:quote(controlID) .. " and user_id = "..userID
   end
   
   local res = sqDB.execute{sql=sqlSTR, live=true}
   
   return res[1].id:S(), res[1].facility_id:S()
end

-----------------------------------------------------------------
function sqDB.THS.createPatient(PID, mrn, NK1, IN1, GT1)
   
   sqUtils.printLog("Creating patient "..mrn.." ...")
   
   sqDB.THS.fillPatientData(PID,mrn)
   
   --[[ temp remove translator_required
   
      sqDB.execute{sql="insert into users (mobile, home_phone, email, mrn, firstname, surname, user_type, Date_of_Birth, "..
      "Gender, Address_1, Address_2, Address_3, Address_4, Address_5, Home_Address, title, status, created, requires_translator) " ..
         "values (trim(replace("..
   ]]
   sqDB.execute{sql="insert into users (mobile, home_phone, email, mrn, firstname, surname, user_type, Date_of_Birth, "..
      "Gender, Address_1, Address_2, Address_3, Address_4, Address_5, Home_Address, title, status, created) " ..
         "values (trim(replace("..
         conn:quote(sqDB.patientData.mobileint)..", ' ', '')), "..
         conn:quote(sqDB.patientData.home_phone)..","..
         conn:quote(sqDB.patientData.email)..","..
         conn:quote(sqDB.patientData.mrn)..","..
         conn:quote(sqDB.patientData.firstname)..","..
         conn:quote(sqDB.patientData.surname)..","..
         conn:quote(sqDB.patientData.user_type)..","..
         conn:quote(sqDB.patientData.dob)..","..
         conn:quote(sqDB.patientData.gender)..","..
         conn:quote(sqDB.patientData.address1)..","..
         conn:quote(sqDB.patientData.address2)..","..
         conn:quote(sqDB.patientData.address3)..","..
         conn:quote(sqDB.patientData.address4)..","..
         conn:quote(sqDB.patientData.address5)..","..
         conn:quote(sqDB.patientData.Home_Address)..","..
         conn:quote(sqDB.patientData.title)..","..
         --temp remove translator
         --conn:quote(sqDB.patientData.interpreter_required)..","..
         "'active', now()"..
         ")"
         , live=false}
   
   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
   local patientID = result[1].id:S()

   --Insert medicare number
   sqDB.THS.insertMedicareNumber(patientID)
   
   --Insert infomedix number
   sqDB.THS.insertIMedixNumber(patientID)
   
   --add update insurance details
   --PB 14.05.24 : moved to general processing fro all messages
   --sqDB.THS.addUpdateInsurance(IN1,patientID)
   
   --add additional patient info
   --alias only on AU implementation for now
   if json.parse{data=sqCfg.extra_params}.customer == 'AU' then
      
      sqDB.THS.addUpdateUserAdditionalInfo(patientID, sqDB.patientData.middleName, sqDB.patientData.spokenLanguage, 
         sqDB.patientData.alias_firstname, sqDB.patientData.alias_surname, sqDB.patientData.alias_middlename,
         sqDB.patientData.aboriginality
      )
   
   end
   
   sqUtils.printLog("Created patient with id: [" ..patientID.."]") 

   if NK1 ~= nil then  
      if not (NK1:isNull()) then
         sqDB.THS.addUpdateNK(patientID, NK1)
      end  
   end
   
   if GT1 ~= nil then  
      if not (GT1:isNull()) then
        sqDB.THS.addUpdateGT(patientID, GT1)
      end  
   end
   
   --Normally used in IMS (Naas)
   --check id PID.15 idicates a (Y)es or (N)o to toggle SMS communication
   if json.parse{data=sqCfg.extra_params}.checkPIDSmsFlag == '1' then
      sqDB.THS.checkAndToggleSMS(patientID, PID)
   end
   
   return patientID
   
end

-----------------------------------------------------------------
function sqDB.THS.updatePatient(id, PID, mrn, NK1, IN1, GT1, msgType)
   
   sqUtils.printLog("Updating patient "..mrn.." ...")
   trace(id)
   sqDB.THS.fillPatientData(PID, mrn)
     
      local sql ="update users " ..
      "set "..
      "mrn = " .. conn:quote(sqDB.patientData.mrn) .. ", "..
      "firstname = " .. conn:quote(sqDB.patientData.firstname) .. ", "..
      "surname = " .. conn:quote(sqDB.patientData.surname) .. ", "..
      "Date_of_Birth = " .. conn:quote(sqDB.patientData.dob) .. ", "..
      "mobile = trim(replace(" .. conn:quote(sqDB.patientData.mobileint) .. ", ' ', '')), "..
      "home_phone = " .. conn:quote(sqDB.patientData.home_phone) .. ", "..
      "email = " .. conn:quote(sqDB.patientData.email) .. ", "..
      "Gender = " .. conn:quote(sqDB.patientData.gender) .. ", "..
      "Address_1 = " .. conn:quote(sqDB.patientData.address1) .. ", "..
      "Address_2 = " .. conn:quote(sqDB.patientData.address2) .. ", "..
      "Address_3 = " .. conn:quote(sqDB.patientData.address3) .. ", "..
      "Address_4 = " .. conn:quote(sqDB.patientData.address4) .. ", "..
      "Address_5 = " .. conn:quote(sqDB.patientData.address5) .. ", "..
      "Home_Address = " .. conn:quote(sqDB.patientData.Home_Address) .. ", "..
      --temp remove translator
      --"requires_translator = " .. conn:quote(sqDB.patientData.interpreter_required) .. ", "..
      "title = " .. conn:quote(sqDB.patientData.title) .. ", "..
      "modified = now() " ..
      "where id = ".. id

      sqDB.execute{sql=sql, live=false}

   --update medicare number
   sqUtils.printLog("Updating medicare number for patient: "..id)
   sqDB.THS.insertMedicareNumber(id)
   
   --update medicare number
   sqUtils.printLog("Updating medicare number for patient: "..id)
   sqDB.THS.insertIMedixNumber(id)
   
   --add update insurance details
   --sqUtils.printLog("Updating insurance details for patient: "..id)
   --PB 14.05.24 - moved to general processing for all messages
   --sqDB.THS.addUpdateInsurance(IN1,id)
   
   --add additional patient info
   --AU specific
   if json.parse{data=sqCfg.extra_params}.customer == 'AU' then
       
      sqDB.THS.addUpdateUserAdditionalInfo(id, sqDB.patientData.middleName, sqDB.patientData.spokenLanguage,
         sqDB.patientData.alias_firstname, sqDB.patientData.alias_surname, sqDB.patientData.alias_middlename,
         sqDB.patientData.aboriginality
      )
      
   end
   
   if msgType == 'A31' then   

      --clear all NOKs for this poatient
      --if NK1 missing then this is a removal of NOK
      --if NK1 exists then the logic to come will add/update as required
      sqDB.THS.removePatientNOKflags(id)
      
      --AU specific
      if json.parse{data=sqCfg.extra_params}.customer == 'AU' then
         --clear the act as emergency flags
         sqDB.THS.removePatientEmergflags(id)
      end

      --update NOK details
      if NK1 ~= nil then
         if not (NK1:isNull()) then
            sqDB.THS.addUpdateNK(id, NK1)
         end
      end
      
      --AU specific
      if json.parse{data=sqCfg.extra_params}.customer == 'AU' then
         --update guadians GT1 details
         if GT1 ~= nil then  
            if not (GT1:isNull()) then
               sqDB.THS.addUpdateGT(id, GT1)
            end  
         end
      end      

   end -- end if A31
   
   --Normally used in IMS (Naas)
   --check id PID.15 idicates a (Y)es or (N)o to toggle SMS communication
   if json.parse{data=sqCfg.extra_params}.checkPIDSmsFlag == '1' then
      sqDB.THS.checkAndToggleSMS(id, PID)
   end
   
   --check if we need to decease / un-decease the patient
   trace(PID[29]:S())
   if PID[29]:S() ~= '' and PID[29]:S() ~= '""' then
      
      sqDB.THS.processDeceasedPatient(id, PID)
      
   end
   
end

-----------------------------------------------------------------
function sqDB.THS.updatePatientNHSNumber(id,NHSNumber)
   
   if sqUtils.isEmpty(NHSNumber:trimWS()) then
      error("sqDB.THS.updatePatientNHSNumber - NHSNumber is empty.")
   end
   
   local sql ="update users " ..
   "set "..
   "nhs_number = " .. conn:quote(NHSNumber) ..", "..
   "modified = now() " ..
   "where id = ".. id
   
   --trace(sql)

   sqDB.execute{sql=sql, live=false}
end
-----------------------------------------------------------------
function sqDB.THS.updatePatientNoMobile(id, PID, mrn, NK1, IN1, GT1, msgType)
   
   sqUtils.printLog("Updating patient "..mrn.." ...")
   trace(id)
   sqDB.THS.fillPatientData(PID, mrn)
     
      local sql ="update users " ..
      "set "..
      "mrn = " .. conn:quote(sqDB.patientData.mrn) .. ", "..
      "firstname = " .. conn:quote(sqDB.patientData.firstname) .. ", "..
      "surname = " .. conn:quote(sqDB.patientData.surname) .. ", "..
      "Date_of_Birth = " .. conn:quote(sqDB.patientData.dob) .. ", "..
      "Gender = " .. conn:quote(sqDB.patientData.gender) .. ", "..
      "Address_1 = " .. conn:quote(sqDB.patientData.address1) .. ", "..
      "Address_2 = " .. conn:quote(sqDB.patientData.address2) .. ", "..
      "Address_3 = " .. conn:quote(sqDB.patientData.address3) .. ", "..
      "Address_4 = " .. conn:quote(sqDB.patientData.address4) .. ", "..
      "Address_5 = " .. conn:quote(sqDB.patientData.address5) .. ", "..
      "Home_Address = " .. conn:quote(sqDB.patientData.Home_Address) .. ", "..
      --temp remove translator
      --"requires_translator = " .. conn:quote(sqDB.patientData.interpreter_required) .. ", "..
      "title = " .. conn:quote(sqDB.patientData.title) .. ", "..
      "modified = now() " ..
      "where id = ".. id

      sqDB.execute{sql=sql, live=false}

   --update medicare number
   sqUtils.printLog("Updating medicare number for patient: "..id)
   sqDB.THS.insertMedicareNumber(id)
   
   --update medicare number
   sqUtils.printLog("Updating medicare number for patient: "..id)
   sqDB.THS.insertIMedixNumber(id)
   
   --add update insurance details
   --sqUtils.printLog("Updating insurance details for patient: "..id)
   --PB 14.05.24 - moved to general processing for all messages
   --sqDB.THS.addUpdateInsurance(IN1,id)
   
   --add additional patient info
   --AU specific
   if json.parse{data=sqCfg.extra_params}.customer == 'AU' then
       
      sqDB.THS.addUpdateUserAdditionalInfo(id, sqDB.patientData.middleName, sqDB.patientData.spokenLanguage,
         sqDB.patientData.alias_firstname, sqDB.patientData.alias_surname, sqDB.patientData.alias_middlename,
         sqDB.patientData.aboriginality
      )
      
   end
   
   if msgType == 'A31' then   

      --clear all NOKs for this poatient
      --if NK1 missing then this is a removal of NOK
      --if NK1 exists then the logic to come will add/update as required
      sqDB.THS.removePatientNOKflags(id)
      
      --AU specific
      if json.parse{data=sqCfg.extra_params}.customer == 'AU' then
         --clear the act as emergency flags
         sqDB.THS.removePatientEmergflags(id)
      end

      --update NOK details
      if NK1 ~= nil then
         if not (NK1:isNull()) then
            sqDB.THS.addUpdateNK(id, NK1)
         end
      end
      
      --AU specific
      if json.parse{data=sqCfg.extra_params}.customer == 'AU' then
         --update guadians GT1 details
         if GT1 ~= nil then  
            if not (GT1:isNull()) then
               sqDB.THS.addUpdateGT(id, GT1)
            end  
         end
      end      

   end -- end if A31
   
   --Normally used in IMS (Naas)
   --check id PID.15 idicates a (Y)es or (N)o to toggle SMS communication
   if json.parse{data=sqCfg.extra_params}.checkPIDSmsFlag == '1' then
      sqDB.THS.checkAndToggleSMS(id, PID)
   end
   
   --check if we need to decease / un-decease the patient
   trace(PID[29]:S())
   if PID[29]:S() ~= '' and PID[29]:S() ~= '""' then
      
      sqDB.THS.processDeceasedPatient(id, PID)
      
   end
   
end

-----------------------------------------------------------------

function sqDB.THS.checkAndToggleSMS(id, PID)
   
   if PID[27][1]:S() == 'Y' then
      sqUtils.printLog("Setting SMS notifications ON for user id : "..id)
      sqDB.THS.changeUserNotificationPreferencesForAllEvents(id,'sms',true)
   elseif PID[27][1]:S() == 'N' then
      sqUtils.printLog("Setting SMS notifications OFF for user id : "..id)
      sqDB.THS.changeUserNotificationPreferencesForAllEvents(id,'sms',false)
   end  
   
end

-----------------------------------------------------------------

function sqDB.THS.updatePatientGP(id, gpNum)

   local sql ="update users " ..
   "set gp_details = "..conn:quote(gpNum)..
   "where id = ".. id

   sqDB.execute{sql=sql, live=false}

end

-----------------------------------------------------------------

function sqDB.THS.addUpdateNK(id, NK1)
   
   --get NOK contacts
   local nokIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'nokIdentifier')
   local nokContacts = sqDB.THS.getNOKContacts(NK1,nokIdentifier)
   trace(nokContacts)

   --get emergency contacts
   local emergencyContactIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'emergencyContactIdentifier')
   local emergencyContacts = sqDB.THS.getNOKContacts(NK1,emergencyContactIdentifier)
   trace(emergencyContacts)

   --get other contacts
   local otherCarerIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'otherCarerIdentifier')
   local otherContacts = sqDB.THS.getNOKContacts(NK1,otherCarerIdentifier)
   trace(otherContacts)
   
   --get guardian contacts
   --get other contacts
   local guardianContactIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'guardianContactIdentifier')
   local guardianContacts = sqDB.THS.getNOKContacts(NK1,guardianContactIdentifier)
   trace(guardianContacts)
   
   --get carers
   local carerIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'carerIdentifier')
   local carerContacts = sqDB.THS.getNOKContacts(NK1,carerIdentifier, 1)
   trace(carerContacts)

   --create nok contact(s)
   if #nokContacts > 0 then   
      trace(#nokContacts)
      for i=1, #nokContacts do
         if nokContacts[i][9]:S() == '' then
            sqDB.THS.updateNKContact(id, nokContacts[i])
         end
      end               
   end

   --create emergency contact(s)
   if #emergencyContacts > 0 then
      --for now add all emergency contacts based on no end date
      trace(#emergencyContacts)
      for i=1, #emergencyContacts do
         if emergencyContacts[i][9]:S() == '' then
            sqDB.THS.updateNKContact(id, emergencyContacts[i])
         end
      end  
   end

   --create other contact(s)
   if #otherContacts > 0 then
      --for now add all other contact based on no end date
      trace(#otherContacts)
      for i=1, #otherContacts do
         if otherContacts[i][9]:S() == '' then
            sqDB.THS.updateNKContact(id, otherContacts[i])
         end
      end  
   end

   --create guardian contact(s)
   if #guardianContacts > 0 then
      --for now add all other contact based on no end date
      trace(#guardianContacts)
      for i=1, #guardianContacts do
         if guardianContacts[i][9]:S() == '' then
            sqDB.THS.updateNKContact(id, guardianContacts[i])
         end
      end  
   end

   --create carer contact(s)
   if #carerContacts > 0 then
      --for now add all other contact based on no end date
      trace(#carerContacts)
      for i=1, #carerContacts do
         if carerContacts[i][9]:S() == '' then
            sqDB.THS.updateNKContact(id, carerContacts[i])
         end
      end  
   end

end

-----------------------------------------------------------------

function sqDB.THS.addUpdateGT(id, GT1)
 
   --get GT contacts
   local gtContacts = {}
   
   for i=1, #GT1 do
      --only process entries with no end date
      if GT1[i][14]:S() == '' then
         sqDB.THS.updateGTContact(id, GT1[i])
      end
   end


end

-----------------------------------------------------------------

function sqDB.THS.removePatientNOKflags(id)

   local sqlStr = "select uai.id as id from users_additional_info uai "..
                  "join users u on u.id = uai.user_id where u.parent = "..conn:quote(id)..
                  " and uai.act_as_nok = 1 "
   local res = sqDB.execute{sql=sqlStr, live=true}

   if #res > 0 then
      
      local inStr = '('
      for i=1, #res do
         inStr = inStr..res[i].id:S()
         if i+1 <= #res then
            inStr = inStr..','
         end
      end
      inStr = inStr..')'

      sqlStr = "update users_additional_info set act_as_nok = 0 "..
      "where id in "..inStr

      local res = sqDB.execute{sql=sqlStr, live=false}

   end

end

-----------------------------------------------------------------

function sqDB.THS.removePatientEmergflags(id)

   local sqlStr = "select uai.id as id from users_additional_info uai "..
                  "join users u on u.id = uai.user_id where u.parent = "..conn:quote(id)..
                  " and uai.emergency_contact = 1 "
   local res = sqDB.execute{sql=sqlStr, live=true}

   if #res > 0 then
      
      local inStr = '('
      for i=1, #res do
         inStr = inStr..res[i].id:S()
         if i+1 <= #res then
            inStr = inStr..','
         end
      end
      inStr = inStr..')'

      sqlStr = "update users_additional_info set emergency_contact = 0 "..
      "where id in "..inStr

      local res = sqDB.execute{sql=sqlStr, live=false}

   end

end

-----------------------------------------------------------------
function sqDB.THS.processDeceasedPatient_OLD(id, PID)
   
   --update the date of death in users_additional_info
   
   if PID == nil or id == '0' then
      return
   end

   local res = sqDB.execute{sql="select count(user_id) as cnt, user_id from users_additional_info where user_id = "..
      id, live=true}
   
   trace(PID[29]:S())
   local dod = dateparse.parse(PID[29]:D())
   local dodString = string.sub(tostring(dod),1,10)
   
   if res[1].cnt:S() == '0' then
      
      sqDB.execute{sql="insert into users_additional_info (user_id, deceased_date) " ..
         "values ("..
         id .. "," ..
         conn:quote(dodString)..
         ")"
         , live=false}
   else
      sqDB.execute{sql="update users_additional_info set " ..
         "deceased_date = "..conn:quote(dodString)..
         " where user_id = "..id
         , live=false}
   end
   
   sqDB.execute{sql="update users set " ..
         "status = 'deceased'"..
         " where id = "..id
         , live=false}
   
   sqUtils.printLog("DOD details updated!")
   
   --ensure communications are switched off
   
   --switch off emails
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 1 and channel_id = 2 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      sqDB.execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "1,2,0)"
      , live=false}
   end
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 2 and channel_id = 2 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      sqDB.execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "2,2,0)"
      , live=false}
   end
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 3 and channel_id = 2 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      sqDB.execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "3,2,0)"
      , live=false}
   end
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 4 and channel_id = 2 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      sqDB.execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "4,2,0)"
      , live=false}
   end
   
   -- switch off SMS
   
   local res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 1 and channel_id = 1 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      sqDB.execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "1,1,0)"
      , live=false}
   end
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 2 and channel_id = 1 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      sqDB.execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "2,1,0)"
      , live=false}
   end
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 3 and channel_id = 1 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      sqDB.execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "3,1,0)"
      , live=false}
   end
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
      conn:quote(id).." and event_id = 4 and channel_id = 1 ", live=true}
   
   if #res > 0 then
      trace(res[1].id:S())
      es = sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
      conn:quote(res[1].id:S()), live=false}
   else
      sqDB.execute{sql=
      "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
      "values (null,"..
      conn:quote(id)..","..
      "4,1,0)"
      , live=false}
   end
  
end


-----------------------------------------------------------------
function sqDB.THS.processDeceasedPatient(userID, PID)
   
   --update the date of death in users_additional_info
   
   if PID == nil or userID == '0' then
      return
   end

   local res = sqDB.execute{sql="select count(user_id) as cnt, user_id from users_additional_info where user_id = "..
      userID, live=true}
   
   trace(PID[29]:S())
   local dod = dateparse.parse(PID[29]:D())
   local dodString = string.sub(tostring(dod),1,10)
   
   if res[1].cnt:S() == '0' then
      
      sqDB.execute{sql="insert into users_additional_info (user_id, deceased_date) " ..
         "values ("..
         userID .. "," ..
         conn:quote(dodString)..
         ")"
         , live=false}
   else
      
      sqDB.execute{sql="update users_additional_info set " ..
         "deceased_date = "..conn:quote(dodString)..
         " where user_id = "..userID
         , live=false}
   
   end
   
   sqDB.execute{sql="update users set " ..
         "status = 'deceased'"..
         " where id = "..userID
         , live=false}
   
   sqUtils.printLog("DOD details updated!")
   
   --ensure communications are switched off
   
   sqUtils.printLog("DOD: Updating communication preferences...")
   
   local res = 0
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_channels where `key` = 'email'", live=true}
   local channelID_emails = res[1].id:S()
   
   res = sqDB.execute{sql="select count(id) as cnt, id from user_notification_channels where `key` = 'sms'", live=true}
   local channelID_sms = res[1].id:S()
   
   res = sqDB.execute{sql="select * from user_notification_events", live=true}
   local eventCount = #res
   
   trace(eventCount)
   
   --loop through all event typoes but only disable emails & sms - FOR NOW
   
   for i=1, eventCount do
      
      -- this is open to additional keys
      if res[i].key:S() == 'appointment.reminders' or res[i].key:S() == 'post_appointment.communication' then

         local eventID = res[i].id:S()
         
         --disable the emails
         
         local res2 = 0

         res2 = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
            userID.." and event_id = "..eventID.." and channel_id = "..channelID_emails, live=true}

         if res2[1].cnt:S() ~= '0' then

            sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
               res2[1].id:S(), live=false}

         else

            sqDB.execute{sql=
               "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
               "values (null,"..
               userID..","..
               eventID..","..
               channelID_emails..",0)"
               , live=false}

         end

         -- disable the SMS
         
         res2 = sqDB.execute{sql="select count(id) as cnt, id from user_notification_preferences where user_id = "..
            userID.." and event_id = "..eventID.." and channel_id = "..channelID_sms, live=true}

         if res2[1].cnt:S() ~= '0' then

            sqDB.execute{sql="update user_notification_preferences set enabled = 0 where id = "..
               res2[1].id:S(), live=false}

         else

            sqDB.execute{sql=
               "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
               "values (null,"..
               userID..","..
               eventID..","..
               channelID_sms..",0)"
               , live=false}

         end
         
      end

   end
   
   sqUtils.printLog("DOD: Communication preferences updated!")
  
end


-----------------------------------------------------------------
function sqDB.THS.getPatientIdByMrn(mrn, facilityID)
   
   local PatientId = sqDB.execute{sql="select count(id), id from users where mrn = " .. conn:quote(mrn) ..
      " and mrn <> '' and id in (select user_id from facility_user where facility_id = "..facilityID..")", live=true}

   if PatientId[1]['count(id)']:S() == '0' then
      return '0'
   else
      return PatientId[1].id:S()
   end
   
end

-----------------------------------------------------------------
function sqDB.THS.mergePatient(id, minorid, facilityID)
   
   sqUtils.printLog("Merging patient...")
   
   -- Validate that major and minor patients exist and 
   -- patients have facility user records for this facility
   local isMajorPatientValid = false
   if (id ~= '0') then
      trace(id)
      local facilityUserCheck = sqDB.execute{sql=
         "select count(id), id from users " ..
         "where id = " .. conn:quote(id) .. 
         " and id in (select user_id from facility_user where facility_id in (" ..facilityID.."))", live=true}
      if facilityUserCheck[1]['count(id)']:S() == '0' then
         error("Unable to process merge request, major number not found")
      else
         isMajorPatientValid = true
         trace(isMajorPatientValid)
      end
   else
      error("Unable to process merge request, major patient id = 0")
   end

   local isMinorPatientValid = false
   if (minorid ~= '0') then
      trace(minorid)
      local facilityUserCheck = sqDB.execute{sql=
         "select count(id), id from users " ..
         "where id = " .. conn:quote(minorid) .. 
         " and id in (select user_id from facility_user where facility_id in (" ..facilityID.."))", live=true}
      if facilityUserCheck[1]['count(id)']:S() == '0' then
         error("Unable to process merge request, minor number not found")
      else
         isMinorPatientValid = true
         trace(isMinorPatientValid)
      end
   else
      trace(minorid)
      error("Unable to process merge request, minor patient id  = 0")
   end
   
   -- DD Addedd 150123
   -- Desc: Added in the correct functionality for merging patients
   -- We need to merge referrals, reservations and patient flow
   --Referrals First
   local MergeRefs = sqDB.execute{sql=
      "select count(id), id from ewaiting_referrals where user_id = " .. minorid,live=true}
   if MergeRefs[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergeRefIDs = sqDB.execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as referralIds from ewaiting_referrals where user_id = " .. minorid,live=true}
      --
      if MergeRefIDs[1]['referralIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, "..
         id ..", "..
         "'{HL7 Merge - Minor ID: " .. minorid .. " ,Major ID: " .. id ..
         " , ReferralIDs Merged: " .. MergeRefIDs[1]['referralIds'] ..
         "}')"
         sqUtils.printLog("mergePatient().Insert merged user details: " .. sql)
         sqDB.execute{sql=sql, live=false}
      end
      --
      local sql ="update ewaiting_referrals set user_id = " ..id..
      " where facility_id = " .. conn:quote(facilityID) ..
      " and user_id = " .. minorid
      sqUtils.printLog(" mergePatient(). Update ewaiting_referrals: " .. sql)
      sqDB.execute{sql=sql, live=false}
      --
   end
   --
   --Reservations Second
   local MergeRes = sqDB.execute{sql=
      "select count(id), id from reservation where user_id = " .. minorid,live=true}
   if MergeRes[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergeResIDs = sqDB.execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as reservationIds from reservation " ..
         "where user_id = " .. minorid,live=true}
      --
      if MergeResIDs[1]['reservationIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, ".. id ..", "..
         "'{HL7 Merge - Minor ID: [" .. minorid .. "] Major ID: [" .. id ..
         "] ReservationIDs Merged: [" .. MergeResIDs[1]['reservationIds'] ..
         "]}')"
         sqDB.execute{sql=sql, live=false}
      end
      --
      local sql ="update reservation set user_id = " ..id..
      " where facility_id in " ..
      "(select id from facilities " ..
      "where id = " .. conn:quote(facilityID) .. 
      " or parent_id = " .. conn:quote(facilityID) .. ")" .. 
      " and user_id = " .. minorid
      sqDB.execute{sql=sql, live=false}
      --
   end
   --
   --Patient Flow Third
   local MergePatFlow = sqDB.execute{sql=
      "select count(id), id from patient_flow where user_id = " .. minorid,live=true}
   if MergePatFlow[1]['count(id)']:S() ~= '0' then
      --Log old ids into the merge users table
      local MergePatFlowIDs = sqDB.execute{sql=
         "select IFNULL(CAST(GROUP_CONCAT(id SEPARATOR ',') AS CHAR),'') as patflowIds from patient_flow where user_id = " .. minorid,live=true}
      --
      if MergePatFlowIDs[1]['patflowIds']:S() ~= '0' then
         local sql ="insert into merged_users values (" ..
         "null, now(), 0, "..
         id ..", "..
         "'{HL7 Merge - Minor ID: [" .. minorid .. "] Major ID: [" .. id ..
         "] PatientFlowIDs Merged: [" .. MergePatFlowIDs[1]['patflowIds'] ..
         "]}')"
         sqDB.execute{sql=sql, live=false}
      end
      --
      local sql ="update patient_flow set user_id = " ..id..
      " where facility_id in " ..
      "(select id from facilities " ..
      "where id = " .. conn:quote(facilityID) .. 
      " or parent_id = " .. conn:quote(facilityID) .. ")"..
      " and user_id = " .. minorid
      sqDB.execute{sql=sql, live=false}
      --
   end
	
   -- Remove the minor one from the Facility User view
   local sql ="delete from facility_user " ..
   "where "..
   "facility_id = " .. conn:quote(facilityID) .. 
   " and user_id = " .. minorid
   sqDB.execute{sql=sql, live=false}
   --
end  

-----------------------------------------------------------------
function sqDB.THS.createAppointment(id, facilityID, SCH, clinCode, reasonID, PV2, PV1, AIP, comment)

   sqUtils.printLog("Creating appointment...")
   
   if sqUtils.isEmpty(reasonID) then
      reasonID = 0
   end

   --extract the res date and time
   --local mrn = SCH[1][1]:S()
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)

   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   --local DXCId = SCH[1][1]:S()
   local DXCId = SCH[2][1]:S()
   --local apptType = PV2[12]:S()
   local apptType = SCH[8][1]:S()
   local apptPriority = PV2[25]:S()
   local apptCategory = PV1[18]:S()
   local appReason = 1
   local DXCReason = SCH[7][1]:S()
   local DXCAIPID = AIP[3][1]:S()
   local DXCSpeciality = AIP[4][1]:S()
   local refID = SCH[4][1]:S()
   local DXCSession = SCH[5][1]:S()
   local episodeNumber = SCH[3]:S()

   local appDuration = SCH[9]:S()
   if appDuration  == '' then
      appDuration = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultAppDuration')
   end

   -- 
   --Check if an appointment already exists for this patient
   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and reservation_date = " 
      .. conn:quote(opdAppointmentString) .. " and status = 'active' and reservation_time = " ..  conn:quote(opdAppointmentTimeString), live=true}

   if ResId[1]['count(id)']:S() == '0'
      then

      --insert the new appointment
      sqDB.execute{sql="insert into reservation (facility_id, user_id, facility_type, consultant_id, location_pref, "..
         "reservation_date, reservation_time, reservation_end_time, google_cal_id, reason_id,hl7_id, recurrence,date,status,attended_by,".. 
         "is_attended,booking_user_id, source, print,stats, quality,capacity_sync, is_urgent, comments, episode_no) " .. 
         "values ("..
         facilityID..","..
         id..","..
         "'f',0"..","..
         conn:quote(DXCId).."," ..
         conn:quote(opdAppointmentString)..","..
         conn:quote(opdAppointmentTimeString)..","..
         "substring(convert("..conn:quote(opdAppointmentEndTimeString)..", time) + interval "..appDuration.." minute, 1, 5)"..","..
         --    conn:quote(opdAppointmentEndTimeString)..","..
         conn:quote(clinCode)..","..
         reasonID..","..
         conn:quote(refID)..","..
         "'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0,"..
         conn:quote(comment)..","..
         conn:quote(episodeNumber)..
         ")"
         , live=false}

      local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
      
      local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")

      --Add A user audit record for the app creation
      sqDB.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
         "values (null, now(),"..sqUser..", 'appointment.created',"..
         result[1].id:S()..","..id..",null)"
         , live=false}      

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'TYPE'"..","..
         conn:quote(apptType)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'PRIORITY'"..","..
         conn:quote(apptPriority)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'CATEGORY'"..","..
         conn:quote(apptCategory)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'REASON'"..","..
         conn:quote(DXCReason)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'AIPID'"..","..
         conn:quote(DXCAIPID)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'DXCSPEC'"..","..
         conn:quote(DXCSpeciality)..
         ")"
         , live=false}

      sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
         "values ("..
         result[1].id:S()..","..
         "'SESSION'"..","..
         conn:quote(DXCSession)..
         ")"
         , live=false}

   else
      sqDB.execute{sql="update reservation set location_pref = " ..conn:quote(DXCId)..", google_cal_id = " ..conn:quote(clinCode).. 
         ", hl7_id = "..conn:quote(refID)..", source = 'h' where id = "..ResId[1].id, live=false}   

      -- check if the reservation_Ipm records exist - if not create them
      local IPMResId = sqDB.execute{sql="select count(res_id), res_id from reservation_ipm where res_id = " .. ResId[1].id, live=true}
      --
      if IPMResId[1]['count(res_id)']:S() == '0' then

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'TYPE'"..","..
            conn:quote(apptType)..
            ")"
            , live=false}

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'PRIORITY'"..","..
            conn:quote(apptPriority)..
            ")"
            , live=false}

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'CATEGORY'"..","..
            conn:quote(apptCategory)..
            ")"
            , live=false}

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'REASON'"..","..
            conn:quote(DXCReason)..
            ")"
            , live=false}

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'AIPID'"..","..
            conn:quote(DXCAIPID)..
            ")"
            , live=false}

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'DXCSPEC'"..","..
            conn:quote(DXCSpeciality)..
            ")"
            , live=false}

         sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            ResId[1].id:S()..","..
            "'SESSION'"..","..
            conn:quote(DXCSession)..
            ")"
            , live=false}
         --
      end
      --
   end
end

-----------------------------------------------------------------
function sqDB.THS.updateAppointment(id, facilityID, SCH, clinCode, reasonID, PV2, PV1, AIP)
   
   sqUtils.printLog("Updating appointment...")
   
   if sqUtils.isEmpty(reasonID) then
      reasonID = 0
   end
   
   --extract the res date and time
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
      
   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   --local DXCId = SCH[1][1]:S()
   --local apptType = PV2[12]:S()
   local apptPriority = PV2[25]:S()
   local apptCategory = PV1[18]:S()
   local appReason = 1
   local refID = SCH[4][1]:S()
   local DXCId = SCH[2][1]:S()
   local apptType = SCH[8][1]:S()
   local DXCReason = SCH[7][1]:S()
   local DXCAIPID = AIP[3][1]:S()
   local DXCSpeciality = AIP[4][1]:S()
   local DXCSession = SCH[5][1]:S()
   local episodeNumber = SCH[3]:S()
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
   
   local appDuration = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultAppDuration')
   
   --local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId), live=true}
   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and reservation_date = " .. conn:quote(opdAppointmentString) ..
      " and status = 'active' and reservation_time = " ..  conn:quote(opdAppointmentTimeString), live=true}

   if ResId[1]['count(id)']:S() == '0'
      then
      
      --check if an existing appointment exists
      local ResIdUpd = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId) ..
         " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0'
      then
         sqDB.execute{sql="update reservation set " .. 
            "status = 'cancelled'" ..
            " where location_pref = " .. conn:quote(DXCId).." and " ..
            "user_id = " .. id
           , live=false}
      end
      
      --insert the new appointment
      sqDB.execute{sql="insert into reservation (facility_id, user_id, facility_type, consultant_id, location_pref, reservation_date, reservation_time,"..
           "reservation_end_time, google_cal_id, reason_id,hl7_id, recurrence,date,status,attended_by, is_attended,booking_user_id, source, print,stats, "..
           "quality,capacity_sync, is_urgent, episode_no) " .. 
            "values ("..
            facilityID..","..
            id..","..
            "'f',0"..","..
            conn:quote(DXCId).."," ..
            conn:quote(opdAppointmentString)..","..
            conn:quote(opdAppointmentTimeString)..","..
            "substring(convert("..conn:quote(opdAppointmentEndTimeString)..", time) + interval "..appDuration.." minute, 1, 5)"..","..
            --conn:quote(opdAppointmentEndTimeString)..","..
            conn:quote(clinCode)..","..
            reasonID..","..
            conn:quote(refID)..","..
            "'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0,"..
            conn:quote(episodeNumber)..
            ")"
            , live=false}  
      
            local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
      
            --Add A user audit record for the app creation
            sqDB.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),"..sqUser..", 'appointment.created',"..
                 result[1].id:S()..","..id..",null)"
            , live=false}
      
            sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'TYPE'"..","..
            conn:quote(apptType)..
             ")"
            , live=false}
      
            sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'PRIORITY'"..","..
            conn:quote(apptPriority)..
             ")"
            , live=false}
      
            sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
            "values ("..
            result[1].id:S()..","..
            "'CATEGORY'"..","..
            conn:quote(apptCategory)..
             ")"
            , live=false}
      
            sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               result[1].id:S()..","..
               "'REASON'"..","..
               conn:quote(DXCReason)..
               ")"
               , live=false}

               sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               result[1].id:S()..","..
               "'AIPID'"..","..
               conn:quote(DXCAIPID)..
               ")"
               , live=false}

               sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               result[1].id:S()..","..
               "'DXCSPEC'"..","..
               conn:quote(DXCSpeciality)..
               ")"
               , live=false}
         
               sqDB.execute{sql="insert into reservation_ipm (res_id, ipm_name, ipm_value) " .. 
               "values ("..
               result[1].id:S()..","..
               "'SESSION'"..","..
               conn:quote(DXCSession)..
               ")"
               , live=false}
      --
   else 

      sqDB.execute{sql="update reservation set " .. 
         "reservation_date = " .. conn:quote(opdAppointmentString)..","..
         "reservation_end_time = substring(convert("..conn:quote(opdAppointmentTimeString)..", time) + interval "..appDuration.." minute, 1, 5)"..","..
         --"reservation_time = " .. conn:quote(opdAppointmentTimeString)..","..
         "google_cal_id = " .. conn:quote(clinCode)..","..
         "hl7_id = " .. conn:quote(refID)..","..
         "modified = now(), modified_source = 'h'"..","..
         "reservation_time = " .. conn:quote(opdAppointmentEndTimeString)..
         " where location_pref = " .. conn:quote(DXCId).." and status = 'active' and " ..
         "user_id = " .. id
         , live=false}


      --update the ewaiting_reservations table
      
      local resID = ResId[1].id:S()
      local res = sqDB.execute{sql="select count(id), id from ewaiting_referrals where control_id = "..conn:quote(refID), live=true}
      
      if sqUtils.isEmpty(refID) then
         sqUtils.printLogW("updateAppointment: Referral ID not found for control id: "..refID)
      end
      
      refID = res[1].id:S()
      
      res = sqDB.execute{sql="select count(ewaiting_referral_id), ewaiting_referral_id from ewaiting_reservations where res_id = "..conn:quote(resID), live=true}
      local origRefID = res[1].ewaiting_referral_id:S()
      
      --update the referral id for the appointment  
      
      if not sqUtils.isEmpty(refID) then

         sqDB.execute{sql="update ewaiting_reservations set " .. 
            "ewaiting_referral_id = " .. conn:quote(refID)..
            " where res_id = " .. conn:quote(resID)
            , live=false}


         --PB 22.05.24 - IR-303 : if we are change referral id and referral status originally in follow_up then move to assigned
         if origRefID ~= refID then

            sqDB.execute{sql="update ewaiting_referrals set " .. 
               "status = 'assigned' "..
               " where status = 'follow_up' and id = " .. conn:quote(refID)
               , live=false}

         end 
         
      end

      --Add A user audit record for the app creation
      sqDB.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
         "values (null, now(),"..sqUser..", 'appointment.rescheduled',"..
         conn:quote(ResId[1]['id']:S())..","..id..",null)"
         , live=false}       

   end
    --
end

-----------------------------------------------------------------
function sqDB.THS.updatePatientFlow(id, facilityID, SCH, clinCode, reasonID)
   
   sqUtils.printLog("Updating patient flow...")
   
   --extract the res date and time
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
      
   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   local DXCId = SCH[2][1]:S()
   local appReason = 1
   
   local ResId = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId), live=true}

   if ResId[1]['count(id)']:S() ~= '0'
      then
      --
      local ResId2 = sqDB.execute{sql="select count(id), id from patient_flow where stage = 'CHECK-IN' and res_id = " .. conn:quote(ResId[1]['id']:S()), live=true}
      
   
      if ResId2[1]['count(id)']:S() == '0' then
         
         --PB 12.02.24
       local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
         
         --insert the new patient flow
         sqDB.execute{sql="insert into patient_flow (id, timestamp, facility_id, user_id, res_id, sq_user_id, stage, nurse, current_stage, bay, source) " .. 
            "values (null, now(),"..
            facilityID..","..
            id..","..
            conn:quote(ResId[1]['id']:S()).."," ..
            conn:quote(sqUser)..",'CHECK-IN', NULL, 'Y', NULL, 'hl7'"..
            ")"
            , live=false}
       end
    end
    --
end

-----------------------------------------------------------------
function sqDB.THS.cancelAppointment(id, facilityID, SCH, reasonID)
   
   sqUtils.printLog("Cancelling appointment...")
   
   --extract the res date and time
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
      
   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   --end
   --get the DXC identifier
   local DXCId = SCH[2][1]:S()
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
   
   local ResIdUpd = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId) ..
         " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0'
      then
   
      sqDB.execute{sql="update reservation set " .. 
         "reservation_date = " .. conn:quote(opdAppointmentString)..","..
         "reservation_time = " .. conn:quote(opdAppointmentTimeString)..","..
         "reservation_end_time = " .. conn:quote(opdAppointmentEndTimeString)..","..
         "status = 'cancelled'" ..","..
         "modified = now(), modified_source = 'h'"..
         " where location_pref = " .. conn:quote(DXCId).." and " ..
         "user_id = " .. id
         , live=false}
      
      --Add A user audit record for the app creation
      sqDB.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),"..sqUser..", 'appointment.cancelled',"..
                 conn:quote(ResIdUpd[1]['id']:S())..","..id..",null)"
            , live=false}  
      
      end
end

-----------------------------------------------------------------
function sqDB.THS.dnaAppointment(id, facilityID, SCH, reasonID)
   
   sqUtils.printLog("DNA appointment...")
  
   --get the DXC identifier
   local DXCId = SCH[2][1]:S()
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
   
   --[[
   if DXCId == '' then
      DXCId = SCH[2][1]:S()
   end
   ]]--
   
   local ResIdUpd = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId) ..
         " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0'
      then
   
      sqDB.execute{sql="update reservation set " .. 
         "is_attended = 'no'," ..
         "modified = now(), modified_source = 'h'"..
         " where id = " .. conn:quote(ResIdUpd[1]['id']:S())
         , live=false}
      
      --Add A user audit record for the app creation
      sqDB.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),"..sqUser..", 'appointment.dna',"..
                 conn:quote(ResIdUpd[1]['id']:S())..","..id..",null)"
            , live=false}  
      
      end
end

-----------------------------------------------------------------
function sqDB.THS.getPatientReferral(id,facilityID,controlID)
   
   sqUtils.printLog("getting patient referral...")
   
   local referralCount = sqDB.execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. 
      facilityID .. " and user_id = " .. id.." and control_id = "..conn:quote(controlID), live=true}
   
   if referralCount[1]['count(id)']:S() == '0'
      then
      return '0'
      else
      return referralCount[1].id:S()
   end
   
end

-----------------------------------------------------------------
function sqDB.THS.createPatientReferral(id,facilityID, clinCode, RF1, PRD, consID)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
   --PB 06.12.23 : account class
   --PB 12.03.2024 : Paddy requested that account class be picked up from the A05 instead of REF message
   --local accClass = RF1[5][2]:S() 
   
   local refID = RF1[6][1]:S()
   
   local referralCount = sqDB.execute
   {sql="select count(id), id from ewaiting_referrals where facility_id = " .. facilityID ..
      " and user_id = " .. id .. " and control_id = "..conn:quote(refID), live=true}

   --if the refarral already exists then skip
   if referralCount[1]['count(id)']:S() ~= '0' then
      sqUtils.printLog("Referral already exists, Skipping creation...")
      return
   end
   
   --referral does not exist so continue
   
   local facilityConditionTag = 0
   local facilitySpeciality = 0
   local facilityCategory = 0
   local FacilitySourceId = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultFacilitySourceId')
   local refExpiry = RF1[8]:S()
   local waitingStatus = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultReferralWaitingStatus')
   local refDateReferred = ''

   if not isEmpty(RF1[9]:S()) then
      refDateReferred = sqUtils.parseDate(RF1[9])
   end

   --Establish the Priority of the Referral
   local refPriority = RF1[2][1]:S()
   
   --PB 07.05.2024 : IR-285
   local refPriorityName = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'refPriorityMap_inbound',RF1[2][2]:S())  
   if sqUtils.isEmpty(refPriorityName) then
      sqUtils.printLogW("Priority mapping not found for: "..RF1[2][2]:S().." in mapping: refPriorityMap_inbound")
      refPriorityName = RF1[2][2]:S()
   end
   
   local refSourceName = ''
   if PRD ~= '' then

      if PRD[1][1]:S() == 'PIMS' then
         refSourceName = PRD[4][9]:S()
      else
         refSourceName = PRD[2][5]:S()..' '..PRD[2][2]:S()..' '..PRD[2][1]:S()
      end
   
   end
   
   local referralPriority = sqDB.execute
   {sql="select count(id), id from ewaiting_categories where facility_id = " .. facilityID ..
      " and name = " .. conn:quote(refPriorityName), live=true}
   
   if referralPriority[1]['count(id)']:S() ~= '0' then
      facilityCategory = referralPriority[1]['id']:S()
   end
   
   --Establish the Referral Speciality
   local refSpeciality = RF1[3][2]:S()
   refSpeciality = sqUtils.checkControlCharacters(refSpeciality,'1')
   
   local referralSpeciality = sqDB.execute{sql="select count(id), id from specialities where name = " 
      .. conn:quote(refSpeciality), live=true}
   
   if referralSpeciality[1]['count(id)']:S() ~= '0' then
      facilitySpeciality = referralSpeciality[1]['id']:S()
   end
   
   --Establish the ConditionTag
   local refCondition = RF1[3][2]:S()
   
   --replace control characters
   refCondition = sqUtils.checkControlCharacters(refCondition,'2')
   trace(refCondition)
   
   local refConditionRes = sqDB.execute{sql="select count(id), id from ewaiting_condition_tags where name = " 
      .. conn:quote(refCondition), live=true}
   
   if refConditionRes[1]['count(id)']:S() ~= '0' then
      facilityConditionTag = refConditionRes[1]['id']:S()
   end
  
   local refExpiry =sqUtils.parseDateDMY(RF1[8])
   
   --if secondary referral then place in followup
   local secondReferralStatus = sqUtils.getMappedValue(sqCfg.extra_params,"secondReferralStatus")
   if RF1[1][1]:S() == secondReferralStatus then
      waitingStatus = sqUtils.getMappedValue(sqCfg.extra_params,"secondReferralTab")
   end
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
      
   --speciality_id, referral_source_id,"..   "referral_source_name, status, `condition`, control_id, \
   sql="insert into ewaiting_referrals (facility_id, user_id, date_referred, category_id, \
   speciality_id, referral_source_id,"..   "referral_source_name, status, control_id, \
   comments, created, created_by) " ..
   "values ("..
   facilityID..","..
   id..","..
   conn:quote(refDateReferred)..","..
   facilityCategory..","..
   facilitySpeciality..","..
   FacilitySourceId..","..
   conn:quote(refSourceName)..","..
   conn:quote(waitingStatus)..","..
   --conn:quote(facilityConditionTag)..","..
   conn:quote(refID)..","..
   "'HL7 Referral'"..","..
   "now()"..","..
   sqUser..
   ")"

   sqDB.execute{sql=sql, live=false}
   sqUtils.printLog("createPatientReferral: " .. sql)

   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
   
   if facilityConditionTag ~= 0 then
      sql = "insert into ewaiting_referral_condition_tags values (" .. result[1].id .."," .. facilityConditionTag .. ")"
      sqUtils.printLog("createPatientReferral:" ..sql)
      sqDB.execute{sql=sql, live=false}
   else
      trace(refCondition)
      sqUtils.printLog("Failed to write to 'ewaiting_referral_condition_tags', "..refCondition.." not found!")
      iguana.logWarning("Failed to write to 'ewaiting_referral_condition_tags', "..refCondition.." not found!")
   end
   
   --PB 02.01.23 : SQAU-154 - Adding to ewaiting_referrals_additional_info
   --Need to create an entry in ewaiting_referrals_additional_info and set the consultant_id = the parent facility id
   
   
   -- pass consultant id here
   
   local refId = result[1].id:S()
   local res = sqDB.execute{sql=
      "select count(id) as cnt from ewaiting_referrals_additional_info where consultant_id = " .. conn:quote(consID) ..
      " and referral_id = " .. conn:quote(refId) , live=true}
	--if the consultant entry for this referral does not already exist then add it
   if res[1].cnt:S() == '0' then   
      sqDB.execute{sql=
         "insert into ewaiting_referrals_additional_info (id, referral_id, created_at, updated_at, consultant_id) " ..
         "values (null," .. refId ..",now(), now(), " .. conn:quote(consID) ..")", live=false}      
   end
   
   -- 

   sqDB.THS.addUpdateReferralExpiryDate(facilityID, result[1].id:S(), refExpiry)
   
   --PB 12.03.2024 : Paddy requested that account class be picked up from the A05 instead of REF message
   --sqDB.THS.addUpdateReferralAccountClass(facilityID, result[1].id:S(), accClass)
   
   
   --if secondary referral then set a follow up reason
   local secondReferralStatus = sqUtils.getMappedValue(sqCfg.extra_params,"secondReferralStatus")
   if RF1[1][1]:S() == secondReferralStatus then

      local followupReasonID = sqUtils.getMappedValue(sqCfg.extra_params,"secondReferralReason")

      local res = sqDB.execute{sql=
         "select count(id) as cnt from ewaiting_referral_follow_up_reasons where ewaiting_referral_id = " ..
         conn:quote(refId) , live=true}

      --if the consultant entry for this referral does not already exist then add it
      if res[1].cnt:S() == '0' then   
         sqDB.execute{sql=
            "insert into ewaiting_referral_follow_up_reasons (id, ewaiting_referral_id, follow_up_reason_id, created_at) " ..
            "values (null," .. refId .."," .. conn:quote(followupReasonID) .. ", now() )", live=false}  
      else
         sqDB.execute{sql=
            "update ewaiting_referral_follow_up_reasons set follow_up_reason_id = "..conn:quote(followupReasonID) ..
            " where ewaiting_referral_id = "..conn:quote(refId), live=false}
      end
   
   end
   
   sqUtils.printLog("Creation of referral completed!") 
   sqUtils.printLog("Exiting "..getCurrentFunctionName().."...")
   
   return result[1].id
   
end
  
-----------------------------------------------------------------
function sqDB.THS.addUpdateReferralExpiryDate(facId, refId, expDate)

   local res = sqDB.execute{sql="select id from ewaiting_custom_fields where facility_id = "..
      conn:quote(facId).." and field_name = 'Expiry Date';", live=true}

   if #res == 0 then
      error("Expiry Date not mapped on ewaiting_custom_fields for facility "..facId)
   end
   
   local customId = res[1]:S()

   res = sqDB.execute{sql="select id from ewaiting_referral_custom_field_values where referral_id = "..
      conn:quote(refId).." and custom_field_id = "..customId, live=true}
   
   if #res == 0 then
      
      --insert new expiry date
      sqDB.execute{sql='insert into ewaiting_referral_custom_field_values (referral_id,value,value_str,custom_field_id) '..
         ' values ('..refId..', """'..expDate..
         '""", "'..expDate..'",'..customId..') ', live=false}
   else
      
      --update the expiry date
      local id = res[1]:S()
      res = sqDB.execute{sql='update ewaiting_referral_custom_field_values '..
         ' SET value = """'..expDate..
         '""" , value_str = "'..expDate..'" where id = '..id, live=false}
   end   
   
end

-----------------------------------------------------------------
function sqDB.THS.addUpdateReferralAccountClass(facId, refId, accClass)

   local res = sqDB.execute{sql="select id from ewaiting_custom_fields where facility_id = "..
      conn:quote(facId).." and field_name = 'Account Class';", live=true}

   if #res == 0 then
      error("Account Class not mapped on ewaiting_custom_fields for facility "..facId)
   end
   
   local customId = res[1]:S()
 
   res = sqDB.execute{sql="select id from ewaiting_referral_custom_field_values where referral_id = "..
      conn:quote(refId).." and custom_field_id = "..customId, live=true}
   
   if #res == 0 then
      
      --insert new account class
      sqDB.execute{sql='insert into ewaiting_referral_custom_field_values (referral_id,value,value_str,custom_field_id) '..
         ' values ('..refId..', """'..accClass..
         '""", '..conn:quote(accClass)..','..customId..') ', live=false}
   else
      
      --update the account class
      local id = res[1]:S()
      res = sqDB.execute{sql='update ewaiting_referral_custom_field_values '..
         ' SET value = """'..accClass..
         '""" , value_str = '..conn:quote(accClass)..' where id = '..id, live=false}
   end   
   --
   
end

-----------------------------------------------------------------
function sqDB.THS.addUpdateReferralConditionType(facId, refId, cpcDesc)

   local conditionType = ''
   
   local res = sqDB.execute{sql="select count(id) as cnt, id from ewaiting_condition_tags where name = " 
      .. conn:quote(cpcDesc), live=true}
   
   if res[1]['cnt']:S() ~= '0' then
      conditionType = res[1]['id']:S()
   end

   if sqUtils.isEmpty(conditionType) then
       sqUtils.printLogW("Condition type not mapped on ewaiting_condition_tags table")
      return
   end
   
   sql = "select referral_id from ewaiting_referral_condition_tags where referral_id = " .. conn:quote(refId)
   res = sqDB.execute{sql=sql, live=true}
   
   if #res == 0 then
      
      --insert condition type
      sql = "insert into ewaiting_referral_condition_tags (referral_id, condition_tag_id) values (" .. 
              res[1].referral_id .."," .. conditionType .. ")"
      sqDB.execute{sql=sql, live=false}
   
   else
      
      --update the condition type
      sql = "update ewaiting_referral_condition_tags set condition_tag_id = "..conditionType..
            " where referral_id = "..conn:quote(refId)
      sqDB.execute{sql=sql, live=false}
     
   end   
   
end

-----------------------------------------------------------------
function sqDB.THS.addUpdateWLComments(patientID, facilityID, controlID, comments)

   sqUtils.printLog("Updating waitng list comments...")
   
   local wlComment = "Waiting List Comment:\n\n"..comments
   
   local suppressFacilityCheck = sqUtils.getMappedValue(sqCfg.extra_params,"suppressReferralFacilityCheck")

   local sqlSTR = "select count(id), id from ewaiting_referrals where facility_id = " .. 
      facilityID .. " and user_id = " .. patientID.." and control_id = "..conn:quote(controlID)
   
   if suppressFacilityCheck == '1' then
      sqlSTR = "select count(id), id from ewaiting_referrals where " .. 
      " user_id = " .. patientID.." and control_id = "..conn:quote(controlID)
   end
   
   local referralCount = sqDB.execute{sql=sqlSTR, live=true}
 
   if referralCount[1]['count(id)']:S() ~= '0' then

      local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")

      local res = sqDB.execute{sql="select id from ewaiting_referrals_condition_notes where ewaiting_referral_id = "..
         referralCount[1].id:S().." and condition_note like 'Waiting List Comment:%'" , live=true}

      if #res == 0 then

         sqDB.execute{sql=
            "insert into ewaiting_referrals_condition_notes (ewaiting_referral_id, sq_user_id, condition_note ) " ..
            "values (" .. referralCount[1].id ..", "..conn:quote(sqUser)..","..conn:quote(wlComment)..")", 
            live=false}

      else

         sqDB.execute{sql=
            "update ewaiting_referrals_condition_notes set "..
            " condition_note = "..conn:quote(wlComment)..","..
            " updated_at = now() "..
            " where id = "..res[1].id:S(), 
            live=false}

      end

   end

end
   
-----------------------------------------------------------------
function sqDB.THS.addUpdateReferralComments(patientID, facilityID, controlID, comments)

   sqUtils.printLog("Updating referral comments...")
   
   local refComment = "Referral Comment:\n\n"..comments
   
   local suppressFacilityCheck = sqUtils.getMappedValue(sqCfg.extra_params,"suppressReferralFacilityCheck")

   local sqlSTR = "select count(id), id from ewaiting_referrals where facility_id = " .. 
      facilityID .. " and user_id = " .. patientID.." and control_id = "..conn:quote(controlID)
   
   if suppressFacilityCheck == '1' then
      sqlSTR = "select count(id), id from ewaiting_referrals where " .. 
      " user_id = " .. patientID.." and control_id = "..conn:quote(controlID)
   end
   
   local referralCount = sqDB.execute{sql=sqlSTR, live=true}

   if referralCount[1]['count(id)']:S() ~= '0' then

      local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")

      local res = sqDB.execute{sql="select id from ewaiting_referrals_condition_notes where ewaiting_referral_id = "..
         referralCount[1].id:S().." and condition_note like 'Referral Comment:%'", live=true}

      if #res == 0 then
         
         sqDB.execute{sql=
            "insert into ewaiting_referrals_condition_notes (ewaiting_referral_id, sq_user_id, condition_note ) " ..
            "values (" .. referralCount[1].id ..", "..conn:quote(sqUser)..","..conn:quote(refComment)..")", 
            live=false}

      else

         sqDB.execute{sql=
            "update ewaiting_referrals_condition_notes set "..
            " condition_note = "..conn:quote(refComment)..","..
            " updated_at = now() "..
            " where id = "..res[1].id:S(), 
            live=false}

      end

   end

end

-----------------------------------------------------------------
function sqDB.THS.updatePatientReferral(id,facilityID, clinCode, RF1, PRD, consID)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
   local refID = RF1[6][1]:S()
   
   local referralCount = sqDB.execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. facilityID ..
      " and user_id = " .. id .. " and control_id = "..conn:quote(refID), live=true}
   
   if referralCount[1]['count(id)']:S() == '0' then
      sqUtils.printLog("Referral does not exist, Skipping update...")
      return
   end
   
   --referral exists so update
   
   local facilityConditionTag = 0
   local facilitySpeciality = 0
   local facilityCategory = 0
   local FacilitySourceId = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultFacilitySourceId')
   
   --PB 06.12.23 : account class / appCategory
   local accClass = RF1[5][2]:S()
   
   local refExpiry =sqUtils.parseDateDMY(RF1[8])
     
   local waitingStatus = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultReferralWaitingStatus')
   
   --Establish the Priority of the Referral
   local refPriority = RF1[2][1]:S()
   
   --PB 16.05.2024 : IR-293
   local refPriorityName = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'refPriorityMap_inbound',RF1[2][2]:S())  
   if sqUtils.isEmpty(refPriorityName) then
      sqUtils.printLogW("Priority mapping not found for: "..RF1[2][2]:S().." in mapping: refPriorityMap_inbound")
      refPriorityName = RF1[2][2]:S()
   end 
   
   --
   local referralPriority = sqDB.execute
   {sql="select count(id), id from ewaiting_categories where facility_id = " .. facilityID ..
      " and name = " .. conn:quote(refPriorityName), live=true}
   
   if referralPriority[1]['count(id)']:S() ~= '0' then
      facilityCategory = referralPriority[1]['id']:S()
   end
   
   --date referred
   local refDateReferred = sqUtils.parseDate(RF1[9])
   
   --referral source name
   local refSourceName = ''
   if PRD ~= '' then
      refSourceName = PRD[2][5]:S()..' '..PRD[2][2]:S()..' '..PRD[2][1]:S()
   end
   
   --Establish the Referral Speciality
   local refSpeciality = RF1[3][2]:S()
   refSpeciality = sqUtils.checkControlCharacters(refSpeciality,'1')
 
   --
   local referralSpeciality = sqDB.execute{sql="select count(id), id from specialities where name = " 
      .. conn:quote(refSpeciality), live=true}
   
   if referralSpeciality[1]['count(id)']:S() ~= '0' then
      facilitySpeciality = referralSpeciality[1]['id']:S()
   end
   --
   --Establish the ConditionTag
   local refCondition = RF1[3][2]:S()
   refCondition = sqUtils.checkControlCharacters(refCondition,'2')
   
   --
   local refCondition = sqDB.execute{sql="select count(id), id from ewaiting_condition_tags where name = " 
      .. conn:quote(refCondition), live=true}
   
   if refCondition[1]['count(id)']:S() ~= '0' then
      facilityConditionTag = refCondition[1]['id']:S()
   end
   
   --PB 12.12.23: Simon suggested this is not required
   --facilityConditionTag = ''
   --
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")

   sqDB.execute{sql="update ewaiting_referrals set "..
      "facility_id = "..facilityID..","..
      "user_Id = "..id..","..
      "category_id = "..facilityCategory..","..
      "speciality_id = "..facilitySpeciality..","..
      "referral_source_id = "..FacilitySourceId..","..
      "date_referred = "..conn:quote(refDateReferred)..","..
      "referral_source_name = "..conn:quote(refSourceName)..","..
      "modified = now(), modified_by = "..sqUser..
      " where id = ".. referralCount[1]['id']:S(), live=false}
   
   --update the consultant id
   local res = sqDB.execute{sql=
      "update ewaiting_referrals_additional_info set consultant_id = " .. conn:quote(consID) ..
      " where referral_id = " .. referralCount[1]['id']:S() , live=false}
	
   
   sqDB.THS.addUpdateReferralExpiryDate(facilityID, referralCount[1]['id']:S(), refExpiry)
   
   --PB 12.03.2024 : Paddy requested that account class be picked up from the A05 instead of REF message
   --sqDB.THS.addUpdateReferralAccountClass(facilityID, referralCount[1]['id']:S(), accClass)
   
   sqUtils.printLog("Updating of referral completed!")
   sqUtils.printLog("Exiting "..getCurrentFunctionName().."...")
   
end

-----------------------------------------------------------------
-- Galway Gold Build customisation of function sqDB.THS.dischargePatientReferral
function sqDB.dischargePatientReferral(patientID, facilityID, refID, disOutcome)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
      
   -- Belt and braces coding.  Making sure we have the correct patient and facility, as well as of course the referralID
   local referralCount = sqDB.execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. facilityID ..
            " and user_id = " .. patientID .. " and id = "..conn:quote(refID), live=true}
      
   if referralCount[1]['count(id)']:S() == '0' then
      sqUtils.printLog("Referral does not exist, Skipping update...")
      return
   end
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
   
   --referral exists so discharge
   
   --set to discharged
   local referralID = referralCount[1].id:S()
   local sql ="update ewaiting_referrals " .."set ".."modified = now(), modified_by ="..conn:quote(sqUser)..","..
   "status = 'discharged'" ..   " where id = ".. referralID
   sqDB.execute{sql=sql, live=false}
   
   sqUtils.printLog("Referral "..referralID.." set to discharged")
   
   -- ***** HANDLE DISCHARGE REASON AND MAY NEED TO ADD RIP AS ONE *****
   local IPMRefDischargeReasons = sqUtils.getJsonMappedValue(sqCfg.extra_params,'IPMRefDischargeReasons')
   trace(IPMRefDischargeReasons)
   
   if IPMRefDischargeReasons == '' then              
      iguana.logError("Error: SQ DB iguana_config extra_params is missing IPMRefDischargeReasons data")       
   end 
   
   local disReasonID = sqUtils.getJsonMappedValue(sqUtils.jsonTableToString(IPMRefDischargeReasons),disOutcome)
   if disReasonID == '' then
      disReasonID = sqUtils.getJsonMappedValue(sqUtils.jsonTableToString(IPMRefDischargeReasons),'default')
   end
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
   local discharge_notes = '' -- Not currently used
   
   -- TO DO: Check if entry has already been created for this referral.  If so, then update - otherwise create a new entry
   
   local sqlStr = "insert into ewaiting_discharges (timestamp, referral_id, discharge_reason_id, discharge_notes,sq_user_id) " ..
   "values (".. "now()"..",".. 
                 referralID..",".. 
                 disReasonID..",".. 
                 conn:quote(discharge_notes)..",".. 
                 conn:quote(sqUser) .. ")"
   trace(sqlStr)
   sqDB.execute{sql=sqlStr, live=false}
   
   -- ***** MAY NEED TO CANCEL ANY OPEN APPOINTMENTS HERE *******

   sqUtils.printLog("Discharging of referral completed!")
   sqUtils.printLog("Exiting "..getCurrentFunctionName().."...")
   
end

-----------------------------------------------------------------
function sqDB.THS.dischargePatientReferral(id, facilityID, refID, disOutcome)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
   
   local referralCount = sqDB.execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. facilityID ..
      " and user_id = " .. id .. " and control_id = "..conn:quote(refID), live=true}
   
   if referralCount[1]['count(id)']:S() == '0' then
      sqUtils.printLog("Referral does not exist, Skipping update...")
      return
   end
   
   --referral exists so discharge
   
   --set to discharged
   local referralID = referralCount[1].id:S()
   local sql ="update ewaiting_referrals " .."set ".."modified = now(), modified_by = "..sqUser..", "..
   "status = 'discharged'" ..   " where id = ".. referralID
   sqDB.execute{sql=sql, live=false}
   
   sqUtils.printLog("Referral "..referralID.." set to discharged")
   
   --get the discharge reason
   local res = sqDB.execute{sql="select count(id) as cnt, id from ewaiting_discharge_reasons where "..
      " code = " .. conn:quote(disOutcome).." and facility_id = "..facilityID, live=true}

   local disReasonID = 0
   if res[1].cnt:S() ~= '0' then
      disReasonID = res[1].id:S()
   end
   
   local sqlStr = "insert into ewaiting_discharges (timestamp, referral_id, discharge_reason_id, discharge_notes,sq_user_id) " ..
   "values (".. "now()"..",".. referralID..",".. disReasonID..",".. "''"..",".. sqUser.. ")"
   trace(sqlStr)
   sqDB.execute{sql=sqlStr, live=false}
   
   -- ***** MAY NEED TO CANCEL ANY OPEN APPOINTMENTS HERE *******

   sqUtils.printLog("Discharging of referral completed!")
   sqUtils.printLog("Exiting "..getCurrentFunctionName().."...")
   
end

-----------------------------------------------------------------
function sqDB.setCancellationCodeOnReferralOrders(referralOrdersID, cancellationCodeID)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
   local sql ="update ewaiting_referral_orders " .."set ".."cancellation_date = now(), "..
   "cancellation_code = " ..cancellationCodeID..   " where id = ".. referralOrdersID
   
   sqDB.execute{sql=sql, live=false}
   
   sqUtils.printLog("ewaiting_referral_orders "..referralOrdersID.." set to cancelled")
   
   sqUtils.printLog("Exiting "..getCurrentFunctionName().."...")
end

-----------------------------------------------------------------
function sqDB.THS.createGPDetails(patientID, PD1, faciliityID)
   
   --extract the relevant GP details
   local gpFirstname = ''
   local gpOrgID = ''
   local gpID = ''
   local gpPractice = ''
   local gpFullName = ''
   
   gpFirstname = PD1[4][3]:S()
   gpSurname = PD1[4][2]:S()
   gpPractice = PD1[3][1]:S()
   gpID = PD1[4][1]:S()
   gpFullName = gpFirstname.." "..gpSurname
   
   if gpFullName == " " then
      return
   end
   
   sqUtils.printLog("Add / Update GP details...")
  
   local gpCount = sqDB.execute{sql="select count(id), id from gp_details"..
      " where medical_council_number = " .. conn:quote(gpID) .. 
      " and firstname = " .. conn:quote(gpFirstname) ..
      " and surname = " .. conn:quote(gpSurname), live=true}
   
   if gpCount[1]['count(id)']:S() == '0' then
      
      --create the new GP
      sqDB.execute{sql="insert into gp_details (id,firstname,surname,address_1,medical_council_number)"..
         " values (null, " .. conn:quote(gpFirstname)..","..conn:quote(gpSurname)..","..
         conn:quote(gpPractice)..","..conn:quote(gpID)..")", live=false}

      local newGPID1 = sqDB.execute{sql="select last_insert_id() as id", live=true}
      local newGPID = newGPID1[1].id:S()
      
      --now insert it into the facility_gp table
      sqDB.execute{sql="insert into facility_gp (id,facility_id,gp_details,hospital_code,gmc_code,dh_code,m_number)"..
         " values (null,"..conn:quote(faciliityID)..","..conn:quote(newGPID)..",0,0,0,0)", live=false}

      conn:commit{live=true}
      --
      --now update the patient details
      sqDB.execute{sql="update users set gp_details = "..conn:quote(newGPID)..","..
         "gp_name = "..conn:quote(gpFullName)..
         " where id = "..patientID, live=false}
   else
      
      --PB 23.05.24 - IR-307 update patient with GP ID
      local GPID = gpCount[1].id:S()
      sqDB.execute{sql="update users set gp_details = "..conn:quote(GPID)..","..
         "gp_name = "..conn:quote(gpFullName)..
         " where id = "..patientID, live=false}
      
   end
   --
end

-----------------------------------------------------------------
function sqDB.THS.getFacilityIDClinic(clinCode, hospitalCode)
   
   local clinicCount = sqDB.execute{sql="select count(id), facility_id from facility_clinic_codes"..
      " where clinic_code = " .. conn:quote(clinCode) .. 
      " and hospital_code = " .. conn:quote(hospitalCode), live=true}
   
   if clinicCount[1]['count(id)']:S() == '0' then
      return '0'
   else
      return clinicCount[1].facility_id:S()
   end
   
end

-----------------------------------------------------------------
function sqDB.THS.createClinicCapacity(facilityID, days, sessionStart, sessionEnd, sessionDur, sessionFreq, clinicCode, clinicDesc)
   
   sqUtils.printLog("Creating clinic capacity...")

   -- first check if we have a template clinic in the env for ipM
   local paramCount = sqDB.execute{sql="select count(id), param_value from application_parameter"..
      " where param_name = 'ipm.parent'", live=true}
   
   if paramCount[1]['count(id)']:S() == '0' then
      error('Application parameter for ipm.parent is not setup. Please add before proceeding.')
   else
      -- find a relevant template clinic to create teh details from
      local templateCount = sqDB.execute{sql="select count(id), id from facilities where parent_id = ".. conn:quote(paramCount[1]['param_value']:S())..
      " and duration = " .. conn:quote(sessionDur), live=true}
      
      if templateCount[1]['count(id)']:S() == '0' then
         error('No template found matching the clinic duration')
      else
         local templateFID = templateCount[1]['id']:S()
         --
         local newFID = createNewFacility(templateFID, clinicDesc)
         --
         createClinicCode(newFID, clinicCode)
         --
         createNewAllowableSlots(newFID, templateFID)
         --
         updateCapacity(newFID, days, sessionStart, sessionEnd, sessionDur, sessionFreq)
         --
      end
   end
end

-----------------------------------------------------------------
function sqDB.THS.updateCapacity(facilityID, days, sessionStart, sessionEnd, sessionDur, sessionFreq)
   
   sqUtils.printLog("Updating clinic capacity...")
   --
   -- Assumption here is that the entries are already set up in aloable_slots
   -- This is JUST an update to existing capacity
   
   local start_time1 = sessionStart:sub(1,2)
   local start_time2 = sessionStart:sub(3,4)
   local start_time = start_time1..':'..start_time2
   --
   local end_time1 = sessionEnd:sub(1,2)
   local end_time2 = sessionEnd:sub(3,4)
   local end_time = end_time1..':'..end_time2
   
   local ipm_day1 = days:split(',')[1]
   local ipm_day2 = days:split(',')[2]
   local ipm_day3 = days:split(',')[3]
   local ipm_day4 = days:split(',')[4]
   local ipm_day5 = days:split(',')[5]
   local ipm_day6 = days:split(',')[6]
   local ipm_day7 = days:split(',')[7]
   
   --********NEED to cater for change in durations********---
   local checkDur = checkClinicDur(facilityID, sessionDur)
   
   --********NEED to add in audit trail of changes********---
      
   --if the durations match then it's a straighforward update
   if checkDur == '1' then
      --reset Calendar Days
      sqDB.execute{sql="update calendar_days " .. 
         " set monday = 'N', tuesday = 'N', wednesday = 'N'," ..
         " thursday = 'N', friday = 'N', saturday = 'N'," ..
         " sunday = 'N'"..
         " where facility_id = " .. facilityID, live=false}
      
      --reset capacity slots back to 0
      sqDB.execute{sql="update allowable_slots set allowable = 0" ..
         " where facility_id = " .. facilityID
         , live=false}

      conn:commit{live=true} -- need to commit to prevent conflict with update
   else
      --we will need to create new allowable slots
   end
   --
   local day1 = setAllowableDays(facilityID, ipm_day1)
   local day2 = setAllowableDays(facilityID, ipm_day2)
   local day3 = setAllowableDays(facilityID, ipm_day3)
   local day4 = setAllowableDays(facilityID, ipm_day4)
   local day5 = setAllowableDays(facilityID, ipm_day5)
   local day6 = setAllowableDays(facilityID, ipm_day6)
   local day7 = setAllowableDays(facilityID, ipm_day7)
    
   --Update the relevant sessions
   if day1 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day1)
   end
   --
   if day2 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day2)
   end
   --
   if day3 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day3)
   end
   --
   if day4 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day4)
   end
   --
   if day5 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day5)
   end
   --
   if day6 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day6)
   end
   --
   if day7 ~= '0' then
      updateAlloableSlots(facilityID, start_time, end_time, day7)
   end
end

-----------------------------------------------------------------
function sqDB.THS.setAllowableDays(facilityID, day)
   
   sqUtils.printLog("Setting allowable days...")
   
   --
   if day == 'MON' then
      sqDB.execute{sql="update calendar_days set monday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      
      return 'Mon'
      --
   elseif day == 'TUE' then
      sqDB.execute{sql="update calendar_days set tuesday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Tue'
      --
   elseif day == 'WED' then
      sqDB.execute{sql="update calendar_days set wednesday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Wed'
      --
   elseif day == 'THU' then
      sqDB.execute{sql="update calendar_days set thursday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Thu'
      --
   elseif day == 'FRI' then
      sqDB.execute{sql="update calendar_days set friday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Fri'
      --
   elseif day == 'SAT' then
      sqDB.execute{sql="update calendar_days set saturday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Sat'
      --
   elseif day == 'SUN' then
      sqDB.execute{sql="update calendar_days set sunday = 'Y'" ..
      " where facility_id = " .. facilityID, live=false}
      --
      conn:commit{live=true}
      --
      return 'Sun'
      --
   else
      return '0'
   end
end

-----------------------------------------------------------------
function sqDB.THS.updateAllowableSlots(facilityID, start_time, end_time, day)
   
   sqUtils.printLog("Update allowable slots...")
   --
   --readjust end_time so that it is not included in the update
   sqDB.execute{sql="update allowable_slots set allowable = 1" ..
      " where facility_id = " .. facilityID ..
      " and (time >= " .. conn:quote(start_time) .. 
      " and time < " .. conn:quote(end_time) .. ")" ..
      " and date = " .. conn:quote(day), live=false}
   --
end

-----------------------------------------------------------------
function sqDB.THS.checkClinicDur(facilityID, sessionDur)
   --
   local clinicDur = sqDB.execute{sql="select count(id), duration from facilities"..
      " where id = " ..conn:quote(facilityID), live=true}
   
   if clinicDur[1]['duration']:S() == sessionDur then
      return '1'
   else
      return clinicDur[1].duration:S()
   end

end

-----------------------------------------------------------------
function sqDB.THS.createNewFacility(templateFID, clinicDesc)
   
   sqUtils.printLog("Creating new facility...")
        
         --**************
         --- WHERE DO I GET THE PARENT ID from
         --**************
         
         -- now create the clinic in the facilities table
         local createFID = sqDB.execute{sql="insert into facilities (id,type_id,facility_type,parent_id,location_id,speciality_id,town_id,firstname,"..
            "surname,short_name,email,password,phone,fax,address,maplink,image,duration,"..
            "notify,morning_start,morning_end,noon_start,noon_end,evening_start,evening_end,"..
            "calander_open,calander_close,status,created,modified,Name,next_avail_time,"..
            "weblink,priority,old_password,pwd_reset_time,is_pwd_expired,start_buffer,"..
            "end_buffer,calendar_color,google_calendar,gmail_id,calendar_id,gmail_password,"..
            "email2,alternate_address,plan,Specialities,Pricelist,Principles,bucket,"..
            "default_date,default_time,activation_code,send_sms,email_template,sms_template,"..
            "plan_type,gp_referral,bo_email,bo_password,override_code,bloods,doctor_id,ODS,"..
            "multiples,kiosk_email,kiosk_password,show_urgents,activation_flag,"..
            "send_facility_confirmation,facility_confirmation_email,confirmation_email,"..
            "confirmation_sms,reminder_email,reminder_sms,future_days,call_centre,new_admin,"..
            "paeds,print_lists,future_days_bo,map_coordinates,practice_phone,searchable,"..
            "activation_code_emailed_at,department,stripe_account_id,stripe_key,"..
            "stripe_plan_id,stripe_customer_id,code,pre_reservation_note,fkey,hl7_endpoint,"..
            "search_ip_restricted) "..
         "select null as id,type_id,facility_type,parent_id,location_id,speciality_id,town_id,".. conn:quote(clinicDesc).." as firstname,"..
            "surname,".. conn:quote(clinicDesc).. " as short_name,email,password,phone,fax,address,maplink,image,duration,"..
            "notify,morning_start,morning_end,noon_start,noon_end,evening_start,evening_end,"..
            "calander_open,calander_close,status,created,modified,Name,next_avail_time,"..
            "weblink,priority,old_password,pwd_reset_time,is_pwd_expired,start_buffer,"..
            "end_buffer,calendar_color,google_calendar,gmail_id,calendar_id,gmail_password,"..
            "email2,alternate_address,plan,Specialities,Pricelist,Principles,bucket,"..
            "default_date,default_time,activation_code,send_sms,email_template,sms_template,"..
            "plan_type,gp_referral,bo_email,bo_password,override_code,bloods,doctor_id,ODS,"..
            "multiples,kiosk_email,kiosk_password,show_urgents,activation_flag,"..
            "send_facility_confirmation,facility_confirmation_email,confirmation_email,"..
            "confirmation_sms,reminder_email,reminder_sms,future_days,call_centre,new_admin,"..
            "paeds,print_lists,future_days_bo,map_coordinates,practice_phone,searchable,"..
            "activation_code_emailed_at,department,stripe_account_id,stripe_key,"..
            "stripe_plan_id,stripe_customer_id,code,pre_reservation_note,null as fkey,hl7_endpoint,"..
            "search_ip_restricted from facilities where id = " .. templateFID, live=true}
         
         local newFID1 = sqDB.execute{sql="select last_insert_id() as id", live=true}
         local newFID = newFID1:S()
         --
         --create the calendar days entries
         local createFIDDays = sqDB.execute{sql="insert into calendar_days (id,timestamp,facility_id,monday,"..
            "tuesday,wednesday,thursday,friday,saturday,sunday) "..
         "select null as id,now() as timestamp,".. conn:quote(newFID) .." as facility_id,monday,tuesday,wednesday,thursday,friday,saturday,"..
            "sunday from calendar_days where facility_id = ".. templateFID, live=true}
         
         --create the calendar actions entries
         local createFIDActions = sqDB.execute{sql="insert into calendar_actions " ..
            "(id,timestamp,facility_id,check_in,call_in,complete,reset,record_bloods,"..
            "print_label,confirm,send_to_nurse,send_to_doctor,discharge,pharmacy_go_ahead,"..
            "follow_up,role,unable_to_attend,record_referral_clinic,record_patient_contacted,"..
            "unable_to_complete,go_ahead_for_treatment,call_log,video_consultation,"..
            "record_quality_issue,cross_referral,convert_to_patient_appointment,"..
            "generate_letter,record_dna,notify_patient_to_come_in,clinic_form,attending,"..
            "test_results,rpa,hide_vaccine_prompt,exam_abandoned,exam_incomplete,exam,writ,"..
            "approved,addendum,prescription_status,reschedule) "..
         "select null as id,now() as timestamp,".. conn:quote(newFID) .." as facility_id,check_in,call_in,complete,reset,record_bloods,"..
            "print_label,confirm,send_to_nurse,send_to_doctor,discharge,pharmacy_go_ahead,"..
            "follow_up,role,unable_to_attend,record_referral_clinic,record_patient_contacted,"..
            "unable_to_complete,go_ahead_for_treatment,call_log,video_consultation,"..
            "record_quality_issue,cross_referral,convert_to_patient_appointment,"..
            "generate_letter,record_dna,notify_patient_to_come_in,clinic_form,attending,"..
            "test_results,rpa,hide_vaccine_prompt,exam_abandoned,exam_incomplete,exam,writ,"..
            "approved,addendum,prescription_status,reschedule from calendar_actions where facility_id = ".. templateFID, live=true}
         
         --create facility_config
         local createFIDConfig = sqDB.execute{sql="insert into facility_config " ..
            "(id,timestamp,facility_id,calendar_days_id,calendar_actions_id,multi_site_id,"..
            "default_allowed,min_age,max_age,fasting_start,fasting_cutoff,nonfasting_start,"..
            "nonfasting_cutoff,inr_start,inr_cutoff,gtt_cutoff,future_months,print_cutoff,"..
            "bays,reception,late_allowance,capacity_cover,cream,has_gp,patient_per_day,"..
            "patient_appointment_limit_period,auto_checkin_time,pin_protected,pin,"..
            "uses_episode,use_call_screen,call_screen_msg_repeat_until,call_screen_msg,"..
            "call_screen_msg_content,show_call_screen_priority_msg,"..
            "call_screen_msg_priority_content,call_screen_display_limit,callin_refresh,"..
            "clinic_sync_notify,blood_test_form,late_checkin_check,late_checkin_time,"..
            "late_checkin_message,late_checkin_proceed,early_checkin_allowed_time,"..
            "patient_arrived_check,median_age,paeds_double_book,use_auto_checkin_time,"..
            "phleb_pre_booked_priority_ordering,uses_tay_sachs,uses_estimates,"..
            "use_uncall_patient_limit,follow_up_after_confirm,occ,under_age_indicator,"..
            "under_age_indicator_age,reschedule_confirmation_message,"..
            "send_appointment_confirmation_message,contact_alt_email_address,hours_buffer,"..
            "uses_pharmacy_units,auto_sync,morning_pharmacy_units,noon_pharmacy_units,"..
            "evening_pharmacy_units,use_pharmacy_units_change_alert,"..
            "pharmacy_units_change_alert_period,uses_menopause_stages,uses_mrn,"..
            "uses_disability_double_booking,use_pre_checkin_stage,use_auto_triage_time,"..
            "auto_triage_time,uses_resources,oncology,is_using_hstat_report,hstat_text,"..
            "alt_patient_label,alt_consultant_label,sports_health,uses_preferred_consultant,"..
            "uses_auto_dna,uses_service_providers,uses_referral_sources,show_consent_form,"..
            "follow_up_dna,uses_patient_groups,uses_next_of_kin,uses_waiting_list_triage,"..
            "send_waiting_list_triage_notification,waiting_list_validation_sms,"..
            "waiting_list_under_age_indicator,waiting_list_under_age_validation_sms,"..
            "can_manage_gps,can_manage_patients,can_book_new_patients,"..
            "show_special_assistance_options,appointment_cancellation_cutoff,"..
            "pre_confirmation_note,letter_left_logo,letter_right_logo,"..
            "share_appointment_group_checkin_code,outcome,use_custom_branding,"..
            "custom_branding_colour_1,custom_branding_colour_2,custom_branding_logo,"..
            "use_restricted_register_form,hide_family_member_option,"..
            "custom_branding_data_processing_consent,"..
            "custom_branding_restrict_user_notifications,"..
            "timescreen_matrix_use_only_allowable_slots,custom_branding_hide_how_to_videos,"..
            "domiciliary_import_format,eWaitingList_show_add_past_appointment_button,"..
            "eWaitingList_generate_letter_on_allocate,opd_ask_for_checkin,reminder_1,"..
            "reminder_2,notify_consultant_on_checkin,email_alternate_address_on_cancellation,"..
            "phlebotomy_show_convert_account_prompt,eWaiting_use_additional_referral_fields,"..
            "auto_notify_of_dna,show_appt_series,display_referral_outpatient,"..
            "call_centre_gp_check,show_terms_on_timescreens,restrict_access_by_ip,"..
            "access_ip_ranges,enforce_allowable_slots_limit,letter_footer_logo,auto_refund,"..
            "full_auto_refund,timescreen_matrix_use_date_ranges,eWaiting_send_cron_sms,"..
            "eWaiting_display_orders,auto_cancel_dna,timescreen_unavailable_time_message,"..
            "timescreen_show_message_as_notice,eWaiting_use_enquirer,"..
            "healthlink_change_facility,eWaiting_use_broadcast_message,facility_pin,"..
            "compact_timescreen_max_display,referral_notify_gp,allow_tfa,"..
            "video_consultation_driver,dom_worklist_SMS,domiciliary_activate_requests,"..
            "use_reception_end_of_day,client_registry_allow_override,"..
            "appointment_info_check_in,appointment_info_check_in_sms,"..
            "comms_broadcast_to_patients,from_email,online_booking,display_clinic_code,"..
            "dynamic_forms,has_vaccines,screening,app_info_content,uses_sq_mrn,uses_vouchers,"..
            "hide_additional_comments,pre_appt_comms,bank_holiday,order_matching,"..
            "timescreen_pin,hse_vax,vax_cert,healthshare,dom_reminder_SMS,sms_driver,classes,"..
            "eWaiting_store_monthly_stats,broadcast_to_forms,max_allowable_slots,"..
            "bypass_mobile_check,reschedule_limit_days,reschedule_limit_count,"..
            "uses_reason_group_gender,uses_reason_groups,slot_base_duration,sms_sender_name,"..
            "requires_vetting,ewaiting_removal_reasons,post_appointment_communication,"..
            "itbf_clinic,call_in_display_check_in_code,call_in_display_mrn,"..
            "reception_highlight_overruns,discharge_week_limit,"..
            "calendar_display_additional_info,reception_walk_in_appts,allow_patients_deceased,"..
            "hse_facilities_search,timescreen_link_restricted,ihi_api,opd_referrals,"..
            "hse_eircode_api,calendar_highlight_special_assistance,self_referral_portal,"..
            "CORU_number,gp_create_waiting_list_referrals,self_referral_gp_letter,"..
            "self_referral_gp_letter_email,self_referral_select_exam,archived_box_number,"..
            "attending_week_limit,domiciliary_county,self_referral_speciality,"..
            "self_referral_select_gp,clinician_name,uses_sequential_limits,"..
            "appt_info_reschedule,practice_label,consultant_as_resource,"..
            "clinic_display_checked_in_time,triage_display_checked_in_time,"..
            "compact_timescreen_hide_appt_details,patient_middle_name,"..
            "appointment_info_reschedule_message,appointment_info_cancellation_message,"..
            "reception_user_addit_info,calendar_all_consultants_no_appts,"..
            "appointment_info_message,appointment_info_cancel,follow_up_additional_info,"..
            "uses_appointment_reason_type,enable_debug_logging,attach_triage_form_to_referral,"..
            "appt_export_has_referral,use_waiting_list_rows_number_modifier,referral_reviews,"..
            "use_site_translations,appointment_info_white_label,mail_driver,user_alerts,"..
            "hide_reassign,allow_patient_gp_add,apply_cutoff_to_online_timescreen) "..
        "select null as id,now() as timestamp,".. conn:quote(newFID) .." as facility_id,calendar_days_id,calendar_actions_id,multi_site_id,"..
            "default_allowed,min_age,max_age,fasting_start,fasting_cutoff,nonfasting_start,"..
            "nonfasting_cutoff,inr_start,inr_cutoff,gtt_cutoff,future_months,print_cutoff,"..
            "bays,reception,late_allowance,capacity_cover,cream,has_gp,patient_per_day,"..
            "patient_appointment_limit_period,auto_checkin_time,pin_protected,pin,"..
            "uses_episode,use_call_screen,call_screen_msg_repeat_until,call_screen_msg,"..
            "call_screen_msg_content,show_call_screen_priority_msg,"..
            "call_screen_msg_priority_content,call_screen_display_limit,callin_refresh,"..
            "clinic_sync_notify,blood_test_form,late_checkin_check,late_checkin_time,"..
            "late_checkin_message,late_checkin_proceed,early_checkin_allowed_time,"..
            "patient_arrived_check,median_age,paeds_double_book,use_auto_checkin_time,"..
            "phleb_pre_booked_priority_ordering,uses_tay_sachs,uses_estimates,"..
            "use_uncall_patient_limit,follow_up_after_confirm,occ,under_age_indicator,"..
            "under_age_indicator_age,reschedule_confirmation_message,"..
            "send_appointment_confirmation_message,contact_alt_email_address,hours_buffer,"..
            "uses_pharmacy_units,auto_sync,morning_pharmacy_units,noon_pharmacy_units,"..
            "evening_pharmacy_units,use_pharmacy_units_change_alert,"..
            "pharmacy_units_change_alert_period,uses_menopause_stages,uses_mrn,"..
            "uses_disability_double_booking,use_pre_checkin_stage,use_auto_triage_time,"..
            "auto_triage_time,uses_resources,oncology,is_using_hstat_report,hstat_text,"..
            "alt_patient_label,alt_consultant_label,sports_health,uses_preferred_consultant,"..
            "uses_auto_dna,uses_service_providers,uses_referral_sources,show_consent_form,"..
            "follow_up_dna,uses_patient_groups,uses_next_of_kin,uses_waiting_list_triage,"..
            "send_waiting_list_triage_notification,waiting_list_validation_sms,"..
            "waiting_list_under_age_indicator,waiting_list_under_age_validation_sms,"..
            "can_manage_gps,can_manage_patients,can_book_new_patients,"..
            "show_special_assistance_options,appointment_cancellation_cutoff,"..
            "pre_confirmation_note,letter_left_logo,letter_right_logo,"..
            "share_appointment_group_checkin_code,outcome,use_custom_branding,"..
            "custom_branding_colour_1,custom_branding_colour_2,custom_branding_logo,"..
            "use_restricted_register_form,hide_family_member_option,"..
            "custom_branding_data_processing_consent,"..
            "custom_branding_restrict_user_notifications,"..
            "timescreen_matrix_use_only_allowable_slots,custom_branding_hide_how_to_videos,"..
            "domiciliary_import_format,eWaitingList_show_add_past_appointment_button,"..
            "eWaitingList_generate_letter_on_allocate,opd_ask_for_checkin,reminder_1,"..
            "reminder_2,notify_consultant_on_checkin,email_alternate_address_on_cancellation,"..
            "phlebotomy_show_convert_account_prompt,eWaiting_use_additional_referral_fields,"..
            "auto_notify_of_dna,show_appt_series,display_referral_outpatient,"..
            "call_centre_gp_check,show_terms_on_timescreens,restrict_access_by_ip,"..
            "access_ip_ranges,enforce_allowable_slots_limit,letter_footer_logo,auto_refund,"..
            "full_auto_refund,timescreen_matrix_use_date_ranges,eWaiting_send_cron_sms,"..
            "eWaiting_display_orders,auto_cancel_dna,timescreen_unavailable_time_message,"..
            "timescreen_show_message_as_notice,eWaiting_use_enquirer,"..
            "healthlink_change_facility,eWaiting_use_broadcast_message,facility_pin,"..
            "compact_timescreen_max_display,referral_notify_gp,allow_tfa,"..
            "video_consultation_driver,dom_worklist_SMS,domiciliary_activate_requests,"..
            "use_reception_end_of_day,client_registry_allow_override,"..
            "appointment_info_check_in,appointment_info_check_in_sms,"..
            "comms_broadcast_to_patients,from_email,online_booking,display_clinic_code,"..
            "dynamic_forms,has_vaccines,screening,app_info_content,uses_sq_mrn,uses_vouchers,"..
            "hide_additional_comments,pre_appt_comms,bank_holiday,order_matching,"..
            "timescreen_pin,hse_vax,vax_cert,healthshare,dom_reminder_SMS,sms_driver,classes,"..
            "eWaiting_store_monthly_stats,broadcast_to_forms,max_allowable_slots,"..
            "bypass_mobile_check,reschedule_limit_days,reschedule_limit_count,"..
            "uses_reason_group_gender,uses_reason_groups,slot_base_duration,sms_sender_name,"..
            "requires_vetting,ewaiting_removal_reasons,post_appointment_communication,"..
            "itbf_clinic,call_in_display_check_in_code,call_in_display_mrn,"..
            "reception_highlight_overruns,discharge_week_limit,"..
            "calendar_display_additional_info,reception_walk_in_appts,allow_patients_deceased,"..
            "hse_facilities_search,timescreen_link_restricted,ihi_api,opd_referrals,"..
            "hse_eircode_api,calendar_highlight_special_assistance,self_referral_portal,"..
            "CORU_number,gp_create_waiting_list_referrals,self_referral_gp_letter,"..
            "self_referral_gp_letter_email,self_referral_select_exam,archived_box_number,"..
            "attending_week_limit,domiciliary_county,self_referral_speciality,"..
            "self_referral_select_gp,clinician_name,uses_sequential_limits,"..
            "appt_info_reschedule,practice_label,consultant_as_resource,"..
            "clinic_display_checked_in_time,triage_display_checked_in_time,"..
            "compact_timescreen_hide_appt_details,patient_middle_name,"..
            "appointment_info_reschedule_message,appointment_info_cancellation_message,"..
            "reception_user_addit_info,calendar_all_consultants_no_appts,"..
            "appointment_info_message,appointment_info_cancel,follow_up_additional_info,"..
            "uses_appointment_reason_type,enable_debug_logging,attach_triage_form_to_referral,"..
            "appt_export_has_referral,use_waiting_list_rows_number_modifier,referral_reviews,"..
            "use_site_translations,appointment_info_white_label,mail_driver,user_alerts,"..
            "hide_reassign,allow_patient_gp_add,apply_cutoff_to_online_timescreen from facility_config where facility_id = ".. templateFID, live=true}
   
   conn:commit{live=true}
   
   return newFID
   
end

-----------------------------------------------------------------
function sqDB.THS.createNewAllowableSlots(newFID, templateFID)
   
   sqUtils.printLog("Creating new allowable slots...")
   
   --create the calendar days entries
   local createAllowableSlots = sqDB.execute{sql="insert into allowable_slots (id,timestamp,facility_id,reason_id,reason_group_id,"..
      "date,time,allowable,end_time,date_range_id) "..
      "select null as id,now() as timestamp,".. conn:quote(newFID) .." as facility_id,reason_id,reason_group_id,"..
      "date,time,allowable,end_time,date_range_id from allowable_slots where facility_id = ".. templateFID, live=true}
   
   conn:commit{live=true}
   
end

-----------------------------------------------------------------
function sqDB.THS.createClinicCode(newFID, sessionCode)
   
   sqUtils.printLog("Creating clinic code...")
   
   --create the facility_clinic_code record
   local createClinicCodes = sqDB.execute{sql="insert into facility_clinic_codes (id,hospital_group_id,facility_id,clinic_code,"..
      "hospital_code,reason_id,active,created_at,updated_at,waiting_status,condition_id,speciality_id,category_id,"..
      "source_id,opd_video) values (null, 14, ".. conn:quote(newFID)..","..conn:quote(sessionCode)..",'NMH',null,0,now(), null,"..
      "'pre_triage',0,0,0,0,0)", live=false}
   
   conn:commit{live=true}
   
end

-----------------------------------------------------------------
function sqDB.THS.createSlotDateRange(facilityID, startDate, endDate)
   
   sqUtils.printLog("Creating slot date range...")
   
   --create a new record for allowable_slots_date_ranges
   local createSlotDateRanges = sqDB.execute{sql="insert into allowable_slots_date_ranges (id,facility_id,"..
      "start_date,end_date,active,created_at,updated_at) values ".. 
      "(null,"..conn:quote(facilityID), ","..conn:quote(startDate)..","..conn:quote(endDate)..","..
      "1, now(), now())", live=false}
   
   conn:commit{live=true}
   
   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
   --
   return result
   --
end

-----------------------------------------------------------------

function sqDB.THS.updateWaitList(patientID, waitListID, controlID, facilityID)
   
   sqUtils.printLog("Updating waitng list...")
   
   local suppressFacilityCheck = sqUtils.getMappedValue(sqCfg.extra_params,"suppressReferralFacilityCheck")
   
   local sqlSTR = "select count(id), id from ewaiting_referrals where facility_id = " .. 
      facilityID .. " and user_id = " .. patientID.." and control_id = "..conn:quote(controlID)
   
   if suppressFacilityCheck == '1' then
      sqlSTR = "select count(id), id from ewaiting_referrals where " .. 
      " user_id = " .. patientID.." and control_id = "..conn:quote(controlID)
   end
   
   local referralCount = sqDB.execute{sql=sqlSTR, live=true}
   
   if referralCount[1]['count(id)']:S() ~= '0' then
      
      local res = sqDB.execute{sql="update ewaiting_referrals set covid_19_id = "..conn:quote(waitListID)..
      " where id = "..conn:quote(referralCount[1]['id']:S()), live=false}
   
      conn:commit{live=true}
      
   end
end

-----------------------------------------------------------------

function sqDB.THS.removeWaitList(patientID, waitListID, facilityID)
   
   sqUtils.printLog("Removing waitng list...")
   
   local suppressFacilityCheck = sqUtils.getMappedValue(sqCfg.extra_params,"suppressReferralFacilityCheck")
   
   local sqlSTR = "select count(id), id from ewaiting_referrals where facility_id = " .. 
      facilityID .. " and user_id = " .. patientID.." and covid_19_id = "..conn:quote(waitListID)
   
   if suppressFacilityCheck == '1' then
      sqlSTR = "select count(id), id from ewaiting_referrals where " .. 
               " user_id = " .. patientID.." and covid_19_id = "..conn:quote(waitListID)
   end
   
   --using the waiting list id check if the WL has referrals
   local referralCount = sqDB.execute{sql=sqlSTR, live=true}
   
   --if found then set the status to 'removed', which hides it on the SQ front end
   if referralCount[1]['count(id)']:S() ~= '0' then
      
      sqlSTR = "update ewaiting_referrals set status = 'removed' "..
               "where facility_id = " .. facilityID .. " and user_id = " .. patientID.." and covid_19_id = "..conn:quote(waitListID)
      
      if suppressFacilityCheck == '1' then
         sqlSTR = "update ewaiting_referrals set status = 'removed' "..
               "where user_id = " .. patientID.." and covid_19_id = "..conn:quote(waitListID)
      end
      
      local res = sqDB.execute{sql=sqlSTR, live=false}
   
      conn:commit{live=true}
      
   end
end

-----------------------------------------------------------------

function sqDB.THS.unRemoveWaitList(patientID, referralID, facilityID)
   
   sqUtils.printLog("Un-Removing waitng list...")
      
   local sqlSTR = "select count(id), id from ewaiting_referrals where facility_id = " .. 
      facilityID .. " and user_id = " .. patientID.." and status = 'removed' and control_id = "..conn:quote(referralID)
   
   local suppressFacilityCheck = sqUtils.getMappedValue(sqCfg.extra_params,"suppressReferralFacilityCheck")
   
   if suppressFacilityCheck == '1' then
      sqlSTR = "select count(id), id from ewaiting_referrals where " .. 
      " user_id = " .. patientID.." and status = 'removed' and control_id = "..conn:quote(referralID)
   end
   
   --using the waiting list id check if the WL has been previously deleted
   local referralCount = sqDB.execute{sql=sqlSTR, live=true}
   
   --if found then set the status to 'assigned', which makes it visable again on the fron end
   if referralCount[1]['count(id)']:S() ~= '0' then
      
      sqlSTR = "update ewaiting_referrals set status = 'triage' "..
      "where facility_id = " .. facilityID .. " and user_id = " .. patientID.." and status = 'removed' and control_id = "..conn:quote(referralID)
      
      if suppressFacilityCheck == '1' then
         sqlSTR = "update ewaiting_referrals set status = 'triage' "..
         "where user_id = " .. patientID.." and status = 'removed' and control_id = "..conn:quote(referralID)
      end
      
      local res = sqDB.execute{sql=sqlSTR, live=false}
   
      conn:commit{live=true}
      
   end
end

-----------------------------------------------------------------
function sqDB.THS.userInFacility(patientId, facilityId)
   
   if facilityId ~= nil and facilityId ~= '' then
   
       local result = sqDB.execute{sql="select id from facility_user where "..
       " user_id = " .. patientId .. " "..
       " and facility_id = " .. facilityId .. ";"
       , live=true}
   
     return result[1].id:nodeValue()
   else
      return nil
   end
end

-----------------------------------------------------------------
function sqDB.THS.addToFacility(patientId, facilityId)
	if sqDB.THS.userInFacility(patientId, facilityId) == ''
   then
	   sqDB.execute{sql="insert into facility_user (user_id, facility_id) values ("..patientId..", "..facilityId..")", live=false}
   end
end

----------------------------------------------------------------------------

--UTILS:

-----------------------------------------------------------------------------

--return the phone number from the PID based on type
function sqDB.THS.getPhoneNumberByType(PID, pType)
   for i=1, #PID[13] do
      if PID[13][i][3]:S() == pType then
         return PID[13][i][1]:S()
      end
   end
   return ''
end

-----------------------------------------------------------------------------
--return the phone number from the PID based on type
function sqDB.THS.getNKPhoneNumberByType(NK1, pType)
   if NK1:childCount() > 1 then
      for i=1, #NK1[5] do
         if NK1[5][i][3]:S() == pType then
            return NK1[5][i][1]:S()
         end
      end
   else
      if NK1[1][5][1][2]:S() == pType then
      return NK1[1][5][1][1]:S()
      end
   end  
   return ''
end

-----------------------------------------------------------------------------
--return the phone number from the PID based on type, SMS , L14. L15 etc
function sqDB.THS.getNKPhoneNumberByType(NK1, pType)

   if NK1:childCount() >=1 then
      for i=1, #NK1[1][5] do
         if NK1[1][5][i][3]:S() == pType then
            return NK1[1][5][i][1]:S()
         end
      end
   else
      if NK1[1][5][1][2]:S() == pType then
      return NK1[1][5][1][1]:S()
      end
   end  
   return ''
end

--------------------------------------------------------------

function sqDB.THS.getPatientName(PID, identifier)
   
   local firstname = ''
   local surname = ''
   local middlename = ''
   local title = ''
   
   for i=1, #PID[5] do
      
      if PID[5][i][7]:S() == identifier then
         
         firstname = PID[5][i][2]:S()
         surname = PID[5][i][1]:S()
         middlename = PID[5][i][3]:S()
         title = PID[5][i][5]:S()
         
         break
      
      end
   
   end
   
   return firstname, surname, middlename, title

end

--------------------------------------------------------------

function sqDB.THS.insertMedicareNumber(patientID)
   
   --[[
      
      NOTE: From Rosie
      
      If a medicare number has no end date then it is the current number
      If all numbers are end dated then we store the number with the latest end date
      
      ]]
   
   trace(patientID, sqDB.patientData.medicareNumber)
   
   if sqUtils.isEmpty(patientID) or sqUtils.isEmpty(sqDB.patientData.medicareNumber) then return end
   
   sqUtils.printLog("Updating medicare numbers for patient ID: "..patientID)
   
   --remove all current medicare numbers for the patient ID
   
   local sqlStr = "delete from user_medical_number_reference where reference_type = 'MC' and user_id = "..patientID
   print(sqlStr)
   sqDB.execute{sql=sqlStr, live=false}
      
   --insert the latest medicare numbers
   
   local mcCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'medicareCode')
   
   local mcNumbers = sqUtils.split(sqDB.patientData.medicareNumber,'|')
   
   local latestEndDate = '0'
   local mcNumber = ''
   local date_from = ''
   local date_to = ''     
   
    
   for i=1, #mcNumbers do
      
      local flds = sqUtils.split(mcNumbers[i],'^')
      trace(flds[1],flds[2],flds[3])
      
      if flds[1] ~= '' then
         
         if flds[3] == '' then

            --no end date to this is the currrent medicare number
            mcNumber = flds[1]
            date_from = conn:quote(sqUtils.parseDateNum(flds[2]))
            if flds[2] == '' then date_from = 'NULL' end
            date_to = conn:quote(sqUtils.parseDateNum(flds[3]))
            if flds[3] == '' then date_to = 'NULL' end

            break

         else
            trace(flds[3],latestEndDate)
            if flds[3] > latestEndDate then

               mcNumber = flds[1]
               date_from = conn:quote(sqUtils.parseDateNum(flds[2]))
               if flds[2] == '' then date_from = 'NULL' end
               date_to = conn:quote(sqUtils.parseDateNum(flds[3]))
               if flds[3] == '' then date_to = 'NULL' end
               
               latestEndDate = flds[3]

            end

         end
         
      end
      
   end
   
   trace(mcNumber,date_from,date_to)
   
   if mcNumber ~= '' then

      local sqlStr="insert into user_medical_number_reference (user_id, reference_type, medical_number, from_date, to_date) " ..
      "values ("..
      patientID .. "," ..
      conn:quote(mcCode)..","..
      conn:quote(mcNumber)..","..date_from..","..
      date_to..
      ")"

      trace(sqlStr)
      print(sqlStr)
      sqDB.execute{sql=sqlStr , live=false}
      
   end
   
end

-----------------------------------------------------------------------------

function sqDB.THS.insertIMedixNumber(patientID)
   
   trace(patientID, sqDB.patientData.imedixNumber)
   
   if sqUtils.isEmpty(patientID) or sqUtils.isEmpty(sqDB.patientData.imedixNumber) then return end
   
   sqUtils.printLog("Updating infomedix number for patient ID: "..patientID)
   
   --remove all current infomedix numbers for the patient ID
   
   local sqlStr = "delete from user_medical_number_reference where reference_type ='PT' and user_id = "..patientID
   print(sqlStr)
   sqDB.execute{sql=sqlStr, live=false}
      
   --insert the latest infomedix number
   
   local imCode = sqUtils.getJsonMappedValue(sqCfg.extra_params,'iMedixCode')

   local sqlStr="insert into user_medical_number_reference (user_id, reference_type, medical_number) " ..
   "values ("..
   patientID .. "," ..
   conn:quote(imCode)..","..
   conn:quote(sqDB.patientData.imedixNumber)..
   ")"

   trace(sqlStr)
   print(sqlStr)
   sqDB.execute{sql=sqlStr , live=false}
 
end

-----------------------------------------------------------------------------

function sqDB.THS.addUpdateInsurance(IN1, patientID)

   if IN1 == nil or patientID == '0' then
      return
   end
   
   local insurance_code = IN1[2][1]:S()
   local insurance_policy = IN1[36]:S()
   local insurance_company = IN1[4]:S()   -------------- AWAITING PLACEHOLDER ON DB *********

   local res = sqDB.execute{sql="select count(user_id) as cnt, user_id from users_additional_info where user_id = "..
      patientID, live=true}
   
   if res[1].cnt:S() == '0' then
      sqDB.execute{sql="insert into users_additional_info (user_id, insurance_code, insurance_company, policy_no) " ..
         "values ("..
         patientID .. "," ..
         conn:quote(insurance_code)..","..
         conn:quote(insurance_company)..","..
         conn:quote(insurance_policy)..
         ")"
         , live=false}
   else
      sqDB.execute{sql="update users_additional_info set " ..
         "insurance_code = "..conn:quote(insurance_code)..","..
         "insurance_company = "..conn:quote(insurance_company)..","..
         "policy_no = "..conn:quote(insurance_policy)..
         " where user_id = "..patientID
         , live=false}
   end
   
   sqUtils.printLog("Insurance details updated!")

end

-----------------------------------------------------------------------------

function sqDB.THS.addUpdateInsurance_NEW(patientID, insJson, insText)
   
   --- TODO : get the ID of the insurance company , 99 for testing
   
   local res = sqDB.execute{sql="select count(id) as cnt, param_value from application_parameter ap where param_name = 'patient.insurance_custom_field_id';"
      , live=true}
   
   if res[1].cnt:S() == '0' then
      error("No mapping found for Insurance custom ID")
   end
   
   local customID = res[1].param_value:S()
 
   res = sqDB.execute{sql="select id from user_custom_fields_values where user_id = "..
      conn:quote(patientID).." and facility_user_custom_fields_id = "..customID , live=true}
   
   if #res == 0 then
      
      --insert new insurance
      sqDB.execute{sql='insert into user_custom_fields_values (user_id, uuid, facility_user_custom_fields_id, value, value_str) '..
         ' values ('..conn:quote(patientID)..', uuid(), '..conn:quote(customID)..', '..
         conn:quote(insJson)..', '..conn:quote(insText)..')', live=false}
   else
      
      --update the insurance
      local id = res[1]:S()
      res = sqDB.execute{sql='update user_custom_fields_values '..
         ' SET value = '..conn:quote(insJson)..
         ' , value_str = '..conn:quote(insText)..' where id = '..id, live=false}
   end   
   --
   
end


----------------------------------------------------------------------------

function sqDB.THS.addUpdateUserAdditionalInfo(patientID, middleName, language, aliasFirstname, aliasSurname, aliasMiddlename, indig_status )

   --validate that indig_status exists on DB  
   local indig_res = nil
   
   local useIndigenousStatus = sqUtils.getJsonMappedValue(sqCfg.extra_params,'useIndigenousStatus')
   if useIndigenousStatus == '1' then
      indig_res = sqDB.execute{sql="select id from indigenous_status where code = "..conn:quote(indig_status) , live=true}
   end
   
   if indig_res ~= nil then
      if #indig_res == 0 then
         sqUtils.printLogW("Indig Status code not found on SQ DB: "..indig_status)
         indig_status=''
      else
         indig_status = indig_res[1].id:S()
      end
   end
   
   local res = sqDB.execute{sql="select count(user_id) as cnt, user_id from users_additional_info where user_id = "..
      patientID, live=true}
  
   local strSQL = ''
   
   if res[1].cnt:S() == '0' then

      strSQL = "insert into users_additional_info (user_id, middlename, translator_language, aka_firstname, aka_surname, aka_middle_name"

      if indig_status ~= '' then
         strSQL = strSQL..", indigenous_status_id"
      end
      
      strSQL = strSQL..") values ("..
      patientID .. "," ..
      conn:quote(middleName)..","..
      conn:quote(language)..", "..
      conn:quote(aliasFirstname)..", "..
      conn:quote(aliasSurname)..", "..
      conn:quote(aliasMiddlename)

      if indig_status ~= '' then
         strSQL = strSQL..", "..conn:quote(indig_status)
      end

      strSQL = strSQL..")"

      sqDB.execute{sql=strSQL, live=false}

   else
      
      strSQL = "update users_additional_info set " ..
         "middlename = "..conn:quote(middleName)..", "..
         "translator_language = "..conn:quote(language)..", "..
         "aka_firstname = "..conn:quote(aliasFirstname)..", "..
         "aka_surname = "..conn:quote(aliasSurname)..", "..
         "aka_middle_name = "..conn:quote(aliasMiddlename)
      
      if indig_status ~= '' then
         strSQL = strSQL..", indigenous_status_id = "..conn:quote(indig_status)
      end
      
      strSQL = strSQL.." where user_id = "..patientID
      
      sqDB.execute{sql=strSQL, live=false}
   end

end

----------------------------------------------------------------------------

function sqDB.THS.getParentFacilityId(facilityID)

   local parentFacilityID = ''
   
   local parentFID = sqDB.execute{sql="select parent_id from facilities where id = "..facilityID, live=true}

   if parentFID[1]['parent_id']:S() ~= '0' then
      parentFacilityID = parentFID[1]['parent_id']:S()
   else
      parentFacilityID = facilityID
   end

   return parentFacilityID

end

----------------------------------------------------------------------------

function sqDB.THS.getCSRparentFacilityId(MsgIn)

   --get the default staging facility if no h.org or refer_to found
   local stagingFacility = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultParentFacility_'..MsgIn.MSH[4][1]:S(),tmp)
   if sqUtils.isEmpty(stagingFacility) then
      sqUtils.printLogW('No default staging facility defined for: '..MsgIn.MSH[4][1]:S())
      stagingFacility = sqUtils.getJsonMappedValue(sqCfg.extra_params,'parentFacilityCatchAll')
   end

   local parentFacilityID = ''

   --What if there are multiple RT's - at the moment just picking the first
   local RT = ''
   for i = 1, #MsgIn.Group2 do
      if (MsgIn.Group2[i].PRD[1][1]:S() == 'RT') then
         RT = MsgIn.Group2[i].PRD
         break
      end
   end
   if RT == '' then
      error("No RT segment found in the referral message!")
   end
   sqUtils.printLog("Referral RT segment: "..RT:S())

   local parentFacilityID = ''
   local consID = ''
   local consName = ''
   
   --determine if we are using the config mapping or CSR lookup
   local useCSRLookup = sqUtils.getJsonMappedValue(sqCfg.extra_params,'useCSRLookup')
  
   --get the parentfacility ID based on 4.1 first
   local clinic = RT[4][1]:S()
   
   if useCSRLookup ~= '1' then

      local tbl7 = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'multiFacilityMap',clinic)
      if not sqUtils.isEmpty(tbl7) then
         local tmp = sqUtils.split(tbl7)
         parentFacilityID = tmp[1]
         consName = tmp[2]
      end
   
   else
      
      parentFacilityID, cons = sqDB.THS.getParentFacilityFromCSR(clinic, MsgIn.MSH[4]:S())
      
   end

   --if lookup fails on 4.1 then try 4.4
   
   if sqUtils.isEmpty(parentFacilityID) then

      clinic = RT[4][4]:S()
      
      if useCSRLookup ~= '1' then
         
         tbl7 = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'multiFacilityMap',clinic)
         if not sqUtils.isEmpty(tbl7) then
            local tmp = sqUtils.split(tbl7,'^')
            parentFacilityID = tmp[1]
            consName = tmp[2]
         end   

      else
         
         parentFacilityID, consID = sqDB.THS.getParentFacilityFromCSR(clinic, MsgIn.MSH[4]:S())

      end

   end

   --if still no parent facilty found then default to a staging area
   if sqUtils.isEmpty(parentFacilityID) then

      parentFacilityID = stagingFacility

   end

   sqUtils.printLog("Multi facility parent facility ID returned: "..parentFacilityID)
   
   --if not using CSR lookup then we need to go back to the DB to find the cons id
   if useCSRLookup ~= '1' then

      if consName == '' then
         consID = '0' -- if cons id not picked up then default to zero
      else

         --lookup the cons ID
         local res = sqDB.execute{sql="select count(id) as cnt, id from ewaiting_consultants where name = "..
            conn:quote(consName), live=true}

         if res[1].cnt:S() ~= '0' then
            consID = res[1].id:S()
         else
            consID = '0'
         end

      end

   end
    
      
   return parentFacilityID, consID

end

----------------------------------------------------------------------------

function sqDB.THS.checkReferralStaging(MsgIn, patientID, parentFacilityID)
   
   sqUtils.printLog("checkReferralStaging: checking if movement from bucket to clinic facility..")

   --get the default staging facility if no h.org or refer_to found
   local stagingFacility = sqUtils.getJsonMappedValue(sqCfg.extra_params,'defaultParentFacility_'..MsgIn.MSH[4][1]:S(),tmp)
   if sqUtils.isEmpty(stagingFacility) then
      error('No default staging facility defined for: '..MsgIn.MSH[4][1]:S())
   end
   
   if parentFacilityID == stagingFacility then
      return
   end
   
   --if we got this far then the i13 indicates a movement of a referral out out 
   --the staging area/bucket into a 'proper' facility
   
   local orderID = MsgIn.RF1[6][1]:S()
   
   --find the order for the same patient in the staging facility
   local sRefID = sqDB.THS.getReferralByControlID(orderID , patientID, stagingFacility)
   trace(sRefID)
   
   --referral found in the staging area so set the facility to the new one
   if not sqUtils.isEmpty(sRefID) then
      
      sqUtils.printLog("checkReferralStaging: moving the referral out of staging area..")
      
      local res = sqDB.execute{sql=
      "update ewaiting_referrals set facility_id = "..parentFacilityID..
      " where id = "..sRefID, live=false}
   
   end

end

----------------------------------------------------------------------------

--set all referrals for a patient / facility to be discharged with a reason of deceased
function sqDB.THS.setDeceasedReferral(patientId, refId, facId)

   sqUtils.printLog("setting referral "..refId.." for patient "..patientId.." to discharged..")

   sqDB.execute{sql=
      "update ewaiting_referrals " ..
      "set status = 'discharged' " ..
      "where id = " .. refId .. " and facility_id = "..facId, live=false}   
   
   --Update the ewaiting_discharges table and add a discharge reason id

   local discReasonId = sqUtils.getMappedValue(sqCfg.extra_params,"RIPReasonCode")

   local sqlStr = "insert into ewaiting_discharges (timestamp, res_id, referral_id, discharge_reason_id, discharge_notes,sq_user_id) " ..
   "values ("..
   "now()"..",null"..
   ","..
   refId..","..
   discReasonId..","..
   "''"..","..
   "0"..
   ")"
   trace(sqlStr)
   sqDB.execute{sql=sqlStr, live=false}

 
end

----------------------------------------------------------------------------

function sqDB.THS.addUpdatePatientAlerts(patientID,parentFacilityID,MsgIn)
   
   sqUtils.printLog("add/updating patient alert(s) for patient ID: "..patientID)
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
   
   --remove existing alerts that where hl7 generated for the patient as 
   --full alert history will come across in message
   local res = sqDB.execute{sql="delete from user_alerts where "..
            "user_id = "..conn:quote(patientID).." and sq_user_id = "..conn:quote(sqUser) , live=false}
   
   for i = 1, #MsgIn.ZPA do  

      local alertStart = MsgIn.ZPA[i][4]:S()
      local alertEnd = MsgIn.ZPA[i][5]:S()
      local alertCode = MsgIn.ZPA[i][3][1]:S()
      local alertDesc = MsgIn.ZPA[i][3][2]:S()
      local alertDomain = MsgIn.ZPA[i][2][1]:S()
      local alertComment = MsgIn.ZPA[i][6]:S()

      --insert the alert
      local sqlStr = "insert into user_alerts (user_id, alert, code, domain, start_date, end_date,sq_user_id, comments, created_At) " ..
      "values ("..conn:quote(patientID)..", ".. 
      conn:quote(alertDesc)..", "..
      conn:quote(alertCode)..", "..
      conn:quote(alertDomain)..", "
      
      if alertStart == '' then
         sqlStr = sqlStr.."NULL"..", "
      else
         sqlStr = sqlStr..conn:quote(alertStart)..", "
      end

      if alertEnd == '' then
         sqlStr = sqlStr.."NULL"..", "
      else
         sqlStr = sqlStr..conn:quote(alertEnd)..", "
      end

      sqlStr = sqlStr..
      conn:quote(sqUser)..", "..
      conn:quote(alertComment)..", "..
      "now()"..
      ")"
      trace(sqlStr)
      sqDB.execute{sql=sqlStr, live=false}

   end
   
end

--------------------------------------------------------------

function sqDB.THS.getMedicareNumbers(PID, idType)
   
   local medicareNumbers = ''
   for i=1, #PID[3] do
      if PID[3][i][5]:S() == idType then
         medicareNumbers = medicareNumbers .. PID[3][i][1]:S().."^".. PID[3][i][7]:S().."^"..PID[3][i][8]:S().."|"
      end
   end
   return medicareNumbers
   
end

--------------------------------------------------------------

function sqDB.THS.getNOKContacts(NK1, nokIdentifier, flag)
   
   --if flag = 1 then search on relationship instead of NK type
   
   local nokContacts = {}
   
   for i=1, #NK1 do
      if NK1[i][7][1]:S() == nokIdentifier or (flag == 1 and NK1[i][3][1]:S() == nokIdentifier) then
         trace(NK1[i])
         table.insert(nokContacts,NK1[i])
      end
   end
  
   return nokContacts
   
end

--------------------------------------------------------------

function sqDB.THS.createNKContact(patientID, nokContacts)
 
   --fill the NOK data variables with NOK details -----------------
   sqDB.THS.fillNKContactData(patientID, nokContacts)

   --insert the NOK contact as a user
   local sql =       
   "insert into users (parent, relationship, mobile, home_phone, firstname, surname," ..
   "Address_1, Address_2, Home_Address, Address_4, Date_of_Birth, title, gender, created) " ..
   "values ( " ..
   conn:quote(sqDB.nokData.nk1_parent)..",".. 
   conn:quote(sqDB.nokData.nk1_rel_id) .. "," ..
   "trim(replace(" .. conn:quote(sqDB.nokData.nk1_mobile_format) .. ", ' ', '')), " ..
   conn:quote(sqDB.nokData.nk1_phone)..","..
   -- conn:quote(email)..","..
   conn:quote(sqDB.nokData.nk1_firstname)..","..
   conn:quote(sqDB.nokData.nk1_surname)..","..
   conn:quote(sqDB.nokData.nk1_address_1)..","..
   conn:quote(sqDB.nokData.nk1_address_2)..","..
   conn:quote(sqDB.nokData.nk1_eircode)..","..
   conn:quote(sqDB.nokData.nk1_address_4)..","..
   conn:quote(sqDB.nokData.nk1_dob)..","..
   conn:quote(sqDB.nokData.nk1_title)..","..
   conn:quote(sqDB.nokData.nk1_gender)..","..
   "now()"..
   ")"

   sqDB.execute{sql=sql, live=false}

   local nk1_user_result = sqDB.execute{sql="select last_insert_id() as id", live=true}

   sqUtils.printLog("Added new related person record into users table, with id [" ..nk1_user_result[1].id:S() .. 
      "], for patient id [" ..sqDB.nokData.nk1_parent .. "]")

   -- Update related persons users_additional_info record to be next of kin if contact role is NOK

   local nokIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'nokIdentifier')   
   if nokContacts[7][1]:S() == nokIdentifier then
      sqDB.execute{sql=
         "insert into users_additional_info (user_id, act_as_nok)" ..
         "values (" ..conn:quote(nk1_user_result[1].id:S()) .. ", '1')", live=false }

      local nk1_userAddInfo_id = sqDB.execute{sql="select last_insert_id() as id", live=true}

      sqUtils.printLog("Created user_additional_info record with id: [" ..nk1_userAddInfo_id[1].id:S().."], for userID: [" ..nk1_user_result[1].id:S().."]")
   end

end

--------------------------------------------------------------

function sqDB.THS.updateNKContact(id, nokContacts)

   --fill the NOK data variables with NOK details -----------------
   sqDB.THS.fillNKContactData(id, nokContacts)
   
   --PB: 13.01.2025 : suppress filing NK1 data if surname , firstname and phone and mobile is blank
   if sqDB.nokData.nk1_surname == '' and sqDB.nokData.nk1_firstname == '' and sqDB.nokData.nk1_phone == ''  and sqDB.nokData.nk1_mobile == '' then
      sqUtils.printLog("Empty firstname, surname, phone and mobile in NK1 segment, skipping NK1 processing for patient id: " .. id)
      return
   end
   --
   
   local NKUserQueryResultSet = sqDB.execute{sql=
   "select count(id), id from users " ..
   "where parent = " .. id .. 
   " and firstname = "..conn:quote(sqDB.nokData.nk1_firstname).." and surname = "..
      conn:quote(sqDB.nokData.nk1_surname).." and date_of_birth = "..conn:quote(sqDB.nokData.nk1_dob) , live=true}
   
   -- Update existing or create new related person record in user table
   if NKUserQueryResultSet[1]['count(id)']:S() ~= '0' then

      sqDB.nokData.nk1_userID = NKUserQueryResultSet[1]['id']:S()

      sqUtils.printLog("Updating related person details from NK1 segment into users table for patient id: " .. id)

      local sql ="update users " ..
      "set "..
      "firstname = " .. conn:quote(sqDB.nokData.nk1_firstname) .. ", "..
      "surname = " .. conn:quote(sqDB.nokData.nk1_surname) .. ", "..
      "mobile = trim(replace(" .. conn:quote(sqDB.nokData.nk1_mobile_format) .. ", ' ', '')), "..
      "home_phone = " .. conn:quote(sqDB.nokData.nk1_phone) .. ", "..
      "Address_1 = " .. conn:quote(sqDB.nokData.nk1_address_1) .. ", "..
      "Address_2 = " .. conn:quote(sqDB.nokData.nk1_address_2) .. ", "..
      "Home_Address = " .. conn:quote(sqDB.nokData.nk1_eircode) .. ", "..
      "Address_4 = " .. conn:quote(sqDB.nokData.nk1_address_4) .. ", "..
      "Address_5 = " .. conn:quote(sqDB.nokData.nk1_address_5) .. ", "..
      "Date_of_Birth = " .. conn:quote(sqDB.nokData.nk1_dob) .. ", "..
      "title = " .. conn:quote(sqDB.nokData.nk1_title) .. ", "..
      "relationship = " .. conn:quote(sqDB.nokData.nk1_rel_id) .. ", "..
      "gender = " .. conn:quote(sqDB.nokData.nk1_gender) ..
      " where id = ".. sqDB.nokData.nk1_userID

      sqDB.execute{sql=sql, live=false}

   elseif NKUserQueryResultSet[1]['count(id)']:S() == '0' then

      sqUtils.printLog("Adding related person details from NK1 segment into users table for patient: " .. id)

      local sql = 
      "insert into users (parent, relationship, mobile, home_phone, firstname, surname," ..
      "Address_1, Address_2, Home_Address, Address_4, Address_5, title, Date_of_Birth, gender, created) " ..
      "values ( " ..
      conn:quote(sqDB.nokData.nk1_parent)..",".. 
      conn:quote(sqDB.nokData.nk1_rel_id) .. "," ..
      "trim(replace(" .. conn:quote(sqDB.nokData.nk1_mobile_format) .. ", ' ', '')), "..
      conn:quote(sqDB.nokData.nk1_phone)..","..
      -- conn:quote(email)..","..
      conn:quote(sqDB.nokData.nk1_firstname)..","..
      conn:quote(sqDB.nokData.nk1_surname)..","..
      conn:quote(sqDB.nokData.nk1_address_1)..","..
      conn:quote(sqDB.nokData.nk1_address_2)..","..
      conn:quote(sqDB.nokData.nk1_eircode)..","..
      conn:quote(sqDB.nokData.nk1_address_4)..","..
      conn:quote(sqDB.nokData.nk1_address_5)..","..
      conn:quote(sqDB.nokData.nk1_title)..","..
      conn:quote(sqDB.nokData.nk1_dob)..","..
      conn:quote(sqDB.nokData.nk1_gender)..","..
      "now()"..
      ")"

      sqDB.execute{sql=sql, live=false}

      local nk1_user_resultSet = sqDB.execute{sql="select last_insert_id() as id", live=true}
      sqDB.nokData.nk1_userID = nk1_user_resultSet[1].id:S()

      sqUtils.printLog("Added new related person record into users table, with id [" ..sqDB.nokData.nk1_userID .. 
         "], for patient ID [" ..sqDB.nokData.nk1_parent .. "]")

   end -- End users record added/updated in users table

   -- If related person is NOK, 
   -- then update existing, or create new, user_additional_info record
   -- and set act_as_nok to '1' (true)

   local nokIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'nokIdentifier')  
   if nokContacts[7][1]:S() == nokIdentifier then

      local NKUserAddInfoResultSet = sqDB.execute{sql=
         "select count(id), id from users_additional_info " ..
         "where user_id = " .. sqDB.nokData.nk1_userID, live=true}

      -- If users_additional_info record exists, update it
      if NKUserAddInfoResultSet[1]['count(id)']:S() ~= '0' then
         local sql = "update users_additional_info " ..
         "set act_as_nok = '1' " .. 
         "where id = ".. NKUserAddInfoResultSet[1]['id']:S()

         sqDB.execute{sql=sql, live=false}

         sqUtils.printLog("Added NOK category status to users_additional_info for user ID [" .. sqDB.nokData.nk1_userID .. "]")

         -- else if users_additional_info record does not exist, then create it
      elseif NKUserAddInfoResultSet[1]['count(id)']:S() == '0' then

         local sql = "insert into users_additional_info (user_id, act_as_nok) " ..
         "values (" ..conn:quote(sqDB.nokData.nk1_userID) .. ", '1')"

         sqDB.execute{sql=sql, live=false}

         local nk1_act_as_nok_result = sqDB.execute{sql="select last_insert_id() as id", live=true}

         --[[sqUtils.printLog("Created user_additional_info record with id: [" ..nk1_act_as_nok_result[1].id:S()..
            "], for userID: [" ..sqDB.nokData.nk1_userID.."]")
         ]]

      end            
   end -- End NK1[7][1] == NOK
   
   
   -- If related person is emergency contact, 
   -- then update existing, or create new, user_additional_info record
   -- and set emergency_contact to '1' (true)

   local emergIdentifier = sqUtils.getJsonMappedValue(sqCfg.extra_params,'emergencyContactIdentifier')  
   if nokContacts[7][1]:S() == emergIdentifier then

      local NKUserAddInfoResultSet = sqDB.execute{sql=
         "select count(id), id from users_additional_info " ..
         "where user_id = " .. sqDB.nokData.nk1_userID, live=true}

      -- If users_additional_info record exists, update it
      if NKUserAddInfoResultSet[1]['count(id)']:S() ~= '0' then
         local sql = "update users_additional_info " ..
         "set emergency_contact = '1' " .. 
         "where id = ".. NKUserAddInfoResultSet[1]['id']:S()

         sqDB.execute{sql=sql, live=false}

         sqUtils.printLog("Added emergency category status to users_additional_info for user ID [" .. sqDB.nokData.nk1_userID .. "]")

         -- else if users_additional_info record does not exist, then create it
      elseif NKUserAddInfoResultSet[1]['count(id)']:S() == '0' then

         local sql = "insert into users_additional_info (user_id, emergency_contact) " ..
         "values (" ..conn:quote(sqDB.nokData.nk1_userID) .. ", '1')"

         sqDB.execute{sql=sql, live=false}

         local nk1_act_as_emergency_result = sqDB.execute{sql="select last_insert_id() as id", live=true}

         sqUtils.printLog("Created user_additional_info record with id: [" ..nk1_act_as_emergency_result[1].id:S()..
            "], for userID: [" ..sqDB.nokData.nk1_userID.."]")
         

      end            
   end -- End NK1[7][1] == EMERG

   
end

--------------------------------------------------------------

function sqDB.THS.updateGTContact(id, gtContact)

   --fill the NOK data variables with NOK details -----------------
   sqDB.THS.fillGTContactData(id, gtContact)
   
   local GTUserQueryResultSet = sqDB.execute{sql=
   "select count(id), id from users " ..
   "where parent = " .. id .. 
   " and firstname = "..conn:quote(sqDB.gtData.gt1_firstname).." and surname = "..
      conn:quote(sqDB.gtData.gt1_surname).." and date_of_birth = "..conn:quote(sqDB.gtData.gt1_dob) , live=true}
   
   -- Update existing or create new related person record in user table
   if GTUserQueryResultSet[1]['count(id)']:S() ~= '0' then

      sqDB.gtData.gt1_userID = GTUserQueryResultSet[1]['id']:S()

      sqUtils.printLog("Updating related person details from GT1 segment into users table for patient id: " .. id)

      local sql ="update users " ..
      "set "..
      "firstname = " .. conn:quote(sqDB.gtData.gt1_firstname) .. ", "..
      "surname = " .. conn:quote(sqDB.gtData.gt1_surname) .. ", "..
      "mobile = trim(replace(" .. conn:quote(sqDB.gtData.gt1_mobile_format) .. ", ' ', '')), "..
      "home_phone = " .. conn:quote(sqDB.gtData.gt1_phone) .. ", "..
      "Address_1 = " .. conn:quote(sqDB.gtData.gt1_address_1) .. ", "..
      "Address_2 = " .. conn:quote(sqDB.gtData.gt1_address_2) .. ", "..
      "Home_Address = " .. conn:quote(sqDB.gtData.gt1_eircode) .. ", "..
      "Address_4 = " .. conn:quote(sqDB.gtData.gt1_address_4) .. ", "..
      "Address_5 = " .. conn:quote(sqDB.gtData.gt1_address_5) .. ", "..
      "Date_of_Birth = " .. conn:quote(sqDB.gtData.gt1_dob) .. ", "..
      "title = " .. conn:quote(sqDB.gtData.gt1_title) .. ", "..
      "relationship = " .. conn:quote(sqDB.gtData.gt1_rel_id) .. ", "..
      "gender = " .. conn:quote(sqDB.gtData.gt1_gender) ..
      " where id = ".. sqDB.gtData.gt1_userID

      sqDB.execute{sql=sql, live=false}

   elseif GTUserQueryResultSet[1]['count(id)']:S() == '0' then

      sqUtils.printLog("Adding related person details from GT1 segment into users table for patient: " .. id)

      local sql = 
      "insert into users (parent, relationship, mobile, home_phone, firstname, surname," ..
      "Address_1, Address_2, Home_Address, Address_4, Address_5, title, Date_of_Birth, gender, created) " ..
      "values ( " ..
      conn:quote(sqDB.gtData.gt1_parent)..",".. 
      conn:quote(sqDB.gtData.gt1_rel_id) .. "," ..
      "trim(replace(" .. conn:quote(sqDB.gtData.gt1_mobile_format) .. ", ' ', '')), "..
      conn:quote(sqDB.gtData.gt1_phone)..","..
      -- conn:quote(email)..","..
      conn:quote(sqDB.gtData.gt1_firstname)..","..
      conn:quote(sqDB.gtData.gt1_surname)..","..
      conn:quote(sqDB.gtData.gt1_address_1)..","..
      conn:quote(sqDB.gtData.gt1_address_2)..","..
      conn:quote(sqDB.gtData.gt1_eircode)..","..
      conn:quote(sqDB.gtData.gt1_address_4)..","..
      conn:quote(sqDB.gtData.gt1_address_5)..","..
      conn:quote(sqDB.gtData.gt1_title)..","..
      conn:quote(sqDB.gtData.gt1_dob)..","..
      conn:quote(sqDB.gtData.gt1_gender)..","..
      "now()"..
      ")"

      sqDB.execute{sql=sql, live=false}

      local gt1_user_resultSet = sqDB.execute{sql="select last_insert_id() as id", live=true}
      sqDB.gtData.gt1_userID = gt1_user_resultSet[1].id:S()

      sqUtils.printLog("Added new related person record into users table, with id [" ..sqDB.gtData.gt1_userID .. 
         "], for patient ID [" ..sqDB.gtData.gt1_parent .. "]")

   end -- End users record added/updated in users table
   
end

--------------------------------------------------------------

function sqDB.THS.getSQFacilityToIPMMApping(facilityId)
   
   local clinic = ''
   local session = ''
   local cons = ''
   local spec = ''

   local res = sqDB.execute{sql=
      "select count(id) as cnt, clinic_code, session_code, consultant_code, speciality_id from facility_clinic_codes where facility_id = "..facilityId, live=true}
   
   if res[1].cnt:S() == '0' then
      error("Missing mapping in CSR table for facility id: "..facilityId)
   end
   
   clinic = res[1].clinic_code:S()
   session = res[1].session_code:S()
   cons = res[1].consultant_code:S()
   
   --lookup the speciality
   res = sqDB.execute{sql=
      "SELECT count(s.id) as cnt, s.code as specCode FROM specialities s "..
      " JOIN facility_specialities fs ON fs.speciality_id = s.id ".. 
      " JOIN facility_clinic_codes fcc ON fcc.facility_id = fs.facility_id "..
      " where fcc.facility_id = "..facilityId, live=true}
   
   if res[1].cnt:S() == '0' then
      error("Missing speciality for facility id: "..facilityId)
   end
   local spec = res[1].specCode:S()
   
   
   return clinic, session, cons, spec
  
   
end

--------------------------------------------------------------

function sqDB.THS.getParentFacilityFromCSR(clinic, hospCode)
   
   local parentFacility = ''
   local cons = ''
   
   local res = sqDB.execute{sql=
      "select count(fcc.id) as cnt, f.parent_id, fcc.consultant_code from facility_clinic_codes fcc join facilities f on f.id = fcc.facility_id "..
      "where fcc.clinic_code = "..conn:quote(clinic).." and fcc.hospital_code = "..conn:quote(hospCode).." limit 1", live=true}
   
   if res[1].cnt:S() ~= '0' then

      parentFacility = res[1].parent_id:S()
      cons = res[1].consultant_code:S()

   end
   
   return parentFacility, cons
   
end

--------------------------------------------------------------

function sqDB.THS.getFacilityFromCSR(site, clinic, session)
   
   local facilityID = ''
   
   local res = sqDB.execute{sql=
      "select count(id) as cnt, facility_id from facility_clinic_codes where hospital_code = "..conn:quote(site)..
      " and clinic_code = "..conn:quote(clinic).." and session_code = "..conn:quote(session), live=true}
   
   if res[1].cnt:S() ~= '1' then
      error("Facility ID not found in CSR table for ("..site.."|"..clinic.."|"..session..")")
   end
   
   facilityID = res[1].facility_id:S()
 
   return facilityID
   
end

--------------------------------------------------------------

function sqDB.THS.closePatientReferral(id, facilityID, controlID, disOutcome)
   
   sqUtils.printLog("Calling "..getCurrentFunctionName().."...")
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID")
   
   local referralCount = sqDB.execute{sql="select count(id), id from ewaiting_referrals where "..
      " user_id = " .. id .. " and control_id = "..conn:quote(controlID), live=true}
   
   if referralCount[1]['count(id)']:S() == '0' then
      sqUtils.printLog("Referral does not exist, Skipping closure...")
      return
   end
   
   --referral exists so discharge
   
   --set to discharged
   local referralID = referralCount[1].id:S()
   local sql ="update ewaiting_referrals " .."set ".."modified = now(), modified_by = "..sqUser..", "..
   "status = 'discharged'" ..   " where id = ".. referralID
   sqDB.execute{sql=sql, live=false}
   
   sqUtils.printLog("Referral "..referralID.." set to discharged")
   
   --get the discharge reason
   local res = sqDB.execute{sql="select count(id) as cnt, id from ewaiting_discharge_reasons where "..
      " code = " .. conn:quote(disOutcome).." and facility_id = "..facilityID, live=true}

   local disReasonID = 0
   if res[1].cnt:S() ~= '0' then
      disReasonID = res[1].id:S()
   end
   
   local sqlStr = "insert into ewaiting_discharges (timestamp, referral_id, discharge_reason_id, discharge_notes,sq_user_id) " ..
   "values (".. "now()"..",".. referralID..",".. disReasonID..",".. "''"..",".. sqUser.. ")"
   trace(sqlStr)
   sqDB.execute{sql=sqlStr, live=false}

   sqUtils.printLog("Closeure / Discharging of referral completed!")
   
end

----------------------------------------------------------------------------

function sqDB.THS.findGp(GP,facilityid)   
   --[[
    search facility_gp table where facility_id and hospital_code and gmc_code and dh_code match
    use gp_details column value from search and find in Gp_Details table
   ]]--  
   
   local sql = 'select gp_details FROM facility_gp where facility_id = '..facilityid..' and hospital_code = '..GP.hospital_code..
   ' and gmc_code = '.. GP.gmc_code..
   ' and dh_code = '.. GP.dh_code..' LIMIT 1'
   
   local facgp_detailsResult =sqDB.execute{sql=sql, live=true}  
   
   if facgp_detailsResult ~= nil then
      sql = 'select count(1) FROM gp_details where id='..facgp_detailsResult[1]['gp_details']:S()
      local gp_detailsResult =sqDB.execute{sql=sql, live=true}  

      if gp_detailsResult[1]['count(1)']:S() == '0' then
         return nil
      else
         return facgp_detailsResult[1]
      end
      
	end
end

----------------------------------------------------------------------------

function sqDB.THS.createGP(GP,facilityid)
	--[[
    create a gp_details table record with firstname, address_1, address_2, address_3, address_4, address_5 and phone
    insert facility_gp with facility_id,gp_details,hospital_code,gmc_code,dh_code
   ]]--
   local sql = 'INSERT gp_details (firstname,address_1,address_2,address_3,address_4,address_5, phone) '..
   'VALUES ('..conn:quote(GP.firstname:S())..','..conn:quote(GP.address_1:S())..','..
   conn:quote(GP.address_2:S())..','..conn:quote(GP.address_3:S())..','..
   conn:quote(GP.address_4:S())..','..conn:quote(GP.address_5:S())..','..conn:quote(GP.phone:S())..')'
     
   sqDB.execute{sql=sql, live=false}

   local gp_details_result = sqDB.execute{sql="select last_insert_id() as id", live=true}

   sql = 'INSERT facility_gp (facility_id,gp_details,hospital_code,gmc_code,dh_code) '..
   'VALUES ('..facilityid..','..
   gp_details_result[1]['id']:S()..','..
   GP.hospital_code..','..
   GP.gmc_code..','..
   GP.dh_code..')'      
   
   sqDB.execute{sql=sql, live=false}   
   
   sqUtils.printLog("GP "..GP.firstname.." ("..GP.hospital_code..") Created!")
   
end

----------------------------------------------------------------------------

function sqDB.THS.updateGP(GP,facilityid)
   --[[
    find record in gp_details table by id
    if facility id is different to found facility_id in gp_details then abort
    update gp_details record
    update facility_gp record, set    
   ]]--
   local sql = 'select gp_details FROM facility_gp where facility_id = '..facilityid..' AND id='..GP.id:S()..' LIMIT 1'
   
   local gp_details_result = sqDB.execute{sql=sql, live=true}
   
   -- if we can't find the GP then abort
   if gp_details_result == nil then      
      return
   end  
   
   sql = 'UPDATE gp_details SET '..
   'firstname='..conn:quote(GP.firstname:S())..',address_1='..conn:quote(GP.address_1:S())..',address_2='..conn:quote(GP.address_2:S())..',address_3='..conn:quote(GP.address_3:S())..',address_4='..conn:quote(GP.address_4:S())..
   ',address_5='..conn:quote(GP.address_5:S())..',phone='..conn:quote(GP.phone:S())..' '..
   'WHERE id='..GP.id:S()
      
   sqDB.execute{sql=sql, live=false}
   
   sql = 'UPDATE facility_gp SET '..
   'hospital_code='..GP.hospital_code..',gmc_code='..GP.gmc_code..',dh_code='..GP.dh_code..' '..  
   'WHERE facility_id = '..facilityid..' AND id='..GP.id:S() 
   
   sqDB.execute{sql=sql, live=false}
   
   sqUtils.printLog("GP "..GP.firstname.." ("..GP.hospital_code..") Updated!")
   
end

----------------------------------------------------------------------------

function sqDB.THS.changeUserNotificationPreferencesForAllEvents(uid,channelname,newstate)
   
   -- generic enable / disable all notification events for specified channel name
   --[[
   -- Set sms notifications - $patient->enableNotificationsForChannel('sms', $body['sms_notifications'] === 'Y');  
    Ok, that part is actually a lil complex, basically, you first need to get the event channel id FROM user_notification_channels (SELECT id FROM user_notification_channels WHERE key = 'sms')
            3:47
            Then you need to get all the Notification Events (SELECT * FROM user_notification_events)
            3:49
            then for each even you need to ensure it's set to enabled in user_notification_preferences using the user_id, event_id, channel_id and enabled (bool)
   --]]
   sqUtils.printLog("Calling sqDB.NAAS.changeuser_notification_preferencesforallevents...")  
   
     
   local sql = "SELECT id From user_notification_channels unc where unc.key = "..conn:quote(channelname)
   local smsid_result = sqDB.execute{sql=sql, live=true}   

   sqUtils.printLog(channelname.." = " .. smsid_result[1].id)

   sql = "SELECT * FROM user_notification_events"
   local un_events_result = sqDB.execute{sql=sql, live=true}   

   for i = 1, #un_events_result do
      trace(un_events_result[i].id)
      
      sql = "SELECT count(id) FROM user_notification_preferences WHERE user_id=".. uid .." AND event_id=" .. un_events_result[i].id .. " AND channel_id = " ..smsid_result[1].id
      trace(sql)
      
      local unpCount = sqDB.execute{sql=sql, live=true}      
      
      if unpCount[1]['count(id)']:S() == '0' then
         -- do insert
         if newstate then
            sql = "INSERT INTO user_notification_preferences (user_id,event_id,channel_id,enabled) VALUES (".. uid .."," .. un_events_result[i].id .. "," ..smsid_result[1].id.. ",1)"
         else
            sql = "INSERT INTO user_notification_preferences (user_id,event_id,channel_id,enabled) VALUES (".. uid .."," .. un_events_result[i].id .. "," ..smsid_result[1].id.. ",0)"
         end
         trace(sql)
         sqDB.execute{sql=sql, live=false}      
      else
         -- do update
         if newstate then        
            sql = "UPDATE user_notification_preferences SET enabled=1 WHERE user_id=".. uid .." AND event_id=" .. un_events_result[i].id .. " AND channel_id = " ..smsid_result[1].id
         else
            sql = "UPDATE user_notification_preferences SET enabled=0 WHERE user_id=".. uid .." AND event_id=" .. un_events_result[i].id .. " AND channel_id = " ..smsid_result[1].id
         end
         trace(sql)
         sqDB.execute{sql=sql, live=false}      
      end
   end   
end

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------

--     RADIOLOGY IMPLEMENTATION  -----


-----------------------------------------------------------------

-----------------------------------------------------------------

function sqDB.RAD.cancelAndRebook(facilityID, patientID, episodeNumber, orderResID, room_code, opdAppointmentString, opdAppointmentTimeString, opdAppointmentEndTimeString)

   local resCheck = sqDB.execute{sql=
      "select count(*), IFNULL(id,0) as res_id from reservation where location_pref = " .. conn:quote(orderResID) .. " and status = 'active'", live=true}
   --
   if (resCheck[1]['res_id']:S() ~= '0') then
      local resCheckUpd = sqDB.execute{sql=
         "update reservation set status = 'cancelled' where location_pref = " .. conn:quote(orderResID).. " and status = 'active'", live=false}  
      conn:commit{live=true}
   end
   --
   local AppCount = sqDB.execute{sql=
      "select count(facility_id), facility_id " ..
      "from reason_exam_codes where facility_id " ..
      "in (select id from facilities where id = "..facilityID.." \
      or parent_id = "..facilityID..") and room_id = " .. 
      conn:quote(room_code) .. " limit 1", live=true}

   if AppCount[1]['count(facility_id)']:S() ~= '0'  then

      local sql ="insert into reservation (facility_id, user_id, facility_type, consultant_id, " ..
      "location_pref, reservation_date,reservation_time, reservation_end_time, google_cal_id, " ..
      "reason_id,recurrence,date,status,attended_by, is_attended,booking_user_id," ..
      "source, print,stats, quality,capacity_sync, is_urgent) " .. 
      "values ("..
      AppCount[1]['facility_id']..",0,'f',0, " ..
      conn:quote(orderResID) ..",".. --''"..","..
      conn:quote(opdAppointmentString)..","..
      conn:quote(opdAppointmentTimeString)..","..
      conn:quote(opdAppointmentEndTimeString)..","..
      "'',1,'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0"..
      ")"

      iguana.logDebug("SQL (Add reservation): ".. sql)
      sqDB.execute{sql=sql, live=false}
      conn:commit{live=true}
   end  


   --[[ CI: 20/10/2023 : Check whether there is a referral associated with this booking, which was previously 'for Swiftqueue'.
   If referral found, then move it to assigned to make sure that the patient is no longer able to book an appointment]]                  
   local referralID = sqDB.RAD.getPatientReferralByEpisode(patientID, facilityID, episodeNumber)
   if referralID ~= '0' then

      local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_Ref")

      local sql ="update ewaiting_referrals " ..
      "set "..
      "modified = now(), modified_by = "..sqUser..", "..
      "status = 'assigned'" ..
      " where id = ".. referralID ..
      " and status in ('pre_triage', 'triage', 'letter_sent', 'active')"
      iguana.logDebug("NSQ:SIU received:update ewaiting_referrals: ".. sql)
      sqDB.execute{sql=sql, live=false}
   end

end

---------------------------------------------------------------------

function sqDB.RAD.process_NFSQ_S12(facilityID, patientID, episodeNumber, orderResID, room_code, opdAppointmentString, opdAppointmentTimeString, opdAppointmentEndTimeString)

   --if referral exists then set as attending   
   
   local referralID = sqDB.RAD.getPatientReferralByEpisode(patientID, facilityID, episodeNumber)
   if referralID ~= '0' then

      local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_Ref")

      local sql ="update ewaiting_referrals " ..
      "set "..
      "modified = now(), modified_by = "..sqUser..", "..
      "status = 'assigned'" ..
      " where id = ".. referralID ..
      " and status in ('pre_triage', 'triage', 'letter_sent', 'active')"
      iguana.logDebug("NSQ:SIU received:update ewaiting_referrals: ".. sql)
      sqDB.execute{sql=sql, live=false}

   else
      
      --referral doesn;t exist so...
      
      local resCheck = sqDB.execute{sql=
         "select count(*), IFNULL(id,0) as res_id from reservation where location_pref = " .. conn:quote(orderResID) .. " and status = 'active'", live=true}
      
      --cancel the app if it exists
      
      if (resCheck[1]['res_id']:S() ~= '0') then
         local resCheckUpd = sqDB.execute{sql=
            "update reservation set status = 'cancelled' where location_pref = " .. conn:quote(orderResID).. " and status = 'active'", live=false}  
         conn:commit{live=true}
      end
      
      --and if the exam and room are manged by SQ then create an appointment
      
      local AppCount = sqDB.execute{sql=
         "select count(facility_id), facility_id " ..
         "from reason_exam_codes where facility_id " ..
         "in (select id from facilities where id = "..facilityID.." \
         or parent_id = "..facilityID..") and room_id = " .. 
         conn:quote(room_code) .. " limit 1", live=true}

      if AppCount[1]['count(facility_id)']:S() ~= '0'  then

         local sql ="insert into reservation (facility_id, user_id, facility_type, consultant_id, " ..
         "location_pref, reservation_date,reservation_time, reservation_end_time, google_cal_id, " ..
         "reason_id,recurrence,date,status,attended_by, is_attended,booking_user_id," ..
         "source, print,stats, quality,capacity_sync, is_urgent) " .. 
         "values ("..
         AppCount[1]['facility_id']..",0,'f',0, " ..
         conn:quote(orderResID) ..",".. --''"..","..
         conn:quote(opdAppointmentString)..","..
         conn:quote(opdAppointmentTimeString)..","..
         conn:quote(opdAppointmentEndTimeString)..","..
         "'',1,'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0"..
         ")"

         iguana.logDebug("SQL (Add reservation): ".. sql)
         sqDB.execute{sql=sql, live=false}

         print("DB Commit at line 194")
         conn:commit{live=true}
      end  

   end

end

-----------------------------------------------------------------

function sqDB.RAD.updateUserAudit(appId, userId, action)
   --Add A user audit record for the app creation
   --Action must be one of the following:
   --appointment.created
   --appointment.rescheduled
   --appointment.cancelled
   --appointment.dna
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_App")
   
   sqDB.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
      "values (null, now(),"..sqUser..","..conn:quote(action)..","..
      conn:quote(appId)..","..userId..",null)"
      , live=false}   
end

----------------------------------------------------------------------------

function sqDB.RAD.getPatientIdByMrn(fid, mrn, firstname, surname, pmobile, pdob)
   
   local pmobile2 = sqUtils.getMappedValue(sqCfg.extra_params,"telAreaCode").. pmobile:sub(2,15)

   local PatientId = sqDB.execute{sql=
      "select count(id), id from users " ..
      "where mrn = " .. conn:quote(mrn) .. 
      " and mrn <> '' and id in (select user_id from facility_user where facility_id = " .. fid ..")", 
      live=true}

   if PatientId[1]['count(id)']:S() == '0' then
      
      local PatientId2 = sqDB.execute{sql=
         "select count(id), id from users " ..
         "where firstname = " .. conn:quote(firstname) .. 
         " and surname = " .. conn:quote(surname) ..
         " and Date_of_Birth = " .. conn:quote(pdob) .. 
         " and mobile = " .. conn:quote(pmobile2),
         live=true}
      --
      if PatientId2[1]['count(id)']:S() == '0' then
         return '0'
      else
         local sql ="update users " ..
         "set "..
         "mrn = " .. conn:quote(mrn) ..
         "where id = ".. PatientId2[1].id:S()

         sqDB.execute{sql=sql, live=false}
         
         return PatientId2[1].id:S()
      end
   else
      return PatientId[1].id:S()
   end
   
end

----------------------------------------------------------------------------

function sqDB.RAD.createPatient(PID)
   
   sqUtils.printLog("Creating patient "..PID[3][1][1]:S().."...")
   
   local telAreaCode = sqUtils.getMappedValue(sqCfg.extra_params,"telAreaCode")
   local telCode_HOME = sqUtils.getMappedValue(sqCfg.extra_params,"telCode_HOME")
   local telCode_MOBILE = sqUtils.getMappedValue(sqCfg.extra_params,"telCode_MOBILE")

   local mrn = PID[3][1][1]:S()
   local firstname = PID[5][2]:nodeValue()
   local surname = PID[5][1]:nodeValue()
   local user_type = "N"
   local user_status = 'active'
   local dob = sqUtils.parseDOB(PID[7])
   local gender = PID[8]:nodeValue()
   local email = PID[11][2][1]:nodeValue()
   local home_phone = PID[14][1]:S()
   local mobile1 = PID[13][1][1]:S()

   if PID[13][1][2]:S() == telCode_HOME then
      home_phone = PID[13][1][1]:S()
   elseif PID[13][1][2]:S() == telCode_MOBILE then
      mobile1 = PID[13][1][1]:S()
   end
   --
   if PID[13][2][2]:S() == telCode_HOME then
      home_phone = PID[13][2][1]:S()
   elseif PID[13][2][2]:S() == telCode_MOBILE then
      mobile1 = PID[13][2][1]:S()
   end

   local address1 = PID[11][1][1]:nodeValue() -- PID[11][1][2]
   local address2 = PID[11][1][2]:nodeValue()
   local address3 = PID[11][1][3]:nodeValue()
   local address4 = PID[11][1][4]:nodeValue()
   local homeAddress = PID[11][1][5]:nodeValue()

   local accountNumber = PID[18]:S()

    local mobile = ''
   if mobile1:sub(2,15) ~= '' then
      mobile = telAreaCode..mobile1:sub(2,15)
   end

    
   sqDB.execute{sql=
      "insert into users " ..
      "(mobile, home_phone, email, mrn, firstname, surname, user_type, status, Date_of_Birth, Gender, " ..
      "Address_1, Address_2, Address_3, Address_4, Home_Address, created, Chart_Number) " ..
      "values ("..
      " trim(replace("..conn:quote(mobile)..", ' ', ''))"..","..
      " trim(replace("..conn:quote(home_phone)..", ' ', ''))"..","..
      conn:quote(email)..","..
      conn:quote(mrn)..","..
      conn:quote(firstname)..","..
      conn:quote(surname)..","..
      conn:quote(user_type)..","..
      conn:quote(user_status)..","..
      conn:quote(dob)..","..
      conn:quote(gender)..","..
      conn:quote(address1)..","..
      conn:quote(address2)..","..
      conn:quote(address3)..","..
      conn:quote(address4)..","..
      conn:quote(homeAddress)..","..
      "now()"..","..
      conn:quote(accountNumber)..
      ")"
      , live=false}

  
   -- Update Comms preferences
   local EmailPref = PID[11][2][7]:S()
   local EmailPrefInd = PID[11][2][8]:S()
   local SmsPref = PID[13][1][2]:S()
   local SmsPrefInd = PID[13][1][3]:S()

   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}

   if EmailPrefInd == 'N' then
      sqDB.execute{sql=
         "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
         "values (null,"..
         conn:quote(result[1].id:S())..","..
         "1,2,0)"
         , live=false}
      sqDB.execute{sql=
         "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
         "values (null,"..
         conn:quote(result[1].id:S())..","..
         "2,2,0)"
         , live=false}  
      sqDB.execute{sql=
         "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
         "values (null,"..
         conn:quote(result[1].id:S())..","..
         "3,2,0)"
         , live=false}  
      sqDB.execute{sql=
         "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
         "values (null,"..
         conn:quote(result[1].id:S())..","..
         "4,2,0)"
         , live=false}  
   end
   --
   if SMSPrefInd == 'N' then
      sqDB.execute{sql=
         "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
         "values (null,"..
         conn:quote(result[1].id:S())..","..
         "1,1,0)"
         , live=false}
      sqDB.execute{sql=
         "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
         "values (null,"..
         conn:quote(result[1].id:S())..","..
         "2,1,0)"
         , live=false}  
      sqDB.execute{sql=
         "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
         "values (null,"..
         conn:quote(result[1].id:S())..","..
         "3,1,0)"
         , live=false}  
      sqDB.execute{sql=
         "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
         "values (null,"..
         conn:quote(result[1].id:S())..","..
         "4,1,0)"
         , live=false}  
   end
   
   sqUtils.printLog("Patient created!")

   return result[1].id:S()

end

----------------------------------------------------------------------------

function sqDB.RAD.updatePatient(id, PID)
   
   sqUtils.printLog("Updating patient...")
   
   local telAreaCode = sqUtils.getMappedValue(sqCfg.extra_params,"telAreaCode")
   local telCode_HOME = sqUtils.getMappedValue(sqCfg.extra_params,"telCode_HOME")
   local telCode_MOBILE = sqUtils.getMappedValue(sqCfg.extra_params,"telCode_MOBILE")

   local mrn = PID[3][1][1]:S()
   local firstname = PID[5][2]:nodeValue()
   local surname = PID[5][1]:nodeValue()
   local dob = sqUtils.parseDOB(PID[7])
   local gender = PID[8]:nodeValue()  
   local mobile = PID[13][1][1]:nodeValue()
   local home_phone = PID[14][1]:nodeValue()
   local address1 = PID[11][1][1]:nodeValue()
   local address2 = PID[11][1][2]:nodeValue()
   local address3 = PID[11][1][3]:nodeValue()
   local address4 = PID[11][1][4]:nodeValue()
   local homeAddress = PID[11][1][5]:nodeValue()
   local accountNumber = PID[18]:S()
   local email = PID[11][2][1]:nodeValue()
   local home_phone = PID[14][1]:S()
   local mobile1 = PID[13][1][1]:S()

   if PID[13][1][2]:S() == telCode_HOME then
      home_phone = PID[13][1][1]:S()
   elseif PID[13][1][2]:S() == telCode_MOBILE then
      mobile1 = PID[13][1][1]:S()
   end
   
   if PID[13][2][2]:S() == telCode_HOME then
      home_phone = PID[13][2][1]:S()
   elseif PID[13][2][2]:S() == telCode_MOBILE then
      mobile1 = PID[13][2][1]:S()
   end

   local mobile = ''
   if mobile1:sub(2,15) ~= '' then
      mobile = telAreaCode..mobile1:sub(2,15)
   end
   
   local sql ="update users " ..
   "set "..
   "firstname = " .. conn:quote(firstname) .. ", "..
   "surname = " .. conn:quote(surname) .. ", "..
   "Date_of_Birth = " .. conn:quote(dob) .. ", "
   
    --PB 12.06.24 : ir-321 - suppress updating email if blank value received
   if not sqUtils.isEmpty(email) then
      sql = sql.."email = " .. conn:quote(email) .. ", "
   end
   
    --PB 29.10.24 : ir-506 - suppress updating mobile if blank value received
   if not sqUtils.isEmpty(mobile) then
      sql = sql.."mobile = " .. " trim(replace("..conn:quote(mobile)..", ' ', ''))"..", "
   end
   
   sql = sql..
   "home_phone = " .. " trim(replace("..conn:quote(home_phone)..", ' ', ''))"..", "..
   "Gender = " .. conn:quote(gender) .. ", "..
   "Address_1 = " .. conn:quote(address1) .. ", "..
   "Address_2 = " .. conn:quote(address2) .. ", "..
   "Address_3 = " .. conn:quote(address3) .. ", "..
   "Address_4 = " .. conn:quote(address4) .. ", "..
   "Home_Address = " .. conn:quote(homeAddress) .. ", "..
   "Chart_Number = " .. conn:quote(accountNumber) ..
   " where id = ".. id


   sqDB.execute{sql=sql, live=false}   
   
   sqUtils.printLog("Patient updated!")
   
end

----------------------------------------------------------------------------

function sqDB.RAD.addToFacility(patientId, facilityId)

   return sqDB.addToFacility(patientId, facilityId)

end
      
----------------------------------------------------------------------------

function sqDB.RAD.getResByOrderId(OrderID)
   
 local res = sqDB.execute{sql=
    "select count(*), IFNULL(id,0) as res_id from reservation where location_pref = " .. conn:quote(OrderID), 
      live=true}
   
 return res

end

----------------------------------------------------------------------------

function sqDB.RAD.getReferralOrderID(OrderID)
   
   local res = sqDB.execute{sql=
      "select count(id), IFNULL(order_id,0) as id from ewaiting_referral_orders where order_id = " .. conn:quote(OrderID), 
      live=true}
   
   return res[1].id:S()

end

----------------------------------------------------------------------------

function sqDB.RAD.updateResByOrderId(OrderID)

   sqUtils.printLog("Setting reservation status to cancelled...")
   
   local res = sqDB.execute{sql=
      "update reservation set status = 'cancelled' where location_pref = " .. conn:quote(OrderID).. " and status = 'active'", 
      live=false}  

   --taken from old version, may not need
   --conn:commit{live=true}

end

----------------------------------------------------------------------------

function sqDB.RAD.getPatientReferralByEpisode(id, facilityID, episodeNumber)

   local res = sqDB.execute{sql=
      "select count(id), id from ewaiting_referrals " ..
      "where facility_id = " .. facilityID .. 
      " and user_id = " .. id .. 
      " and id in " ..
      "(select ewaiting_referral_id from ewaiting_referral_orders " ..
      "where episode_number = " .. conn:quote(episodeNumber) .. ")", 
      live=true}

   if res[1]['count(id)']:S() == '0' then
      return '0'
   else
      return res[1].id:S()
   end

end

----------------------------------------------------------------------------

function sqDB.RAD.dischargeReferral(referralID)

   sqUtils.printLog("Vetting status is cancelled , setting referral to discharged...")
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_Ref")
   
   local sql ="update ewaiting_referrals " ..
   "set "..
   "modified = now(), modified_by = "..sqUser..", "..
   "status = 'discharged'" ..
   " where id = ".. referralID

   sqDB.execute{sql=sql, live=false}    

end

----------------------------------------------------------------------------

function sqDB.RAD.activateReferral(referralID)

   sqUtils.printLog("Setting referral to active...")

   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_Ref")

   local sql ="update ewaiting_referrals " ..
   "set "..
   "modified = now(), modified_by = "..sqUser..", "..
   "status = 'active'" ..
   " where id = ".. referralID ..
   " and status in ('pre_triage', 'triage', 'letter_sent')"
   --DD Added 200123
   --Desc: Only update status of referral to active if the status is at a pre-active state
   
   sqDB.execute{sql=sql, live=false}   

end

----------------------------------------------------------------------------

function sqDB.RAD.assignReferral(referralID)
   
   sqUtils.printLog("Setting referral to assigned...")
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_Ref")
   
   local sql ="update ewaiting_referrals " ..
   "set "..
   "modified = now(), modified_by = "..sqUser..", "..
   "status = 'assigned'" ..
   " where id = ".. referralID ..
   " and status in ('pre_triage', 'triage', 'letter_sent', 'active')"

   sqDB.execute{sql=sql, live=false}
   
end

----------------------------------------------------------------------------

function sqDB.RAD.setReferralLetterSent(referralID)

   sqUtils.printLog("Setting referral status to 'letter_sent'...")

   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_Ref")
   --update the referral
   local sql ="update ewaiting_referrals " ..
   "set "..
   "modified = now(), modified_by = "..sqUser..", "..
   "status = 'letter_sent'" ..
   " where id = ".. referralID

   sqDB.execute{sql=sql, live=false}   

end

----------------------------------------------------------------------------

function sqDB.RAD.createPatientReferral(id,facilityID, OrderID, ExamCode, 
      ExamDesc, episodeNumber, accessionNumber, orderGrouping, checkZSeg, checkZSegVisitflag, 
      SiteId, RefSourceName)
   
   sqUtils.printLog("Creating patient referral...")
 
   local patientType = sqUtils.getMappedValue(sqCfg.extra_params,"defaultPatientType")
     
   --PB 05.05.2024 : lokup condition tag rather than default, default if not found
   local facilityConditionTag = sqDB.getFacilityCondition(facilityID)
   if facilityConditionTag == nil then
      facilityConditionTag = sqUtils.getMappedValue(sqCfg.extra_params,"defaultFacilityConditionTag")
   end
   --
   
   --PB 05.05.2024 : lokup speciality rather than default, default if not found
   local facilitySpeciality = sqDB.getFacilitySpecialty(facilityID)
   if facilitySpeciality == nil then
      facilitySpeciality = sqUtils.getMappedValue(sqCfg.extra_params,"defaultFacilitySpeciality")
   end
   --
   
   --PB 05.05.2024 : lokup category rather than default, default if not found
   local facilityCategory = sqDB.getFacilityCategory(facilityID)
   if facilityCategory == nil then
      facilityCategory = sqUtils.getMappedValue(sqCfg.extra_params,"defaultFacilityCategory")
   end
   --
   
   --PB 05.05.2024 : lokup category rather than default, default if not found
   local facilitySourceId = sqDB.getFacilitySource(facilityID)
   if facilitySourceId == nil then
      facilitySourceId = sqUtils.getMappedValue(sqCfg.extra_params,"defaultFacilitySourceId")
   end
   --

   if checkZSeg == 'NSQ' then
      if checkZSegVisitflag == 'Y' then
         patientType = 'Inpatient'
      end
   end

   sqDB.execute{sql=
      "insert into ewaiting_referrals (facility_id, user_id, date_referred, category_id, speciality_id, " ..
      "referral_source_id, referral_source_name, status, `condition`, comments, created, created_by, " ..
      "pas_verified, patient_type) " ..
      "values ("..
      facilityID..","..
      id..","..
      -- conn:quote(serviceDateFormat) ..","..  
      "now()"..","..
      facilityCategory..","..
      facilitySpeciality..","..
      facilitySourceId..","..
      --"'via HL7'"..","..
      conn:quote(RefSourceName)..","..
      "'pre_triage'"..","..
      conn:quote(facilityConditionTag)..","..
      "'HL7 Referral'"..","..
      "now()"..","..
      "19598".. "," ..
      "0" .."," ..
      conn:quote(patientType) ..
      ")"
      , live=false}

   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}

   sqDB.execute{sql=
      "insert into ewaiting_referral_condition_tags " ..
      "values (" .. result[1].id .."," .. facilityConditionTag .. ")", 
      live=false}
   
   sqUtils.printLog("Referral created!")

   return result[1].id:S()

end

----------------------------------------------------------------------------

function sqDB.RAD.createPatientReferralOrder(id, OrderID, episodeNumber, ExamCode,ExamDesc,accessionNumber, time, orderResID)

   sqUtils.printLog("Creating referral order...")
   
   if OrderID == '0' then
      local errorStr = "ER0001: "
      local errorDesc = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'errorCodes','ER0001')
      if errorDesc ~= nil then
         errorStr = errorStr..errorDesc.." : "
      end
      errorStr = errorStr..orderResID
      error(errorStr)
   end

   sqDB.execute{sql=
      "insert into ewaiting_referral_orders " ..
      "(id,ewaiting_referral_id,order_id,episode_number,exam_code,exam_name,accession_number, created_at, cancellation_code) " ..
      "values (null," .. 
      id .."," .. 
      conn:quote(OrderID) .. "," .. 
      conn:quote(episodeNumber) .. "," .. 
      conn:quote(ExamCode) .. "," .. 
      conn:quote(ExamDesc) .. "," ..
      conn:quote(accessionNumber) .. "," .. 
      "now(), '')", 
      live=false}

   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
   
   sqUtils.printLog("Referral order created!")

   return result[1].id:S()

end

----------------------------------------------------------------------------

function sqDB.RAD.cancelReferralOrder(referralID, OrderID, resID, cancellationCode)
   
   sqUtils.printLog("Cancelling referral order...")
   
   --Requested to remove check by Steve , 19.12.2024
   --[[
   if resID == '0' then
      error("Error trying to cancel a referral order for appointment than does not exist : "..OrderID)
   end
   ]]

   local sql = ''
   
   if sqUtils.isEmpty(resID) or resID == '0' then
      sql ="update ewaiting_referral_orders " ..
      "set "..
      "cancellation_code = "..cancellationCode..", "..
      "updated_at = now(), " ..
      "cancellation_date = now(), " ..
      "cancellation_processed = 0, " ..
      "cancellation_processed_success = 0" ..
      " where ewaiting_referral_id = " .. referralID ..
      " and order_id = " .. conn:quote(OrderID)
   else
      --set the res ID
      sql ="update ewaiting_referral_orders " ..
      "set "..
      "cancellation_code = "..cancellationCode..", "..
      "updated_at = now(), " ..
      "cancellation_date = now(), " ..
      "cancellation_processed = 0, " ..
      "cancellation_processed_success = 0, " ..
      "res_id = "..resID..
      " where ewaiting_referral_id = " .. referralID ..
      " and order_id = " .. conn:quote(OrderID)
   end

   sqDB.execute{sql=sql, live=false}      

end

----------------------------------------------------------------------------

function sqDB.RAD.updateReferralOrderResID(referralID, OrderID, resID)

   sqUtils.printLog("Setting resID for referral order...")

   local catchAllCode = sqUtils.getMappedValue(sqCfg.extra_params,"cancel_CatchAll")
   
   local sql ="update ewaiting_referral_orders " ..
   "set "..
   "cancellation_code = "..catchAllCode..", "..
   "cancellation_processed = 0, " ..
   "cancellation_processed_success = 0, " ..
   "updated_at = now(), cancellation_date = now()," ..
   "res_id = " .. resID ..
   " where ewaiting_referral_id = " .. referralID ..
   " and order_id = " .. conn:quote(OrderID)
   
   sqDB.execute{sql=sql, live=false}

end

----------------------------------------------------------------------------

function sqDB.RAD.updatePatientFlow(id, facilityID, OBR)
   
   sqUtils.printLog("Updating patient flow...")

   local exam_code = OBR[4][1]:S()
   local OrderPFID = OBR[3][1]:S()

   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_App")

   --PB 08.08.2024 - IR-383 : added extra logic to only use a rervation where facility id has this sites parent facility
   --[[
   local ResId = sqDB.execute{sql=
      "select count(id), max(id) as id from reservation " ..
      "where user_id = " .. id .. 
      " and reservation_date = date_format(now(), '%Y-%m-%d')", live=true}
   ]]
   local ResId = sqDB.execute{sql=
      "select count(id), max(id) as id from reservation " ..
      "where user_id = " .. id .. 
      " and reservation_date = date_format(now(), '%Y-%m-%d')"..
      " and facility_id in (select id from facilities where parent_id = "..facilityID..
      " or id = "..facilityID..")", live=true}
   ---

   if ResId[1]['count(id)']:S() ~= '0' then

      local ResId3 = sqDB.execute{sql=
         "select count(id), max(id) as id " ..
         "from patient_flow " ..
         "where res_id = " .. conn:quote(ResId[1]['id']:S()) .. " and stage = 'CHECK-IN'", 
         live=true}
      --
      if ResId3[1]['count(id)']:S() == '0' then
         --insert the new appointment
         sqDB.execute{sql=
            "insert into patient_flow " ..
            "(id, timestamp, facility_id, user_id, res_id, sq_user_id, stage, nurse, current_stage, bay, source) " .. 
            "values (null, now(),"..
            facilityID..","..
            id..","..
            conn:quote(ResId[1]['id']:S()).."," ..sqUser..",'CHECK-IN', NULL, 'Y', NULL, 'hl7'"..
            ")"
            , live=false}  
      end
   
   else
      
      local ResId4 = sqDB.execute{sql=
         "select count(id), ewaiting_referral_id from ewaiting_referral_orders " ..
         "where order_id = '" .. OrderPFID.."'", live=true}
      --
      if ResId4[1]['count(id)']:S() ~= '0' then
         --update the referral
         local sql ="update ewaiting_referrals " ..
         "set "..
         "status = 'assigned'" ..
         " where id = ".. ResId4[1]['ewaiting_referral_id']:S()

         sqDB.execute{sql=sql, live=false}
      
      end
   
   end
   
   sqUtils.printLog("Patient flow updated!")
   
end

----------------------------------------------------------------------------

function sqDB.RAD.updatePatientFlowComplete(id, facilityID, OBR)
   
   sqUtils.printLog("Updating patient flow COMPLETE...")

   local exam_code = OBR[4][1]:S()
   local OrderPFID = OBR[3][1]:S()
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_App")

   --PB 08.08.2024 - IR-383 : added extra logic to only use a rervation where facility id has this sites parent facility
   --[[
   local ResId = sqDB.execute{sql=
      "select count(id), max(id) as id " ..
      "from reservation " ..
      "where user_id = " .. id .. " and reservation_date = date_format(now(), '%Y-%m-%d')", 
      live=true}
   ]] 
   local ResId = sqDB.execute{sql=
      "select count(id), max(id) as id from reservation " ..
      "where user_id = " .. id .. 
      " and reservation_date = date_format(now(), '%Y-%m-%d')"..
      " and facility_id in (select id from facilities where parent_id = "..facilityID..
      " or id = "..facilityID..")", live=true}
   ---

   if ResId[1]['count(id)']:S() ~= '0' then
      
      local ResId4 = sqDB.execute{sql=
         "select count(id), max(id) as id " ..
         "from patient_flow " ..
         "where res_id = " .. conn:quote(ResId[1]['id']:S()) .. " and stage = 'COMPLETE'", live=true}
      --
      if ResId4[1]['count(id)']:S() == '0' then 

         sqDB.execute{sql=
            "update patient_flow " ..
            "set current_stage = 'N' " ..
            "where res_id = " .. conn:quote(ResId[1]['id']:S()) .. 
            " and user_id = " ..id ..
            " and facility_id = " .. facilityID,
            live = false}

         --insert the new appointment flow
         sqDB.execute{sql=
            "insert into patient_flow " ..
            "(id, timestamp, facility_id, user_id, res_id, sq_user_id, stage, nurse, current_stage, bay, source) " .. 
            "values (null, now(),"..
            facilityID..","..
            id..","..
            conn:quote(ResId[1]['id']:S()).."," ..sqUser..",'COMPLETE', NULL, 'Y', NULL, 'hl7'"..
            ")"
            , live=false} 
      end
      
   else
      
      local ResId5 = sqDB.execute{sql=
         "select count(id), ewaiting_referral_id from ewaiting_referral_orders " ..
         "where order_id = '" .. OrderPFID .."'", 
         live=true}
      --
      if ResId5[1]['count(id)']:S() ~= '0' then
         --update the referral
         local sql ="update ewaiting_referrals " ..
         "set "..
         "status = 'discharged'" ..
         " where id = ".. ResId5[1]['ewaiting_referral_id']:S()

         sqDB.execute{sql=sql, live=false}
      
      end
   
   end
   
end

----------------------------------------------------------------------------

function sqDB.RAD.getOrderResID(referralID, OrderID)
   
   local res = sqDB.execute{sql=
      "select IFNULL(res_id,0) as res_id from ewaiting_referral_orders " ..
      "where ewaiting_referral_id = " .. conn:quote(referralID) .. 
      " and order_id = " .. conn:quote(OrderID), live=true}  
   
	return res[1]['res_id']:S()
   
end

----------------------------------------------------------------------------

function sqDB.RAD.getCancellationID(reason)
   
   local res = sqDB.execute{sql=
                  "select count(id) as cnt, id from healthshare_cancellation_codes " ..
                  "where reason = " .. conn:quote(reason), 
                  live=true}
   
   return res[1]['cnt']:S(), res[1]['id']:S()
   
end

----------------------------------------------------------------------------

function sqDB.RAD.isValidExamCode(facilityID, examCode)

   local res = true
   
   local examCodeCheck = sqDB.execute{sql=
      "select count(facility_id) as count_fid, facility_id from reason_exam_codes "  ..
      "where exam_code =" .. conn:quote(examCode)..
      " and facility_id in (select id from facilities where parent_id = " .. facilityID .. ") limit 1 ", 
      live=true}

   if (examCodeCheck[1]['count_fid']:S() == '0') then
      res = false
   end

   return res

end

----------------------------------------------------------------------------

function sqDB.RAD.isValidRoomCode(facilityID, roomCode)

   local res = true
   
   local roomCodeCheck = sqDB.execute{sql=
      "select count(facility_id) as count_fid, facility_id from reason_exam_codes "  ..
      "where room_id =" .. conn:quote(roomCode)..
      " and facility_id in (select id from facilities where parent_id = " .. facilityID .. ") limit 1 ", 
      live=true}

   if (roomCodeCheck[1]['count_fid']:S() == '0') then
      res = false
   end

   return res

end

----------------------------------------------------------------------------

function sqDB.RAD.mergePatient(id, minorid, facilityID)

   sqUtils.printLog("Merging patient ID:"..minorid.." into ID:"..id.." ...")
 
   -- Remove the minor one from teh Facility view
   local sql ="delete from facility_user " ..
   "where "..
   "facility_id = " .. conn:quote(facilityID) .. 
   " and user_id = " .. minorid

   sqDB.execute{sql=sql, live=false}

   local referralCount = sqDB.execute{sql="select count(id), id from ewaiting_referrals " ..
      "where facility_id = " .. facilityID .. 
      " and status in ('active', 'pre_triage') " ..
      "and user_id = " .. minorid, live=true}

   if referralCount[1]['count(id)']:S() ~= '0'
      then
      --Update any referrals from minor id into the major id
      local sql ="update ewaiting_referrals " ..
      "set "..
      "user_id = " .. id .. 
      " where user_id = " .. minorid

      sqDB.execute{sql=sql, live=false}
   end

   --insert into merged_users table
   local sql ="insert into merged_users values (" ..
   "null, now(), 0, "..
   id ..", "..
   "'{TUH Merge - Minor ID: " .. minorid ..
   " Major ID: " .. id ..
   "ReferralIDs Merged: " .. referralCount[1]['id'] ..
   "}')"

   sqDB.execute{sql=sql, live=false}

   
end

----------------------------------------------------------------------------

function sqDB.RAD.isReflexAppointment(episodeNumber, ExamCode, appStart, appEnd)

   local appRescheduledId = sqUtils.getMappedValue(sqCfg.extra_params,"appRescheduledId")
   
   local res = sqDB.execute{sql=
      "select count(id), IFNULL(id,0) as oID, IFNULL(res_id,0) as resID, IFNULL(ewaiting_referral_id,0) as refID " ..
      "from ewaiting_referral_orders " ..
      "where episode_number = " .. conn:quote(episodeNumber) .. 
      " and exam_code = " .. conn:quote(ExamCode) .." and (cancellation_processed = '0' or cancellation_code = '"..appRescheduledId.."')",
      live=true}  

   return res[1].oID:S(), res[1].resID:S(), res[1].refID:S()
end

----------------------------------------------------------------------------

function sqDB.RAD.isAppReschedule(episodeNumber, ExamCode)

   local appRescheduledId = sqUtils.getMappedValue(sqCfg.extra_params,"appRescheduledId")
   
   local res = sqDB.execute{sql=
      "select count(id), IFNULL(id,0) as oID, IFNULL(res_id,0) as resID, IFNULL(ewaiting_referral_id,0) as refID " ..
      "from ewaiting_referral_orders " ..
      "where episode_number = " .. conn:quote(episodeNumber) .. 
      " and exam_code = " .. conn:quote(ExamCode) .." and (cancellation_processed = '0' or cancellation_code = '"..appRescheduledId.."')",
      live=true}  

   return res[1].oID:S(), res[1].resID:S(), res[1].refID:S()
end

----------------------------------------------------------------------------

function sqDB.RAD.createAppointment(MsgIn, patientID, facilityID, episodeNumber, orderResID, oID, examCode, examDesc, accessionNumber, 
      room_code, opdAppointmentTimeString, opdAppointmentEndTimeString, opdAppointmentString)
   
   sqUtils.printLog("Creating appointment...")
   
   local facilityType = sqUtils.getMappedValue(sqCfg.extra_params,"defaultFacilityType")
   local consultantID = sqUtils.getMappedValue(sqCfg.extra_params,"defaultconsultantID")
   
   local referralID = sqDB.RAD.getPatientReferralByEpisode(patientID, facilityID, episodeNumber)

   --Check if the Study Id exists
   local AppCount = sqDB.execute{sql="select count(id), id, ewaiting_referral_id " .. 
      "from ewaiting_referral_orders where order_id = " .. conn:quote(orderResID), live=true}  
   
   --order doesnt exist then create it
   if AppCount[1]['count(id)']:S() == '0'  then
      
      oID = sqDB.RAD.createPatientReferralOrder(referralID, oID, episodeNumber, examCode, examDesc, accessionNumber,
      opdAppointmentTimeString, orderResID)

   end

   -- check if a res id already exists
   local AppResCount = sqDB.execute{sql=
      "select count(id), id, IFNULL(res_id,0) as res_id, ewaiting_referral_id " ..
      "from ewaiting_referral_orders " ..
      "where order_id = " .. conn:quote(orderResID) .. 
      " and exam_code = " .. conn:quote(examCode), live=true}    
    
   if AppResCount[1]['res_id']:S() ~= '0' then 
      
      local AppCount = sqDB.execute{sql="select count(facility_id), facility_id " ..
         " from reason_exam_codes where room_id = " .. conn:quote(room_code) .. 
         " and facility_id in (select id from facilities where parent_id = " .. facilityID .. ") limit 1 ", 
         live=true}

      if AppCount[1]['count(facility_id)']:S() ~= '0'  then -- 1

         local AppResCountResch = sqDB.execute{sql=
            "select reservation_time, reservation_end_time from reservation " ..
            "where id = " ..conn:quote(AppResCount[1]['res_id']:S()), live=true}     

         if AppResCountResch[1]['reservation_end_time']:S() == conn:quote(opdAppointmentTimeString) then

            local sql="update reservation set facility_id = ".. AppCount[1]['facility_id'].. 
            ", reservation_end_time = ".. conn:quote(opdAppointmentEndTimeString) ..
            " where id = " .. conn:quote(AppResCount[1]['res_id']:S())

            sqDB.execute{sql=sql, live=false}
         else

            local sql="update reservation set facility_id = ".. AppCount[1]['facility_id'].. 
            ", reservation_time = " .. conn:quote(opdAppointmentTimeString) .. 
            ", reservation_end_time = ".. conn:quote(opdAppointmentEndTimeString) ..
            " where id = " .. conn:quote(AppResCount[1]['res_id']:S())

            sqDB.execute{sql=sql, live=false}
         end
      end
   end

   
   local AppCount = sqDB.execute{sql=
      "select count(facility_id), facility_id from reason_exam_codes " ..
      "where room_id = " .. conn:quote(room_code) .. 
      " and facility_id in (select id from facilities where parent_id = " .. facilityID .. ") limit 1 ", 
      live=true}

   if AppCount[1]['count(facility_id)']:S() ~= '0'  then -- 1

      --[[ CI: Get appointment reason id ]]
      local reasonId = '0'
      local reasonCheck = sqDB.execute{sql="select count(reason_id), reason_id from reason_exam_codes rec where rec.facility_id = '"..
         AppCount[1]['facility_id'].."' and rec.exam_code = '"..examCode.."'", live = true}

      if (reasonCheck[1]["count(reason_id)"]:S()) ~= '0' then
         reasonId = reasonCheck[1].reason_id:S()                                                     
      end


      local sql =
      "insert into reservation " ..
      "(facility_id, user_id, facility_type, consultant_id, location_pref, reservation_date, " ..
      "reservation_time, reservation_end_time, google_cal_id, reason_id,recurrence,date,status," ..
      "attended_by, is_attended,booking_user_id, source, print,stats, quality,capacity_sync, is_urgent) " .. 
      "values ("..
      AppCount[1]['facility_id']..","..
      patientID..","..
      conn:quote(facilityType)..","..
      consultantID..", ''"..","..
      conn:quote(opdAppointmentString)..","..
      conn:quote(opdAppointmentTimeString)..","..
      conn:quote(opdAppointmentEndTimeString)..","..
      "'',"..conn:quote(reasonId)..",'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0"..
      ")"

      sqDB.execute{sql=sql, live=false}
      --
      local resresult = sqDB.execute{sql="select last_insert_id() as id", live=true}
      --

      --update the referral
      local sql ="update ewaiting_referrals " ..
      "set "..
      "status = 'assigned'" ..
      " where id = ".. referralID

      sqDB.execute{sql=sql, live=false}
      --
      --link the res and referral
      local sql ="insert into ewaiting_reservations values (" ..
      referralID..","..
      resresult[1].id..")"

      sqDB.execute{sql=sql, live=false}

      --check if there is an existing appointment against this order
      local AppCountRes3 = sqDB.execute{sql=
         "select IFNULL(res_id,0) as res_id from ewaiting_referral_orders " ..
         "where order_id = " .. conn:quote(orderResID), live=true}

      if AppCountRes3[1]['res_id']:S() ~= '0'  then
         --
         local sql ="update reservation " ..
         "set "..
         "status = 'cancelled'" ..
         " where id = " .. conn:quote(AppCountRes3[1].res_id:S())

         sqDB.execute{sql=sql, live=false}

      end
      --
      local sql ="update ewaiting_referral_orders " ..
      "set "..
      "res_id = " .. resresult[1].id..
      " where ewaiting_referral_id = " .. referralID ..
      " and order_id = " .. conn:quote(orderResID)

      sqDB.execute{sql=sql, live=false}
      
      --print("DB Commit at line 786")
      --conn:commit{live=true}

   else
      --update the referral
      local sql ="update ewaiting_referrals " ..
      "set "..
      "status = 'assigned'" ..
      " where id = ".. referralID

      sqDB.execute{sql=sql, live=false}

   end 
   
end

----------------------------------------------------------------------------

function sqDB.RAD.rescheduleAppointmentS12(facilityID, resID, oID, refID, orderResID, opdAppointmentTimeNewString, 
      opdAppointmentEndTimeString, room_code, episodeNumber, accessionNumber, examCode, examDesc)

   sqUtils.printLog("Rescheduling appointment from S12...")
   
   local appRescheduledId = sqUtils.getMappedValue(sqCfg.extra_params,"appRescheduledId")

   local sql ="update ewaiting_referral_orders " ..
   "set "..
   "reschedule_date = " .. conn:quote(opdAppointmentTimeNewString).. ", " ..
   " reschedule_end_time = " .. conn:quote(opdAppointmentEndTimeString) .. ", " ..
   " cancellation_code = "..appRescheduledId..
   ", cancellation_processed = 0 ".. 
   ", cancellation_processed_success = 0 " ..
   ", updated_at = now(), cancellation_date = now()" ..
   " where id = " .. conn:quote(oID)

   sqDB.execute{sql=sql, live=false}

   local AppCount22 = sqDB.execute{sql=
      "select count(facility_id), facility_id from reason_exam_codes "  ..
      "where room_id = " ..conn:quote(room_code) .. 
      " and facility_id in (select id from facilities where parent_id = " .. facilityID .. ") limit 1 ", 
      live=true}

   -- Only update the facility if the rescheduled appt is for a room managed by SQ (check result of AppCount22)
   if AppCount22[1]['count(facility_id)']:S() ~= '0'  then

      local sql="update reservation set facility_id = " ..AppCount22[1]['facility_id']..
      " where id = " .. conn:quote(resID)

      sqDB.execute{sql=sql, live=false}

   end
   
   --PB 18.02.2025 : Flag only used when processing healthshare cancel+rebook
   local processHSCancelAndRebook = json.parse{data=sqCfg.extra_params}.processHSCancelAndRebook or ''

   if processHSCancelAndRebook ~= '1' then
      
      --
      --Check if the Study Id exists
      local AppCount33 = sqDB.execute{sql=
         "select count(id), id, ewaiting_referral_id " .. 
         "from ewaiting_referral_orders where order_id = " .. conn:quote(orderResID), live=true}  
      --
      if AppCount33[1]['count(id)']:S() == '0'  then

         --create a new Order
         --
         sqDB.execute{sql=
            "insert into ewaiting_referral_orders " ..
            "(id,ewaiting_referral_id,order_id,episode_number,exam_code," ..
            "exam_name,accession_number, created_at, res_id, cancellation_code) " ..
            "values (null," .. conn:quote(refID) .."," .. 
            conn:quote(orderResID) .. "," .. conn:quote(episodeNumber) .. "," 
            .. conn:quote(examCode) .. "," .. conn:quote(examDesc) .. "," ..
            conn:quote(accessionNumber) .. "," .. "now()," 
            ..conn:quote(resID) ..", '')", live=false}   
         --
      end
      
   else

      --specific to healthshare cancel+rebook

      --PB 18.02.2025 : if we've received an appointment as part of the cance+rebook process on soliton
      --                the the message will have the same episode ID but different order id, so we need
      --                so we need to update the order id on the order

      local sqlStr = "select count(ero.id) as cnt, IFNULL(ero.id,0) as id from ewaiting_referrals er join ewaiting_referral_orders ero on er.id = ero.ewaiting_referral_id"..
      " where er.facility_id = "..conn:quote(facilityID).." and ero.episode_number = "..conn:quote(episodeNumber)..
      " and order_id <> "..conn:quote(orderResID)
      local AppCount33 = conn:execute{sql=sqlStr, live=true}

      if AppCount33[1].id:S() ~= '0' then
         sqlStr = "update ewaiting_referral_orders set order_id = "..orderResID.." where id = "..AppCount33[1].id:S()
         conn:execute{sql=sqlStr, live=false}
      end

   end
   
   conn:commit{live=true}

end

----------------------------------------------------------------------------

--[[
function sqDB.RAD.rescheduleAppointmentS13(MsgIn, patientID, facilityID)
   
   sqUtils.printLog("S13: Rescheduling appointment...")

   local parentFacilityID = sqUtils.getMappedValue(sqCfg.extra_params,"parentFacilityID")

   local OrderResID = MsgIn.SCH[2][1]:S() 
   local episodeNumber = MsgIn.PV1[19]:S()
   
   local opdAppointment = dateparse.parse(MsgIn.SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(MsgIn.SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
   local opdAppointmentTimeNewString = tostring(dateparse.parse(MsgIn.SCH[11][4]:T()))

   local opdAppointmentEndTime = dateparse.parse(MsgIn.SCH[11][5]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentEndTime),12,16)

   local dept_code = MsgIn.AIL[1][3][1]:S()
   local room_code = MsgIn.AIL[2][3][1]:S() 
   
   local referralID = sqDB.RAD.getPatientReferralByEpisode(patientID, facilityID, episodeNumber)
   
   -------
   
   local appRescheduledId = sqUtils.getMappedValue(sqCfg.extra_params,"appRescheduledId")

   local sql ="update ewaiting_referral_orders " ..
   "set "..
   "reschedule_date = " .. conn:quote(opdAppointmentTimeNewString).. ", " ..
   " reschedule_end_time = " .. conn:quote(opdAppointmentEndTimeString) .. ", " ..
   " cancellation_code = "..appRescheduledId..
   ", cancellation_processed = 0 ".. 
   ", cancellation_processed_success = 0 " ..
   ", updated_at = now(), cancellation_date = now()" ..
   " where id = " .. conn:quote(oID)

   sqDB.execute{sql=sql, live=false}
   
   -------
   
   
   local AppCountRes = conn:execute{sql=
      "select count(res_id) as cnt, IFNULL(res_id,0) as res_id from ewaiting_referral_orders " ..
      "where order_id = " .. conn:quote(OrderResID), live=true}

   if AppCountRes[1]['res_id']:S() ~= '0'  then

      local AppCount2 = conn:execute{sql=
         "select count(facility_id), facility_id from reason_exam_codes " ..
         "where facility_id in (select id from facilities where id = "..facilityID.." or parent_id = "..parentFacilityID..") " ..
         "and room_id = " .. conn:quote(room_code) .. " limit 1", live=true}

      if AppCount2[1]['count(facility_id)']:S() ~= '0'  then   

         local AppResCountResch2 = conn:execute{sql=
            "select reservation_time, reservation_end_time from reservation " ..
            "where id = " ..conn:quote(AppCountRes[1]['res_id']:S()), live=true}     

         if AppResCountResch2[1]['reservation_end_time']:S() == conn:quote(opdAppointmentTimeString) then 

            local sql ="update reservation " ..
            "set "..
            "reservation_date = " .. conn:quote(opdAppointmentString)..","..
            "reservation_end_time = " .. conn:quote(opdAppointmentEndTimeString)..","..
            "facility_id = " .. AppCount2[1]['facility_id'] ..
            " where id = " .. conn:quote(AppCountRes[1].res_id:S())

            conn:execute{sql=sql, live=false}

         else

            local sql ="update reservation " ..
            "set "..
            "reservation_date = " .. conn:quote(opdAppointmentString)..","..
            "reservation_time = " .. conn:quote(opdAppointmentTimeString)..","..
            "reservation_end_time = " .. conn:quote(opdAppointmentEndTimeString)..","..
            "facility_id = " .. AppCount2[1]['facility_id'] ..
            " where id = " .. conn:quote(AppCountRes[1].res_id:S())

            conn:execute{sql=sql, live=false}
         end
      end 
   end  
   
end
]]

----------------------------------------------------------------------------

function sqDB.RAD.cancelAppointment(id, facilityID, SCH, reasonID)
   
   sqUtils.printLog("Cancelling appointment...")
   
   --extract the res date and time
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
      
   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
  
   local orderID = SCH[2][1]:S()
   
   --[[
   local ResIdUpd = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId) ..
      " and status = 'active'", live=true}
   ]]
   
   local SQL = "select count(r.id), r.id from reservation r "..
   "left outer join ewaiting_referral_orders ero on ero.res_id = r.id ".. 
   " where r.status = 'active' and ero.order_id = "..conn:quote(orderID).." and r.user_id = "..conn:quote(id)  
   local ResIdUpd = sqDB.execute{sql=SQL, live=true}

   local resID = ResIdUpd[1].id:S()
   trace(resID)
   
   if ResIdUpd[1]['count(r.id)']:S() ~= '0' then

      sqDB.execute{sql="update reservation set " .. 
         "reservation_date = " .. conn:quote(opdAppointmentString)..","..
         "reservation_time = " .. conn:quote(opdAppointmentTimeString)..","..
         "reservation_end_time = " .. conn:quote(opdAppointmentEndTimeString)..","..
         "status = 'cancelled'" ..","..
         "modified = now(), modified_source = 'h'"..
         " where id = " .. resID .." and " ..
         "user_id = " .. id
         , live=false}

      --Add A user audit record for the app creation
      sqDB.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
         "values (null, now(),22, 'appointment.cancelled',"..
         conn:quote(resID)..","..id..",null)"
         , live=false}  
      
      sqUtils.printLog("Appointment cancelled!")

   else
      sqUtils.printLog("Cancelling appointment failed, Appointment ID "..orderID.." not found!")
   end
end

-----------------------------------------------------------------
function sqDB.RAD.dnaAppointment(id, facilityID, SCH, reasonID)
   
   sqUtils.printLog("DNA appointment...")
  
   --get the DXC identifier
   local DXCId = SCH[2][1]:S()
   
   --[[
   if DXCId == '' then
      DXCId = SCH[2][1]:S()
   end
   ]]--
   
   local ResIdUpd = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId) ..
         " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0'
      then
   
      sqDB.execute{sql="update reservation set " .. 
         "is_attended = 'no'," ..
         "modified = now(), modified_source = 'h'"..
         " where id = " .. conn:quote(ResIdUpd[1]['id']:S())
         , live=false}
      
      --Add A user audit record for the app creation
      sqDB.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),22, 'appointment.dna',"..
                 conn:quote(ResIdUpd[1]['id']:S())..","..id..",null)"
            , live=false}  
      
      end
end
----------------------------------------------------------------------------

function sqDB.RAD.setReferralStatus(orderResID, status)
   
   local res = sqDB.execute{sql=
      "select count(id), ewaiting_referral_id " ..
      "from ewaiting_referral_orders " ..
      "where order_id = " .. conn:quote(orderResID), live=true}
   
   if res[1]["count(id)"]:S() ~= '0' then

      local refID = res[1].ewaiting_referral_id:S()

      local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_Ref")

      local sql ="update ewaiting_referrals " ..
      "set "..
      "modified = now(), modified_by = "..sqUser..", "..
      "status = 'assigned'" ..
      " where id = ".. refID

      sqDB.execute{sql=sql, live=false}
 
   end
   
end

----------------------------------------------------------------------------

--------------------------------------------------------------------
--     TUH RAD IMPLEMENTATION  -----
-----------------------------------------------------------------
-----------------------------------------------------------------

----------------------------------------------------------------------------

function sqDB.TUH.RAD.getOrderResID(referralID, OrderID)
   
   local res = sqDB.execute{sql=
      "select IFNULL(res_id,0) as res_id from ewaiting_referral_orders " ..
      "where ewaiting_referral_id = " .. conn:quote(referralID) .. 
      " and order_id = " .. conn:quote(OrderID), live=true}  
   
	return res[1]['res_id']:S()
   
end

----------------------------------------------------------------------------

function sqDB.TUH.RAD.getCancellationID(reason)
   
   local res = sqDB.execute{sql=
                  "select count(id) as cnt, id from healthshare_cancellation_codes " ..
                  "where reason = " .. conn:quote(reason), 
                  live=true}
   
   return res[1]['cnt']:S(), res[1]['id']:S()
   
end

----------------------------------------------------------------------------

function sqDB.TUH.RAD.cancelReferralOrder(referralID, OrderID, resID, cancellationCode)
   
   sqUtils.printLog("Cancelling referral order...")

   local sql = ''
   
   if sqUtils.isEmpty(resID) or resID == '0' then
      sql ="update ewaiting_referral_orders " ..
      "set "..
      "cancellation_code = "..cancellationCode..", "..
      "updated_at = now(), " ..
      "cancellation_date = now(), " ..
      "cancellation_processed = 0, " ..
      "cancellation_processed_success = 0" ..
      " where ewaiting_referral_id = " .. referralID ..
      " and order_id = " .. conn:quote(OrderID)
   else
      --set the res ID
      sql ="update ewaiting_referral_orders " ..
      "set "..
      "cancellation_code = "..cancellationCode..", "..
      "updated_at = now(), " ..
      "cancellation_date = now(), " ..
      "cancellation_processed = 0, " ..
      "cancellation_processed_success = 0, " ..
      "res_id = "..resID..
      " where ewaiting_referral_id = " .. referralID ..
      " and order_id = " .. conn:quote(OrderID)
   end

   sqDB.execute{sql=sql, live=false}      

end

----------------------------------------------------------------------------

function sqDB.TUH.RAD.setReferralToDischarged(appID)

   local findReferral = sqDB.execute{sql="select count(id), ewaiting_referral_id from ewaiting_referral_orders where res_id = " .. appID, live=true}
   if findReferral[1]['count(id)']:S() ~= '0' then
      sqDB.execute{sql="update ewaiting_referrals set status = 'discharged' where id = " .. findReferral[1]['ewaiting_referral_id']:S(), live=false}   
   end

end

-----------------------------------------------------------------

function sqDB.TUH.RAD.createPatientReferral(id,facilityID, facilitySpeciality, OrderID, ExamCode, ExamDesc, episodeNumber, accessionNumber, orderGrouping, referringDoctorID)

   --local facilityConditionTag = sqUtils.getJsonMappedValue(sqConfig.extra_params,'defaultFacilityConditionTag')
   --local facilityCategory = sqUtils.getJsonMappedValue(sqConfig.extra_params,'defaultFacilityCategory')
   --local FacilitySourceId = sqUtils.getJsonMappedValue(sqConfig.extra_params,'defaultFacilitySourceId')
   
   --PB 05.05.2024 : lokup condition tag rather than default, default if not found
   local facilityConditionTag = sqDB.getFacilityCondition(facilityID)
   if facilityConditionTag == nil then
      facilityConditionTag = sqUtils.getMappedValue(sqCfg.extra_params,"defaultFacilityConditionTag")
   end
   --
   
   --PB 05.05.2024 : lokup category rather than default, default if not found
   local facilityCategory = sqDB.getFacilityCategory(facilityID)
   if facilityCategory == nil then
      facilityCategory = sqUtils.getMappedValue(sqCfg.extra_params,"defaultFacilityCategory")
   end
   --
   
   --PB 05.05.2024 : lokup category rather than default, default if not found
   local facilitySourceId = sqDB.getFacilitySource(facilityID)
   if facilitySourceId == nil then
      facilitySourceId = sqUtils.getMappedValue(sqCfg.extra_params,"defaultFacilitySourceId")
   end
   --

   local referralCount = sqDB.execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. facilityID .. " and user_id = " .. id .. 
      " and id in (select ewaiting_referral_id from ewaiting_referral_orders where episode_number = " .. conn:quote(episodeNumber) .. ")", live=true}

   if referralCount[1]['count(id)']:S() == '0' then

      local orderCountAll = sqDB.execute{sql="select count(id), order_id, ewaiting_referral_id from ewaiting_referral_orders where episode_number = " .. conn:quote(episodeNumber), live=true}

      if orderCountAll[1]['count(id)']:S() == '0' then

         local defaultRefCreatedBy = sqUtils.getJsonMappedValue(sqConfig.extra_params,'defaultRefCreatedBy')
         local defaultReferralStatus = sqUtils.getJsonMappedValue(sqConfig.extra_params,'defaultReferralStatus')

         local sql = "insert into ewaiting_referrals (facility_id, user_id, date_referred, category_id, speciality_id, referral_source_id, referral_source_name, "..
         "status, `condition`, comments, created, created_by, pas_verified, gp_details_id) " ..
         "values ("..
         facilityID..","..
         id..","..
         "now()"..","..
         facilityCategory..","..
         facilitySpeciality..","..
         facilitySourceId..","..
         "'via HL7'"..","..
         "'"..defaultReferralStatus.."'"..","..
         conn:quote(facilityConditionTag)..","..
         "'HL7 Referral'"..","..
         "now()"..","..conn:quote(defaultRefCreatedBy).. "," ..
         "0" ..","..
         conn:quote(referringDoctorID)..
         ")"
         
         sqDB.execute{sql=sql, live=false}

         local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
       
         local sql = "insert into ewaiting_referral_condition_tags values (" .. result[1].id .."," .. facilityConditionTag .. ")"
         sqDB.execute{sql=sql, live=false}

         local sql = "insert into ewaiting_referral_orders (id,ewaiting_referral_id,order_id,episode_number,exam_code,exam_name,accession_number, created_at) values (null," .. 
         result[1].id .."," .. conn:quote(OrderID) .. "," .. conn:quote(episodeNumber) .. "," .. conn:quote(ExamCode) .. "," .. 
         conn:quote(ExamDesc) .. "," ..conn:quote(accessionNumber) .. "," .. "now())"
         
         sqDB.execute{sql=sql, live=false}
      
      else
      
         local sql = "insert into ewaiting_referral_orders (id,ewaiting_referral_id,order_id,episode_number,exam_code,exam_name,accession_number, created_at) values (null," .. 
         orderCountAll[1]['ewaiting_referral_id'] .."," .. conn:quote(OrderID) .. "," .. conn:quote(episodeNumber) .. "," .. conn:quote(ExamCode) .. "," .. conn:quote(ExamDesc) .. 
         "," ..conn:quote(accessionNumber) .. "," .. "now())"
         
         sqDB.execute{sql=sql, live=false}         
   
      end
   
   else
      --if orderGrouping ~= 'A.1' then
      --check if the order already exists
      local orderCount = sqDB.execute{sql="select count(id), order_id from ewaiting_referral_orders where order_id = " .. OrderID, live=true}

      if orderCount[1]['count(id)']:S() == '0' then
         sqDB.execute{sql="insert into ewaiting_referral_orders (id,ewaiting_referral_id,order_id,episode_number,exam_code,exam_name,accession_number, created_at) values (null," .. 
            referralCount[1].id:S() .."," .. conn:quote(OrderID) .. "," .. conn:quote(episodeNumber) .. "," .. conn:quote(ExamCode) .. "," .. 
            conn:quote(ExamDesc) .. "," ..conn:quote(accessionNumber) .. "," .. "now())", live=false}      
      end
      
   end
   
end

-----------------------------------------------------------------

function sqDB.TUH.RAD.updateReferralStatus(referralID, ewaitingCat, status)
   
   --IR-567 : PB 18.11.2024
   local status7 = sqUtils.getJsonMappedValue2(sqConfig.extra_params,'referralCategoryStatusMap','7')
   local status8 = sqUtils.getJsonMappedValue2(sqConfig.extra_params,'referralCategoryStatusMap','8')
   local res = sqDB.execute{sql="select status from ewaiting_referrals where id = " .. referralID, live=true}
   local existingStatus = res[1].status
   trace(status, existingStatus:S())
   if status == status8 and existingStatus:S() == status7 then
      sqUtils.printLog("Attempt to set referral status to 8 (active) when status is already at 7 (follow_up), Ignoring status update...")
      return
   end
   --
   
   --update the referral
   local sql ="update ewaiting_referrals " ..
   "set "..
   "status = '"..status.."' " .. ", " ..
   "category_id = " .. ewaitingCat ..
   " where id = ".. referralID

   sqDB.execute{sql=sql, live=false}
   
end

-----------------------------------------------------------------

function sqDB.TUH.RAD.getPatientReferral(id,facilityID,episodeNumber)
   
   local referralCount = sqDB.execute{sql="select count(id), id from ewaiting_referrals where facility_id = " .. facilityID .. " and user_id = " .. id .. 
      " and id in (select ewaiting_referral_id from ewaiting_referral_orders where episode_number = " .. conn:quote(episodeNumber) .. ")", live=true}

   if referralCount[1]['count(id)']:S() == '0' then
      return '0'
   else
      return referralCount[1].id:S()
   end
   
end

-----------------------------------------------------------------

function sqDB.TUH.RAD.createPatient(PID)
   
   local mrn = PID[3][1][1]:S()
   local firstname = PID[5][1][2]:nodeValue()
   local surname = PID[5][1][1]:nodeValue()
   local user_type = "N"
   local user_status = 'active'
   local dob = sqUtils.parseDOB(PID[7])
   local gender = PID[8]:nodeValue()   
   
   --Jira IR-8: TUH PC Radiology - Interface: Phone numbers from iPMS presenting incorrectly
   --Original code assumed that PID[13][1][1] is a semi-colon ; concantenated list of phone numbers, where the 
   --first list item is mobile, and the second list item is home.
   --From evidence in the Iguana logs, this assumption is incorrect.
   --PID[13][1][1] can be mobile; home_phone
   --                  or home_phone; mobile
   --                  or any combination of above + a third phone number
   local phoneNumberList = PID[13][1][1]:S()
   local firstNumber = phoneNumberList:split(';')[1]
   local secondNumber = phoneNumberList:split(';')[2]
   
   if firstNumber ~= nil then
      firstNumber = firstNumber:trimWS()
   end
   
   if secondNumber ~= nil then
      secondNumber = secondNumber:trimWS()
   end
   
   -- deal with scenario where phoneNumberList is something like 014013949   0876814884; 0
   if secondNumber == '0' then
      -- Check if firstNumber is white space concantenated
      local tempNumber = firstNumber
      firstNumber = firstNumber:split(' ')[1]
      
      secondNumber = tempNumber:gsub(firstNumber, '')
      secondNumber = secondNumber:trimWS()
   end
     
   local mobileSplitVal = sqUtils.getJsonMappedValue(sqConfig.extra_params,'mobileSplitVal')
   local mobile_split = ''
   
   if firstNumber ~= nil then
      if firstNumber:sub(0,2) == mobileSplitVal then   
         mobile_split = firstNumber 
         if secondNumber ~= nil then
            home_phone_split = secondNumber
         end
      else
         if secondNumber ~= nil then
            mobile_split = secondNumber
         end
         home_phone_split = firstNumber
      end
   else
      -- not expected to happen
      if secondNumber ~= nil then
         if secondNumber:sub(0,2) == mobileSplitVal then 
            mobile_split = secondNumber   
            home_phone_split = firstNumber -- would be nil
         end
      end
   end
      
   local mobilePrefix = sqUtils.getJsonMappedValue(sqConfig.extra_params,'mobilePrefix')
   local mobile = ''
  
   if mobile_split ~= '' then
      mobile = mobilePrefix..mobile_split:sub(2)
   end
   
   if home_phone_split == nil then
      home_phone_split = 'none'
   end
   
   local address1 = PID[11][1][1]:nodeValue() -- PID[11][1][2]
   local address2 = PID[11][1][2]:nodeValue()
   local address3 = PID[11][1][3]:nodeValue()
   local address4 = PID[11][1][4]:nodeValue()
   local address5 = PID[11][1][5]:nodeValue()
   local accountNumber = PID[18]:S()
   
   sqDB.execute{sql="insert into users (mobile, home_phone, mrn, firstname, surname, user_type, status, Date_of_Birth, \
      Gender, Address_1, Address_2, Address_3, Address_4, Address_5, created, Chart_Number) " ..
      "values ("..
      "mobile = " .. " trim(replace("..conn:quote(mobile)..", ' ', ''))"..", "..
      "home_phone = " .. " trim(replace("..conn:quote(home_phone_split)..", ' ', ''))"..", "..
      conn:quote(mrn)..","..
      conn:quote(firstname)..","..
      conn:quote(surname)..","..
      conn:quote(user_type)..","..
      conn:quote(user_status)..","..
      conn:quote(dob)..","..
      conn:quote(gender)..","..
      conn:quote(address1)..","..
      conn:quote(address2)..","..
      conn:quote(address3)..","..
      conn:quote(address4)..","..
      conn:quote(address5)..","..
      "now()"..","..
      conn:quote(accountNumber)..
      ")"
      , live=false}
	  
   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
   
   return result[1].id:S()
   
end

-----------------------------------------------------------------

function sqDB.TUH.RAD.updatePatient(id, PID)
   
   sqUtils.printLog("Updating patient...")
   
   local mrn = PID[3][1][1]:S()
   
   --PB 26.05.23: replaced as causing error
   --local firstname = PID[5][2]:nodeValue()
   local firstname = PID[5][1][2]:nodeValue()
   
   --PB 26.05.23: replaced as causing error
   --local surname = PID[5][1]:nodeValue()
   local surname = PID[5][1][1]:nodeValue()
   
   local dob = sqUtils.parseDOB(PID[7])
   local gender = PID[8]:nodeValue()  
      
   --Jira IR-8: TUH PC Radiology - Interface: Phone numbers from iPMS presenting incorrectly
   --Original code assumed that PID[13][1][1] is a semi-colon ; concantenated list of phone numbers, where the first 
   --list item is mobile, and the second list item is home.
   --From evidence in the Iguana logs, this assumption is incorrect.
   --PID[13][1][1] can be mobile; home_phone
   --                  or home_phone; mobile
   --                  or any combination of above + a third phone number
   local phoneNumberList = PID[13][1][1]:S()
   local firstNumber = phoneNumberList:split(';')[1]
   local secondNumber = phoneNumberList:split(';')[2]
   
   if firstNumber ~= nil then
      firstNumber = firstNumber:trimWS()
   end
   
   if secondNumber ~= nil then
      secondNumber = secondNumber:trimWS()
   end
   
   -- deal with scenario where phoneNumberList is something like 014013949   0876814884; 0
   if secondNumber == '0' then
      -- Check if firstNumber is white space concantenated
      local tempNumber = firstNumber
      firstNumber = firstNumber:split(' ')[1]
      
      secondNumber = tempNumber:gsub(firstNumber, '')
      secondNumber = secondNumber:trimWS()
   end
   
   local mobileSplitVal = sqUtils.getJsonMappedValue(sqConfig.extra_params,'mobileSplitVal')
   local mobile_split = ''
   
   if firstNumber ~= nil then
      if firstNumber:sub(0,2) == mobileSplitVal then   
         mobile_split = firstNumber 
         if secondNumber ~= nil then
            home_phone_split = secondNumber
            trace(mobile_split, home_phone_split)
         end
      else
         if secondNumber ~= nil then
            mobile_split = secondNumber
         end
         home_phone_split = firstNumber
         trace(mobile_split, home_phone_split)
      end
   else
      -- not expected to happen
      if secondNumber ~= nil then
         if secondNumber:sub(0,2) == mobileSplitVal then 
            mobile_split = secondNumber   
            home_phone_split = firstNumber -- would be nil
         end
      end
   end
   
   local mobilePrefix = sqUtils.getJsonMappedValue(sqConfig.extra_params,'mobilePrefix')
   local mobile = ''
   
   trace(mobile_split,mobile_split:sub(2))
   
   if mobile_split ~= '' then
      mobile = mobilePrefix..mobile_split:sub(2)
   end
   
   if home_phone_split == nil then
      home_phone_split = 'none'
   end
   
   --PB 26.05.23: Perform IPM lookup. If we get a phone number then replace the number from the NIMIS message
   --BUGS-437 IR-8
   ---------------------------------------------------------------------
   
   trace(home_phone_split)
   trace(mobile)
   
   local performPASLookup = sqUtils.getJsonMappedValue(sqConfig.extra_params,'performPASLookup')
   
   if performPASLookup == "1" then
      
      local qMobile = nil
      local qHomePhone = nil

      iguana.logInfo("Sending QRY^A19 to IPM for: "..mrn)
      local success, qHomePhone, qMobile, msa = pcall(getPhoneNumbersFromIPM, mrn)

      if not success then
         iguana.logDebug("Problem calling IPM query for patient "..mrn.." : "..qHomePhone)
      else
         if qHomePhone..qMobile ~= '' then
            iguana.logInfo("ADR^A19 from IPM returned the following for patient "
               ..mrn.." : ".."Homephone: "..qHomePhone.." Mobile: "..qMobile)

            if qHomePhone ~= '' then
               home_phone_split = qHomePhone
            end

            if qMobile ~= '' then
               mobile = qMobile
            end
         end

      end
   end
   
   trace(home_phone_split)
   trace(mobile)
   
   ---------------------------------------
   
   local address1 = PID[11][1][1]:nodeValue()
   local address2 = PID[11][1][2]:nodeValue()
   local address3 = PID[11][1][3]:nodeValue()
   local address4 = PID[11][1][4]:nodeValue()
   local address5 = PID[11][1][5]:nodeValue()
   local accountNumber = PID[18]:S()

   local sql ="update users " ..
   "set "..
   "mrn = " .. conn:quote(mrn) .. ", "..
   "firstname = " .. conn:quote(firstname) .. ", "..
   "surname = " .. conn:quote(surname) .. ", "..
   "Date_of_Birth = " .. conn:quote(dob) .. ", "..
   "mobile = " .. " trim(replace("..conn:quote(mobile)..", ' ', ''))"..", "..
   "home_phone = " .. " trim(replace("..conn:quote(home_phone_split)..", ' ', ''))"..", "..
   "Gender = " .. conn:quote(gender) .. ", "..
   "Address_1 = " .. conn:quote(address1) .. ", "..
   "Address_2 = " .. conn:quote(address2) .. ", "..
   "Address_3 = " .. conn:quote(address3) .. ", "..
   "Address_4 = " .. conn:quote(address4) .. ", "..
   "Address_5 = " .. conn:quote(address5) .. ", "..
   "Chart_Number = " .. conn:quote(accountNumber) ..
   " where id = ".. id

   sqDB.execute{sql=sql, live=false}

   sqUtils.printLog("Patient updated!")
   
end

-----------------------------------------------------------------

function sqDB.TUH.RAD.createFacilityGP(gp_details, facility_id)
   
   sqDB.execute{sql="insert into facility_gp (gp_details, facility_id, hospital_code, gmc_code, dh_code, m_number) " ..
         "values ("..
         conn:quote(gp_details)..","..
         conn:quote(facility_id)..","..
         "0" ..","..
         "0" ..","..
         "0" ..","..
         "0" ..
         ")"
         , live=false}
   
   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
   
   return result[1].id:S()
end

-----------------------------------------------------------------

function sqDB.TUH.RAD.createGP(firstname, surname, medical_council_number)
   
   sqDB.execute{sql="insert into gp_details (firstname, surname, medical_council_number) " ..
         "values ("..
         conn:quote(firstname)..","..
         conn:quote(surname)..","..
         conn:quote(medical_council_number)..
         ")"
         , live=false}
   
   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
   
   return result[1].id:S()
   
end

-----------------------------------------------------------------

function sqDB.TUH.RAD.findGP(medical_council_number, facility_id)
           
   local GPdata = sqDB.execute{sql="select count(id), id from gp_details where medical_council_number = " .. conn:quote(medical_council_number) .. " and medical_council_number <> ''", live=true}
    
   local countStr = GPdata[1]['count(id)']:S()
  
   local count = tonumber(countStr)
   
   local gpIndexStr = ''
   local gpIndex = nil
   
   for index=1, count, 1
   do
      if GPdata[index].id ~= nil then
         
          if GPdata[index].id:S() ~= 'NULL' then
            gpIndexStr = GPdata[index].id:S() 
            gpIndex = tonumber(gpIndexStr)
         
          end
      end 
   end

   if count > 1 and gpIndexStr ~= '' then
  
      local GPdetails = sqDB.execute{sql="select id from facility_gp where gp_details = " .. conn:quote(gpIndexStr) .. " and facility_id = " .. conn:quote(facility_id), live=true}
           
      local gpDetailsStr = GPdetails[1].id:S()
      
      if gpDetailsStr == 'NULL' then
         -- Add an entry into the facility_gp for this GP and Facility
         local result = createFacilityGP(gpIndex, facility_id)
         
       end      
   end
   
   if GPdata[1]['count(id)']:S() == '0' then
      return ''
   else
      return gpIndexStr
   end
   
end

----------------------------------------------------------------------------



return sqDB