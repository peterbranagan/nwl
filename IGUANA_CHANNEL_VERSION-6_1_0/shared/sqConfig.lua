--[[

Author: Peter Branagan
Date: Aug 2023

Config settings are stored in a table called iguana on the database
Database audit is stored in a table called iguana_db_audit on the database


]]

--sqDB = require('sqDatabase')
sqUtils = require('sqUtils')

--global used throughot the channel
conn = nil

sqConfig = {
   
   -- set the filename containing the DB credentials
   dbFile = _G.DbFile,
   
   --Config varibales to be set in the init() function
   
   ie_instance = nil, -- holds the instance tyope stored in iguana environment variable
   ie_channel = nil, -- holds the name of the running channel
   parent_facility = nil,
	child_facilities = {}, -- hold child facility ids for parent
   ack_email = nil, -- email adress(s) of alert receivers
   
   --Inbound and outbound config variables set in getConfigData()
   
   --Inbound vars
   msg_types_allowed = nil, -- HL7 msg types allowed, eg: ADT^A05, SIU^S12 etc
   assigning_facility = nil,
   canc_email_add = nil, -- cancellation email address
   title_map = nil, -- j soncontaining patient title mappings
   clin_code_map = nil, -- used for mapping IPM clinic codes to SQ facility Ids
   sess_code_map = nil, -- used for mapping IPM session codes to SQ facility Ids
   referral_params = nil, --default values when creating a referral
   exam_map = nil, --clinic to exam mappings
   prov_source_map, --clinic to provider source map
   appreason_map = nil, --CH08 specific
   conditiontype_map = nil, 
   referralsource_map = nil,
   sess_code_map = nil, 
   specialty_map = nil,
   priority_map = nil,
   merge_site_code = nil,
   app_visit_codes = nil, -- NAT codes mappings for visit type codes
   extra_params = nil, -- used to store misc parameters
   
   --Outnound vars
   res_mod_source_types = {}, -- hold values relating to the modified_source in json
   app_to_3p = nil, -- if set to 1 or 0 for processing ORM , SIU , ADT outbound
   clinic_code = nil 
   
}

--[[

dbCredentials 

]]

----------------------------------------------------------------------------
sqConfig.dbCredentials = {
   
   db_name = nil,
   db_user = nil,
   db_pass = nil
}

----------------------------------------------------------------------------
function sqConfig.dbCredentials.Get()
   
   local raw_db_credentials = sqUtils.readFile(sqConfig.dbFile)
   local dbo = raw_db_credentials:split('\n')
   sqConfig.dbCredentials.db_name = dbo[1]
   sqConfig.dbCredentials.db_user = dbo[2]
   sqConfig.dbCredentials.db_pass = dbo[3]
   
end

--[[

Config 

]]

----------------------------------------------------------------------------
function sqConfig.init()

   print("Initializing config..")
   
   --Path to the server side file containing credentials
   --sqConfig.dbFile = '/users/peterbranagan/documents/demo.txt'
   --sqConfig.dbFile = '/root/demo.txt'
   --IE_INSTANCE environment variable should be one of DEV, TST, PRD
   sqConfig.ie_instance = os.getenv("IE_INSTANCE")
   sqConfig.ie_channel = iguana.channelName()
   sqConfig.dbCredentials.Get()
   
end

----------------------------------------------------------------------------
function sqConfig.dbConnect()

   print("connecting to DB..")
   
   return db.connect{api=db.MY_SQL, name=sqConfig.dbCredentials.db_name, 
      user=sqConfig.dbCredentials.db_user, 
      password=sqConfig.dbCredentials.db_pass, live=true}

end

----------------------------------------------------------------------------
function sqConfig.getConfigData()
   
   print("Retrieving config data..")
   
   conn = sqConfig.dbConnect()
   
   local chData = conn:execute{sql=
      "select * from iguana_config where channel = "..conn:quote(sqConfig.ie_channel)..
         " and ie_instance = "..conn:quote(sqConfig.ie_instance), live=true}
   
   if #chData > 0 then
 
      sqConfig.parent_facility = chData[1].facility_id
      --Populate the child facilities
      --sqConfig.child_facilities = sqDB.getFacilitiesByParent(conn, sqConfig.parent_facility:S())
      --Populate reservation modified_source values
      sqConfig.res_mod_source_types = sqUtils.split(chData[1].res_mod_source_types,",")
      --determine if we send out ORM , SIU , ADT
      sqConfig.app_to_3p = chData[1].app_to_3p:S()
      sqConfig.clinic_code = chData[1].clinic_code:S()
      sqConfig.assigning_facility = chData[1].assigning_facility:S()
      sqConfig.canc_email_add = chData[1].canc_email_add:S()
      sqConfig.title_map = chData[1].title_map:S()
      --list of clinic codes and their facility id's
      sqConfig.clin_code_map = chData[1].clin_code_map:S()
      sqConfig.sess_code_map = chData[1].sess_code_map:S()
      --referall parameters for creating referrals
      sqConfig.referral_params = chData[1].referral_params:S()
      --clinic to exam mapping
      sqConfig.exam_map = chData[1].exam_map:S()
      --clinic to provder source mapping
      sqConfig.prov_source_map = chData[1].prov_source_map:S()
      --category and specialty below used in referrakl creation
      sqConfig.specialty_map = chData[1].specialty_map:S()
      --variables used for app reason id mapping
      sqConfig.appreason_map = chData[1].appreason_map:S()
      --variables used for appointment condition types
      sqConfig.conditiontype_map = chData[1].conditiontype_map:S()
      --variables used for referral source
      sqConfig.referralsource_map = chData[1].referralsource_map:S()
      --variables used for priotity
      sqConfig.priority_map = chData[1].priority_map:S()
      --code use to audit merges
      sqConfig.merge_site_code = chData[1].merge_site_code:S()
      --get the list of allowed message types
      sqConfig.msg_types_allowed = chData[1].msg_types_allowed:S()
      --visit typew code mappings
      sqConfig.app_visit_codes = chData[1].app_visit_codes:S()
      --send ack email yes/no (1 or 0)
      sqConfig.ack_email = chData[1].ack_email:S()
      
      --pull out additional misc params
      sqConfig.extra_params = chData[1].extra_params:S()
      
   else
      error("Unable to retrive interface configuration")
   end

end

----------------------------------------------------------------------------
function sqConfig.inChildFacilities(val)

   --Find for val in child facilities
   
   local found = false
   if sqConfig.child_facilities ~= nil then
      for i=1, #sqConfig.child_facilities do
         if tostring(val) == sqConfig.child_facilities[i].id:S() then
            return true
         end
      end
   end
   return found   
end

--return the config and database connection
return sqConfig, conn
