--[[

Author: Peter Branagan
Date: Feb 2024

Used for validation layer


]]

local sqValidation = {
   
  
   
}


function sqValidation.validate(customer, parentFacilityID, facilityID, MsgIn)
   
   if customer == 'NWL' then

      return sqValidation.validateNWL(parentFacilityID, facilityID, MsgIn)
   
   elseif customer == 'MSE' then
      
      return sqValidation.validateMSE(parentFacilityID, facilityID, MsgIn)
      
   elseif customer == 'EMRAD' then
      
      return sqValidation.validateEMRAD(parentFacilityID, facilityID, MsgIn)
      
   end
   
   
end


-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Validation methods - EMRAD
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

function sqValidation.validateEMRAD(parentFacilityID, facilityID, MsgIn)
   
   local continue = true
   
   if sqUtils.isEmpty(parentFacilityID) then
      error("Parent facility ID must be set in config!")
   end
   
   if MsgIn.MSH[9][1]:S() == 'SIU' then
      
      -- Validate Filler Order Category
      
      local examCatCheck = MsgIn.AIS[3][1]:S()
      if MsgIn.MSH[9][2]:S() == 'S12' and sqUtils.isEmpty(examCatCheck) then
         error('Order Exam Category not present in field AIS.3.1. Message not processed.')
      end  
      
      local examCodeCheck = MsgIn.AIS[3][2]:S()
      if MsgIn.MSH[9][2]:S() == 'S12' and sqUtils.isEmpty(examCodeCheck) then
         error('Order Exam Code not present in field AIS.3.2. Message not processed.')
      elseif MsgIn.MSH[9][2]:S() == 'S12' and not sqUtils.isEmpty(examCodeCheck) then
         --if the exam code does exist, check if its set up on SQ
         if not sqDB.RAD.isValidExamCode(facilityID, examCatCheck) then
            error('Order Exam Code not present in SQ , Message not processed.')
         end 
      end
      
      local ApptDateCheck = MsgIn.SCH[11][4]:S()
      if sqUtils.isEmpty(ApptDateCheck) then
         error('Appointment Date not present in field SCH.11.4. Message not processed.')
      end
      
      local ApptEndDateCheck = MsgIn.SCH[11][5]:S()
      if sqUtils.isEmpty(ApptEndDateCheck) then
         error('Appointment End Date not present in field SCH.11.5. Message not processed.')
      end
      
   end
   
   -- Validate Filler Order Category
   if MsgIn.MSH[9][1]:S() == 'ORM' then
      
      -- Validate Filler Order Category
      
      local examCatCheck = MsgIn.OBR[4][1]:S()
      if sqUtils.isEmpty(examCatCheck) then
         error('Order Exam Category not present in field OBR.4.1. Message not processed.')
      end  
      --
      local examCodeCheck = MsgIn.OBR[4][2]:S()
      if sqUtils.isEmpty(examCodeCheck) then
         error('Order Exam Code not present in field OBR.4.1. Message not processed.')
      end 
      
      if not sqDB.RAD.isValidExamCode(facilityID, examCatCheck) then
         error('Order Exam Code not present in SQ , Message not processed. Facility: '..facilityID..', Exam: '..examCatCheck)
      end 
      
   end
   
  
   
   --Validate Patient create/update
   if sqUtils.isEmpty(MsgIn.PID[3][1][1]:S()) then
      error('PID segment missing a patient number.')
   end
   
   --Validate for merge
   if MsgIn:childCount('MRG') > 0 then    
      if sqUtils.isEmpty(MsgIn.MRG[1][1]:S()) then
         error('MRG segment missing a minor patient number.')
      end
   end
   
   --Validate child facility
   --
   
   return continue

end

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Validation methods - MSE
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

function sqValidation.validateMSE(parentFacilityID, facilityID, MsgIn)
   
   local continue = true
   
   if sqUtils.isEmpty(parentFacilityID) then
      error("Parent facility ID must be set in config!")
   end
   
   if MsgIn.MSH[9][1]:S() == 'SIU' then
      
      -- Validate Filler Order Category
      
      local examCatCheck = MsgIn.AIS[3][1]:S()
      if MsgIn.MSH[9][2]:S() == 'S12' and sqUtils.isEmpty(examCatCheck) then
         error('Order Exam Category not present in field AIS.3.1. Message not processed.')
      end  
      
      local examCodeCheck = MsgIn.AIS[3][2]:S()
      if MsgIn.MSH[9][2]:S() == 'S12' and sqUtils.isEmpty(examCodeCheck) then
         error('Order Exam Code not present in field AIS.3.2. Message not processed.')
      elseif MsgIn.MSH[9][2]:S() == 'S12' and not sqUtils.isEmpty(examCodeCheck) then
         --if the exam code does exist, check if its set up on SQ
         if not sqDB.RAD.isValidExamCode(facilityID, examCatCheck) then
            error('Order Exam Code not present in SQ , Message not processed.')
         end 
      end
      
      local ApptDateCheck = MsgIn.SCH[11][4]:S()
      if sqUtils.isEmpty(ApptDateCheck) then
         error('Appointment Date not present in field SCH.11.4. Message not processed.')
      end
      
      local ApptEndDateCheck = MsgIn.SCH[11][5]:S()
      if sqUtils.isEmpty(ApptEndDateCheck) then
         error('Appointment End Date not present in field SCH.11.5. Message not processed.')
      end
      
   end
   
   -- Validate Filler Order Category
   if MsgIn.MSH[9][1]:S() == 'ORM' then
      
      -- Validate Filler Order Category
      
      local examCatCheck = MsgIn.OBR[4][1]:S()
      if sqUtils.isEmpty(examCatCheck) then
         error('Order Exam Category not present in field OBR.4.1. Message not processed.')
      end  
      --
      local examCodeCheck = MsgIn.OBR[4][2]:S()
      if sqUtils.isEmpty(examCodeCheck) then
         error('Order Exam Code not present in field OBR.4.1. Message not processed.')
      end 
      
      if not sqDB.RAD.isValidExamCode(facilityID, examCatCheck) then
         error('Order Exam Code not present in SQ , Message not processed. Facility: '..facilityID..', Exam: '..examCatCheck)
      end 
      
   end
   
  
   
   --Validate Patient create/update
   if sqUtils.isEmpty(MsgIn.PID[3][1][1]:S()) then
      error('PID segment missing a patient number.')
   end
   
   --Validate for merge
   if MsgIn:childCount('MRG') > 0 then    
      if sqUtils.isEmpty(MsgIn.MRG[1][1]:S()) then
         error('MRG segment missing a minor patient number.')
      end
   end
   
   --Validate child facility
   --
   
   return continue

end

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Validation methods - NWL
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

function sqValidation.validateNWL(parentFacilityID, facilityID, MsgIn)
   
    local continue = true
   
   if sqUtils.isEmpty(parentFacilityID) then
      error("Parent facility ID must be set in config!")
   end
   
   if MsgIn.MSH[9][1]:S() == 'SIU' then
      
      -- Validate Filler Order Category
      
      local examCatCheck = MsgIn.AIS[3][1]:S()
      if MsgIn.MSH[9][2]:S() == 'S12' and sqUtils.isEmpty(examCatCheck) then
         error('Order Exam Category not present in field AIS.3.1. Message not processed.')
      end  
      
      local examCodeCheck = MsgIn.AIS[3][2]:S()
      if MsgIn.MSH[9][2]:S() == 'S12' and sqUtils.isEmpty(examCodeCheck) then
         error('Order Exam Code not present in field AIS.3.2. Message not processed.')
         --[[
      elseif MsgIn.MSH[9][2]:S() == 'S12' and not sqUtils.isEmpty(examCodeCheck) then
         --if the exam code does exist, check if its set up on SQ
         if not sqDB.RAD.isValidExamCode(facilityID, examCatCheck) then
            error('Order Exam Code not present in SQ , Message not processed.')
         end 
         ]]
      end
      
      --[[ PB 25.07.2024 -IR-346 : as per steves email on 25th, process regardless of empty app date but only set referral to active
      So this check is no longer required
      local ApptDateCheck = MsgIn.SCH[11][4]:S()
      if sqUtils.isEmpty(ApptDateCheck) then
         local roomCode = MsgIn.AIL[2][3][1]:S()
         if sqDB.RAD.isValidRoomCode(facilityID, roomCode) then
            continue = true
         else
            sqUtils.printLog("Empty appointment date found for room code: '"..roomCode.."' which is not managed, skipping message..")
            continue = false
         end
      end
      
      --PB 24.07.2024 : Added check for start date, if not start date then no pint in checking the end date
      if not sqUtils.isEmpty(ApptDateCheck) then
         local ApptEndDateCheck = MsgIn.SCH[11][5]:S()
         if sqUtils.isEmpty(ApptEndDateCheck) then
            error('Appointment End Date not present in field SCH.11.5. Message not processed.')
         end
      end
      ]]
      
   end
   
   -- Validate Filler Order Category
   if MsgIn.MSH[9][1]:S() == 'ORM' then
      
      -- Validate Filler Order Category
      
      local examCatCheck = MsgIn.OBR[4][1]:S()
      if sqUtils.isEmpty(examCatCheck) then
         error('Order Exam Category not present in field OBR.4.1. Message not processed.')
      end  
      --
      local examCodeCheck = MsgIn.OBR[4][2]:S()
      if sqUtils.isEmpty(examCodeCheck) then
         error('Order Exam Code not present in field OBR.4.1. Message not processed.')
      end 
      
      if not sqDB.RAD.isValidExamCode(facilityID, examCatCheck) then
         sqUtils.printLogW('Order Exam Code not present in SQ , Message not processed. Facility: '..facilityID..', Exam: '..examCatCheck)
         continue = false
      end 
      
   end
   
  
   
   --Validate Patient create/update
   if sqUtils.isEmpty(MsgIn.PID[3][1][1]:S()) then
      error('PID segment missing a patient number.')
   end
   
   --Validate for merge
   if MsgIn:childCount('MRG') > 0 then    
      if sqUtils.isEmpty(MsgIn.MRG[1][1]:S()) then
         error('MRG segment missing a minor patient number.')
      end
   end
   
   --Validate child facility
   --
   
   return continue

end

-----------------------------------------------------------------------------

return sqValidation

