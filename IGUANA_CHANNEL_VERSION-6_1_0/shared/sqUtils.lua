--[[

Author: Peter Branagan
Date: Aug 2023

Utility functions that can be used across channels.


]]

 
local sqUtils = {
   --emailSubject = 'ERROR - ???? Interface',
   --ApiErrorCode = nil, -- used to check against error returned from API and send alert
   pCall_retryCount = 5,
   pCall_Secs = 3,
   
}

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Misc Utilities
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

function sqUtils.inStr(str, val)
   
   local tbl = str:split(',')
   local res = sqUtils.inTable(tbl,val)
   return res 
   
end

-----------------------------------------------------------------------------

function sqUtils.mapHL7Message(newMessage, msgIn)
   
   sqUtils.printLog("Mapping HL7...")
   
   local map = json.parse{data=sqCfg.extra_params}.mapping
   
   for destField, sourceField in pairs(map) do
      
      sqUtils.printLog("Mapping source "..sourceField.." to destination "..destField)
      
      local sourceVal = sqUtils.getHL7FieldValue(msgIn, sourceField)
      sqUtils.setHL7FieldValue(newMessage, destField, sourceVal)
   
   end
   
   return newMessage
   
end

-----------------------------------------------------------------------------

function sqUtils.getHL7FieldValue(msg, field)
   
   --[[
   Take in a message and field string and return the value in the message
   
   Example: field = 'PID.3.1'
   ]]
   
   local parts = field:split('.')

   local segment = parts[1]

   local currentNode

   local success, err = pcall(function()
         currentNode = msg[segment]
      end)

   if success then
      trace(#parts)
      for i = 2, #parts do
         local index = tonumber(parts[i])
         if currentNode and currentNode[index] then
            currentNode = currentNode[index]
         else
            error("Invalid field path: " .. field)
         end
      end
      return currentNode:S()
   end

   return ''
   
end

-----------------------------------------------------------------------------

function sqUtils.checkControlCharacters(str, ver)
   
   local newStr = ''
   
   if ver == '1' then
      --swap \T\ with &
      newStr = string.gsub(str, '\\T\\', '&', 1)
   elseif ver == '2' then
      newStr = string.gsub(str, '\\T\\', '&amp;', 1)
   end
   
   return newStr
end

-----------------------------------------------------------------------------

function sqUtils.parseOpenlinkMessage(Data)
   
   buffers = {}
   
   --match buffers starting with "DAT", followed by 3 digits, any 3 characters, and "NLS"
   local pattern = "(DAT%d%d%d...NLS.-)(?=DAT%d%d%d)"

   local startPos = 1
   while true do
      local s, e = string.find(Data, "DAT%d%d%d...NLS", startPos)
      if not s then break end
      local nextPos = string.find(Data, "DAT%d%d%d...NLS", e + 1)
      if nextPos then
         table.insert(buffers, Data:sub(s, nextPos - 1))
         startPos = nextPos
      else
         table.insert(buffers, Data:sub(s))
         break
      end
   end

   --handle the final buffer
   local endPos = string.find(buffers[#buffers], "DAT%d%d%d...END")
   local endBuffer = buffers[#buffers]:sub(endPos)
   buffers[#buffers] = buffers[#buffers]:sub(1,endPos-1)
   table.insert(buffers,endBuffer)
   
   return buffers
   
end

-----------------------------------------------------------------------------

function sqUtils.stringToTable(s)
   local t = {}
   -- Remove curly braces and split by commas
   s = s:match("{(.*)}")
   for word in s:gmatch("'(.-)'") do
      table.insert(t, word)
   end
   return t
end

-----------------------------------------------------------------------------

function sqUtils.sendMail(emailSubject, text, emailTo)
   
   local eUsername, ePassword, eServer = sqUtils.getEmailCredentials()
   
   local tblTo = sqUtils.convertToTable(emailTo)
   
   -- Set up the parameters to be used with sending an email
   local smtpparams={
      header = {--To = "'"..tostring(strEmails).."'"; 
                From = 'no-reply@swiftqueue.com'; 
                --Date = 'Thu, 23 Aug 2001 21:27:04 -0400';
                subject = emailSubject}, 
      username = eUsername,
      password = ePassword,
      server = eServer, 
      to = tblTo,
      from = 'no-reply@swiftqueue.com',
      body = text,
      use_ssl = 'try',
      live = false, 
      debug = true
   } 
 
    net.smtp.send(smtpparams) 
   
end

-----------------------------------------------------------------------------

function sqUtils.sendEmailAlert(eUsername, ePassword, eServer, emailSubject, text, emailTo)
   
   local tblTo = sqUtils.convertToTable(emailTo)
   
   -- Set up the parameters to be used with sending an email
   local smtpparams={
      header = {--To = "'"..tostring(strEmails).."'"; 
                From = 'no-reply@swiftqueue.com'; 
                --Date = 'Thu, 23 Aug 2001 21:27:04 -0400';
                subject = emailSubject}, 
      username = eUsername,
      password = ePassword,
      server = eServer, 
      to = tblTo,
      from = 'no-reply@swiftqueue.com',
      body = text,
      use_ssl = 'try',
      live = false, 
      debug = true
   } 
 
    net.smtp.send(smtpparams) 
   
end

-----------------------------------------------------------------------------

-- Function to check if a date string in the format 'yyyy-mm-dd' is valid
function sqUtils.isValidDate(dateStr)
   
    -- Define the pattern for yyyy-mm-dd
    local pattern = "^(%d%d%d%d)-(%d%d)-(%d%d)$"
    
    -- Match the date string with the pattern and capture year, month, and day
    local year, month, day = dateStr:match(pattern)
    
    -- If the pattern doesn't match, return false
    if not year then
        return false
    end
    
    -- Convert captures to numbers
    year = tonumber(year)
    month = tonumber(month)
    day = tonumber(day)
    
    -- Check if the month is valid (1 to 12)
    if month < 1 or month > 12 then
        return false
    end
    
    -- Check if the day is valid for the given month and year
    local daysInMonth = {
        31, -- January
        (year % 4 == 0 and year % 100 ~= 0 or year % 400 == 0) and 29 or 28, -- February (leap year check)
        31, -- March
        30, -- April
        31, -- May
        30, -- June
        31, -- July
        31, -- August
        30, -- September
        31, -- October
        30, -- November
        31  -- December
    }
    
    if day < 1 or day > daysInMonth[month] then
        return false
    end
    
    -- If all checks passed, the date is valid
    return true
end

-----------------------------------------------------------------------------

-- Function to check if a time string in the format 'HH:MM' is valid
function sqUtils.isValidTime(timeStr)
    -- Define the pattern for HH:MM
    local pattern = "^(%d%d):(%d%d)$"
    
    -- Match the time string with the pattern and capture hour and minute
    local hour, minute = timeStr:match(pattern)
    
    -- If the pattern doesn't match, return false
    if not hour then
        return false
    end
    
    -- Convert captures to numbers
    hour = tonumber(hour)
    minute = tonumber(minute)
    
    -- Check if the hour is valid (0 to 23)
    if hour < 0 or hour > 23 then
        return false
    end
    
    -- Check if the minute is valid (0 to 59)
    if minute < 0 or minute > 59 then
        return false
    end
    
    -- If all checks passed, the time is valid
    return true
end

-----------------------------------------------------------------------------

function sqUtils.addMinutes(dateTimeStr, minutesToAdd)
   -- Parse the date-time string into individual components
   local year, month, day, hour, min = dateTimeStr:match("(%d+)-(%d+)-(%d+) (%d+):(%d+)")

   -- Convert to numbers
   year = tonumber(year)
   month = tonumber(month)
   day = tonumber(day)
   hour = tonumber(hour)
   min = tonumber(min)

   -- Add minutes
   min = min + minutesToAdd

   -- Handle overflow of minutes into hours and days
   while min >= 60 do
      min = min - 60
      hour = hour + 1
   end

   while hour >= 24 do
      hour = hour - 24
      day = day + 1
   end

   -- Days in each month (not accounting for leap years for simplicity)
   local daysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}

   while day > daysInMonth[month] do
      day = day - daysInMonth[month]
      month = month + 1
      if month > 12 then
         month = 1
         year = year + 1
      end
   end

   -- Format the new date-time as a string
   return string.format("%04d-%02d-%02d %02d:%02d", year, month, day, hour, min)
end

----------------------------------------------------------------------------

function sqUtils.sendMail(emailSubject, text, emailTo)
   
   local eUsername, ePassword, eServer = sqUtils.getEmailCredentials()
   
   local tblTo = sqUtils.convertToTable(emailTo)
   
   -- Set up the parameters to be used with sending an email
   local smtpparams={
      header = {--To = "'"..tostring(strEmails).."'"; 
                From = 'no-reply@swiftqueue.com'; 
                --Date = 'Thu, 23 Aug 2001 21:27:04 -0400';
                subject = emailSubject}, 
      username = eUsername,
      password = ePassword,
      server = eServer, 
      to = tblTo,
      from = 'no-reply@swiftqueue.com',
      body = text,
      use_ssl = 'try',
      live = false, 
      debug = true
   } 
 
    net.smtp.send(smtpparams) 
   
end

-----------------------------------------------------------------------------

function sqUtils.segmentExists(message, segmentName)
   for i = 1, #message do
      local segment = message[i]
      if segment:sub(1, 3) == segmentName then
         return true
      end
   end
   return false
end

-----------------------------------------------------------------------------

--split an email list string
function sqUtils.split(s, delimiter)
    result = {};
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match);
    end
   
    return result;
end

-----------------------------------------------------------------------------

function sqUtils.convertToTable(inputString)
    local resultTable = {}
    for word in inputString:gmatch("[^,%s]+") do
        table.insert(resultTable, word)
    end
    return resultTable
end

-----------------------------------------------------------------------------

function sqUtils.isEmpty(s)
  return s == nil or s == '' or type(s) == 'userdata' or s == 'NULL' or s == json.NULL
end

-----------------------------------------------------------------------------

function sqUtils.setAckWarning(errorCode)
   
   --NOTE: Each implementation should have _G.ackMessage initialized as a global variable in main.lua
   
   local errorMsgs = sqUtils.getJsonMappedValue(sqCfg.extra_params,'errorMsgs')
   local str = sqUtils.getJsonMappedValue(sqUtils.jsonTableToString(errorMsgs),tostring(errorCode))
   if str == '' then str = 'DEFAULT WARNING' end
   
   if _G.ackMessage == 'OK' then
      sqUtils.printLog("Setting ACK warning to: "..str)
      _G.ackMessage = "CA:"..str
   else
      sqUtils.printLog("Adding "..str.." to ACK warning")
      _G.ackMessage = _G.ackMessage..", "..str
   end
   
end

-----------------------------------------------------------------------------

function sqUtils.printLog(str,...)
   
   if str == nil or str == '' or str == json.NULL then
      return
   end
   
   --prints a log string with the line number it was called from
   --takes multiple strings as parameters
   
   --ignore the initial lines
   local s = debug.traceback()
   local cnt = 0
   for line in s:gmatch("[^\r\n]+") do
      if not ( line:find("traceback") or line:find('printLog') ) then
         cnt = cnt + 1
         s = line:gsub("\t","")
         break
      end
   end
   
   --strip out the garbage
   s = s:gsub("string ","")
   s = s:gsub('%["',"")
   s = s:gsub('"]',"")
   s = s:gsub("<","")
   s = s:gsub(">","")
   local pos = s:find(" in function ")
   if pos > 2 then
      s = s:sub(1,pos-2)
   end
   trace(s)
   
   --only show the filename
   local lastIndex = nil
   local startPos = 1
   repeat
      local charPos = string.find(s, "/", startPos, true)
      if charPos then
         lastIndex = charPos
         startPos = charPos + 1
      end
   until not charPos
   trace(lastIndex)
   if lastIndex ~= nil then     
      s = s:sub(lastIndex+1,999)    
   end
   
   
   argStr = str
   local argCount = select("#", ...) 
   for i = 1, argCount do
      local arg = select(i, ...) 
      argStr = argStr..", "..arg
   end
   
   local msgID = _G.msgCtrlID
   if msgID == nil then msgID = '' end
   
   logStr = argStr.."   ("..s..", MsgID:"..msgID..")"
   
   iguana.logDebug("SQDEBUG: "..logStr) 
   
end

-----------------------------------------------------------------------------
--same as printLog above except writes to the warning queue

function sqUtils.printLogW(str,...)
   
   if str == nil or str == '' or str == json.NULL then
      return
   end
   
   --prints a log string with the line number it was called from
   --takes multiple strings as parameters
   
   --ignore the initial lines
   local s = debug.traceback()
   local cnt = 0
   for line in s:gmatch("[^\r\n]+") do
      if not ( line:find("traceback") or line:find('printLogW') ) then
         cnt = cnt + 1
         s = line:gsub("\t","")
         break
      end
   end
   
   --strip out the garbage
   s = s:gsub("string ","")
   s = s:gsub('%["',"")
   s = s:gsub('"]',"")
   s = s:gsub("<","")
   s = s:gsub(">","")
   local pos = s:find(" in function ")
   if pos > 2 then
      s = s:sub(1,pos-2)
   end
   trace(s)
   
   --only show the filename
   local lastIndex = nil
   local startPos = 1
   repeat
      local charPos = string.find(s, "/", startPos, true)
      if charPos then
         lastIndex = charPos
         startPos = charPos + 1
      end
   until not charPos
   trace(lastIndex)
   if lastIndex ~= nil then     
      s = s:sub(lastIndex+1,999)    
   end
   
   
   argStr = str
   local argCount = select("#", ...) 
   for i = 1, argCount do
      local arg = select(i, ...) 
      argStr = argStr..", "..arg
   end
   
   logStr = argStr.."   ("..s..")"
   
   iguana.logWarning(logStr)
   
end

-----------------------------------------------------------------------------

--[[

Get the value of a key in a config 1D json string

Example json: {
                "KEY1":"VALUE1","KEY2":"VALUE2"
              }

Usage: getMappedValue(source json string , "KEY1") -> VALUE1

]]
function sqUtils.getMappedValue(source,key,key2)
   --set key to '' to do reverse find
   
   local tmpTbl = sqUtils.split(sqUtils.getJsonMappedValue(source,key,key2),"^")
   local res1 = tmpTbl[1]
   local res2 = tmpTbl[2]
   if res1 == '' then
      tmpTbl = sqUtils.split(sqUtils.getJsonMappedValue(source,"default",key2),"^")
      res1 = tmpTbl[1]
      res2 = tmpTbl[2]
   end
   
   if res1 == nil then res1 = '' end
   if res2 == nil then res2 = '' end
   
   return res1, res2
   
end

-----------------------------------------------------------------------------

--function wrapper for reverse find on getMappedValue
function sqUtils.getMappedValueR(source,key,key2)
   --set key to '' to do reverse find
   res1,res2 = sqUtils.getMappedValue(source,'',key2)
   
   return res1, res2
   
end

-----------------------------------------------------------------------------

--[[

Get the value of a key in a config 2D json string

Example json: "{
           "1234":{"xxx":"VALUE1","yyy":"VALUE2"}
	        "9999":{"aaa":"VALUE1","bbb":"VALUE300"}
          }

Usage: getMappedValue2(source json string , "9999", "yyy") -> VALUE2

]]
function sqUtils.getMappedValue2(source, key, key2)
   
   local tmpStr = sqUtils.getJsonMappedValue2(source,key,key2)
   if tmpStr == nil then
      return '',''
   end
   
   local tmpTbl = sqUtils.split(tmpStr,"^")
   local res1 = tmpTbl[1]
   local res2 = tmpTbl[2] 
   
   if res1 == '' then
      tmpTbl = sqUtils.split(sqUtils.getJsonMappedValue2(source,"default"),"^")
      res1 = tmpTbl[1]
      res2 = tmpTbl[2]
   end
   
   if res1 == nil then res1 = '' end
   if res2 == nil then res2 = '' end
   
   return res1, res2
   
end

-----------------------------------------------------------------------------

function sqUtils.addMinutesToDate(strDate, mins)
   
   --[[
   Exmaple usage:
   Accepts a date string in the format "yyyyMMddHHmmss"
   dt = "20230704123000"
   --Add 5 minutes
	local res = addMinutesToDate(dt,5)
   --subtract 5 minutes
	local res = addMinutesToDate(dt,-5)
   --Add a day
	local res = addMinutesToDate(dt,60*24)
   ]]
   
   local year = tonumber(strDate:sub(1, 4))
   local month = tonumber(strDate:sub(5, 6))
   local day = tonumber(strDate:sub(7, 8))
   local hour = tonumber(strDate:sub(9, 10))
   local minute = tonumber(strDate:sub(11, 12))
   local second = tonumber(strDate:sub(13, 14))

   local timestamp = os.ts.time({
         year = year,
         month = month,
         day = day,
         hour = hour,
         min = minute,
         sec = second
      })
   
   local new_timestamp = timestamp + (mins * 60)
   local nt = os.ts.date("*t",new_timestamp)
   return nt.year..string.format("%02d",nt.month)..string.format("%02d",nt.day)..
           string.format("%02d",nt.hour)..string.format("%02d",nt.min)..string.format("%02d",nt.sec)
end

-----------------------------------------------------------------------------

function sqUtils.getEmailCredentials()
   
   local user = sqUtils.getJsonMappedValue(sqCfg.ack_email,'user')
   local pass = sqUtils.getJsonMappedValue(sqCfg.ack_email,'password')
   local server = sqUtils.getJsonMappedValue(sqCfg.ack_email,'server')
   trace(user,pass,server)
   return user, pass, server
end

-----------------------------------------------------------------------------

function sqUtils.getCurrentFunctionName()
   
    local level = 2 -- Adjust this level to match the desired function's call stack depth.
    local info = debug.getinfo(level, "n")
     
    return info and info.name or "unknown"
    
end

-----------------------------------------------------------------------------

function sqUtils.inTable(tbl, val)
   local found = false
   for i=1, #tbl do
      if tostring(val) == tbl[i] then
         return true
      end
   end
   return found
end

-----------------------------------------------------------------------------

function sqUtils.pCallRetry(func,...)
   local retry = 0

   repeat
      local Success, Er = pcall(func,...)
      if Er then print(Er) end
      if ( not Success) then
         if not sqUtils.isEmpty(Er) then
            print((sqUtils.isEmpty(Er.code) and '')..": "..(sqUtils.isEmpty(Er.message) and '').."\n\nRetrying...........")
         else
            print("\n\nRetrying...........")
         end
         if not iguana.isTest() then util.sleep(sqUtils.pCall_Secs*1000) end
      end
      retry = retry + 1
      if retry >= sqUtils.pCall_retryCount and not Success then 
         if Er ~= nill then 
            if sqUtils.isEmpty(Er) then Er = 'ERROR' end
            error(Er) 
         else
            error("Error retriggered")
         end
      end
   until Success or retry > sqUtils.pCall_retryCount
end

-----------------------------------------------------------------------------

-- returns a file's contents given a path
function sqUtils.readFile(file)
   local File = io.open(file, 'r')
   local FileContents = File:read('*a')
   File:close()
   
   return FileContents
   
end

-----------------------------------------------------------------------------

function sqUtils.getMRNbyAssigningFacility(PID, aFacility)
   for i=1, #PID[3] do
      if PID[3][i][5]:S() == aFacility then
         return PID[3][i][1]:S()
      end
   end
   return ''
end

-----------------------------------------------------------------------------

function sqUtils.getMRNbyAssigningFacilityPID34(PID, aFacility)
   --look to PID 3.4
   for i=1, #PID[3] do
      if PID[3][i][4]:S() == aFacility then
         return PID[3][i][1]:S()
      end
   end
   return ''
end


-----------------------------------------------------------------------------

function sqUtils.getMRNbyAssigningFacilityExt(PID, aFacility)
   
   --extended version checks for last effective date
   
   local mrn = ''
   local lastEffectiveDate = ''
   
   for i=1, #PID[3] do
      
      if PID[3][i][5]:S() == aFacility then
         
         local effectiveDate = PID[3][i][7]:S()
         local expiryDate = PID[3][i][8]:S()
         
         if effectiveDate >= lastEffectiveDate and expiryDate == '' then
            lastEffectiveDate = effectiveDate
            mrn = PID[3][i][1]:S()
         end
      
      end
   
   end
   
   return mrn

end

-----------------------------------------------------------------------------

function sqUtils.parseDOB(Data)
   if Data:nodeValue() == ''
	   then
      return ''
   else
      local T = dateparse.parse(Data:nodeValue())
      return os.date('%d/%m/%Y', T)
   end
end

-----------------------------------------------------------------------------

function sqUtils.parseDate(Data)
   if Data:nodeValue() == ''
	   then
      return ''
   else
      local T = dateparse.parse(Data:nodeValue())
      return os.date('%Y-%m-%d', T)
   end
end

-----------------------------------------------------------------------------

function sqUtils.parseDateTime(Data)
   if Data:nodeValue() == ''
	   then
      return ''
   else
      local T = dateparse.parse(Data:nodeValue())
      return os.date('%Y-%m-%d %H:%M:%S', T)
   end
end

-----------------------------------------------------------------------------

function sqUtils.parseDateNum(Data)
   if Data == '' then
      return ''
   else
      local T = dateparse.parse(Data)
      return os.date('%Y-%m-%d', T)
   end
end

-----------------------------------------------------------------------------

function sqUtils.parseDateDMY(Data)
   if Data:nodeValue() == ''
	   then
      return ''
   else
      local T = dateparse.parse(Data:nodeValue())
      return os.date('%d/%m/%Y', T)
   end
end

-----------------------------------------------------------------------------

--return the phone number from the PID based on type, SMS , L14. L15 etc
function sqUtils.getPhoneNumberByType(PID, pType)
   for i=1, #PID[13] do
      if PID[13][i][2]:S() == pType then
         return PID[13][i][1]:S()
      end
   end
   return ''
end

-----------------------------------------------------------------------------

--return the phone number from the PID based on type, SMS , L14. L15 etc
function sqUtils.getNKPhoneNumberByType(NK1, pType)
   if NK1:childCount() > 1 then
      for i=1, #NK1[5] do
         if NK1[5][i][2]:S() == pType then
            return NK1[5][i][1]:S()
         end
      end
   else
      if NK1[1][5][1][2]:S() == pType then
      return NK1[1][5][1][1]:S()
      end
   end  
   return ''
end

-----------------------------------------------------------------------------

--retrieve a mapped value from a json string
function sqUtils.getJsonMappedValue(jsonStr, val1, val2 )
	--NOTE: If you want to do a reverse find then set the val1 to ''
   --      It will then return the k rather than the value
   local res = nil
   local tbl = json.parse{data=jsonStr}
   for k, v in pairs(tbl) do
      trace(val1,val2,k,v)
      if val1 ~= '' then
         if k == val1 then
            return v
         end
      else
         trace(val2,v)
         if v == val2 or v == tostring(val2) then
            trace(k)
            return k
         end
      end
   end
   return ''
end

-----------------------------------------------------------------------------

function sqUtils.getJsonMappedValue2(str,val1,val2)
   
   --[[
   search for the json mapped value in a 2D json string
   example: 
   { 
   "v1": 
      {
        "a":"99"
      }, 
   "v2": 
      {
        "c":"22",
        "xx":"99"
      } 
   }
   ]]
   
   local res = nil
   local tbl = json.parse{data=str}
   for k, v in pairs(tbl) do
      trace(k,val1)
      if k == tostring(val1) then
         for k2, v2 in pairs(v) do
            trace(k2,val2)
            if k2 == tostring(val2) then
               res = v2
               return res
            end
         end
      end
   end
   return res
end

-----------------------------------------------------------------------------

function sqUtils.getJsonMappedValue2R(str,val)
   --reverse find in 2d json map
   --look for val and return the 2 keys that hold it
   local key1 = ''
   local key2 = ''
   local res = nil
   local tbl = json.parse{data=str}
   for k, v in pairs(tbl) do
      for k2, v2 in pairs(v) do
         trace(v2,val,k,k2)
         if v2 == tostring(val) then
            key1 = k
            key2 = k2
            return key1, key2
         end
      end

   end
   return key1, key2
end

-----------------------------------------------------------------------------

function sqUtils.jsonTableToString(tbl)
    local result = '{'
    for key, value in pairs(tbl) do
        result = result .. "'" .. tostring(key) .. "' :'" .. tostring(value) .. "', "
    end
   result = result .. '}'
    return result
end

-----------------------------------------------------------------------------

function sqUtils.messageTypeAllowed(str, val)
 
   local hl7Str = str
   local msgs_allowed = sqUtils.split(string.gsub(sqCfg.msg_types_allowed, "%s", ""),",")
   
   if sqUtils.inTable(msgs_allowed,val) then
      return true
   end
   return false
   
end

-----------------------------------------------------------------------------

function sqUtils.msgTypeAllowed(str, val)
 
   local hl7Str = str
trace(sqCfg.extra_params)
   local allowedTypes = sqUtils.getJsonMappedValue(sqCfg.extra_params,'msgTypesAllowed')
  
   local msgs_allowed = sqUtils.split(string.gsub(allowedTypes, "%s", ""),",")
   
   if sqUtils.inTable(msgs_allowed,val) then
      return true
   end
   return false
   
end

-----------------------------------------------------------------------------

function sqUtils.isIn(value, valueList)
    for _, v in ipairs(valueList) do
        if v == value then
            return true
        end
    end
    return false
end

-----------------------------------------------------------------------------

function sqUtils.getQueuedMessageCount()
   
   local thisChannel = iguana.channelName()
   local status = iguana.status()
   local res = xml.parse{data=status}
   local chCount = tonumber(res.IguanaStatus.NumberOfChannels:S())
   
   local chNum = 0
   for i = 1 , chCount do
      if res.IguanaStatus:child("Channel",i).Name:S() == thisChannel then
         chNum = i
         break
      end
   end
   
   return res.IguanaStatus:child("Channel", chNum).MessagesQueued:S()
   
end

-----------------------------------------------------------------------------

function sqUtils.getErrorMessageCount()
   
   local thisChannel = iguana.channelName()
   local status = iguana.status()
   local res = xml.parse{data=status}
   local chCount = tonumber(res.IguanaStatus.NumberOfChannels:S())
   
   local chNum = 0
   for i = 1 , chCount do
      if res.IguanaStatus:child("Channel",i).Name:S() == thisChannel then
         chNum = i
         break
      end
   end
   
   return res.IguanaStatus:child("Channel", chNum).TotalErrors:S()
   
end

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
---- DATEPARSE
-----------------------------------------------------------------------------

-- $Revision: 1.6 $
-- $Date: 2013-08-16 15:21:54 $

--
-- The dateparse module
-- Copyright (c) 2011-2012 iNTERFACEWARE Inc. ALL RIGHTS RESERVED
-- iNTERFACEWARE permits you to use, modify, and distribute this file in accordance
-- with the terms of the iNTERFACEWARE license agreement accompanying the software
-- in which it is used.
--

local wdays = { 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',
                'Saturday', 'Sunday' }

local months = { 'January', 'February', 'March', 'April', 'May', 'June', 'July',
                 'August', 'September', 'October', 'November', 'December' }

local wdays_by_name, months_by_name = {}
if true then
   local function index_by_name(array)
      local dict = {}
      for i,name in pairs(array) do
         name = name:lower()
         dict[name] = i
         dict[name:sub(1,3)] = i  -- Abbrev.
      end
      return dict
   end
   wdays_by_name  = index_by_name(wdays)
   months_by_name = index_by_name(months)
end

-- Validate week-day names and abbreviations.
local function lookup_wday(s)
   local wday = wdays_by_name[s:lower()]
   if not wday then error('expected week-day, got "'..s..'"') end
   return wday
end

-- Translate month names and abbreviations to numbers.  E.g., Jan -> 1.
local function lookup_month(s)
   local month = months_by_name[s:lower()]
   if not month then error('expected month, got "'..s..'"') end
   return month
end

-- If we find PM (or P), we need to adjust the hour that was read.
local function fix_hour(AM,PM)
   return function(s,d)
             if s:upper() == PM then
                if d.hour ~= 12 then d.hour = d.hour + 12 end
             elseif s:upper() == AM then
                if d.hour == 12 then d.hour = 0 end
             else
                error('expected '..AM..' or '..PM..', got "'..s..'"')
             end
          end
end

-- Time zone information can be parsed and stored in the date/time
-- table.  It is not used to adjust the time value returned.
local known_tzs = {
   ACDT='+10:30', ACST='+09:30', ACT ='+08:00', ADT  ='-03:00', AEDT ='+11:00',
   AEST='+10:00', AFT ='+04:30', AKDT='-08:00', AKST ='-09:00', AMST ='+05:00',
   AMT ='+04:00', ART ='-03:00', AST ='+03:00', AST  ='+04:00', AST  ='+03:00',
   AST ='-04:00', AWDT='+09:00', AWST='+08:00', AZOST='-01:00', AZT  ='+04:00',
   BDT ='+08:00', BIOT='+06:00', BIT ='-12:00', BOT  ='-04:00', BRT  ='-03:00',
   BST ='+06:00', BST ='+01:00', BTT ='+06:00', CAT  ='+02:00', CCT  ='+06:30',
   CDT ='-05:00', CEDT='+02:00', CEST='+02:00', CET  ='+01:00', CHAST='+12:45',
   CIST='-08:00', CKT ='-10:00', CLST='-03:00', CLT  ='-04:00', COST ='-04:00',
   COT ='-05:00', CST ='-06:00', CST ='+08:00', CVT  ='-01:00', CXT  ='+07:00',
   CHST='+10:00', DFT ='+01:00', EAST='-06:00', EAT  ='+03:00', ECT  ='-04:00',
   ECT ='-05:00', EDT ='-04:00', EEDT='+03:00', EEST ='+03:00', EET  ='+02:00',
   EST ='-05:00', FJT ='+12:00', FKST='-03:00', FKT  ='-04:00', GALT ='-06:00',
   GET ='+04:00', GFT ='-03:00', GILT='+12:00', GIT  ='-09:00', GMT  ='+00:00',
   GST ='-02:00', GYT ='-04:00', HADT='-09:00', HAST ='-10:00', HKT  ='+08:00',
   HMT ='+05:00', HST ='-10:00', IRKT='+08:00', IRST ='+03:30', IST  ='+05:30',
   IST ='+01:00', IST ='+02:00', JST ='+09:00', KRAT ='+07:00', KST  ='+09:00',
   LHST='+10:30', LINT='+14:00', MAGT='+11:00', MDT  ='-06:00', MIT  ='-09:30',
   MSD ='+04:00', MSK ='+03:00', MST ='+08:00', MST  ='-07:00', MST  ='+06:30',
   MUT ='+04:00', NDT ='-02:30', NFT ='+11:30', NPT  ='+05:45', NST  ='-03:30',
   NT  ='-03:30', OMST='+06:00', PDT ='-07:00', PETT ='+12:00', PHOT ='+13:00',
   PKT ='+05:00', PST ='-08:00', PST ='+08:00', RET  ='+04:00', SAMT ='+04:00',
   SAST='+02:00', SBT ='+11:00', SCT ='+04:00', SLT  ='+05:30', SST  ='-11:00',
   SST ='+08:00', TAHT='-10:00', THA ='+07:00', UTC  ='+00:00', UYST ='-02:00',
   UYT ='-03:00', VET ='-04:30', VLAT='+10:00', WAT  ='+01:00', WEDT ='+01:00',
   WEST='+01:00', WET ='+00:00', YAKT='+09:00', YEKT ='+05:00',
   -- US Millitary (for RFC-822)
   Z='+00:00', A='-01:00', M='-12:00', N='+01:00', Y='+12:00',
}

-- Compute the tz_offset in minute given (+/-)HH:MM or (+/-)HHMM.
local function parse_tz_offset(s,d)
   local sign, hour, min = s:match('([-+])(%d%d):?(%d%d)')
   d.tz = 'UTC'..s:gsub('([-+]%d%d):?(%d%d)', '%1:%2')
   return (d.tz_offset or 0) + (sign .. (hour*60 + min))
end

-- Set tz_offset given a time zone name.
local function parse_tz(s,d)
   local offset = known_tzs[s:upper()]
   if not offset then error('expected time zone, got "'..s..'"') end
   d.tz_offset = parse_tz_offset(offset,d)
   return s:upper()
end

-- HL7 timestamps can specify very accurate time values, up to one
-- tenth of a millisecond (four decimal points).
local function parse_sec_fraction(s)
   return tonumber('.'..s)
end

-- We do not want to pass the date table to tonumber(), just the string.
local function parseint(s)
   return tonumber(s)
end

-- The known date/time format codes.  The default action is
-- parseint(), since most values are just integers, exactly as we need
-- them.
local fmt_details = {
   yy   = { '%d%d', 'year',
            function(s)
               local year = tonumber(s) + 1900
               if year < 1969 then  -- POSIX
                  year = year + 100
               end
               return year
            end };
   yyyy = { '%d%d%d%d', 'year' };
   m    = { '%d+',    'month' };
   mm   = { '%d%d',   'month' };
   mmm  = { '%a%a%a', 'month', lookup_month }; -- Abbrev month name.
   mmmm = { '%a+',    'month', lookup_month }; -- Abbrev or full month.
   d    = { '%d+',  'day' };
   dd   = { '%d%d', 'day' };
   ddd  = { '%a%a%a', 'wday', lookup_wday };
   dddd = { '%a+',    'wday', lookup_wday };
   H    = { '%d+',  'hour' };
   HH   = { '%d%d', 'hour' };
   M    = { '%d+',  'min' };
   MM   = { '%d%d', 'min' };
   S    = { '%d+',  'sec' };
   SS   = { '%d%d', 'sec' };
   ssss = { '%d+',  'sec_fraction', parse_sec_fraction };
   t    = { '%a',   'A or P',   fix_hour('A', 'P' ) };
   tt   = { '%a%a', 'AM or PM', fix_hour('AM','PM') };
   zzzz = { '[-+]%d%d:?%d%d', 'tz_offset', parse_tz_offset };
   ZZZ  = { '%a+',            'tz',        parse_tz };
   [' '] = { '%s*', 'whitespace' }; -- Allow omission.
   [','] = { '%s*,?', 'a comma' };  -- Allow omission and leading whitespace.
   w     = { '%a+', 'a word' };     -- Value ignored.
   n     = { '%d+', 'a number' };   -- Value ignored.
}

-- Splits one part of a format string off; returns that and the rest.
local function split_fmt(fmt)
   local c = fmt:match('^(%a)')
   if c then
      return fmt:match('^('..c..'+)(.*)')
   elseif #fmt > 0 then
      return fmt:sub(1,1), fmt:sub(2)
   end
end

-- Parses the string, s, according to the format, fmt.
local function parse_date(s, fmt)
   local matched, d = '', {year=1969,day=1,month=1,hour=0,min=0,sec=0}
   local function fail(what, pattern)
      if pattern then what = what..' ('..pattern..')' end
      if matched ~= '' then what = what..' after "'..matched..'"' end
      error('expected '..what..', got "'..s..'"')
   end
   while fmt ~= '' do
      local head_fmt, rest_fmt = split_fmt(fmt)
      local pattern, field, fun = unpack(fmt_details[head_fmt] or {})
      local part, rest
      if pattern then
         part, rest = s:match('^('..pattern..')(.*)')
         if not part then fail(field,head_fmt) end
         d[field] = (fun or parseint)(part,d)
         matched = matched .. part
      elseif head_fmt:find('^%a') then
         error('unknown date/time pattern: '..head_fmt)
      elseif s:sub(1,#head_fmt) ~= head_fmt then
         fail('"'..head_fmt..'"')
      else
         matched = matched .. s:sub(1,#head_fmt)
         rest = s:sub(#head_fmt + 1)
      end
      s, fmt = rest, rest_fmt
   end
   if s ~= '' then fail('nothing') end
   return d
end

-- Expands all valid combinations of a string with optional areas
-- denoted by brackets.  E.g., "a[b]" expands to { "ab", "a" }.
local function expand_fmt(s, out)
   if not out then out = {} end
   local function add_fmt(s)
      local i, j = s:find('%b[]')
      if i then
         add_fmt(s:sub(1,i-1)..s:sub(i+1,j-1)..s:sub(j+1))
         add_fmt(s:sub(1,i-1)        ..        s:sub(j+1))
      else
         out[#out+1] = s
      end
   end
   add_fmt(s)
   return out
end

-- Date/time formats for the fuzzy parser.  These patterns must be
-- structurally unambiguous.  E.g., "mm/dd/yy" will match everything
-- that "yy/mm/dd" would, including "77/10/20".  This is intentional
-- as numerical differentiation is problematic.  E.g., which pattern
-- would match "02/03/04"?
local known_fmts = {}
if true then
   local templates = {
      -- HL7 Standard
      'yyyy[mm[dd[HHMM[SS[.ssss]]]]][zzzz]',
      -- Common formats (US)
      'm/d/yy[yy][ H:MM[:SS][ tt][ ZZZ]]',
      'yyyy-m-d[ w][ H:MM[:SS][ tt][ ZZZ]]',
      '[dddd, ]mmmm d[w], yyyy[ H:MM[:SS][ tt][ ZZZ]]',
      'H:MM[:SS][ tt][ ZZZ][, dddd], mmmm d[w], yyyy',
      -- Internet Standards (relaxed; RFC-822 and RFC-1123)
      '[dddd, ]d[w] mmmm yy[yy][ HH:MM[:SS][ tt][ ZZZ]]',
      '[ddd, ]dd mmm yy[yy] HH:MM:SS zzzz',
      -- The os.date('%c') Format
      '[dddd, ]mmmm dd, H:MM[:SS][ tt] yyyy',
      -- Other common formats
      'd-mmmm-yy[yy][ H:MM[:SS][ tt][ ZZZ]]',
   }
   for _,s in pairs(templates) do
      expand_fmt(s, known_fmts)
   end
end

-- The fuzzy date/time parser.  We try all the patterns we know, until
-- we find one that matches the given string.
local function fuzzy_parse(s)
   for _,fmt in pairs(known_fmts) do
      local ok, result = pcall(parse_date, s, fmt)
      if ok then
         return result
      end
   end
   error('unknown date/time: '..s, 3)
end

-- Once we find a matching date/time pattern, we just have to ensure
-- each value (e.g., minute) is in the allowed range.  If validation
-- fails, no other patterns are tried; see the note above as to why.
local function mktime(din,s)
   local t = os.time(din)
   if not t then
      error('invalid date/time: '..s)
   end
   local dout = os.date('*t',t)
   for _,k in pairs({'sec','min','hour','day','month','year'}) do
      if din[k] ~= dout[k] then
         error('invalid '..k..', '..din[k]..', in '..s, 3)
      end
   end
   for _,k in pairs({'tz','tz_offset','sec_fraction'}) do
      if din[k] then dout[k] = din[k] end
   end
   return t, dout
end

--
-- Public API
--

dateparse = {}
function dateparse.parse(s,fmt)
   if type(s) ~= 'string' or s == '' or s == 'NULL' then
      return nil
   end
   local function strip(s)
      return s:gsub('^%s+',''):gsub('%s+$',''):gsub('%s+',' ')
   end
   s = strip(s)
   if not fmt then
      return mktime(fuzzy_parse(s), s)
   end
   fmt = strip(fmt)
   local all_errors = {}
   for i,fmt in ipairs(expand_fmt(fmt)) do
      local ok, result = pcall(parse_date, s, fmt)
      if ok then return mktime(result,s) end
      local clean_message = result:match('^.-:.-:%s*(.*)') or result
      all_errors[i] = '"'..fmt..'" '..clean_message
   end
   error('"'..s..'" does not match:\n'..
         table.concat(all_errors, '\n'), 2)
end

-- Convert a time value to a database timestamp.  Automatically
-- detects the input format, unless you specify one (fmt).
--
function string:T(fmt)
   local t = dateparse.parse(self, fmt)
   return t and os.date('%Y-%m-%d %H:%M:%S', t)
end

-- Exactly like string:T(), except that it only produces a
-- date value for a database (i.e., time is 00:00:00).
--
function string:D(fmt)
   local t = dateparse.parse(self, fmt)
   return t and os.date('%Y-%m-%d 00:00:00', t)
end

-- Convert a time value to an HL7 timestamp.  Automatically
-- detects the input format, unless you specify one (fmt).
--
function string:TS(fmt)
   local t = dateparse.parse(self, fmt)
   return t and os.date('%Y%m%d%H%M%S', t)
end

-- Shortcuts for node values.
function node:T (fmt) return tostring(self):T (fmt) end
function node:D (fmt) return tostring(self):D (fmt) end
function node:TS(fmt) return tostring(self):TS(fmt) end


----------------------------------------------------------------------------
-- OpenLink - get Common items maps (every line has these (excluding subheader))
--- Inputs: items
--- Return: items
function sqUtils.openLinkGetCommonMaps(items, nocase)
	-- ItemName = StartPosition, EndPosition, Active
   ---items['NetworkHeader']          ={1,16,true}
	-- Header items
	items['DAT']                        = { 1, 3,true}
   items['BufferSequenceNumber']       = { 4, 6,true}
   items['MessageSequenceNumber']      = { 7, 9,true}
   items['LastNotLast']                = {10,12,true}
   items['ByteCount']                  = {13,16,true}
   items['TransactionType']            = {17,18,true}
   items['SequenceNumber']             = {19,19,true}
   if ( nocase ~= true ) then 
      items['CaseNo']                  = {20,21,true}
   end
   items = sqUtils.openLinkGetSubHeaderMaps(items)
	return items
end
   
----------------------------------------------------------------------------
-- OpenLink - get Sub Header maps
--- Inputs: items
--- Return: items
function sqUtils.openLinkGetSubHeaderMaps(items)
   --- Not common, this are subheader
   items['SUB,Space']                   = {21,24,true}
   items['SUB,100']                     = {25,27,true}
   items['SUB,RouteCode']               = {28,29,true}
   items['SUB,Zero']                    = {30,30,true}
   
   return items
end

----------------------------------------------------------------------------
-- OpenLink - Transaction type specific items
--- Inputs: items, singleItem, version (i.e, AP=7, PR=8) , transaction type
--- Return: items, singleItem
function sqUtils.openLinkGetMaps(items, singleItem, version, transactionType)
   local versionT, tab, dynamic, found = {}, {}, {}, nil
	if version == nil then version = '' end
   trace(items)
   -- Number of sections are version dependent (These are current highest 11/11/2024) - can override with version config item
   tab["AP"] = 9  -- Outpatient Appointment (from PC)
   tab["AT"] = 12 -- Outpatient Appointment Records (From PC)
   tab["PR"] = 10 -- PMI Registration Details
   tab["MP"] = 6  -- PMI Merge Patient
   tab["OC"] = 9  -- Outpatient Attendance Cancellation
   tab["PB"] = 8  -- Patient Basic Details
   tab["AD"] = 1  -- Inpatient Admission
   tab["IP"] = 1  -- Integration Kiosk
   tab["PD"] = 2  -- Patient Update Record
   tab["SS"] = 1  -- Clinic Session Details (from PC)
   dynamic["SS"] = 2 -- SS has dynamic sessions after 1!
   tab["SC"] = 1  -- Appointment type outbound (from PC)
   tab["IS"] = 1  -- Book OP appointment (SQ to PC)
   tab["SN"] = 1  -- Cancel/Reinstant OP appointment (SQ to PC)
   
   -- If version contains version for specific sections, set them
   -- i.e. PR=8,AB=7
   versionT = sqUtils.splitStringSubValue(version, ",", "=")
   if #versionT>0 then
      for k,v in pairs(tab) do
         found = versionT[k]
         if found ~= nil then
            tab[k] = found
         end
      end
   end
  
   if tab[transactionType] ~= nil then
      -- All segments numbers are +1 for the DAT location
      items, singleItem = sqUtils.openLinkGetMap(items, singleItem, transactionType, tab[transactionType], dynamic[transactionType])
      
   else
      sqUtils.printLog("No openlink mappings for "..transactionType)
   end
   return items, singleItem, tab
end

----------------------------------------------------------------------------
-- Split string to table twice (i.e. AP=7, PB=8 will return [AP]=7, [PB]=8 etc)
--- Inputs: pString, pSplit subscript divider, pSplit2 value divider
--- Return: oTable
function sqUtils.splitStringSubValue(pString, pSplit, pSplit2)
   local oTable, oTable2, k, v, oSplit = sqUtils.splitString(pString, pSplit), {}
   for k,v in pairs(oTable) do
      oSplit = sqUtils.split(v, pSplit2)
      oTable2[oSplit[1]] = oSplit[2]
   end
   return oTable2
end

----------------------------------------------------------------------------
-- Split string to table
--- Inputs: pString, pPattern
--- Return: oTable
function sqUtils.splitString(pString, pPattern)
   local oTable = {}
   local fpat = "(.-)" .. pPattern
   local last_end = 1
   local s, e, cap = pString:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(oTable, cap)
      end
      last_end = e+1
      s, e, cap = pString:find(fpat, last_end)
   end
   if last_end <= #pString then
      cap = pString:sub(last_end)
      table.insert(oTable, cap)
   end
   return oTable
end
----------------------------------------------------------------------------
-- OpenLink - call Open Link Get
--- Inputs: transaction type, segment, items
--- Return: items
function sqUtils.callOpenLinkGet(transactionType, segment, items)
   local func
   -- Build function name
   func = "openLinkGet"..transactionType.."Segment"..segment
   trace(func)
   -- Common across every map
   -- Incase we ever need to compare between DAT blocks headers
   ----------------------------------------------------------={Start,End,Active}
   items[transactionType .. segment .. ',NetworkHeader']           = {1,16,true}
   items[transactionType .. segment .. ',TransactionType']         = {17,18,true}
   items[transactionType .. segment .. ',SequenceNumber']          = {19,19,true}
   --- Only call if exists
   if sqUtils[func] then
      items = sqUtils[func](items)
      trace(items)
   else
      sqUtils.printLog("Calling Function "..func.." but doesn't exist")
   end
   return items
end

----------------------------------------------------------------------------
-- Openlink - Get all openlink maps for transaction type from 1 to MaxSegments
--- Inputs: items, singleItem, transaction type, maxsegments, dynamicStart (start section of dynamic)
--- Return: items, singleItem
function sqUtils.openLinkGetMap(items, singleItem, transactionType, maxSegments, dynamicStart)
   local processSingle, i, func, s, e, item, current = true, 1

   if dynamicStart == nil then dynamicStart = 0 end
   
   --- Walk upto max segments (excluding dynamic)
   while i < (maxSegments+1) do
	   items = sqUtils.callOpenLinkGet(transactionType, i, items)
      i = i+1
   end

   --- Has dynamic section ("N")
   if dynamicStart>0 then
	   items = sqUtils.callOpenLinkGet(transactionType, "N", items)
      processSingle = false -- Can't have singles as will all be the same name! (or do we? TODO)
   end
   
   --- Create singleItem array (for quick lookup, or any segment lookup)
	if processSingle == true then
      for k,v in pairs(items) do
         trace(k)
         trace(v)
         s, e = string.find(k, ",")
         if s ~= nil then
            item = string.sub(k, s+1, #k)           
            if #item > 0 then
               current = singleItem[item]
               if ( current == nil ) then
                  current = ""
               else
                  current = current..","
               end
               singleItem[item] = current..string.sub( k, 1, s-1 )
            end
         end
      end
   end
   return items, singleItem
end

----------------------------------------------------------------------------
--- openLink - Common Patient Information Segment
--- Inputs: items, tType
--- Return: items
function sqUtils.openLinkGetPatientInfoSegment(items, tType)
   ---         1         2         3         4         5         6         7
   ---1234567890123456789012345678901234567890123456789012345678901234567890
   ---DAT005015NLS0269AP402                              0        4000000000       01                             COE M                                                                                W                                                                           
   local lItems, pos = {}, 1
   lItems[ pos ], pos = { 'CaseNo', 2, true},               pos + 1
   lItems[ pos ], pos = { 'AddressComments', 30, true},     pos + 1
   lItems[ pos ], pos = { 'DeathIndicator', 1, true},       pos + 1
   lItems[ pos ], pos = { 'DateOfDeath', 8, true},          pos + 1
   lItems[ pos ], pos = { 'NHSNumber', 17, true},           pos + 1
   lItems[ pos ], pos = { 'NHSStatusCode', 2, true},        pos + 1
   lItems[ pos ], pos = { 'WorkTelephoneNumber', 23, true}, pos + 1
   lItems[ pos ], pos = { 'GDPCode', 6, true},              pos + 1
   lItems[ pos ], pos = { 'ReligionCode', 4, true},         pos + 1
   lItems[ pos ], pos = { 'MaritalStatusCode', 1, true},    pos + 1
   lItems[ pos ], pos = { 'PlaceOfBirth', 20, true},        pos + 1
   lItems[ pos ], pos = { 'Occupation', 20, true},          pos + 1
   lItems[ pos ], pos = { 'School', 20, true},              pos + 1
   lItems[ pos ], pos = { 'OccupationSpouse', 20, true},    pos + 1
   lItems[ pos ], pos = { 'EthnicGroupCode', 4, true},      pos + 1
   lItems[ pos ], pos = { 'PatientBirthName', 24, true},    pos + 1
   lItems[ pos ], pos = { 'FirstPreviousName', 24, true},   pos + 1
   lItems[ pos ], pos = { 'SecondPreviousName', 24, true},  pos + 1
   return sqUtils.openLinkLengthToPos( tType, 20, items, lItems, extra )
end

----------------------------------------------------------------------------
--- openLink - Common Demographics Segment
--- Inputs: items (current items maps), tType (Transaction Type)
--- Return: items (current and new items map)
function sqUtils.openLinkGetDemographicsSegment(items, tType, extra)
   local lItems, pos = {}, 1
   lItems[ pos ], pos = { 'CaseNo', 2, true},                pos + 1
   lItems[ pos ], pos = { 'InternalPatientNumber', 9, true}, pos + 1
   lItems[ pos ], pos = { 'DistrictNumber', 14, true},       pos + 1
   lItems[ pos ], pos = { 'Surname', 24, true},              pos + 1
   lItems[ pos ], pos = { 'Forenames', 20, true},            pos + 1
   lItems[ pos ], pos = { 'DateOfBirth', 8, true},           pos + 1
   lItems[ pos ], pos = { 'Sex', 1, true},                   pos + 1
   lItems[ pos ], pos = { 'Title', 5, true},                 pos + 1
   lItems[ pos ], pos = { 'HomeTelephoneNumber', 23, true},  pos + 1
   lItems[ pos ], pos = { 'PatientAddressLine1', 20, true},  pos + 1
   lItems[ pos ], pos = { 'PatientAddressLine2', 20, true},  pos + 1
   lItems[ pos ], pos = { 'PatientAddressLine3', 20, true},  pos + 1
   lItems[ pos ], pos = { 'PatientAddressLine4', 20, true},  pos + 1
   lItems[ pos ], pos = { 'PatientPostCode', 10, true},      pos + 1
   return sqUtils.openLinkLengthToPos( tType, 20, items, lItems, extra )
end

----------------------------------------------------------------------------
--- openLink - Common NOK Segment
--- Inputs: items (current items maps), tType (Transaction Type)
--- Return: items (current and new items map)
function sqUtils.openLinkGetNOKSegment(items, tType)
   local lItems, pos = {}, 1
   lItems[ pos ], pos = { 'CaseNo', 2, true},                    pos + 1
   lItems[ pos ], pos = { 'ThirdPreviousName', 24, true},        pos + 1
   lItems[ pos ], pos = { 'NextOfKinName', 30, true},            pos + 1
   lItems[ pos ], pos = { 'NOKRelationshipCode', 9, true},       pos + 1
   lItems[ pos ], pos = { 'NOKAddressLine1', 20, true},          pos + 1
   lItems[ pos ], pos = { 'NOKAddressLine2', 20, true},          pos + 1
   lItems[ pos ], pos = { 'NOKAddressLine3', 20, true},          pos + 1
   lItems[ pos ], pos = { 'NOKAddressLine4', 20, true},          pos + 1
   lItems[ pos ], pos = { 'NOKAddressLine5', 20, true},          pos + 1
   lItems[ pos ], pos = { 'NOKPostCode', 10, true},              pos + 1
   lItems[ pos ], pos = { 'NOKTelephoneHome', 23, true},         pos + 1
   lItems[ pos ], pos = { 'NOKTelephoneWork', 23, true},         pos + 1
   lItems[ pos ], pos = { 'NOKComments', 30, true},              pos + 1
   lItems[ pos ], pos = { 'AlternativePatientNumber', 18, true}, pos + 1
   return sqUtils.openLinkLengthToPos( tType, 20, items, lItems, extra )
end

----------------------------------------------------------------------------
--- openLink - Common GP Segment
--- Inputs: items (current items maps), tType (Transaction Type)
--- Return: items (current and new items map)
function sqUtils.openLinkGetGPSegment(items, tType, extra)
   local lItems, pos = {}, 1
   lItems[ pos ], pos = { 'CaseNo', 2, true},           pos + 1
   lItems[ pos ], pos = { 'GPCode', 6, true},           pos + 1
   lItems[ pos ], pos = { 'GPSurname', 24, true},       pos + 1
   lItems[ pos ], pos = { 'GPInitials', 4, true},       pos + 1
   lItems[ pos ], pos = { 'GPTitle', 4, true},          pos + 1
   lItems[ pos ], pos = { 'GPHAACode', 6, true},        pos + 1
   lItems[ pos ], pos = { 'GPNationalCode', 8, true},   pos + 1
   lItems[ pos ], pos = { 'GPAddressLine1', 20, true},  pos + 1
   lItems[ pos ], pos = { 'GPAddressLine2', 20, true},  pos + 1
   lItems[ pos ], pos = { 'GPAddressLine3', 20, true},  pos + 1
   lItems[ pos ], pos = { 'GPAddressLine4', 20, true},  pos + 1
   lItems[ pos ], pos = { 'GPPostCode', 10, true},      pos + 1
   lItems[ pos ], pos = { 'GPTelephone', 23, true},     pos + 1
   lItems[ pos ], pos = { 'GPDOHCode', 8, true},        pos + 1
   lItems[ pos ], pos = { 'GPPracticeCode', 6, true},   pos + 1
   lItems[ pos ], pos = { 'GPFundHolderCode', 5, true}, pos + 1
   return sqUtils.openLinkLengthToPos( tType, 20, items, lItems, extra )
end

----------------------------------------------------------------------------
--- openLink - Common GP with MRSA Segment
--- Inputs: items (current items maps), tType (Transaction Type)
--- Return: items (current and new items map)
function sqUtils.openLinkGetGPwithMRSASegment(items, tType, extra)
   if extra == nil then extra = {} end
   extra[ 'MRSA' ]     = {3, true}
   extra[ 'MRSADate' ] = {8, true}
	return sqUtils.openLinkGetGPSegment( items, tType, extra)
end
   
----------------------------------------------------------------------------
--- openLink - Common PMI Segment
--- Inputs: items (current items maps), tType (Transaction Type)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPMISegment(items, tType, extra)
   local lItems, pos = {}, 1
   lItems[ pos ], pos = { 'CaseNo', 2, true },                               pos + 1
   lItems[ pos ], pos = { 'PMIPatientAlternateNumber', 18, true },           pos + 1
   lItems[ pos ], pos = { 'PMIPatientPreviousSurname1', 24, true },          pos + 1
   lItems[ pos ], pos = { 'PMIPatientPreviousSurname2', 24, true },          pos + 1
   lItems[ pos ], pos = { 'PMIPatientPreviousSurname3', 24, true },          pos + 1
   lItems[ pos ], pos = { 'WorkTelephoneNumber2', 23, true },                pos + 1
   lItems[ pos ], pos = { 'GPUserID', 4, true },                             pos + 1   
   -- GPXGPUsername should be 30, is 18 only in PB transaction
   if (tType:sub(0,2):upper()=='PB') then      
      lItems[ pos ], pos = { 'GPXGPUsername', 18, true },                       pos + 1
   else
      lItems[ pos ], pos = { 'GPXGPUsername', 30, true },                       pos + 1
   end   
   lItems[ pos ], pos = { 'PatientsEmailAddress', 60, true },                pos + 1
   lItems[ pos ], pos = { 'ConsentToCommunicateViaMessage', 1, true },       pos + 1
   lItems[ pos ], pos = { 'ConsentToCommunicateViaEmail', 1, true },         pos + 1
   lItems[ pos ], pos = { 'ConsentToCommunicateShareInformation', 1, true }, pos + 1
   return sqUtils.openLinkLengthToPos( tType, 20, items, lItems, extra )
end

----------------------------------------------------------------------------
--- openLink - Common Extended Address Segment
--- Inputs: items (current items maps), tType (Transaction Type)
--- Return: items (current and new items map)
function sqUtils.openLinkGetExtendedAddressSegment(items, tType)
	-- ItemName = StartPosition, EndPosition, Active (for future version, easy disable)
   items[ tType..',CaseNo' ]                                  = { 20, 21,true}
   items[ tType..',ExtendedAddressLine1' ]                    = { 22, 56,true}
   items[ tType..',ExtendedAddressLine2' ]                    = { 57, 91,true}
   items[ tType..',ExtendedAddressLine3' ]                    = { 92,126,true}
   items[ tType..',ExtendedAddressLine4' ]                    = {127,151,true}
   items[ tType..',ExtendedAddressLine5' ]                    = {152,184,true}
   items[ tType..',ExtendedPostCode' ]                        = {185,194,true}
   items[ tType..',PrimaryPatientIdenfierNumberExtended' ]    = {195,204,true}
   items[ tType..',MobileTelephoneNumber' ]                   = {221,238,true}
   
   return items
end

----------------------------------------------------------------------------
--- openLink - Common Clinic Segment
--- Inputs: items (current items maps), tType (Transaction Type)
--- Return: items (current and new items map)
function sqUtils.openLinkGetClinicSegment(items, tType)
	-- ItemName = StartPosition, EndPosition, Active (for future version, easy disable)
   items[ tType..',CaseNo' ]                 = {20,21,true}
   items[ tType..',ClinicCode' ]             = {22,29,true}
   items[ tType..',ClinicConsCode' ]         = {30,35,true}
   items[ tType..',ClinicDescription' ]      = {36,55,true}
   items[ tType..',ClinicHospitalCode' ]     = {56,59,true}
   items[ tType..',ClinicSpecialtyCode' ]    = {60,63,true}
   items[ tType..',ReportToLocation' ]       = {64,67,true}
   items[ tType..',LocationDescription' ]    = {68,87,true}
   return items
end

----------------------------------------------------------------------------
--- openLink - Common Consultant Segment
--- Inputs: items (current items maps), tType (Transaction Type)
--- Return: items (current and new items map)
function sqUtils.openLinkGetConsultantSegment(items, tType)
	-- ItemName = StartPosition, EndPosition, Active (for future version, easy disable)
   items[ tType..',CaseNo' ]                        = { 20, 21,true}
   items[ tType..',ConsultantCode' ]                = { 22, 27,true}
   items[ tType..',ConsultantCode2' ]               = { 28, 31,true}
   items[ tType..',SpecialtyCode' ]                 = { 32, 35,true}
   items[ tType..',Deleted' ]                       = { 36, 36,true}
   items[ tType..',ExternalConsultant' ]            = { 37, 39,true}
   items[ tType..',ForenamePrefix' ]                = { 40, 43,true}
   items[ tType..',GMCCode' ]                       = { 44, 51,true}
   items[ tType..',Initials' ]                      = { 52, 57,true}
   items[ tType..',KornerCode' ]                    = { 58, 60,true}
   items[ tType..',Midwife' ]                       = { 61, 63,true}
   items[ tType..',ConsultantCodeConcatenated' ]    = { 64, 93,true}
   items[ tType..',PrimarySpecialtyCode' ]          = { 94, 97,true}
   items[ tType..',ConsultantProviderCode' ]        = { 98,101,true}
   items[ tType..',Surname' ]                       = {102,125,true}
   return items
end

----------------------------------------------------------------------------
-- openLink - AP Segment 1
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetAPSegment1(items)
	-- ItemName = StartPosition, EndPosition, Active (for future version, easy disable)
   items[ 'AP1,CaseNo' ]                    = { 20, 21,true}
   items[ 'AP1,InternalPatientNumber' ]     = { 22, 30,true}
   items[ 'AP1,DistrictNumber' ]            = { 31, 40,true}
   items[ 'AP1,CaseNoteNumber' ]            = { 41, 54,true}
   items[ 'AP1,EpisodeNumber' ]             = { 55, 63,true}
   items[ 'AP1,AccountNumber' ]             = { 64, 72,true}
   items[ 'AP1,ClinicCode' ]                = { 73, 80,true}
   items[ 'AP1,DoctorCode' ]                = { 81, 88,true}
   items[ 'AP1,AppointmentDateTime' ]       = { 89,100,true}
   items[ 'AP1,AppointmentTypeCode' ]       = {101,103,true}
   items[ 'AP1,TransportCode' ]             = {104,105,true}
   items[ 'AP1,Comment' ]                   = {106,130,true}
   items[ 'AP1,CategoryCode' ]              = {131,133,true}
   items[ 'AP1,BookedDate' ]                = {134,145,true}
   items[ 'AP1,PurchaserReferenceNumber' ]  = {146,155,true}
   items[ 'AP1,WaitingGuaranteeException' ] = {156,157,true}
   items[ 'AP1,BookedAdmissionsFlag' ]      = {158,161,true}
   items[ 'AP1,PatientComment' ]            = {172,232,true}   
   items[ 'AP1,CABRef' ]                    = {222,251,true}
   return items
end
  
----------------------------------------------------------------------------
-- openLink - AP Segment 2
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetAPSegment2(items)
	-- ItemName = StartPosition, EndPosition, Active (for future version, easy disable)
   items[ 'AP2,CaseNo' ]                    = {20,21,true}
   items[ 'AP2,DROC' ]                      = {22,23,true}
   items[ 'AP2,EROD' ]                      = {24,31,true}
   items[ 'AP2,PatientPostCode' ]           = {32,39,true}
   items[ 'AP2,HospitalPostCode' ]          = {40,47,true}
   items[ 'AP2,TransportMode' ]             = {48,62,true}
   items[ 'AP2,JourneyWait' ]               = {63,64,true}
   items[ 'AP2,TimeLocateReception' ]       = {65,67,true}
   items[ 'AP2,MultipleRTTAFDT' ]           = {68,68,true}
   items[ 'AP2,ConsGMC' ]                   = {69,76,true}
   return items
end

----------------------------------------------------------------------------
-- openLink - AP Segment 3
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetAPSegment3(items)
   items=sqUtils.openLinkGetDemographicsSegment( items, "AP3" )
   items[ 'AP3,DHAorHA' ]                   = {216,218,true}
	return items
end

----------------------------------------------------------------------------
-- openLink - AP Segment 4
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetAPSegment4(items)
   return sqUtils.openLinkGetPatientInfoSegment( items, "AP4" )
end

----------------------------------------------------------------------------
-- openLink - AP Segment 5
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetAPSegment5(items)
   return sqUtils.openLinkGetNOKSegment( items, "AP5" )
end

----------------------------------------------------------------------------
-- openLink - AP Segment 6
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetAPSegment6(items)
   items=sqUtils.openLinkGetGPwithMRSASegment( items, "AP6" )
	return items
end

----------------------------------------------------------------------------
-- openLink - AP Segment 7
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetAPSegment7(items)
   return sqUtils.openLinkGetClinicSegment( items, "AP7" )
end

----------------------------------------------------------------------------
-- openLink - AP Segment 8
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetAPSegment8(items)
   return sqUtils.openLinkGetPMISegment( items, "AP8" )
end

----------------------------------------------------------------------------
-- openLink - AP Segment 9
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetAPSegment9(items)
   items=sqUtils.openLinkGetExtendedAddressSegment( items, "AP9" )
   items[ 'AP9,PatientCurrentGenderExtended' ]            = {229,242,true}
   items[ 'AP9,PatientCurrentGender' ]                    = {243,243,true}
	return items
end

----------------------------------------------------------------------------
-- openLink - AT Segment 1
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetATSegment1(items)
	-- ItemName = StartPosition, EndPosition, Active (for future version, easy disable)
   items[ 'AT1,CaseNo' ]                      = { 20, 21,true}
   items[ 'AT1,EpisodeNumber' ]               = { 22, 30,true}
   items[ 'AT1,AccountNumber' ]               = { 31, 40,true}
   items[ 'AT1,ClinicCode' ]                  = { 41, 47,true}
   items[ 'AT1,DoctorCode' ]                  = { 48, 55,true}
   items[ 'AT1,AppointmentDateTime' ]         = { 56, 67,true}
   items[ 'AT1,AppointmentTypeCode' ]         = { 68, 70,true}
   items[ 'AT1,AttendanceStatus' ]            = { 71, 73,true}
   items[ 'AT1,DisposalCode' ]                = { 74, 77,true}
   items[ 'AT1,DNAFollowUpComment' ]          = { 78,107,true}
   items[ 'AT1,FollowUpAction' ]              = {108,108,true}
   items[ 'AT1,GradeOfStaff' ]                = {109,112,true}
   items[ 'AT1,DerivedAttendanceDateTime' ]   = {113,124,true}
   return items
end

----------------------------------------------------------------------------
-- openLink - AT Segment 2
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetATSegment2(items)
	-- ItemName = StartPosition, EndPosition, Active (for future version, easy disable)
   items[ 'AT2,CaseNo' ]                      = {20,21,true}
   items[ 'AT2,ProviderDOHCode' ]             = {22,26,true}
   items[ 'AT2,RTTPathwayID' ]                = {27,46,true}
   items[ 'AT2,RTTPathwayStartDate' ]         = {47,54,true}
   items[ 'AT2,RTTPeriodStatus' ]             = {55,57,true}
   items[ 'AT2,AttendanceCodeExtended' ]      = {58,61,true}
   return items
end

----------------------------------------------------------------------------
-- openLink - AT Segment 3
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetATSegment3(items)
   return sqUtils.openLinkGetDemographicsSegment( items, "AT3" )
end

----------------------------------------------------------------------------
-- openLink - AT Segment 4
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetATSegment4(items)
   return sqUtils.openLinkGetPatientInfoSegment( items, "AT4" )
end

----------------------------------------------------------------------------
-- openLink - AT Segment 5
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetATSegment5(items)
   return sqUtils.openLinkGetNOKSegment( items, "AT5" )
end

----------------------------------------------------------------------------
-- openLink - AT Segment 6
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetATSegment6(items)
   return sqUtils.openLinkGetGPwithMRSASegment( items, "AT6" )
end

----------------------------------------------------------------------------
-- openLink - AT Segment 7
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetATSegment7(items)
   items[ 'AT7,CaseNo' ]                      = { 20, 21,true}
   items[ 'AT7,AFirstRegDayNightAdmission' ]  = { 22, 24,true}
   items[ 'AT7,AccidentCode' ]                = { 25, 25,true}
   items[ 'AT7,AdmissionReason' ]             = { 26, 85,true}
   items[ 'AT7,Ward' ]                        = { 86, 89,true}
   items[ 'AT7,AdmissionDateTime' ]           = { 90,101,true}
   items[ 'AT7,Bed' ]                         = {102,105,true}
   items[ 'AT7,BenefitCode' ]                 = {106,106,true}
   items[ 'AT7,BR409Required' ]               = {107,107,true}
   items[ 'AT7,CaseNoteNumber' ]              = {108,121,true}
   items[ 'AT7,Category' ]                    = {122,124,true}
   items[ 'AT7,Comment' ]                     = {125,154,true}
   items[ 'AT7,ConsultantCode' ]              = {155,160,true}
   items[ 'AT7,BR409SentOn' ]                 = {161,168,true}
   items[ 'AT7,DateOnWaitingList' ]           = {169,176,true}
   items[ 'AT7,DecisionToRefer' ]             = {177,184,true}
   items[ 'AT7,DestinationOnDischarge' ]      = {185,186,true}
   items[ 'AT7,DischargeDate' ]               = {187,198,true}
   items[ 'AT7,DistrictNumber' ]              = {199,212,true}
   items[ 'AT7,EpisodeGPCode' ]               = {213,220,true}
   items[ 'AT7,EpisodeGPPracticeCode' ]       = {221,226,true}
   items[ 'AT7,EpisodeNumber' ]               = {227,235,true}
   items[ 'AT7,EpisodeCurrentStatus' ]        = {236,246,true}
   items[ 'AT7,ExpectedLengthOfStay' ]        = {247,249,true}
   return items
end

----------------------------------------------------------------------------
-- openLink - AT Segment 8
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetATSegment8(items)
   items[ 'AT8,CaseNo' ]                      = { 20, 21,true}
   items[ 'AT8,HospitalCode' ]                = { 22, 25,true}
   items[ 'AT8,IntendedManagement' ]          = { 26, 26,true}
   items[ 'AT8,InternalPatientNumber' ]       = { 27, 35,true}
   items[ 'AT8,JointConsultantCode1' ]        = { 36, 41,true}
   items[ 'AT8,JointSpecialty1' ]             = { 42, 45,true}
   items[ 'AT8,JointConsultantCode2' ]        = { 46, 51,true}
   items[ 'AT8,JointSpecialty2' ]             = { 52, 55,true}
   items[ 'AT8,JointConsultantCode3' ]        = { 56, 61,true}
   items[ 'AT8,JointSpecialty3' ]             = { 62, 65,true}
   items[ 'AT8,JointConsultantCode4' ]        = { 66, 71,true}
   items[ 'AT8,JointSpecialty4' ]             = { 72, 75,true}
   items[ 'AT8,LiveStillBirthIndicator' ]     = { 76, 76,true}
   items[ 'AT8,Lodger' ]                      = { 77, 79,true}
   items[ 'AT8,MethodOfAdmission' ]           = { 80, 81,true}
   items[ 'AT8,MethodOfDischarge' ]           = { 82, 83,true}
   items[ 'AT8,Operation' ]                   = { 84,203,true}
   items[ 'AT8,OSVStatus' ]                   = {204,204,true}
   items[ 'AT8,OutlierIndicator' ]            = {205,205,true}
   items[ 'AT8,ReasonForReferral' ]           = {206,209,true}
   items[ 'AT8,ReferralCode' ]                = {210,210,true}
   items[ 'AT8,Room' ]                        = {211,214,true}
   items[ 'AT8,SourceOfAdmission' ]           = {215,216,true}
   items[ 'AT8,Specialty' ]                   = {217,220,true}
   items[ 'AT8,TheatreTime' ]                 = {221,230,true}
   items[ 'AT8,TransferFrom' ]                = {231,243,true}
   items[ 'AT8,TransferTo' ]                  = {244,257,true}
   items[ 'AT8,PatientClassification' ]       = {258,258,true}
   return items
end

----------------------------------------------------------------------------
-- openLink - AT Segment 9
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetATSegment9(items)
   return sqUtils.openLinkGetClinicSegment( items, "AT9" )
end
----------------------------------------------------------------------------
-- openLink - AT Segment 10
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetATSegment10(items)
   return sqUtils.openLinkGetConsultantSegment( items, "AT10" )
end

----------------------------------------------------------------------------
-- openLink - AT Segment 11
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetATSegment11(items)
   return sqUtils.openLinkGetPMISegment( items, "AT11" )
end

----------------------------------------------------------------------------
-- openLink - AT Segment 12
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetATSegment12(items)
   items = sqUtils.openLinkGetExtendedAddressSegment( items, "AT12" )
   items[ 'AT12,PatientCurrentGender' ]         = {229,242,true}
   items[ 'AT12,PatientCurrentGenderExternal' ] = {243,243,true}
	return items
end

----------------------------------------------------------------------------
-- openLink - MP Segment 1
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetMPSegment1(items)
	-- ItemName = StartPosition, EndPosition, Active (for future version, easy disable)
   items[ 'MP1,CaseNo' ]                    = {20,21,true}
   items[ 'MP1,OldInternalPatientNumber' ]  = {22,30,true}
   items[ 'MP1,NewInternalPatientNumber' ]  = {31,39,true}
   items[ 'MP1,OldDistrictNumber' ]         = {40,49,true}
   items[ 'MP1,NewDistrictNumber' ]         = {50,59,true}
   items[ 'MP1,OldExtendedDistrictNumber' ] = {60,73,true}
   items[ 'MP1,NewExtendedDistrictNumber' ] = {74,87,true}
   return items
end

----------------------------------------------------------------------------
-- openLink - MP Segment 2
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetMPSegment2(items)
   return sqUtils.openLinkGetDemographicsSegment( items, "MP2" )
end

----------------------------------------------------------------------------
-- openLink - MP Segment 3
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetMPSegment3(items)
   return sqUtils.openLinkGetPatientInfoSegment( items, "MP3" )
end

----------------------------------------------------------------------------
-- openLink - MP Segment 4
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetMPSegment4(items)
   return sqUtils.openLinkGetNOKSegment( items, "MP4" )
end

----------------------------------------------------------------------------
-- openLink - MP Segment 5
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetMPSegment5(items)
   return sqUtils.openLinkGetGPwithMRSASegment( items, "MP5" )
end

----------------------------------------------------------------------------
-- openLink - MP Segment 6
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetMPSegment6(items)
   return sqUtils.openLinkGetPMISegment( items, "MP6" )
end

----------------------------------------------------------------------------
-- openLink - PB Segment 1
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPBSegment1(items)
   local lItems = {}
   lItems[  1 ]   = { 'CaseNo', 2, true }
   lItems[  2 ]   = { 'InternalPatientNumber', 9, true } 
   lItems[  3 ]   = { 'PrimaryPatientIdentifierNumber', 10, true } 
   lItems[  4 ]   = { 'Surname', 24, true }
   lItems[  5 ]   = { 'Forenames', 20, true }
   lItems[  6 ]   = { 'DateOfBirth', 8, true }
   lItems[  7 ]   = { 'Sex', 1, true }
   lItems[  8 ]   = { 'Title', 5, true }
   lItems[  9 ]   = { 'HomeTelephoneNumber', 23, true }
   lItems[ 10 ]   = { 'PatientAddressLine1', 20, true }
   lItems[ 11 ]   = { 'PatientAddressLine2', 20, true }
   lItems[ 12 ]   = { 'PatientAddressLine3', 20, true }
   lItems[ 13 ]   = { 'PatientAddressLine4', 20, true }
   lItems[ 14 ]   = { 'PatientPostCode', 8, true }
   lItems[ 15 ]   = { 'Comment', 30, true }
   lItems[ 16 ]   = { 'DeathIndicator', 1, true }
   lItems[ 17 ]   = { 'DateOfDeath', 8, true }
   lItems[ 18 ]   = { 'NHSNumber', 16, true }
   lItems[ 19 ]   = { 'DHAorHA', 3, true }
   lItems[ 20 ]   = { 'NHSStatusCode1Internal', 2, true }
   lItems[ 21 ]   = { 'NHSStatusCode1External', 2, true }
   return sqUtils.openLinkLengthToPos( "PB1", 20, items, lItems )
end

----------------------------------------------------------------------------
-- openLink - convert length table to position table
--- Inputs: tType, startPos, items (current items maps as lengths), lItems, extra
--- Return: items (current and new items map as startpos, endpos)
function sqUtils.openLinkLengthToPos( tType, startPos, items, lItems, extra)
	local pos, k, v, pos, len, tab
   pos = startPos
   trace(lItems)
   for k,v in pairs(lItems) do
      trace(k)
      trace(v)
      len = pos + v[2] - 1
      items[ tType..","..v[1] ] = { pos, len, v[3] }
      trace(v[1],items[ tType..","..v[1] ])
      pos = len + 1
   end
	--- Add on extra fields
   if extra ~= nil then
      for k,v in pairs(extra) do
         len = pos + v[1] - 1
         items[ tType..","..k ] = { pos, len, v[2] }
         pos = len
      end
   end
   return items
end

----------------------------------------------------------------------------
-- openLink - PB Segment 2
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPBSegment2(items)
   local extra = {}
   extra[ 'ConsentForAppointmentConsultantionViaVideo' ]     = {1, true}
   extra[ 'ConsentForAppointmentConsultantionViaTelephone' ] = {1, true}
   items = sqUtils.openLinkGetPMISegment( items, "PB2", extra )
   return items
end

----------------------------------------------------------------------------
-- openLink -  PB Segment 3
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPBSegment3(items)
   --- openLinkGetDemographicsSegment
   items = sqUtils.openLinkGetExtendedAddressSegment( items, "PB3" )
   items[ 'PB3,ConsentReceiveTextMessage' ]                        = {207,209,true}
   items[ 'PB3,PatientCurrentGender' ]                             = {210,210,true}
   items[ 'PB3,PatientCurrentGenderExternal' ]                     = {211,224,true}
   return items
end

----------------------------------------------------------------------------
-- openLink - PB Segment 4
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPBSegment4(items)
   local extra = {}
   extra[ 'DistrictOrResidenceCode' ] = {3, true}
   return sqUtils.openLinkGetDemographicsSegment( items, "PB4", extra )
end

----------------------------------------------------------------------------
-- openLink - PB Segment 5
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPBSegment5(items)
   return sqUtils.openLinkGetPatientInfoSegment( items, "PB5" )
end

----------------------------------------------------------------------------
-- openLink - PB Segment 6
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPBSegment6(items)
   return sqUtils.openLinkGetNOKSegment( items, "PB6" )
end

----------------------------------------------------------------------------
-- openLink - PB Segment 7
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPBSegment7(items)
   return sqUtils.openLinkGetGPSegment( items, "PB7" )
end

----------------------------------------------------------------------------
-- openLink - PB Segment 8
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPBSegment8(items)
	-- ItemName = StartPosition, EndPosition, Active (for future version, easy disable)
   items[ 'PB8,CaseNo' ]                          = {20,21,true}
   items[ 'PB8,AccommodationStatus' ]             = {22,51,true}
   items[ 'PB8,SpokenLanguage' ]                  = {52,81,true}
   items[ 'PB8,OSVChargingCategory' ]             = {82,82,true}
   items[ 'PB8,WithheldIdentifyReason' ]          = {83,84,true}
   return items
end

----------------------------------------------------------------------------
-- openLink - PR Segment 1
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPRSegment1(items)
	-- ItemName = StartPosition, EndPosition, Active (for future version, easy disable)
   items[ 'PR1,CaseNo' ]                           = { 20,21,true}
   items[ 'PR1,InternalPatientNumber' ]            = { 22,30,true}
   items[ 'PR1,PatientPrimaryIdentifierNumber' ]   = { 31,40,true}
   items[ 'PR1,GDPCode' ]                          = { 41,46,true}
   items[ 'PR1,GPCode' ]                           = { 47,52,true}
   items[ 'PR1,ReligionCode' ]                     = { 53,56,true}
   items[ 'PR1,MaritalStatusCode' ]                = { 57,57,true}
   items[ 'PR1,PlaceOfBirth' ]                     = { 58,77,true}
   items[ 'PR1,NHSNumber' ]                        = { 78,94,true}
   items[ 'PR1,Occupation' ]                       = { 95,114,true}
   items[ 'PR1,School' ]                           = {115,134,true}
   items[ 'PR1,OccupationSpouse' ]                 = {135,154,true}
   items[ 'PR1,EthnicGroup' ]                      = {155,158,true}
   items[ 'PR1,PatientBirthName' ]                 = {159,182,true}
   items[ 'PR1,SexualOrientationCode' ]            = {183,184,true}
   return items
end

----------------------------------------------------------------------------
-- openLink - PR Segment 2
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPRSegment2(items)
   -- TODO different NOK layout (move to new generic)
	-- ItemName = StartPosition, EndPosition, Active (for future version, easy disable)
   items[ 'PR2,CaseNo' ]                           = { 20,21,true}
   items[ 'PR2,NextOfKinName' ]                    = { 22,51,true}
   items[ 'PR2,RelationshipCode' ]                 = { 52,61,true}
   items[ 'PR2,NOKAddressLine1' ]                  = { 62,81,true}
   items[ 'PR2,NOKAddressLine2' ]                  = { 82,101,true}
   items[ 'PR2,NOKAddressLine3' ]                  = {102,121,true}
   items[ 'PR2,NOKAddressLine4' ]                  = {122,141,true}
   items[ 'PR2,NOKPostCode' ]                      = {142,151,true}
   items[ 'PR2,NOKTelephoneHome' ]                 = {152,174,true}
   items[ 'PR2,NOKTelephoneWork' ]                 = {175,197,true}
   items[ 'PR2,NOKComments' ]                      = {198,227,true}
   items[ 'PR2,NOKPostCodeExtended' ]              = {228,239,true}
   return items
end

----------------------------------------------------------------------------
-- openLink - PR Segment 3
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPRSegment3(items)
   return sqUtils.openLinkGetGPSegment( items, "PR3" )
end

----------------------------------------------------------------------------
-- openLink - PR Segment 4
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPRSegment4(items)
	-- ItemName = StartPosition, EndPosition, Active (for future version, easy disable)
   items[ 'PR4,CaseNo' ]                   = { 20,21,true}
   items[ 'PR4,EpisodicAddress1' ]         = { 22,41,true}
   items[ 'PR4,EpisodicAddress2' ]         = { 42,61,true}
   items[ 'PR4,EpisodicAddress3' ]         = { 62,81,true}
   items[ 'PR4,EpisodicAddress4' ]         = { 82,101,true}
   items[ 'PR4,EpisodicPostCode' ]         = {102,111,true}
   items[ 'PR4,EpisodicPseudoPostCode' ]   = {112,121,true}
   items[ 'PR4,NOKEmail' ]                 = {122,181,true}
   return items
end

----------------------------------------------------------------------------
-- openLink - PR Segment 5
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPRSegment5(items)
   items=sqUtils.openLinkGetDemographicsSegment( items, "PR5" )
   items[ 'PR5,DistrictOrResidenceCode' ]   = {206,215,true}
   return items
end

----------------------------------------------------------------------------
-- openLink - PR Segment 6
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPRSegment6(items)
   return sqUtils.openLinkGetPatientInfoSegment( items, "PR6" )
end

----------------------------------------------------------------------------
-- openLink - PR Segment 7
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPRSegment7(items)
   return sqUtils.openLinkGetNOKSegment( items, "PR7" )
end

----------------------------------------------------------------------------

-- openLink -  PR Segment 8
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPRSegment8(items)
   items = sqUtils.openLinkGetGPwithMRSASegment( items, "PR8" )
   return items
end

----------------------------------------------------------------------------

-- openLink - PR Segment 9
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPRSegment9(items)
   return sqUtils.openLinkGetPMISegment( items, "PR9" )
end
----------------------------------------------------------------------------

-- openLink - PR Segment 10
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPRSegment10(items)
   return sqUtils.openLinkGetExtendedAddressSegment( items, "PR10" )
end

----------------------------------------------------------------------------
-- openLink - PD Segment 1
--- TODO no spec, this is a guess!
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPDSegment1(items)
   items[ 'PD1,CaseNo' ]                       = {20,21,true}
   items[ 'PD1,InternalPatientNumber' ]        = {22,30,true}
   items[ 'PD1,Surname' ]                      = {41,64,true}
   items[ 'PD1,Forenames' ]                    = {65,84,true}
   items[ 'PD1,DateOfBirth' ]                  = {85,92,true}
   items[ 'PD1,Sex' ]                          = {93,93,true}
   items[ 'PD1,Title' ]                        = {94,97,true}
   return items
end

----------------------------------------------------------------------------
-- openLink - PD Segment 2
--- TODO no spec, this is a guess!
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetPDSegment2(items)
   items[ 'PD2,CaseNo' ]                              = { 20,21,true}
   items[ 'PD2,PatientAddressLine1' ]                 = { 22,60,true}
   items[ 'PD2,PatientAddressLine2' ]                 = { 61,80,true}
   items[ 'PD2,PatientAddressLine3' ]                 = { 81,100,true}
   items[ 'PD2,PatientPostCode' ]                     = {101,109,true}
   --- todo LOTS missing!
   return items
end

----------------------------------------------------------------------------
-- openLink - OC Segment 1
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetOCSegment1(items)
	-- ItemName = StartPosition, EndPosition, Active (for future version, easy disable)
   items[ 'OC1,CaseNo' ]                           = { 20,21,true}
   items[ 'OC1,InternalPatientNumber' ]            = { 22,30,true}
   items[ 'OC1,DistrictNumber' ]                   = { 31,40,true}
   items[ 'OC1,CaseNoteNumber' ]                   = { 41,53,true}
   items[ 'OC1,EpisodeNumber' ]                    = { 54,62,true}
   items[ 'OC1,AccountNumber' ]                    = { 63,71,true}
   items[ 'OC1,DoctorGroupCode' ]                  = { 73,80,true}
   items[ 'OC1,DoctorCode' ]                       = { 81,88,true}
   items[ 'OC1,AppointmentDateTime' ]              = { 89,100,true}
   items[ 'OC1,CancelledBy' ]                      = {100,100,true} -- Really length one? (spec says so - TODO)
   items[ 'OC1,CancellationDateTime' ]             = {101,112,true}
   items[ 'OC1,AppointmentCancellationReason' ]    = {113,142,true}
   items[ 'OC1,AppointmentCancellationComment' ]   = {143,172,true}
   items[ 'OC1,ReasonCode' ]                       = {173,176,true}
   return items
end
----------------------------------------------------------------------------

-- openLink - OC Segment 2
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetOCSegment2(items)
   items = sqUtils.openLinkGetDemographicsSegment( items, "OC2" )
   items[ 'OC2,DistrictOfReasidenceCode' ]         = {216,218,true}
   return items
end

----------------------------------------------------------------------------

-- openLink - OC Segment 3
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetOCSegment3(items)
   return sqUtils.openLinkGetPatientInfoSegment( items, "OC3" )
end

----------------------------------------------------------------------------

-- openLink - OC Segment 4
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetOCSegment4(items)
   return sqUtils.openLinkGetPatientInfoSegment( items, "OC4" )
end

----------------------------------------------------------------------------

-- openLink - OC Segment 5
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetOCSegment5(items)
   items=sqUtils.openLinkGetGPwithMRSASegment( items, "OC5" )
	return items
end
----------------------------------------------------------------------------

-- openLink - OC Segment 6
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetOCSegment6(items)
   return sqUtils.openLinkGetClinicSegment( items, "OC6" )
end
----------------------------------------------------------------------------

-- openLink - OC Segment 7
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetOCSegment7(items)
   return sqUtils.openLinkGetConsultantSegment( items, "OC7" )
end

----------------------------------------------------------------------------

-- openLink - OC Segment 8
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetOCSegment8(items)
   return sqUtils.openLinkGetPMISegment( items, "OC8" )
end

----------------------------------------------------------------------------

-- OpenLink - OC Segment 9
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetOCSegment9(items)
   items = sqUtils.openLinkGetExtendedAddressSegment( items, "OC9" )
   items[ 'OC9,PatientCurrentGenderExtended' ]            = {229,242,true}
   items[ 'OC9,PatientCurrentGender' ]                    = {243,243,true}
	return items
end

----------------------------------------------------------------------------

--- OpenLink - AD Segment 1
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetADSegment1(items)
	-- ItemName = StartPosition, EndPosition, Active (for future version, easy disable)
   --- TODO
   return items
end

----------------------------------------------------------------------------
-- openLink - IP Segment 1
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetIPSegment1(items)
   items[ 'IP1,CaseNo' ]                                = {20,21,true}
   items[ 'IP1,InternalPatientNumber' ]                 = {22,30,true}
   items[ 'IP1,PrimaryPatientInternalNumberExtended' ]  = {31,43,true}
   items[ 'IP1,EpisodeNumber' ]                         = {44,52,true}
   items[ 'IP1,AppointmentDateTime' ]                   = {53,64,true}
   items[ 'IP1,PatientTrackingStep' ]                   = {65,68,true}
   items[ 'IP1,TrackingStepDateTime' ]                  = {69,80,true}
   items[ 'IP1,TrackingStepComment' ]                   = {81,110,true}
   items[ 'IP1,TPVReference' ]                          = {111,116,true}
   items[ 'IP1,ErrorSuccessCode' ]                      = {117,119,true}
   return items
end

----------------------------------------------------------------------------
-- openLink - SS Segment 1
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetSSSegment1(items)
   local lItems, pos = {}, 1
   lItems[ pos ], pos = { 'CaseNo', 2, true },                               pos + 1
   lItems[ pos ], pos = { 'ClinicCode', 8, true },                           pos + 1
   lItems[ pos ], pos = { 'SessionDate', 8, true },                          pos + 1
   lItems[ pos ], pos = { 'DoctorCode', 8, true },                           pos + 1
   lItems[ pos ], pos = { 'SessionStartTime', 4, true },                     pos + 1
   lItems[ pos ], pos = { 'SessionStopTime', 4, true },                      pos + 1
   lItems[ pos ], pos = { 'SessionTotalBookedUnits', 4, true },              pos + 1
   lItems[ pos ], pos = { 'SessionUsedUnits', 4, true },                     pos + 1
   lItems[ pos ], pos = { 'SessionFreeBookingUnits', 4, true },              pos + 1
   lItems[ pos ], pos = { 'SessionStatus', 1, true },                        pos + 1
   lItems[ pos ], pos = { 'SessionCancelReasonComments', 30, true },         pos + 1
   lItems[ pos ], pos = { 'SessionSetupFrequencyCode', 2, true },            pos + 1
   lItems[ pos ], pos = { 'SessionCreationComment', 65, true },              pos + 1
   lItems[ pos ], pos = { 'SessionCancellationReasonCode', 65, true },       pos + 1
   lItems[ pos ], pos = { 'UserAccessCode', 4, true },                       pos + 1
   return sqUtils.openLinkLengthToPos( "SS1", 20, items, lItems )
end

----------------------------------------------------------------------------
-- openLink - SS Segment N (Dynamic)
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetSSSegmentN(items)
   local lItems, pos = {}, 1
   lItems[ pos ], pos = { 'CaseNo', 2, true },                               pos + 1
   lItems[ pos ], pos = { 'TimeSlotStartTimeInterval', 4, true },            pos + 1
   lItems[ pos ], pos = { 'TimeSlotStopTimeInterval', 4, true },             pos + 1
   lItems[ pos ], pos = { 'AppointmentType', 3, true },                      pos + 1
   lItems[ pos ], pos = { 'AppointmentTypeTotalBookingUnits', 3, true },     pos + 1
   lItems[ pos ], pos = { 'AppointmentTypeTotalUsedUnits', 3, true },        pos + 1
   lItems[ pos ], pos = { 'AppointmentTypeTotalFreeUnits', 3, true },        pos + 1
   lItems[ pos ], pos = { 'TotalTimeslotTotalBookingUnits', 3, true },       pos + 1
   lItems[ pos ], pos = { 'TotalTimeslotTotalUsedUnits', 3, true },          pos + 1
   lItems[ pos ], pos = { 'TotalTimeslotTotalFreeUnits', 3, true },          pos + 1
   lItems[ pos ], pos = { 'TotalTimeslotStatus', 1, true },                  pos + 1
   lItems[ pos ], pos = { 'ReportToLocation', 4, true },                     pos + 1
   lItems[ pos ], pos = { 'AreaCode', 10, true },                            pos + 1
   lItems[ pos ], pos = { 'RoomCode', 10, true },                            pos + 1
   lItems[ pos ], pos = { 'FlagDoesNextRecordExist', 4, true },              pos + 1
   return sqUtils.openLinkLengthToPos( "SSN", 20, items, lItems )
end

----------------------------------------------------------------------------
-- openLink - IS Segment 1
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetISSegment1(items)
   local lItems, pos = {}, 1
   lItems[ pos ], pos = { 'CaseNo', 2, true },                               pos + 1
   lItems[ pos ], pos = { 'InternalPatientNumber', 9, true },                pos + 1
   lItems[ pos ], pos = { 'ExternalPatientNumber', 10, true },               pos + 1
   lItems[ pos ], pos = { 'CaseNoteNumber', 14, true },                      pos + 1
   lItems[ pos ], pos = { 'EpisodeNumber', 9, true },                        pos + 1
   lItems[ pos ], pos = { 'SystemAccountNumber', 9, true },                  pos + 1
   lItems[ pos ], pos = { 'ClinicCode', 8, true },                           pos + 1
   lItems[ pos ], pos = { 'DoctorCode', 8, true },                           pos + 1
   lItems[ pos ], pos = { 'AppointmentDateTimeInternal', 12, true },         pos + 1
   lItems[ pos ], pos = { 'AppointmentType', 3, true },         pos + 1
   lItems[ pos ], pos = { 'ModeofTransport', 2, true },         pos + 1
   lItems[ pos ], pos = { 'PatientCategory', 3, true },         pos + 1
   lItems[ pos ], pos = { 'BookedOnDateTimeInternal', 12, true },         pos + 1
   lItems[ pos ], pos = { 'Booking Type', 4, true },         pos + 1
   lItems[ pos ], pos = { 'Comments', 60, true },                            pos + 1
   lItems[ pos ], pos = { 'UBRN', 30, true },                            pos + 1
   lItems[ pos ], pos = { 'SQRefNo1', 20, true },                            pos + 1
   lItems[ pos ], pos = { 'SQRefNo2', 20, true },                            pos + 1
   lItems[ pos ], pos = { 'TPVReference', 6, true },                         pos + 1
   lItems[ pos ], pos = { 'UserID', 4, true },                               pos + 1
   lItems[ pos ], pos = { 'StatusCode', 3, true },                           pos + 1   
   return sqUtils.openLinkLengthToPos( "IS1", 20, items, lItems )
end

----------------------------------------------------------------------------
-- openLink - SC Segment 1
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetSCSegment1(items)
   local lItems, pos = {}, 1
   lItems[ pos ], pos = { 'CaseNo', 2, true },                               pos + 1
   lItems[ pos ], pos = { 'AppointmentTypeCode', 3, true },                  pos + 1
   lItems[ pos ], pos = { 'AppointmentTypeDescription', 20, true },          pos + 1
   lItems[ pos ], pos = { 'AppointmentCategory', 3, true },                  pos + 1
   lItems[ pos ], pos = { 'AppointmentClassification', 3, true },            pos + 1
   lItems[ pos ], pos = { 'IsUrgent', 1, true },                             pos + 1
   lItems[ pos ], pos = { 'IsEBSeRS', 1, true },                             pos + 1
   lItems[ pos ], pos = { 'NewRTTPeriodForOPAppointment', 1, true },         pos + 1
   lItems[ pos ], pos = { 'NewRTTPeriodForSGAppointment', 1, true },         pos + 1
   lItems[ pos ], pos = { 'AppointmentMode', 1, true },                      pos + 1
   return sqUtils.openLinkLengthToPos( "SC1", 20, items, lItems )
end

----------------------------------------------------------------------------
-- openLink - SN Segment 1
--- Inputs: items (current items maps)
--- Return: items (current and new items map)
function sqUtils.openLinkGetSNSegment1(items)
   local lItems, pos = {}, 1
   lItems[ pos ], pos = { 'CaseNo', 2, true },                               pos + 1
   lItems[ pos ], pos = { 'InternalPatientNumber', 9, true },                pos + 1
   lItems[ pos ], pos = { 'ExternalPatientNumber', 10, true },               pos + 1
   lItems[ pos ], pos = { 'EpisodeNumber', 9, true },                        pos + 1
   lItems[ pos ], pos = { 'AppointmentDateTimeInternal', 12, true },         pos + 1
   lItems[ pos ], pos = { 'ClinicCode', 8, true },                           pos + 1
   lItems[ pos ], pos = { 'DoctorCode', 8, true },                           pos + 1
   lItems[ pos ], pos = { 'CancelledBy', 1, true },                          pos + 1
   lItems[ pos ], pos = { 'CancelledDateTime', 12, true },                   pos + 1
   lItems[ pos ], pos = { 'CancelCode', 4, true },                           pos + 1
   lItems[ pos ], pos = { 'Comment1', 30, true },                            pos + 1
   lItems[ pos ], pos = { 'Comment2', 30, true },                            pos + 1
   lItems[ pos ], pos = { 'TPVReference', 6, true },                         pos + 1
   lItems[ pos ], pos = { 'UserID', 4, true },                               pos + 1
   lItems[ pos ], pos = { 'StatusCode', 3, true },                           pos + 1   
   return sqUtils.openLinkLengthToPos( "SN1", 20, items, lItems )
end

----------------------------------------------------------------------------
--- Justify string/number
--- Inputs: itemIn (item we want to justify), leftOrRight (just "L"eft or "R"ight) - Default "R", size (length of field required) - Default (size of itemIn) , fill with (default " ")
--- Return: result (justified, padded, filled item)
function sqUtils.justify( itemIn, leftOrRight, size, fill )
   local k, result, dir = nil, tostring( itemIn ), string.sub( string.upper( leftOrRight ), 1 )
    
	if ( leftOrRight == nil) then leftOrRight = "R" end
   if ( size == nil ) then size = #result end
   if ( fill == nil ) then fill = " " end
   
   for k = 1, size do
      if #result >= size then
         break
      end
      if ( dir == "R" ) then
         result = fill..result
         trace(result)
      else
         result = result..fill
      end
   end
   return result
end

----------------------------------------------------------------------------
--- Remove trailing characters
--- Inputs: itemIn, trail string
--- Return: result
function sqUtils.removeTrailing(itemIn, trail)
   local i, result = nil, itemIn
   for i = #itemIn, 1, -1 do
      if string.sub( result, i, i ) == trail then
         result = string.sub( result, 1, i-1 )
      else
	      break
      end
   end
   return result
end

----------------------------------------------------------------------------
-- Get config item (with default)
--- Inputs: name, default
--- Return: result
function sqUtils.getConfigItem(sqCfg, name, default)
   local result = sqUtils.getJsonMappedValue( sqCfg.extra_params, name)
   if #result == 0 then
      result = default
   end
	return result
end

return sqUtils