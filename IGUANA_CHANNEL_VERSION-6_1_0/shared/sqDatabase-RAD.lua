--[[

Author: Peter Branagan
Date: Mar 2025

All database activity needs be executed from this modules, separated from any other scripts


]]

local sqDB = {

   patientData = {
      
      mrn = '',
      firstname = '',
      surname = '',
      dob = '',
      user_type = '',
      user_status = '',
      gender = '',   
      mobile = '',
      email = '',
      home_phone = '',
      mobile = '',
      address1 = '',
      address2 = '',
      address3 = '',
      address4 = ''
   
   },
   
   RAD = {}
   
}

----------------------------------------------------------------------------

function sqDB.execute(val)
   
   --[[
         NOTE: **** The audit table being written to this function 
                     will grow, will need a script to purge monthly ****
   ]]
   
   local channel = iguana.channelName()
   local sql = val.sql:gsub("\\'","'")
   sql = sql:gsub("\n","")
   local msgid = _G.msgCtrlID
   local insert = val.sql:upper():find('INSERT INTO')
   if insert ~= nil then insert = true end
   local update = val.sql:upper():find('UPDATE ')
   if update ~= nil then update = true end
   local delete = val.sql:upper():find('DELETE FROM ')
   if delete ~= nil then delete = true end
   
   if insert or update or delete then
      --check the sql string for HL7 control characters and replace them
      local decodeSpecialCharacters = sqUtils.getMappedValue(sqCfg.extra_params,"decodeSpecialCharacters")
      if decodeSpecialCharacters == '1' then
         val.sql = sqDB.unEncodeString(val.sql)
      end
   end
   
   trace(val.sql)
   
   --execute the SQL caommand
   
   local res = conn:execute(val)
   
   --log the DB activity if set to do so
   
   if _G.dbAudit == '1' then 

      --log all inserts and updates to the iguana db audit table

      if insert or update or delete then
         
         --[[
         conn:execute{sql="insert into iguana_db_audit (channel_name, msgid, sql_str) values ("..    
            conn:quote(channel)..","..
            conn:quote(msgid)..","..
            conn:quote(sql)..")"
            , live=false}
         ]]
         
         local dt = os.date("%Y-%m-%d %H:%M:%S")
         local line = "\""..dt.."\",\""..channel.."\",\""..msgid.."\",\""..sql.."\""
         
         if not iguana.isTest() then
            --sqUtils.printLog("Writing SQL statement to DB Audit File..")
            --local filePath = "/root/iguana_db_audit.txt"
            local filePath = os.getenv("DB_AUDIT_FILEPATH")
            local file, err = io.open(filePath, "a")
            if not file then
               iguana.logError("Error opening file: " .. err)
            end
           
            file:write(line .. "\n") -- Add a newline character to separate lines
            
            --write the returned ID from the insert
            if insert then
               local result = conn:execute{sql="select last_insert_id() as id", live=true}
               sql = "ID FROM LAST INSERT = "..result[1].id:S()
               line = "\""..dt.."\",\""..channel.."\",\""..msgid.."\",\""..sql.."\""
               file:write(line .. "\n")
            end
         
            file:close()
         end
      end
   end
     
   return res
   
end

----------------------------------------------------------------------------

function sqDB.userInFacility(patientId, facilityId)
   local result = sqDB.execute{sql="select id from facility_user where "..
   " user_id = " .. patientId .. " "..
   " and facility_id = " .. facilityId .. ";"
   , live=true}
   if #result == 0 then return '' end
   return result[1].id:nodeValue()
end

----------------------------------------------------------------------------

function sqDB.addToFacility(patientId, facilityId)
   sqUtils.printLog("Calling sqDB.addToFacility...")
   
	if sqDB.userInFacility(patientId, facilityId) == ''
   then
	   sqDB.execute{sql="insert into facility_user (user_id, facility_id) values ("..patientId..", "..facilityId..")", live=false}
   end
end

----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------

--     RADIOLOGY IMPLEMENTATION  -----


-----------------------------------------------------------------

-----------------------------------------------------------------

function sqDB.RAD.cancelAndRebook(facilityID, patientID, episodeNumber, orderResID, room_code, opdAppointmentString, opdAppointmentTimeString, opdAppointmentEndTimeString)

   local resCheck = sqDB.execute{sql=
      "select count(*), IFNULL(id,0) as res_id from reservation where location_pref = " .. conn:quote(orderResID) .. " and status = 'active'", live=true}
   --
   if (resCheck[1]['res_id']:S() ~= '0') then
      local resCheckUpd = sqDB.execute{sql=
         "update reservation set status = 'cancelled' where location_pref = " .. conn:quote(orderResID).. " and status = 'active'", live=false}  
      conn:commit{live=true}
   end
   --
   local AppCount = sqDB.execute{sql=
      "select count(facility_id), facility_id " ..
      "from reason_exam_codes where facility_id " ..
      "in (select id from facilities where id = "..facilityID.." \
      or parent_id = "..facilityID..") and room_id = " .. 
      conn:quote(room_code) .. " limit 1", live=true}

   if AppCount[1]['count(facility_id)']:S() ~= '0'  then

      local sql ="insert into reservation (facility_id, user_id, facility_type, consultant_id, " ..
      "location_pref, reservation_date,reservation_time, reservation_end_time, google_cal_id, " ..
      "reason_id,recurrence,date,status,attended_by, is_attended,booking_user_id," ..
      "source, print,stats, quality,capacity_sync, is_urgent) " .. 
      "values ("..
      AppCount[1]['facility_id']..",0,'f',0, " ..
      conn:quote(orderResID) ..",".. --''"..","..
      conn:quote(opdAppointmentString)..","..
      conn:quote(opdAppointmentTimeString)..","..
      conn:quote(opdAppointmentEndTimeString)..","..
      "'',1,'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0"..
      ")"

      iguana.logDebug("SQL (Add reservation): ".. sql)
      sqDB.execute{sql=sql, live=false}
      conn:commit{live=true}
   end  


   --[[ CI: 20/10/2023 : Check whether there is a referral associated with this booking, which was previously 'for Swiftqueue'.
   If referral found, then move it to assigned to make sure that the patient is no longer able to book an appointment]]                  
   local referralID = sqDB.RAD.getPatientReferralByEpisode(patientID, facilityID, episodeNumber)
   if referralID ~= '0' then

      local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_Ref")

      local sql ="update ewaiting_referrals " ..
      "set "..
      "modified = now(), modified_by = "..sqUser..", "..
      "status = 'assigned'" ..
      " where id = ".. referralID ..
      " and status in ('pre_triage', 'triage', 'letter_sent', 'active')"
      iguana.logDebug("NSQ:SIU received:update ewaiting_referrals: ".. sql)
      sqDB.execute{sql=sql, live=false}
   end

end

---------------------------------------------------------------------

function sqDB.RAD.process_NFSQ_S12(facilityID, patientID, episodeNumber, orderResID, room_code, opdAppointmentString, opdAppointmentTimeString, opdAppointmentEndTimeString)

   --if referral exists then set as attending   
   
   local referralID = sqDB.RAD.getPatientReferralByEpisode(patientID, facilityID, episodeNumber)
   if referralID ~= '0' then

      local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_Ref")

      local sql ="update ewaiting_referrals " ..
      "set "..
      "modified = now(), modified_by = "..sqUser..", "..
      "status = 'assigned'" ..
      " where id = ".. referralID ..
      " and status in ('pre_triage', 'triage', 'letter_sent', 'active')"
      iguana.logDebug("NSQ:SIU received:update ewaiting_referrals: ".. sql)
      sqDB.execute{sql=sql, live=false}

   else
      
      --referral doesn;t exist so...
      
      local resCheck = sqDB.execute{sql=
         "select count(*), IFNULL(id,0) as res_id from reservation where location_pref = " .. conn:quote(orderResID) .. " and status = 'active'", live=true}
      
      --cancel the app if it exists
      
      if (resCheck[1]['res_id']:S() ~= '0') then
         local resCheckUpd = sqDB.execute{sql=
            "update reservation set status = 'cancelled' where location_pref = " .. conn:quote(orderResID).. " and status = 'active'", live=false}  
         conn:commit{live=true}
      end
      
      --and if the exam and room are manged by SQ then create an appointment
      
      local AppCount = sqDB.execute{sql=
         "select count(facility_id), facility_id " ..
         "from reason_exam_codes where facility_id " ..
         "in (select id from facilities where id = "..facilityID.." \
         or parent_id = "..facilityID..") and room_id = " .. 
         conn:quote(room_code) .. " limit 1", live=true}

      if AppCount[1]['count(facility_id)']:S() ~= '0'  then

         local sql ="insert into reservation (facility_id, user_id, facility_type, consultant_id, " ..
         "location_pref, reservation_date,reservation_time, reservation_end_time, google_cal_id, " ..
         "reason_id,recurrence,date,status,attended_by, is_attended,booking_user_id," ..
         "source, print,stats, quality,capacity_sync, is_urgent) " .. 
         "values ("..
         AppCount[1]['facility_id']..",0,'f',0, " ..
         conn:quote(orderResID) ..",".. --''"..","..
         conn:quote(opdAppointmentString)..","..
         conn:quote(opdAppointmentTimeString)..","..
         conn:quote(opdAppointmentEndTimeString)..","..
         "'',1,'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0"..
         ")"

         iguana.logDebug("SQL (Add reservation): ".. sql)
         sqDB.execute{sql=sql, live=false}

         print("DB Commit at line 194")
         conn:commit{live=true}
      end  

   end

end

-----------------------------------------------------------------

function sqDB.RAD.updateUserAudit(appId, userId, action)
   --Add A user audit record for the app creation
   --Action must be one of the following:
   --appointment.created
   --appointment.rescheduled
   --appointment.cancelled
   --appointment.dna
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_App")
   
   sqDB.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
      "values (null, now(),"..sqUser..","..conn:quote(action)..","..
      conn:quote(appId)..","..userId..",null)"
      , live=false}   
end

----------------------------------------------------------------------------

function sqDB.RAD.getPatientIdByMrn(fid, mrn, firstname, surname, pmobile, pdob)
   
   local pmobile2 = sqUtils.getMappedValue(sqCfg.extra_params,"telAreaCode").. pmobile:sub(2,15)

   local PatientId = sqDB.execute{sql=
      "select count(id), id from users " ..
      "where mrn = " .. conn:quote(mrn) .. 
      " and mrn <> '' and id in (select user_id from facility_user where facility_id = " .. fid ..")", 
      live=true}

   if PatientId[1]['count(id)']:S() == '0' then
      
      local PatientId2 = sqDB.execute{sql=
         "select count(id), id from users " ..
         "where firstname = " .. conn:quote(firstname) .. 
         " and surname = " .. conn:quote(surname) ..
         " and Date_of_Birth = " .. conn:quote(pdob) .. 
         " and mobile = " .. conn:quote(pmobile2),
         live=true}
      --
      if PatientId2[1]['count(id)']:S() == '0' then
         return '0'
      else
         local sql ="update users " ..
         "set "..
         "mrn = " .. conn:quote(mrn) ..
         "where id = ".. PatientId2[1].id:S()

         sqDB.execute{sql=sql, live=false}
         
         return PatientId2[1].id:S()
      end
   else
      return PatientId[1].id:S()
   end
   
end

----------------------------------------------------------------------------

function sqDB.RAD.createPatient(PID)
   
   sqUtils.printLog("Creating patient "..PID[3][1][1]:S().."...")
   
   local telAreaCode = sqUtils.getMappedValue(sqCfg.extra_params,"telAreaCode")
   local telCode_HOME = sqUtils.getMappedValue(sqCfg.extra_params,"telCode_HOME")
   local telCode_MOBILE = sqUtils.getMappedValue(sqCfg.extra_params,"telCode_MOBILE")

   local mrn = PID[3][1][1]:S()
   local firstname = PID[5][2]:nodeValue()
   local surname = PID[5][1]:nodeValue()
   local user_type = "N"
   local user_status = 'active'
   local dob = sqUtils.parseDOB(PID[7])
   local gender = PID[8]:nodeValue()
   local email = PID[11][2][1]:nodeValue()
   local home_phone = PID[14][1]:S()
   local mobile1 = PID[13][1][1]:S()

   if PID[13][1][2]:S() == telCode_HOME then
      home_phone = PID[13][1][1]:S()
   elseif PID[13][1][2]:S() == telCode_MOBILE then
      mobile1 = PID[13][1][1]:S()
   end
   --
   if PID[13][2][2]:S() == telCode_HOME then
      home_phone = PID[13][2][1]:S()
   elseif PID[13][2][2]:S() == telCode_MOBILE then
      mobile1 = PID[13][2][1]:S()
   end

   local address1 = PID[11][1][1]:nodeValue() -- PID[11][1][2]
   local address2 = PID[11][1][2]:nodeValue()
   local address3 = PID[11][1][3]:nodeValue()
   local address4 = PID[11][1][4]:nodeValue()
   local homeAddress = PID[11][1][5]:nodeValue()

   local accountNumber = PID[18]:S()

    local mobile = ''
   if mobile1:sub(2,15) ~= '' then
      mobile = telAreaCode..mobile1:sub(2,15)
   end

    
   sqDB.execute{sql=
      "insert into users " ..
      "(mobile, home_phone, email, mrn, firstname, surname, user_type, status, Date_of_Birth, Gender, " ..
      "Address_1, Address_2, Address_3, Address_4, Home_Address, created, Chart_Number) " ..
      "values ("..
      " trim(replace("..conn:quote(mobile)..", ' ', ''))"..","..
      " trim(replace("..conn:quote(home_phone)..", ' ', ''))"..","..
      conn:quote(email)..","..
      conn:quote(mrn)..","..
      conn:quote(firstname)..","..
      conn:quote(surname)..","..
      conn:quote(user_type)..","..
      conn:quote(user_status)..","..
      conn:quote(dob)..","..
      conn:quote(gender)..","..
      conn:quote(address1)..","..
      conn:quote(address2)..","..
      conn:quote(address3)..","..
      conn:quote(address4)..","..
      conn:quote(homeAddress)..","..
      "now()"..","..
      conn:quote(accountNumber)..
      ")"
      , live=false}

  
   -- Update Comms preferences
   local EmailPref = PID[11][2][7]:S()
   local EmailPrefInd = PID[11][2][8]:S()
   local SmsPref = PID[13][1][2]:S()
   local SmsPrefInd = PID[13][1][3]:S()

   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}

   if EmailPrefInd == 'N' then
      sqDB.execute{sql=
         "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
         "values (null,"..
         conn:quote(result[1].id:S())..","..
         "1,2,0)"
         , live=false}
      sqDB.execute{sql=
         "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
         "values (null,"..
         conn:quote(result[1].id:S())..","..
         "2,2,0)"
         , live=false}  
      sqDB.execute{sql=
         "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
         "values (null,"..
         conn:quote(result[1].id:S())..","..
         "3,2,0)"
         , live=false}  
      sqDB.execute{sql=
         "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
         "values (null,"..
         conn:quote(result[1].id:S())..","..
         "4,2,0)"
         , live=false}  
   end
   --
   if SMSPrefInd == 'N' then
      sqDB.execute{sql=
         "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
         "values (null,"..
         conn:quote(result[1].id:S())..","..
         "1,1,0)"
         , live=false}
      sqDB.execute{sql=
         "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
         "values (null,"..
         conn:quote(result[1].id:S())..","..
         "2,1,0)"
         , live=false}  
      sqDB.execute{sql=
         "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
         "values (null,"..
         conn:quote(result[1].id:S())..","..
         "3,1,0)"
         , live=false}  
      sqDB.execute{sql=
         "insert into user_notification_preferences (id, user_id, event_id, channel_id, enabled) " ..
         "values (null,"..
         conn:quote(result[1].id:S())..","..
         "4,1,0)"
         , live=false}  
   end
   
   sqUtils.printLog("Patient created!")

   return result[1].id:S()

end

----------------------------------------------------------------------------

function sqDB.RAD.updatePatient(id, PID)
   
   sqUtils.printLog("Updating patient...")
   
   local telAreaCode = sqUtils.getMappedValue(sqCfg.extra_params,"telAreaCode")
   local telCode_HOME = sqUtils.getMappedValue(sqCfg.extra_params,"telCode_HOME")
   local telCode_MOBILE = sqUtils.getMappedValue(sqCfg.extra_params,"telCode_MOBILE")

   local mrn = PID[3][1][1]:S()
   local firstname = PID[5][2]:nodeValue()
   local surname = PID[5][1]:nodeValue()
   local dob = sqUtils.parseDOB(PID[7])
   local gender = PID[8]:nodeValue()  
   local mobile = PID[13][1][1]:nodeValue()
   local home_phone = PID[14][1]:nodeValue()
   local address1 = PID[11][1][1]:nodeValue()
   local address2 = PID[11][1][2]:nodeValue()
   local address3 = PID[11][1][3]:nodeValue()
   local address4 = PID[11][1][4]:nodeValue()
   local homeAddress = PID[11][1][5]:nodeValue()
   local accountNumber = PID[18]:S()
   local email = PID[11][2][1]:nodeValue()
   local home_phone = PID[14][1]:S()
   local mobile1 = PID[13][1][1]:S()

   if PID[13][1][2]:S() == telCode_HOME then
      home_phone = PID[13][1][1]:S()
   elseif PID[13][1][2]:S() == telCode_MOBILE then
      mobile1 = PID[13][1][1]:S()
   end
   
   if PID[13][2][2]:S() == telCode_HOME then
      home_phone = PID[13][2][1]:S()
   elseif PID[13][2][2]:S() == telCode_MOBILE then
      mobile1 = PID[13][2][1]:S()
   end

   local mobile = ''
   if mobile1:sub(2,15) ~= '' then
      mobile = telAreaCode..mobile1:sub(2,15)
   end
   
   local sql ="update users " ..
   "set "..
   "firstname = " .. conn:quote(firstname) .. ", "..
   "surname = " .. conn:quote(surname) .. ", "..
   "Date_of_Birth = " .. conn:quote(dob) .. ", "
   
    --PB 12.06.24 : ir-321 - suppress updating email if blank value received
   if not sqUtils.isEmpty(email) then
      sql = sql.."email = " .. conn:quote(email) .. ", "
   end
   
    --PB 29.10.24 : ir-506 - suppress updating mobile if blank value received
   if not sqUtils.isEmpty(mobile) then
      sql = sql.."mobile = " .. " trim(replace("..conn:quote(mobile)..", ' ', ''))"..", "
   end
   
   sql = sql..
   "home_phone = " .. " trim(replace("..conn:quote(home_phone)..", ' ', ''))"..", "..
   "Gender = " .. conn:quote(gender) .. ", "..
   "Address_1 = " .. conn:quote(address1) .. ", "..
   "Address_2 = " .. conn:quote(address2) .. ", "..
   "Address_3 = " .. conn:quote(address3) .. ", "..
   "Address_4 = " .. conn:quote(address4) .. ", "..
   "Home_Address = " .. conn:quote(homeAddress) .. ", "..
   "Chart_Number = " .. conn:quote(accountNumber) ..
   " where id = ".. id


   sqDB.execute{sql=sql, live=false}   
   
   sqUtils.printLog("Patient updated!")
   
end

----------------------------------------------------------------------------

function sqDB.RAD.addToFacility(patientId, facilityId)

   return sqDB.addToFacility(patientId, facilityId)

end
      
----------------------------------------------------------------------------

function sqDB.RAD.getResByOrderId(OrderID)
   
 local res = sqDB.execute{sql=
    "select count(*), IFNULL(id,0) as res_id from reservation where location_pref = " .. conn:quote(OrderID), 
      live=true}
   
 return res

end

----------------------------------------------------------------------------

function sqDB.RAD.getReferralOrderID(OrderID)
   
   local res = sqDB.execute{sql=
      "select count(id), IFNULL(order_id,0) as id from ewaiting_referral_orders where order_id = " .. conn:quote(OrderID), 
      live=true}
   
   return res[1].id:S()

end

----------------------------------------------------------------------------

function sqDB.RAD.updateResByOrderId(OrderID)

   sqUtils.printLog("Setting reservation status to cancelled...")
   
   local res = sqDB.execute{sql=
      "update reservation set status = 'cancelled' where location_pref = " .. conn:quote(OrderID).. " and status = 'active'", 
      live=false}  

   --taken from old version, may not need
   --conn:commit{live=true}

end

----------------------------------------------------------------------------

function sqDB.RAD.getPatientReferralByEpisode(id, facilityID, episodeNumber)

   local res = sqDB.execute{sql=
      "select count(id), id from ewaiting_referrals " ..
      "where facility_id = " .. facilityID .. 
      " and user_id = " .. id .. 
      " and id in " ..
      "(select ewaiting_referral_id from ewaiting_referral_orders " ..
      "where episode_number = " .. conn:quote(episodeNumber) .. ")", 
      live=true}

   if res[1]['count(id)']:S() == '0' then
      return '0'
   else
      return res[1].id:S()
   end

end

----------------------------------------------------------------------------

function sqDB.RAD.dischargeReferral(referralID)

   sqUtils.printLog("Vetting status is cancelled , setting referral to discharged...")
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_Ref")
   
   local sql ="update ewaiting_referrals " ..
   "set "..
   "modified = now(), modified_by = "..sqUser..", "..
   "status = 'discharged'" ..
   " where id = ".. referralID

   sqDB.execute{sql=sql, live=false}    

end

----------------------------------------------------------------------------

function sqDB.RAD.activateReferral(referralID)

   sqUtils.printLog("Setting referral to active...")

   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_Ref")

   local sql ="update ewaiting_referrals " ..
   "set "..
   "modified = now(), modified_by = "..sqUser..", "..
   "status = 'active'" ..
   " where id = ".. referralID ..
   " and status in ('pre_triage', 'triage', 'letter_sent')"
   --DD Added 200123
   --Desc: Only update status of referral to active if the status is at a pre-active state
   
   sqDB.execute{sql=sql, live=false}   

end

----------------------------------------------------------------------------

function sqDB.RAD.assignReferral(referralID)
   
   sqUtils.printLog("Setting referral to assigned...")
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_Ref")
   
   local sql ="update ewaiting_referrals " ..
   "set "..
   "modified = now(), modified_by = "..sqUser..", "..
   "status = 'assigned'" ..
   " where id = ".. referralID ..
   " and status in ('pre_triage', 'triage', 'letter_sent', 'active')"

   sqDB.execute{sql=sql, live=false}
   
end

----------------------------------------------------------------------------

function sqDB.RAD.setReferralLetterSent(referralID)

   sqUtils.printLog("Setting referral status to 'letter_sent'...")

   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_Ref")
   --update the referral
   local sql ="update ewaiting_referrals " ..
   "set "..
   "modified = now(), modified_by = "..sqUser..", "..
   "status = 'letter_sent'" ..
   " where id = ".. referralID

   sqDB.execute{sql=sql, live=false}   

end

----------------------------------------------------------------------------

function sqDB.RAD.createPatientReferral(id,facilityID, OrderID, ExamCode, 
      ExamDesc, episodeNumber, accessionNumber, orderGrouping, checkZSeg, checkZSegVisitflag, 
      SiteId, RefSourceName)
   
   sqUtils.printLog("Creating patient referral...")
 
   local patientType = sqUtils.getMappedValue(sqCfg.extra_params,"defaultPatientType")
     
   --PB 05.05.2024 : lokup condition tag rather than default, default if not found
   local facilityConditionTag = sqDB.RAD.getFacilityCondition(facilityID)
   if facilityConditionTag == nil then
      facilityConditionTag = sqUtils.getMappedValue(sqCfg.extra_params,"defaultFacilityConditionTag")
   end
   --
   
   --PB 05.05.2024 : lokup speciality rather than default, default if not found
   local facilitySpeciality = sqDB.RAD.getFacilitySpecialty(facilityID)
   if facilitySpeciality == nil then
      facilitySpeciality = sqUtils.getMappedValue(sqCfg.extra_params,"defaultFacilitySpeciality")
   end
   --
   
   --PB 05.05.2024 : lokup category rather than default, default if not found
   local facilityCategory = sqDB.RAD.getFacilityCategory(facilityID)
   if facilityCategory == nil then
      facilityCategory = sqUtils.getMappedValue(sqCfg.extra_params,"defaultFacilityCategory")
   end
   --
   
   --PB 05.05.2024 : lokup category rather than default, default if not found
   local facilitySourceId = sqDB.RAD.getFacilitySource(facilityID)
   if facilitySourceId == nil then
      facilitySourceId = sqUtils.getMappedValue(sqCfg.extra_params,"defaultFacilitySourceId")
   end
   --

   if checkZSeg == 'NSQ' then
      if checkZSegVisitflag == 'Y' then
         patientType = 'Inpatient'
      end
   end

   sqDB.execute{sql=
      "insert into ewaiting_referrals (facility_id, user_id, date_referred, category_id, speciality_id, " ..
      "referral_source_id, referral_source_name, status, `condition`, comments, created, created_by, " ..
      "pas_verified, patient_type) " ..
      "values ("..
      facilityID..","..
      id..","..
      -- conn:quote(serviceDateFormat) ..","..  
      "now()"..","..
      facilityCategory..","..
      facilitySpeciality..","..
      facilitySourceId..","..
      --"'via HL7'"..","..
      conn:quote(RefSourceName)..","..
      "'pre_triage'"..","..
      conn:quote(facilityConditionTag)..","..
      "'HL7 Referral'"..","..
      "now()"..","..
      "19598".. "," ..
      "0" .."," ..
      conn:quote(patientType) ..
      ")"
      , live=false}

   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}

   sqDB.execute{sql=
      "insert into ewaiting_referral_condition_tags " ..
      "values (" .. result[1].id .."," .. facilityConditionTag .. ")", 
      live=false}
   
   sqUtils.printLog("Referral created!")

   return result[1].id:S()

end

----------------------------------------------------------------------------

function sqDB.RAD.createPatientReferralOrder(id, OrderID, episodeNumber, ExamCode,ExamDesc,accessionNumber, time, orderResID)

   sqUtils.printLog("Creating referral order...")
   
   if OrderID == '0' then
      local errorStr = "ER0001: "
      local errorDesc = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'errorCodes','ER0001')
      if errorDesc ~= nil then
         errorStr = errorStr..errorDesc.." : "
      end
      errorStr = errorStr.."orderResID="..orderResID
      error(errorStr)
   end

   sqDB.execute{sql=
      "insert into ewaiting_referral_orders " ..
      "(id,ewaiting_referral_id,order_id,episode_number,exam_code,exam_name,accession_number, created_at, cancellation_code) " ..
      "values (null," .. 
      id .."," .. 
      conn:quote(OrderID) .. "," .. 
      conn:quote(episodeNumber) .. "," .. 
      conn:quote(ExamCode) .. "," .. 
      conn:quote(ExamDesc) .. "," ..
      conn:quote(accessionNumber) .. "," .. 
      "now(), '')", 
      live=false}

   local result = sqDB.execute{sql="select last_insert_id() as id", live=true}
   
   sqUtils.printLog("Referral order created!")

   return result[1].id:S()

end

----------------------------------------------------------------------------

function sqDB.RAD.cancelReferralOrder(referralID, OrderID, resID, cancellationCode)
   
   sqUtils.printLog("Cancelling referral order...")
   
   --Requested to remove check by Steve , 19.12.2024
   --[[
   if resID == '0' then
      error("Error trying to cancel a referral order for appointment than does not exist : "..OrderID)
   end
   ]]

   local sql = ''
   
   if sqUtils.isEmpty(resID) or resID == '0' then
      sql ="update ewaiting_referral_orders " ..
      "set "..
      "cancellation_code = "..cancellationCode..", "..
      "updated_at = now(), " ..
      "cancellation_date = now(), " ..
      "cancellation_processed = 0, " ..
      "cancellation_processed_success = 0" ..
      " where ewaiting_referral_id = " .. referralID ..
      " and order_id = " .. conn:quote(OrderID)
   else
      --set the res ID
      sql ="update ewaiting_referral_orders " ..
      "set "..
      "cancellation_code = "..cancellationCode..", "..
      "updated_at = now(), " ..
      "cancellation_date = now(), " ..
      "cancellation_processed = 0, " ..
      "cancellation_processed_success = 0, " ..
      "res_id = "..resID..
      " where ewaiting_referral_id = " .. referralID ..
      " and order_id = " .. conn:quote(OrderID)
   end

   sqDB.execute{sql=sql, live=false}      

end

----------------------------------------------------------------------------

function sqDB.RAD.updateReferralOrderResID(referralID, OrderID, resID)

   sqUtils.printLog("Setting resID for referral order...")

   local catchAllCode = sqUtils.getMappedValue(sqCfg.extra_params,"cancel_CatchAll")
   
   local sql ="update ewaiting_referral_orders " ..
   "set "..
   "cancellation_code = "..catchAllCode..", "..
   "cancellation_processed = 0, " ..
   "cancellation_processed_success = 0, " ..
   "updated_at = now(), cancellation_date = now()," ..
   "res_id = " .. resID ..
   " where ewaiting_referral_id = " .. referralID ..
   " and order_id = " .. conn:quote(OrderID)
   
   sqDB.execute{sql=sql, live=false}

end

----------------------------------------------------------------------------

function sqDB.RAD.updatePatientFlow(id, facilityID, OBR)
   
   sqUtils.printLog("Updating patient flow...")

   local exam_code = OBR[4][1]:S()
   local OrderPFID = OBR[3][1]:S()

   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_App")

   --PB 08.08.2024 - IR-383 : added extra logic to only use a rervation where facility id has this sites parent facility
   --[[
   local ResId = sqDB.execute{sql=
      "select count(id), max(id) as id from reservation " ..
      "where user_id = " .. id .. 
      " and reservation_date = date_format(now(), '%Y-%m-%d')", live=true}
   ]]
   local ResId = sqDB.execute{sql=
      "select count(id), max(id) as id from reservation " ..
      "where user_id = " .. id .. 
      " and reservation_date = date_format(now(), '%Y-%m-%d')"..
      " and facility_id in (select id from facilities where parent_id = "..facilityID..
      " or id = "..facilityID..")", live=true}
   ---

   if ResId[1]['count(id)']:S() ~= '0' then

      local ResId3 = sqDB.execute{sql=
         "select count(id), max(id) as id " ..
         "from patient_flow " ..
         "where res_id = " .. conn:quote(ResId[1]['id']:S()) .. " and stage = 'CHECK-IN'", 
         live=true}
      --
      if ResId3[1]['count(id)']:S() == '0' then
         --insert the new appointment
         sqDB.execute{sql=
            "insert into patient_flow " ..
            "(id, timestamp, facility_id, user_id, res_id, sq_user_id, stage, nurse, current_stage, bay, source) " .. 
            "values (null, now(),"..
            facilityID..","..
            id..","..
            conn:quote(ResId[1]['id']:S()).."," ..sqUser..",'CHECK-IN', NULL, 'Y', NULL, 'hl7'"..
            ")"
            , live=false}  
      end
   
   else
      
      local ResId4 = sqDB.execute{sql=
         "select count(id), ewaiting_referral_id from ewaiting_referral_orders " ..
         "where order_id = '" .. OrderPFID.."'", live=true}
      --
      if ResId4[1]['count(id)']:S() ~= '0' then
         --update the referral
         local sql ="update ewaiting_referrals " ..
         "set "..
         "status = 'assigned'" ..
         " where id = ".. ResId4[1]['ewaiting_referral_id']:S()

         sqDB.execute{sql=sql, live=false}
      
      end
   
   end
   
   sqUtils.printLog("Patient flow updated!")
   
end

----------------------------------------------------------------------------

function sqDB.RAD.updatePatientFlowComplete(id, facilityID, OBR)
   
   sqUtils.printLog("Updating patient flow COMPLETE...")

   local exam_code = OBR[4][1]:S()
   local OrderPFID = OBR[3][1]:S()
   
   local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_App")

   --PB 08.08.2024 - IR-383 : added extra logic to only use a rervation where facility id has this sites parent facility
   --[[
   local ResId = sqDB.execute{sql=
      "select count(id), max(id) as id " ..
      "from reservation " ..
      "where user_id = " .. id .. " and reservation_date = date_format(now(), '%Y-%m-%d')", 
      live=true}
   ]] 
   local ResId = sqDB.execute{sql=
      "select count(id), max(id) as id from reservation " ..
      "where user_id = " .. id .. 
      " and reservation_date = date_format(now(), '%Y-%m-%d')"..
      " and facility_id in (select id from facilities where parent_id = "..facilityID..
      " or id = "..facilityID..")", live=true}
   ---

   if ResId[1]['count(id)']:S() ~= '0' then
      
      local ResId4 = sqDB.execute{sql=
         "select count(id), max(id) as id " ..
         "from patient_flow " ..
         "where res_id = " .. conn:quote(ResId[1]['id']:S()) .. " and stage = 'COMPLETE'", live=true}
      --
      if ResId4[1]['count(id)']:S() == '0' then 

         sqDB.execute{sql=
            "update patient_flow " ..
            "set current_stage = 'N' " ..
            "where res_id = " .. conn:quote(ResId[1]['id']:S()) .. 
            " and user_id = " ..id ..
            " and facility_id = " .. facilityID,
            live = false}

         --insert the new appointment flow
         sqDB.execute{sql=
            "insert into patient_flow " ..
            "(id, timestamp, facility_id, user_id, res_id, sq_user_id, stage, nurse, current_stage, bay, source) " .. 
            "values (null, now(),"..
            facilityID..","..
            id..","..
            conn:quote(ResId[1]['id']:S()).."," ..sqUser..",'COMPLETE', NULL, 'Y', NULL, 'hl7'"..
            ")"
            , live=false} 
      end
      
   else
      
      local ResId5 = sqDB.execute{sql=
         "select count(id), ewaiting_referral_id from ewaiting_referral_orders " ..
         "where order_id = '" .. OrderPFID .."'", 
         live=true}
      --
      if ResId5[1]['count(id)']:S() ~= '0' then
         --update the referral
         local sql ="update ewaiting_referrals " ..
         "set "..
         "status = 'discharged'" ..
         " where id = ".. ResId5[1]['ewaiting_referral_id']:S()

         sqDB.execute{sql=sql, live=false}
      
      end
   
   end
   
end

----------------------------------------------------------------------------

function sqDB.RAD.getOrderResID(referralID, OrderID)
   
   local res = sqDB.execute{sql=
      "select IFNULL(res_id,0) as res_id from ewaiting_referral_orders " ..
      "where ewaiting_referral_id = " .. conn:quote(referralID) .. 
      " and order_id = " .. conn:quote(OrderID), live=true}  
   
	return res[1]['res_id']:S()
   
end

----------------------------------------------------------------------------

function sqDB.RAD.getCancellationID(reason)
   
   local res = sqDB.execute{sql=
                  "select count(id) as cnt, id from healthshare_cancellation_codes " ..
                  "where reason = " .. conn:quote(reason), 
                  live=true}
   
   return res[1]['cnt']:S(), res[1]['id']:S()
   
end

----------------------------------------------------------------------------

function sqDB.RAD.isValidExamCode(facilityID, examCode)

   local res = true
   
   local examCodeCheck = sqDB.execute{sql=
      "select count(facility_id) as count_fid, facility_id from reason_exam_codes "  ..
      "where exam_code =" .. conn:quote(examCode)..
      " and facility_id in (select id from facilities where parent_id = " .. facilityID .. ") limit 1 ", 
      live=true}

   if (examCodeCheck[1]['count_fid']:S() == '0') then
      res = false
   end

   return res

end

----------------------------------------------------------------------------

function sqDB.RAD.isValidRoomCode(facilityID, roomCode)

   local res = true
   
   local roomCodeCheck = sqDB.execute{sql=
      "select count(facility_id) as count_fid, facility_id from reason_exam_codes "  ..
      "where room_id =" .. conn:quote(roomCode)..
      " and facility_id in (select id from facilities where parent_id = " .. facilityID .. ") limit 1 ", 
      live=true}

   if (roomCodeCheck[1]['count_fid']:S() == '0') then
      res = false
   end

   return res

end

----------------------------------------------------------------------------

function sqDB.RAD.mergePatient(id, minorid, facilityID)

   sqUtils.printLog("Merging patient ID:"..minorid.." into ID:"..id.." ...")
 
   -- Remove the minor one from teh Facility view
   local sql ="delete from facility_user " ..
   "where "..
   "facility_id = " .. conn:quote(facilityID) .. 
   " and user_id = " .. minorid

   sqDB.execute{sql=sql, live=false}

   local referralCount = sqDB.execute{sql="select count(id), id from ewaiting_referrals " ..
      "where facility_id = " .. facilityID .. 
      " and status in ('active', 'pre_triage') " ..
      "and user_id = " .. minorid, live=true}

   if referralCount[1]['count(id)']:S() ~= '0'
      then
      --Update any referrals from minor id into the major id
      local sql ="update ewaiting_referrals " ..
      "set "..
      "user_id = " .. id .. 
      " where user_id = " .. minorid

      sqDB.execute{sql=sql, live=false}
   end

   --insert into merged_users table
   local sql ="insert into merged_users values (" ..
   "null, now(), 0, "..
   id ..", "..
   "'{TUH Merge - Minor ID: " .. minorid ..
   " Major ID: " .. id ..
   "ReferralIDs Merged: " .. referralCount[1]['id'] ..
   "}')"

   sqDB.execute{sql=sql, live=false}

   
end

----------------------------------------------------------------------------

function sqDB.RAD.isReflexAppointment(episodeNumber, ExamCode, appStart, appEnd)

   local appRescheduledId = sqUtils.getMappedValue(sqCfg.extra_params,"appRescheduledId")
   
   local res = sqDB.execute{sql=
      "select count(id), IFNULL(id,0) as oID, IFNULL(res_id,0) as resID, IFNULL(ewaiting_referral_id,0) as refID " ..
      "from ewaiting_referral_orders " ..
      "where episode_number = " .. conn:quote(episodeNumber) .. 
      " and exam_code = " .. conn:quote(ExamCode) .." and (cancellation_processed = '0' or cancellation_code = '"..appRescheduledId.."')",
      live=true}  

   return res[1].oID:S(), res[1].resID:S(), res[1].refID:S()
end

----------------------------------------------------------------------------

function sqDB.RAD.isAppReschedule(episodeNumber, ExamCode)

   local appRescheduledId = sqUtils.getMappedValue(sqCfg.extra_params,"appRescheduledId")
   
   local res = sqDB.execute{sql=
      "select count(id), IFNULL(id,0) as oID, IFNULL(res_id,0) as resID, IFNULL(ewaiting_referral_id,0) as refID " ..
      "from ewaiting_referral_orders " ..
      "where episode_number = " .. conn:quote(episodeNumber) .. 
      " and exam_code = " .. conn:quote(ExamCode) .." and (cancellation_processed = '0' or cancellation_code = '"..appRescheduledId.."')",
      live=true}  

   return res[1].oID:S(), res[1].resID:S(), res[1].refID:S()
end

----------------------------------------------------------------------------

function sqDB.RAD.createAppointment(MsgIn, patientID, facilityID, episodeNumber, orderResID, oID, examCode, examDesc, accessionNumber, 
      room_code, opdAppointmentTimeString, opdAppointmentEndTimeString, opdAppointmentString)
   
   sqUtils.printLog("Creating appointment...")
   
   local facilityType = sqUtils.getMappedValue(sqCfg.extra_params,"defaultFacilityType")
   local consultantID = sqUtils.getMappedValue(sqCfg.extra_params,"defaultconsultantID")
   
   local referralID = sqDB.RAD.getPatientReferralByEpisode(patientID, facilityID, episodeNumber)

   --Check if the Study Id exists
   local AppCount = sqDB.execute{sql="select count(id), id, ewaiting_referral_id " .. 
      "from ewaiting_referral_orders where order_id = " .. conn:quote(orderResID), live=true}  
   
   --order doesnt exist then create it
   if AppCount[1]['count(id)']:S() == '0'  then
      
      oID = sqDB.RAD.createPatientReferralOrder(referralID, oID, episodeNumber, examCode, examDesc, accessionNumber,
      opdAppointmentTimeString, orderResID)

   end

   -- check if a res id already exists
   local AppResCount = sqDB.execute{sql=
      "select count(id), id, IFNULL(res_id,0) as res_id, ewaiting_referral_id " ..
      "from ewaiting_referral_orders " ..
      "where order_id = " .. conn:quote(orderResID) .. 
      " and exam_code = " .. conn:quote(examCode), live=true}    
    
   if AppResCount[1]['res_id']:S() ~= '0' then 
      
      local AppCount = sqDB.execute{sql="select count(facility_id), facility_id " ..
         " from reason_exam_codes where room_id = " .. conn:quote(room_code) .. 
         " and facility_id in (select id from facilities where parent_id = " .. facilityID .. ") limit 1 ", 
         live=true}

      if AppCount[1]['count(facility_id)']:S() ~= '0'  then -- 1

         local AppResCountResch = sqDB.execute{sql=
            "select reservation_time, reservation_end_time from reservation " ..
            "where id = " ..conn:quote(AppResCount[1]['res_id']:S()), live=true}     

         if AppResCountResch[1]['reservation_end_time']:S() == conn:quote(opdAppointmentTimeString) then

            local sql="update reservation set facility_id = ".. AppCount[1]['facility_id'].. 
            ", reservation_end_time = ".. conn:quote(opdAppointmentEndTimeString) ..
            " where id = " .. conn:quote(AppResCount[1]['res_id']:S())

            sqDB.execute{sql=sql, live=false}
         else

            local sql="update reservation set facility_id = ".. AppCount[1]['facility_id'].. 
            ", reservation_time = " .. conn:quote(opdAppointmentTimeString) .. 
            ", reservation_end_time = ".. conn:quote(opdAppointmentEndTimeString) ..
            " where id = " .. conn:quote(AppResCount[1]['res_id']:S())

            sqDB.execute{sql=sql, live=false}
         end
      end
   end

   
   local AppCount = sqDB.execute{sql=
      "select count(facility_id), facility_id from reason_exam_codes " ..
      "where room_id = " .. conn:quote(room_code) .. 
      " and facility_id in (select id from facilities where parent_id = " .. facilityID .. ") limit 1 ", 
      live=true}

   if AppCount[1]['count(facility_id)']:S() ~= '0'  then -- 1

      --[[ CI: Get appointment reason id ]]
      local reasonId = '0'
      local reasonCheck = sqDB.execute{sql="select count(reason_id), reason_id from reason_exam_codes rec where rec.facility_id = '"..
         AppCount[1]['facility_id'].."' and rec.exam_code = '"..examCode.."'", live = true}

      if (reasonCheck[1]["count(reason_id)"]:S()) ~= '0' then
         reasonId = reasonCheck[1].reason_id:S()                                                     
      end


      local sql =
      "insert into reservation " ..
      "(facility_id, user_id, facility_type, consultant_id, location_pref, reservation_date, " ..
      "reservation_time, reservation_end_time, google_cal_id, reason_id,recurrence,date,status," ..
      "attended_by, is_attended,booking_user_id, source, print,stats, quality,capacity_sync, is_urgent) " .. 
      "values ("..
      AppCount[1]['facility_id']..","..
      patientID..","..
      conn:quote(facilityType)..","..
      consultantID..", ''"..","..
      conn:quote(opdAppointmentString)..","..
      conn:quote(opdAppointmentTimeString)..","..
      conn:quote(opdAppointmentEndTimeString)..","..
      "'',"..conn:quote(reasonId)..",'N',now(), 'active', 0,'yes',0,'h','N','y','Y','N',0"..
      ")"

      sqDB.execute{sql=sql, live=false}
      --
      local resresult = sqDB.execute{sql="select last_insert_id() as id", live=true}
      --

      --update the referral
      local sql ="update ewaiting_referrals " ..
      "set "..
      "status = 'assigned'" ..
      " where id = ".. referralID

      sqDB.execute{sql=sql, live=false}
      --
      --link the res and referral
      local sql ="insert into ewaiting_reservations values (" ..
      referralID..","..
      resresult[1].id..")"

      sqDB.execute{sql=sql, live=false}

      --check if there is an existing appointment against this order
      local AppCountRes3 = sqDB.execute{sql=
         "select IFNULL(res_id,0) as res_id from ewaiting_referral_orders " ..
         "where order_id = " .. conn:quote(orderResID), live=true}

      if AppCountRes3[1]['res_id']:S() ~= '0'  then
         --
         local sql ="update reservation " ..
         "set "..
         "status = 'cancelled'" ..
         " where id = " .. conn:quote(AppCountRes3[1].res_id:S())

         sqDB.execute{sql=sql, live=false}

      end
      --
      local sql ="update ewaiting_referral_orders " ..
      "set "..
      "res_id = " .. resresult[1].id..
      " where ewaiting_referral_id = " .. referralID ..
      " and order_id = " .. conn:quote(orderResID)

      sqDB.execute{sql=sql, live=false}
      
      --print("DB Commit at line 786")
      --conn:commit{live=true}

   else
      --update the referral
      local sql ="update ewaiting_referrals " ..
      "set "..
      "status = 'assigned'" ..
      " where id = ".. referralID

      sqDB.execute{sql=sql, live=false}

   end 
   
end

----------------------------------------------------------------------------

function sqDB.RAD.rescheduleAppointmentS12(facilityID, resID, oID, refID, orderResID, opdAppointmentTimeNewString, 
      opdAppointmentEndTimeString, room_code, episodeNumber, accessionNumber, examCode, examDesc)

   sqUtils.printLog("Rescheduling appointment from S12...")
   
   local appRescheduledId = sqUtils.getMappedValue(sqCfg.extra_params,"appRescheduledId")

   local sql ="update ewaiting_referral_orders " ..
   "set "..
   "reschedule_date = " .. conn:quote(opdAppointmentTimeNewString).. ", " ..
   " reschedule_end_time = " .. conn:quote(opdAppointmentEndTimeString) .. ", " ..
   " cancellation_code = "..appRescheduledId..
   ", cancellation_processed = 0 ".. 
   ", cancellation_processed_success = 0 " ..
   ", updated_at = now(), cancellation_date = now()" ..
   " where id = " .. conn:quote(oID)

   sqDB.execute{sql=sql, live=false}

   local AppCount22 = sqDB.execute{sql=
      "select count(facility_id), facility_id from reason_exam_codes "  ..
      "where room_id = " ..conn:quote(room_code) .. 
      " and facility_id in (select id from facilities where parent_id = " .. facilityID .. ") limit 1 ", 
      live=true}

   -- Only update the facility if the rescheduled appt is for a room managed by SQ (check result of AppCount22)
   if AppCount22[1]['count(facility_id)']:S() ~= '0'  then

      local sql="update reservation set facility_id = " ..AppCount22[1]['facility_id']..
      " where id = " .. conn:quote(resID)

      sqDB.execute{sql=sql, live=false}

   end
   
   --PB 18.02.2025 : Flag only used when processing healthshare cancel+rebook
   local processHSCancelAndRebook = json.parse{data=sqCfg.extra_params}.processHSCancelAndRebook or ''

   if processHSCancelAndRebook ~= '1' then
      
      --
      --Check if the Study Id exists
      local AppCount33 = sqDB.execute{sql=
         "select count(id), id, ewaiting_referral_id " .. 
         "from ewaiting_referral_orders where order_id = " .. conn:quote(orderResID), live=true}  
      --
      if AppCount33[1]['count(id)']:S() == '0'  then

         --create a new Order
         --
         sqDB.execute{sql=
            "insert into ewaiting_referral_orders " ..
            "(id,ewaiting_referral_id,order_id,episode_number,exam_code," ..
            "exam_name,accession_number, created_at, res_id, cancellation_code) " ..
            "values (null," .. conn:quote(refID) .."," .. 
            conn:quote(orderResID) .. "," .. conn:quote(episodeNumber) .. "," 
            .. conn:quote(examCode) .. "," .. conn:quote(examDesc) .. "," ..
            conn:quote(accessionNumber) .. "," .. "now()," 
            ..conn:quote(resID) ..", '')", live=false}   
         --
      end
      
   else

      --specific to healthshare cancel+rebook

      --PB 18.02.2025 : if we've received an appointment as part of the cance+rebook process on soliton
      --                the the message will have the same episode ID but different order id, so we need
      --                so we need to update the order id on the order

      local sqlStr = "select count(ero.id) as cnt, IFNULL(ero.id,0) as id from ewaiting_referrals er join ewaiting_referral_orders ero on er.id = ero.ewaiting_referral_id"..
      " where er.facility_id = "..conn:quote(facilityID).." and ero.episode_number = "..conn:quote(episodeNumber)..
      " and order_id <> "..conn:quote(orderResID)
      local AppCount33 = conn:execute{sql=sqlStr, live=true}

      if AppCount33[1].id:S() ~= '0' then
         sqlStr = "update ewaiting_referral_orders set order_id = "..orderResID.." where id = "..AppCount33[1].id:S()
         conn:execute{sql=sqlStr, live=false}
      end

   end
   
   conn:commit{live=true}

end

----------------------------------------------------------------------------

function sqDB.RAD.cancelAppointment(id, facilityID, SCH, reasonID)
   
   sqUtils.printLog("Cancelling appointment...")
   
   --extract the res date and time
   local opdAppointment = dateparse.parse(SCH[11][4]:D())
   local opdAppointmentString = string.sub(tostring(opdAppointment),1,10)

   local opdAppointmentTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentTimeString = string.sub(tostring(opdAppointmentTime),12,16)
      
   local opdAppointmentEndTime = dateparse.parse(SCH[11][4]:T())
   local opdAppointmentEndTimeString = string.sub(tostring(opdAppointmentTime),12,16)
  
   local orderID = SCH[2][1]:S()
   
   --[[
   local ResIdUpd = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId) ..
      " and status = 'active'", live=true}
   ]]
   
   local SQL = "select count(r.id), r.id from reservation r "..
   "left outer join ewaiting_referral_orders ero on ero.res_id = r.id ".. 
   " where r.status = 'active' and ero.order_id = "..conn:quote(orderID).." and r.user_id = "..conn:quote(id)  
   local ResIdUpd = sqDB.execute{sql=SQL, live=true}

   local resID = ResIdUpd[1].id:S()
   trace(resID)
   
   if ResIdUpd[1]['count(r.id)']:S() ~= '0' then

      sqDB.execute{sql="update reservation set " .. 
         "reservation_date = " .. conn:quote(opdAppointmentString)..","..
         "reservation_time = " .. conn:quote(opdAppointmentTimeString)..","..
         "reservation_end_time = " .. conn:quote(opdAppointmentEndTimeString)..","..
         "status = 'cancelled'" ..","..
         "modified = now(), modified_source = 'h'"..
         " where id = " .. resID .." and " ..
         "user_id = " .. id
         , live=false}

      --Add A user audit record for the app creation
      sqDB.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
         "values (null, now(),22, 'appointment.cancelled',"..
         conn:quote(resID)..","..id..",null)"
         , live=false}  
      
      sqUtils.printLog("Appointment cancelled!")

   else
      sqUtils.printLog("Cancelling appointment failed, Appointment ID "..orderID.." not found!")
   end
end

-----------------------------------------------------------------
function sqDB.RAD.dnaAppointment(id, facilityID, SCH, reasonID)
   
   sqUtils.printLog("DNA appointment...")
  
   --get the DXC identifier
   local DXCId = SCH[2][1]:S()
   
   --[[
   if DXCId == '' then
      DXCId = SCH[2][1]:S()
   end
   ]]--
   
   local ResIdUpd = sqDB.execute{sql="select count(id), id from reservation where user_id = " .. id .. " and location_pref = " .. conn:quote(DXCId) ..
         " and status = 'active'", live=true}
      --
      if ResIdUpd[1]['count(id)']:S() ~= '0'
      then
   
      sqDB.execute{sql="update reservation set " .. 
         "is_attended = 'no'," ..
         "modified = now(), modified_source = 'h'"..
         " where id = " .. conn:quote(ResIdUpd[1]['id']:S())
         , live=false}
      
      --Add A user audit record for the app creation
      sqDB.execute{sql="insert into user_audit (id,timestamp,sq_user_id,action,res_id,user_id,data) " .. 
                 "values (null, now(),22, 'appointment.dna',"..
                 conn:quote(ResIdUpd[1]['id']:S())..","..id..",null)"
            , live=false}  
      
      end
end
----------------------------------------------------------------------------

function sqDB.RAD.setReferralStatus(orderResID, status)
   
   local res = sqDB.execute{sql=
      "select count(id), ewaiting_referral_id " ..
      "from ewaiting_referral_orders " ..
      "where order_id = " .. conn:quote(orderResID), live=true}
   
   if res[1]["count(id)"]:S() ~= '0' then

      local refID = res[1].ewaiting_referral_id:S()

      local sqUser = sqUtils.getMappedValue(sqCfg.extra_params,"SQUserID_Ref")

      local sql ="update ewaiting_referrals " ..
      "set "..
      "modified = now(), modified_by = "..sqUser..", "..
      "status = 'assigned'" ..
      " where id = ".. refID

      sqDB.execute{sql=sql, live=false}
 
   end
   
end

----------------------------------------------------------------------------

function sqDB.RAD.getFacilityCondition(facilityID)

   sqUtils.printLog("Calling sqDB.getFacilityCondition...")

   --[[
   
   PB: 11.12.2024
   
   NOTE: This query will change, at the moment RAD is only usng X-Ray but DB is set up with multiple
   conditions for the facility. Messaging will dictate the condition in future version and 
   hardcoding will be removed
   
   ]]
   
   local res = sqDB.execute{sql="select facility_id, condition_tag_id, ect.name, ect.status "..
      " from ewaiting_facility_condition_tags efct join ewaiting_condition_tags ect on efct.condition_tag_id = ect.id "..
  		"where facility_id = "..facilityID.." and ect.name = 'X-Ray' order by condition_tag_id desc limit 1", live=true}
   

   if #res == 0 then return nil end

   return res[1].condition_tag_id:S()
end

----------------------------------------------------------------------------

function sqDB.RAD.getFacilitySpecialty(facilityID)
   
   sqUtils.printLog("Calling sqDB.getFacilitySpecialty...")

   local res = sqDB.execute{sql=
      "select speciality_id from facility_specialities "..
      " where facility_id = "..conn:quote(facilityID).." order by id desc limit 1", live=true}

   if #res == 0 then return nil end

   return res[1].speciality_id:S()
end

----------------------------------------------------------------------------

function sqDB.RAD.getFacilityCategory(facilityID)
   
   sqUtils.printLog("Calling sqDB.getFacilityCategory...")
   
   --[[
   
   PB: 11.12.2024
   
   NOTE: This query will change, at the moment RAD is only usng Routine but DB is set up with multiple
   categories for the facility. Messaging will dictate the condition in future version 
   and hardcoding will be removed
   
   ]]

   local res = sqDB.execute{sql=
      "select id from ewaiting_categories where name ='Routine' "..
      " and facility_id = "..conn:quote(facilityID).." order by id desc limit 1", live=true}

   if #res == 0 then return nil end

   return res[1].id:S()
end

----------------------------------------------------------------------------

function sqDB.RAD.getFacilitySource(facilityID)
   
   sqUtils.printLog("Calling sqDB.getFacilitySource...")

   local res = sqDB.execute{sql=
      "select referral_source_id from ewaiting_facility_referral_sources "..
      " where facility_id = "..conn:quote(facilityID).." order by referral_source_id desc limit 1", live=true}

   if #res == 0 then return nil end

   return res[1].referral_source_id:S()
end

----------------------------------------------------------------------------


return sqDB