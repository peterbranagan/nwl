sqUtils = require('sqUtils')

--[[

Mappings module is customized per site
Code here will change and is not generic
Usually we'll send out an ORM or SIU message
depending on client requirements

]]

Mappings = {

   outboundMsgType = '',
   
   siu = {},
   orm = {}

}

----------------------------------------------------------------------------

function Mappings.buildSIU(Message, Value, Value2)
   
   sqUtils.printLog("Calling Mappings.buildSIU...")
     
   Message = Mappings.siu.mapMSH(Message, Value.appointment)
   Message = Mappings.siu.mapEVN(Message, Value.appointment)
   Message = Mappings.siu.mapSCH(Message, Value.appointment, Value2)
   Message = Mappings.siu.mapPID(Message, Value.patient, Value2)
   Message = Mappings.siu.mapPV1(Message, Value.appointment)
   Message = Mappings.siu.mapAIL(Message, Value.appointment, Value2)
   
   if sqUtils.getMappedValue(sqCfg.extra_params,"processAppComments") == '1' then
      if Mappings.outboundMsgType == 'S12' or Mappings.outboundMsgType == 'S13' then
         Message = Mappings.siu.mapNTE(Message, Value.appointment)
      end
   end
	
   return Message

end

----------------------------------------------------------------------------

function Mappings.buildORM(Value, Value2, status)
   
   sqUtils.printLog("Calling Mappings.buildORM...")
   
   local vmdFile = sqUtils.getMappedValue(sqCfg.extra_params,"outboundVMDFile")
   local Message = hl7.message{vmd=vmdFile, name='ORMO01'}
     
   Message = Mappings.orm.mapMSH(Message, Value.appointment)
   Message = Mappings.orm.mapPID(Message, Value.patient, Value2)
   Message = Mappings.orm.mapORC(Message, status, Value.appointment)
   Message = Mappings.orm.mapOBR(Message, Value.appointment, Value2)   
	
   return Message

end

----------------------------------------------------------------------------

-- SIU Mappings

----------------------------------------------------------------------------

function Mappings.siu.mapMSH(Message, Appointment)
   
   sqUtils.printLog("Calling Mappings.siu.mapMSH...")

   Message.MSH[3][1] = 'SWIFTQUEUE'
   
   local customer = sqUtils.getMappedValue(sqCfg.extra_params,"customer")
   if customer == 'NWL' then
      Message.MSH[4][1] = getSendingFacility(Appointment.facility_id)
   else
      Message.MSH[4][1] = sqUtils.getMappedValue(sqCfg.extra_params,"receivingIdentifier")
   end
  
   Message.MSH[5][1] = sqUtils.getMappedValue(sqCfg.extra_params,"receivingApp")
   Message.MSH[6][1] = sqUtils.getMappedValue(sqCfg.extra_params,"receivingIdentifier")
   Message.MSH[7] = os.ts.date('!%Y%m%d%H%M%S')

   Message.MSH[9][1] = 'SIU'
   Message.MSH[9][2] = Mappings.outboundMsgType

   --Message.MSH[10] = os.ts.date('!%Y%m%d%H%M%S')..Appointment.id
   -- use this if looking for different control id 
   Message.MSH[10] = _G.msgCtrlID
   Message.MSH[11][1] = sqUtils.getMappedValue(sqCfg.extra_params,"processingID")
   Message.MSH[12] = sqUtils.getMappedValue(sqCfg.extra_params,"hl7Version")


   return Message

end

----------------------------------------------------------------------------

function Mappings.siu.mapEVN(Message, Appointment, PatientFlow)
   
   sqUtils.printLog("Calling Mappings.siu.mapEVN...")
 
   Message.EVN[1] = Mappings.outboundMsgType 
   Message.EVN[2] = os.ts.date('!%Y%m%d%H%M%S')

   return Message

end

----------------------------------------------------------------------------

function Mappings.siu.mapSCH(Message, Appointment, Order)
   
   sqUtils.printLog("Calling Mappings.siu.mapSCH...")

   local Appid = 0

   Appid = Order.order_id
   SQAppid = Appointment.id
   Accessionid = Order.order_id

   Message.SCH[1][1] = Appid
   Message.SCH[2][1] = SQAppid
   Message.SCH[3] = Accessionid
   Message.SCH[9] = Appointment.duration
   Message.SCH[10][1] = 'MIN'
   Message.SCH[11][4] = Appointment.start

   local Appmod = Appointment.modified

   if Appointment.modified_source == 'b' then
      if Appointment.status == 'active' then
         Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appRescheduleCode")
      else
         Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appCancelCode")
      end
   elseif Appointment.modified_source == 'c' then
      if Appointment.status == 'active' then
         Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appRescheduleCode")
      else
         Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appCancelCode")
      end
   elseif Appointment.modified_source == 'PR' then
      if Appointment.status == 'active' then
         Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appRescheduleCode")
      else
         Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appCancelCode")
      end
   elseif Appointment.modified_source == 'o' then
      if Appointment.status == 'active' then
         Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appRescheduleCode")
      else
         Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appCancelCode")
      end
   else
      Message.SCH[25][1] = sqUtils.getMappedValue(sqCfg.extra_params,"appNewCode")
   end
   
   return Message
end

----------------------------------------------------------------------------

function Mappings.siu.mapPID(Message, Patient, Order)
   
   sqUtils.printLog("Calling Mappings.siu.mapPID...")

   Message.PID[1] = Patient.mrn   -- Patient.id
   Message.PID[2][1] = Patient.mrn   -- Patient.id
   Message.PID[3][1][1] = Patient.mrn
   Message.PID[5][1] = Patient.surname
   Message.PID[5][2] = Patient.firstname
   Message.PID[7] = Patient.birth_year_field .. Patient.birth_month_field .. Patient.birth_date_field
   Message.PID[8] = Patient.Gender
   Message.PID[18][1] = Order.episode_number

   return Message
end  

----------------------------------------------------------------------------

function Mappings.siu.mapPV1(Message, Appointment, PatientFlow, Order)
   
   sqUtils.printLog("Calling Mappings.siu.mapPV1...")
   
   Message.PV1[2] = sqUtils.getMappedValue(sqCfg.extra_params,"patientClass")
   Message.PV1[3][1] = sqUtils.getMappedValue(sqCfg.extra_params,"pointOfCare")
   Message.PV1[3][4][1] = sqUtils.getMappedValue(sqCfg.extra_params,"assignedPatientLocation")
   Message.PV1[44] = os.ts.date('!%Y%m%d%H%M%S')
   Message.PV1[51] = sqUtils.getMappedValue(sqCfg.extra_params,"visitIdicator")
   
   return Message
end

----------------------------------------------------------------------------

function Mappings.siu.mapAIL(Message, Appointment, Order)

   sqUtils.printLog("Calling Mappings.siu.mapAIL...")

   AppReasonId = Appointment.reason_id
   FacilityId = Appointment.facility_id
   AppId = Appointment.id
   
   Message.AIL[1] = '1'
   Message.AIL[3][1] = Appointment.custom_fields.room_id
   
   --room code not in payload so do a DB lookup
   if sqUtils.isEmpty(Appointment.custom_fields.room_id) then
      
      sqUtils.printLog("Room code not received in SQ poayload, performing DB lookup...")
      
      local findRoom = conn:execute{sql=
         "select count(id), value_str from reservation_additional_info where reservation_id = " .. AppId, live=true}

      local JTRoom = findRoom[1]['value_str']:S() 

      if findRoom[1]['count(id)']:S() ~= '0' then
         Message.AIL[3][1] = findRoom[1]['value_str']:S()
      end
      
   end
   
   if Message.AIL[3][1]:S() == '' then
      error("AIL Segment: Room code not found")
   end

	--get the site ID
   local findRoom2 = conn:execute{sql=
      "select count(id),room_id, site_id from reason_exam_codes " ..
      "where reason_id = " .. AppReasonId .. " and facility_id = " .. FacilityId .. " limit 1", live=true}

   Message.AIL[3][2] = findRoom2[1]['site_id']:S()
   Message.AIL[3][4][1] = findRoom2[1]['site_id']:S()

   return Message
end

----------------------------------------------------------------------------

function Mappings.siu.mapNTE(Message, Appointment)
   
   sqUtils.printLog("Calling Mappings.siu.mapNTE...")
   
   -- NTE|1||this is the note from the patient|SQ Note
   
   Message.NTE[1] = '1'
   Message.NTE[3] = Appointment.comments
   Message.NTE[4][1] = 'SQ Note'
   
   return Message
end

----------------------------------------------------------------------------

-- ORM Mappings

----------------------------------------------------------------------------

function Mappings.orm.mapMSH(Message, Appointment)
   
   sqUtils.printLog("Calling Mappings.orm.mapMSH...")

   Message.MSH[3][1] = 'SWIFTQUEUE'
   
   local customer = sqUtils.getMappedValue(sqCfg.extra_params,"customer")
   if customer == 'NWL' then
      Message.MSH[4][1] = getSendingFacility(Appointment.facility_id)
   else
      Message.MSH[4][1] = sqUtils.getMappedValue(sqCfg.extra_params,"receivingIdentifier")
   end
  
   Message.MSH[5][1] = sqUtils.getMappedValue(sqCfg.extra_params,"receivingApp")
   Message.MSH[6][1] = sqUtils.getMappedValue(sqCfg.extra_params,"receivingIdentifier")
   Message.MSH[7] = os.ts.date('!%Y%m%d%H%M%S')

   Message.MSH[9][1] = 'ORM'
   Message.MSH[9][2] = Mappings.outboundMsgType

   --Message.MSH[10] = os.ts.date('!%Y%m%d%H%M%S')..Appointment.id
   -- use this if looking for different control id 
   Message.MSH[10] = _G.msgCtrlID
   Message.MSH[11][1] = sqUtils.getMappedValue(sqCfg.extra_params,"processingID")
   Message.MSH[12] = sqUtils.getMappedValue(sqCfg.extra_params,"hl7Version")


   return Message

end

----------------------------------------------------------------------------

function Mappings.orm.mapPID(Message, Patient, Order)
   
   sqUtils.printLog("Calling Mappings.orm.mapPID...")

   Message.PID[1] = Patient.mrn   
   Message.PID[2][1] = Patient.mrn  
   Message.PID[3][1][1] = Patient.mrn
   Message.PID[3][1][4][1] = json.parse{data=sqCfg.extra_params}.crisAssigningAuthority
   Message.PID[3][1][5] = json.parse{data=sqCfg.extra_params}.crisIdentifierTypeCode
   Message.PID[5][1] = Patient.surname
   Message.PID[5][2] = Patient.firstname
   Message.PID[7] = Patient.birth_year_field .. Patient.birth_month_field .. Patient.birth_date_field
   Message.PID[8] = Patient.Gender
 

   return Message
end  

----------------------------------------------------------------------------

function Mappings.orm.mapORC(Message, status, Patient, Order)
   
   sqUtils.printLog("Calling Mappings.orm.mapORC...")
   
   local orderID = '99'  -- Order.order_id ??
   local crisOrderID = '55' --  Order.accession_number ??
   local orderingProviderID = '3333'
   local orderingClinicianID = '111'
   local location = 'ABC'
   local specialty = '4'
   local orderingFacilityName = 'XYZ'
   
   -- comments below taken from spec....
   
   Message.ORC[1] = json.parse{data=sqCfg.extra_params}.statusChangeCode
   Message.ORC[2][1] = orderID -- Unique order ID allocated by the sending system. This field is required for New order messages. 
   Message.ORC[3][1] = crisOrderID -- Unique order ID allocated by CRIS. Unless CRIS has been configured to persist the placer order \ 
                                   -- number this field should be populated on all order change or cancel messages. For New order messages it should be blank.
   Message.ORC[5] = status
   Message.ORC[12][1][1] = orderingProviderID -- The clinician responsible for entering the request, this field maps to the referrer field on CRIS. Only <id number> is used.
   Message.ORC[13][1] = location -- This maps to the ward field on CRIS. Only <point of care> is used.
   Message.ORC[17][1] = specialty -- <identifier> should contain the specialty of the ordering clinician.

   --not required
   --Message.ORC[19] = 'XX'
   --[[
   Should identify the person who initiated the event that triggered this message, In the case of a NW order this would be the same person 
   identified in ORC:10, in the case of a CA it would be the person who cancelled the order. CRIS does not currently make use of this field
   ]]
   
   Message.ORC[21][1][1] = orderingFacilityName -- <id number> should contain the code identifying the hospital / GP practice etc. that the request was made from. 
                                                -- In the case of a ward request CRIS can default this value based on the ward code.

   return Message
end  

----------------------------------------------------------------------------

function Mappings.orm.mapOBR(Message, Patient, Order)
   
   sqUtils.printLog("Calling Mappings.orm.mapOBR...")
   
   local examCode = 'X009'
   local accessionNumber = 'V12345678' -- Order.accession_number ??
   local dateTime = '20250310153000'

   -- comments below taken from spec....
   
   Message.OBR[4][1] = examCode -- The examination being requested. Only <identifier> is used.
   Message.OBR[20] = accessionNumber -- Accession Number. If present on a new order the accession number of the created exam will be set to this value, 
                                     -- otherwise CRIS will generate an accession number. 
   
   Message.OBR[27][1] = dateTime
  
   --dont think this is required
   -- Message.OBR[30] = 'XX'
   --[[
   Should identify the person who initiated the event that triggered this message, In the case of a NW order this would be the same person 
   identified in ORC:10, in the case of a CA it would be the person who cancelled the order. CRIS does not currently make use of this field
   ]]

   return Message
end  

----------------------------------------------------------------------------

----------------------------------------------------------------------------

----------------------------------------------------------------------------



function getSendingFacility(facilityID)
  
   local sendingFacility = sqUtils.getJsonMappedValue2(sqCfg.extra_params,'outboundNACSMap', facilityID)
   
   if sqUtils.isEmpty(sendingFacility) then
      error("Failed to find NACS mapping for : "..facilityID)
   end
   
	return sendingFacility
   
end

----------------------------------------------------------------------------
----------------------------------------------------------------------------
return Mappings